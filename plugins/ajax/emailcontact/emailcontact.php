<?php
class plgAjaxEmailcontact extends JPlugin
{
	public function onAjaxEmailcontact()
	{
		// Check for request forgeries.
		if (!JSession::checkToken())
		{
			JError::raiseWarning(403, JText::_('JINVALID_TOKEN'));
			return false;
		}

		$input = JFactory::getApplication()->input;
		$this->_results = new stdClass();
		// load the captcha plugin
		// verify response
		JPluginHelper::importPlugin('captcha');
		$dispatcher = JDispatcher::getInstance();
		$this->_results->success = $dispatcher->trigger('onCheckAnswer');
		$this->_results->success = $this->_results->success[0];

		if (!$this->_results->success)
		{
			JError::raiseWarning(403, "Invalid Captcha");
			return false;
		}

		$app    = JFactory::getApplication();
		$path 	= JModelLegacy::addIncludePath(JPATH_SITE.'/components/com_contact/models');
		$model  = JModelLegacy::getInstance('Contact', 'ContactModel');
		$params = JComponentHelper::getParams('com_contact');
		$stub   = $input->getString('id');
		$id     = (int) $stub;

		// Get the data from POST
		$data    = $input->post->get('jform', array(), 'array');
		$contact = $model->getItem($id);

		$params->merge($contact->params);

		// Check for a valid session cookie
		if ($params->get('validate_session', 0))
		{
			if (JFactory::getSession()->getState() != 'active')
			{
				JError::raiseWarning(403, JText::_('COM_CONTACT_SESSION_INVALID'));

				// Save the data in the session.
				$app->setUserState('com_contact.contact.data', $data);

				return false;
			}
		}

		// Contact plugins
		JPluginHelper::importPlugin('contact');
		$dispatcher = JEventDispatcher::getInstance();

		// Include the necessary search path for form.php `loadForm()` method
		JForm::addFormPath(JPATH_SITE . '/components/com_contact/models/forms');
		JForm::addFieldPath(JPATH_SITE . '/components/com_contact/models/fields');
		// Validate the posted data.
		$form = $model->getForm();

		if (!$form)
		{
			JError::raiseError(500, $model->getError());

			return false;
		}

		$validate = $model->validate($form, $data);

		if ($validate === false)
		{
			// Get the validation messages.
			$errors	= $model->getErrors();

			// Push up to three validation messages out to the user.
			for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++)
			{
				if ($errors[$i] instanceof Exception)
				{
					$app->enqueueMessage($errors[$i]->getMessage(), 'warning');
				}
				else
				{
					$app->enqueueMessage($errors[$i], 'warning');
				}
			}

			// Save the data in the session.
			$app->setUserState('com_contact.contact.data', $data);

			return false;
		}

		// Validation succeeded, continue with custom handlers
		$results = $dispatcher->trigger('onValidateContact', array(&$contact, &$data));

		foreach ($results as $result)
		{
			if ($result instanceof Exception)
			{
				return false;
			}
		}

		// Passed Validation: Process the contact plugins to integrate with other applications
		$dispatcher->trigger('onSubmitContact', array(&$contact, &$data));

		return $this->_results;
	}
}