<?php

// No direct access.
defined('_JEXEC') or die;

/**
 * Contact custom email reply plugin.
 *
 * @package		Joomla.Plugin
 * @subpackage	Contact.customreply
 */
class plgContactCustomreply extends JPlugin
{

	public function onSubmitContact($contact, $data)
	{
		return $this->_sendEmail($data, $contact);
	}

	private function _sendEmail($data, $contact)
	{
		$app		= JFactory::getApplication();
		$params 	= JComponentHelper::getParams('com_contact');
		if ($contact->email_to == '' && $contact->user_id != 0) {
			$contact_user = JUser::getInstance($contact->user_id);
			$contact->email_to = $contact_user->get('email');
		}
		$mailfrom	= $app->getCfg('mailfrom');
		$fromname	= $app->getCfg('fromname');
		$sitename	= $app->getCfg('sitename');

		$name		= $data['contact_name'];
		$email		= $data['contact_email'];
		$subject	= $data['contact_subject'];
		$body		= nl2br(stripslashes($data['contact_message']));
		$postalCode = $data['contact_postalcode'];
		$company	= $data['contact_company'];
		$phone		= $data['contact_phone'];
		$timestamp	= date("m/d/Y");

		// Prepare email body
		$prefix = JText::sprintf('COM_CONTACT_ENQUIRY_TEXT', JURI::base());
		$body 	=
<<<EOT
		{$prefix}<br /><br />
		<strong>Issue Origination Date:</strong> {$timestamp} <br />
		<strong>Request/Demande:</strong> {$contact->name} <br />
		<strong>Comments/Commentaires:</strong> {$body} <br />
		<strong>Name/Nom:</strong> {$name} <br />
		<strong>Company/Soci&eacute;t&eacute;:</strong> {$company} <br />
		<strong>Email:</strong> <a href="mailto:{$email}">{$email}</a> <br />
		<strong>Phone/Tel:</strong> {$phone} <br />
		<strong>Country/Postal Code:</strong> {$postalCode} <br />
EOT;

		$mail = JFactory::getMailer();
		$mail->addRecipient($contact->email_to);
		$mail->addReplyTo(array($email, $name));
		$mail->setSender(array($mailfrom, $fromname));
		$mail->setSubject($sitename.': '.$subject);
		$mail->setBody($body);
		$mail->IsHTML(true);
		$sent = $mail->Send();

		return $sent;
	}

}