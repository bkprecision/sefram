<?php

// No direct access.
defined('_JEXEC') or die;

/**
 * Contact save request plugin class.
 *
 * @package		Joomla.Plugin
 * @subpackage	Contact.saverequest
 */
class plgContactSaverequest extends JPlugin
{
	
	public function onSubmitContact($contact, $data)
	{

		$reqs	= array('contact_name', 'contact_email', 'contact_subject', 'contact_message');
		
		foreach($reqs as $requirement)
		{
			if(!array_key_exists($requirement, $data) OR empty($data[$requirement]))
			{
				return false;
			}
		}
		
		$db 	= JFactory::getDBO();
		
		$qryChk = $db->getQuery(true)
						->select('count(*)')
						->from('#__contact_requests')
						->where('name="'.$data['contact_name'].'"')
						->where('email="'.$data['contact_email'].'"')
						->where('subject="'.$data['contact_subject'].'"');
		
		$count 	= (int)$db->setQuery($qryChk)->loadResult();
		
		if(!$count)
		{
			$qry 	= $db->getQuery(true)
							->insert('#__contact_requests')
							->set('name="'.$data['contact_name'].'"')
							->set('email="'.$data['contact_email'].'"')
							->set('subject="'.$data['contact_subject'].'"')
							->set('message="'.$data['contact_message'].'"')
							->set('timestamp="'.date('Y-m-d h:i:s').'"');

			$db->setQuery($qry)->query();
		}
		
		
		return true;
	}
	
}