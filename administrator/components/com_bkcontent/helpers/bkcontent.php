<?php
// No direct access to this file
defined('_JEXEC') or die;

/**
 * Bkcontent component helper.
 */

class BkcontentHelper extends JHelperContent {

	public static $extension = 'com_bkcontent';
	/**
	 * Configure the Linkbar.
	 */
	public static function addSubmenu($submenu) {

		JHtmlSidebar::addEntry(
			JText::_('COM_BKCONTENT_SUBMENU_INVTIDS'),
			'index.php?option=com_bkcontent&view=bkproducts',
			$submenu == 'bkproducts'
		);

		JHtmlSidebar::addEntry(
		JText::_('COM_BKCONTENT_SUBMENU_SERIES'),
		'index.php?option=com_bkcontent&view=serieslists',
		$submenu == 'serieslists'
				);

		JHtmlSidebar::addEntry(
			JText::_('COM_BKCONTENT_SUBMENU_CATEGORIES'),
			'index.php?option=com_categories&extension=com_bkcontent',
			$submenu == 'categories'
		);

		JHtmlSidebar::addEntry(
			JText::_('COM_BKCONTENT_SUBMENU_ACCESSORIESLIST'),
			'index.php?option=com_bkcontent&view=accessorieslists',
			$submenu == 'accessorieslists'
		);

		JHtmlSidebar::addEntry(
			JText::_('COM_BKCONTENT_SUBMENU_FILTERS'),
			'index.php?option=com_bkcontent&view=bkproductfilters',
			$submenu == 'bkproductfilters'
		);

		JHtmlSidebar::addEntry(
			JText::_('COM_BKCONTENT_SUBMENU_SOFTWARE'),
			'index.php?option=com_bkcontent&view=softwares',
			$submenu == 'softwares'
		);

		JHtmlSidebar::addEntry(
			JText::_('COM_BKCONTENT_SUBMENU_DOWNLOADS'),
			'index.php?option=com_bkcontent&view=downloads',
			$submenu == 'downloads'
		);

		JHtmlSidebar::addEntry(
			JText::_('COM_BKCONTENT_SUBMENU_WHERETOBUY'),
			'index.php?option=com_bkcontent&view=wheretobuys',
			$submenu == 'wheretobuys'
		);

		JHtmlSidebar::addEntry(
			JText::_('COM_BKCONTENT_SUBMENU_VIDEOS'),
			'index.php?option=com_bkcontent&view=videos',
			$submenu == 'videos'
		);

		JHtmlSidebar::addEntry(
			JText::_('COM_BKCONTENT_SUBMENU_CONTACT'),
			'index.php?option=com_bkcontent&view=contacts',
			$submenu == 'contacts'
		);

		/**
		// set some global property
		$document = JFactory::getDocument();
		$document->addStyleDeclaration('.icon-48-bkproduct {background-image: url(../media/com_bkcontent/images/bk48x48.png);}');
		if ($submenu == 'categories') {
			$document->setTitle(JText::_('COM_BKCONTENT_ADMINISTRATION_CATEGORIES'));
		}**/
	}

	public static function getActions($categoryId = 0, $id = 0, $assetName = '') {
		$user	= JFactory::getUser();
		$result	= new JObject;

		if (empty($id)) {
			$assetName = 'com_bkcontent';
		}
		else {
			$assetName = 'com_bkcontent.bkproduct.'.(int) $id;
		}

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.delete'
		);

		foreach ($actions as $action) {
			$result->set($action,	$user->authorise($action, $assetName));
		}

		return $result;
	}
}