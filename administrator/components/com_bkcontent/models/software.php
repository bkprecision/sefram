<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');


use Aws\S3\S3Client;
/**
 * Software Model
 */
class BkcontentModelSoftware extends JModelAdmin {

	/**
	 * @var        string    The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_BKCONTENT';

	/**
	 * The type alias for this content type (for example, 'com_content.article').
	 *
	 * @var      string
	 * @since    3.2
	 */
	public $typeAlias = 'com_bkcontent.software';

	/*
	 * AWS settings
	*/
	private $bucket;

	private $client;

	private $softwareDir;

	private $software = array();

	public function __construct() {
		$params 			= JComponentHelper::getParams('com_bkcontent');

		$this->client 		= S3Client::factory(array(
				"key"		=> $params->get("accessKey"),
				"secret"	=> $params->get("secretKey")
		));
		$this->bucket 		= $params->get("s3bucket");
		$this->softwareDir 	= $params->get("softwareDir");

		parent::__construct(array());
	}

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'Software', $prefix = 'BkcontentTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		Data for the form.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	mixed	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true) {

		// Get the form.
		$form = $this->loadForm('com_bkcontent.software', 'software',
		                        array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	public function save($data) {

		$db			 = JFactory::getDbo();
		$input 		 = JFactory::getApplication()->input;
		$jform 		 = $input->get("jform", array(), "array");
		$fileName 	 = $jform["file_name"];
		$description = $jform["description"];
		$version 	 = $jform["version"];
		$oldVersion  = $jform["oldVersion"];
		$createdBy   = $jform["created_by"];
		$created 	 = $jform["created"];
		$modifiedBy  = $jform["modified_by"];
		$modified 	 = $jform["modified"];
		$modelCount  = count($jform["modelList"]);
		$glue 		 = "', '{$fileName}','{$version}', '{$description}', '{$createdBy}', '{$created}', '{$modifiedBy}', '{$modified}'),('";
		// remove the current item from the list
		if(($key = array_search($data["invt_id"], $jform["modelList"])) !== false) {
			unset($jform["modelList"][$key]);
		}
		$deleteQry 	 = $db->getQuery(true);

		$deleteQry->delete("#__bksoftware")
			->where("file_name = '{$fileName}'")
			->where("id <> {$data['id']}");

		if ($oldVersion !== $version) {
			$deleteQry->where("version = '{$oldVersion}'");
		}
		else {
			$deleteQry->where("version = '{$version}'");
		}

		// delete all items with the same version and file_name, except the current one
		$db->setQuery($deleteQry)->execute();

		$insertData  = "'" . implode($glue, $jform["modelList"]) . "', '{$fileName}','{$version}', '{$description}', '{$createdBy}', '{$created}', '{$modifiedBy}', '{$modified}'";
		$insertQry 	 = $db->getQuery(true);
		$insertQry->insert("#__bksoftware")
			->columns("invt_id, file_name, version, description, created_by, created, modified_by, modified")
			->values($insertData);

		// attempt to insert all newly saved models
		// only if we have more than one model
		if ($modelCount > 1) {
			$result = $db->setQuery($insertQry)->execute();
		}

		// assign the default invt_id
		$invtId = $jform["modelList"][0];
		if (empty($data["invt_id"]) && !empty($invtId)) {
			$data["invt_id"] = $invtId;
		}
		else if (empty($invtId) && empty($data["invt_id"])) {
			return false;
		}

		if (!key_exists("file_name", $data)) {
			$data["file_name"] = $fileName;
		}

		return parent::save($data);
	}

	/**
	 * Function to include other records in the db when deleting.
	 * @see JModelAdmin::delete()
	 */
	public function delete(&$pks) {

		$tmp = array();

		foreach ($pks as $pk) {
			$item = $this->getItem($pk);
			$db   = JFactory::getDbo();
			$qry  = $db->getQuery(true)
				->select("id")
				->from("#__bksoftware")
				->where("file_name = '{$item->file_name}'")
				->where("version = '{$item->version}'");

			$tmp  = array_merge($tmp, $db->setQuery($qry)->loadColumn());
		}

		$pks = array_unique($tmp);

		parent::delete($pks);
	}

	public function getSoftware() {

		$result = $this->client->getIterator("listObjects", array(
				"Bucket" => $this->bucket,
				"Prefix" => $this->softwareDir
		));

		foreach ($result as $software) {
			if ((int)$software["Size"] > 0) {
				$pieces = explode("/", $software["Key"]);
				$this->software[] = $pieces[count($pieces)-1];
			}
		}

		return $this->software;
	}

	public function getModels($item = null) {

		if (!empty($item)) {
			$db  = JFactory::getDbo();
			$qry = $db->getQuery(true);

			$qry->select("invt_id")
				->from("#__bksoftware")
				->where("file_name = '{$item->file_name}'")
				->where("version = '{$item->version}'");

			$db->setQuery($qry);

			return $db->loadColumn();
		}
	}

	public function getModelList() {

		$db = JFactory::getDbo();
		$db->setQuery($db->getQuery(true)
				->select("invt_id")
				->from("#__bkproducts"));

		return $db->loadColumn();
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData() {

		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_bkcontent.edit.software.data', array());
		if (empty($data)) {
			$data = $this->getItem();
		}

		return $data;
	}
}