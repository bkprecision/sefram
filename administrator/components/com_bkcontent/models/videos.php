<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Where To Buy Model
 */
class BkcontentModelVideos extends JModelList {

	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @since   1.6
	 * @see     JController
	 */
	public function __construct($config = array()) {
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
					'playlist_id', 'invt_ids'
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param	string	An optional ordering field.
	 * @param	string	An optional direction (asc|desc).
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function populateState($ordering = null, $direction = null) {

		// List state information.
		parent::populateState('id', 'desc');
	}

	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return	string	An SQL query
	 */
	protected function getListQuery() {

		// Create a new query object.
		$db 	= JFactory::getDBO();
		$query  = $db->getQuery(true);
		// Select some fields
		$query->select("*")
			  ->from("#__xref_prods_videos");

		$search = $this->state->get("filter.search");

		if (!empty($search)) {
			$search = $db->Quote('%'.$db->escape($search, true).'%');
			$query->where("(playlist_id LIKE {$search} OR invt_ids LIKE {$search})");
		}

		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering', 'id');
		$orderDirn = $this->state->get('list.direction', 'desc');

		$query->order($db->escape($orderCol . ' ' . $orderDirn));

		return $query;
	}
}
