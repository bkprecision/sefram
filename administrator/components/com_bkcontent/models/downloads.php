<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * BkcontentList Model
 */
class BkcontentModelDownloads extends JModelList
{

	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @since   1.6
	 * @see     JController
	 */
	public function __construct($config = array()) {
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
					'd.filename','filename'
			);
		}

		parent::__construct($config);
	}
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param	string	An optional ordering field.
	 * @param	string	An optional direction (asc|desc).
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{

		$app		= JFactory::getApplication();
		//var_export($app->getUserStateFromRequest($this->context . '.list', 'list', array(), 'array'));die;

		// Adjust the context to support modal layouts.
		if ($layout = $app->input->get('layout')) {
			$this->context .= '.' . $layout;
		}

		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		// List state information.
		parent::populateState('d.filename', 'asc');

	}
	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return	string	An SQL query
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		// Select some fields
		$query->select(
			$this->getState(
				'list.select',
				'd.id, d.filename, d.description'
			)
		);

		// From the products table
		$query->from('#__bkdownloads AS d');

		// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			$search = $db->Quote('%'.$db->escape($search, true).'%');
			$query->where('(d.filename LIKE '.$search.')');
		}

		$listOrdering	= $this->getState('list.ordering');
		$listDirn		= $db->escape($this->getState('list.direction'));

		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering', 'd.filename');
		$orderDirn = $this->state->get('list.direction', 'asc');

		$query->order($db->escape($orderCol . ' ' . $orderDirn));

		return $query;
	}
}