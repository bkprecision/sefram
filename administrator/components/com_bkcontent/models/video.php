<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

class BkcontentModelVideo extends JModelAdmin {

	/**
	 * @var        string    The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_BKCONTENT';

	/**
	 * The type alias for this content type (for example, 'com_content.article').
	 *
	 * @var      string
	 * @since    3.2
	 */
	public $typeAlias = 'com_bkcontent.video';

	/**
	 * Overridden Method to save the form data.
	 *
	 * @param   array  $data  The form data.
	 *
	 * @return  boolean  True on success, False on error.
	 *
	 * @since   12.2
	 */
	public function save($data) {

		$data['invt_ids'] = implode(",", array_filter($data['invt_ids']));

		return parent::save($data);
	}

	public function getModels() {
		$db 	= JFactory::getDBO();
		$query 	= $db->getQuery(true)->select("invt_id")->from("#__bkproducts");
		$models = array();

		$models = array_merge($models, (array)$db->setQuery($query)->loadColumn());
		$query 	= $db->getQuery(true)->select("DISTINCT file_name")->from("#__bkproducts");
		$models = array_filter(array_merge($models, (array)$db->setQuery($query)->loadColumn()));
		sort($models);

		return array_unique($models);
	}

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'Video', $prefix = 'BkcontentTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		Data for the form.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	mixed	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true) {

		// Get the form.
		$form = $this->loadForm('com_bkcontent.video', 'video',
		                        array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData() {

		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_bkcontent.edit.video.data', array());
		if (empty($data)) {
			$data = $this->getItem();
		}

		return $data;
	}
}