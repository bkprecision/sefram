<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * BkcontentList Model
 */
class BkcontentModelBkproducts extends JModelList {

	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @since   1.6
	 * @see     JController
	 */
	public function __construct($config = array()) {
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
					'invt_id', 'p.invt_id', 'access',
					'title', 'p.title', 'a.title',
					'state', 'p.state', 'a.state',
					'language', 'a.language',
					'ordering',	'category_id'
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param	string	An optional ordering field.
	 * @param	string	An optional direction (asc|desc).
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function populateState($ordering = null, $direction = null) {

		$app		= JFactory::getApplication();
		//var_export($app->getUserStateFromRequest($this->context . '.list', 'list', array(), 'array'));die;

		// Adjust the context to support modal layouts.
		if ($layout = $app->input->get('layout')) {
			$this->context .= '.' . $layout;
		}

		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$access = $this->getUserStateFromRequest($this->context . '.filter.access', 'filter_access', 0, 'int');
		$this->setState('filter.access', $access);

		$categoryId = $this->getUserStateFromRequest($this->context . '.filter.category_id', 'filter_category_id');
		$this->setState('filter.category_id', $categoryId);

		$language = $this->getUserStateFromRequest($this->context.'.filter.language', 'filter_language', '');
		$this->setState('filter.language', $language);

		// List state information.
		parent::populateState('p.invt_id', 'asc');

	}
	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return	string	An SQL query
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);

		// Select some fields
		$query->select(
			$this->getState(
				'list.select',
				'p.id as id, p.title, p.state, p.short_desc, p.invt_id, a.title as category, p.cat_id, a.language'
			)
		);

		// From the products table
		$query->from('#__bkproducts AS p');
		$query->leftJoin('#__categories AS a on a.id=p.cat_id');

		// Join over the language
		$query->select('l.title AS language_title');
		$query->join('LEFT', $db->quoteName('#__languages').' AS l ON l.lang_code = a.language');

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search)) {
			$search = $db->Quote('%'.$db->escape($search, true).'%');
			$query->where('(p.title LIKE '.$search.' OR p.invt_id LIKE '.$search.')');
		}

		// get category if there is one and filter by it
		$categoryId = $this->getState('filter.category_id');
		if (is_numeric($categoryId)) {
			$cat_tbl = JTable::getInstance('Category', 'JTable');
			$cat_tbl->load($categoryId);
			$rgt = $cat_tbl->rgt;
			$lft = $cat_tbl->lft;
			$query->where('a.lft >= '.(int) $lft);
			$query->where('a.rgt <= '.(int) $rgt);
		}
		elseif (is_array($categoryId)) {
			JArrayHelper::toInteger($categoryId);
			$categoryId = implode(',', $categoryId);
			$query->where('a.catid IN ('.$categoryId.')');
		}

		// Filter on the language.
		if ($language = $this->getState('filter.language')) {
			$query->where('a.language = '.$db->quote($language));
		}

		// if state is set filter by it, otherwise include all states
		$state = $this->getState('filter.state');
		if (is_numeric($state)) {
			$query->where('p.state = ' . (int) $state);
		}
		else {
			$query->where('(p.state = 0 OR p.state = 1 OR p.state = 2)');
		}

		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering', 'p.invt_id');
		$orderDirn = $this->state->get('list.direction', 'asc');

		if ($orderCol == 'p.invt_id') {
			$orderCol = 'p.invt_id ' . $orderDirn . ', p.state ';
		}

		$query->order($db->escape($orderCol . ' ' . $orderDirn));

		return $query;
	}

}