<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * BkcontentList Model
 */
class BkcontentModelAccessorieslists extends JModelList {

	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @since   1.6
	 * @see     JController
	 */
	public function __construct($config = array()) {
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
					'invt_id', 'a.invt_id', 'state',
					'a.state', 'ordering'
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param	string	An optional ordering field.
	 * @param	string	An optional direction (asc|desc).
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function populateState($ordering = null, $direction = null) {

		$app		= JFactory::getApplication();

		// Adjust the context to support modal layouts.
		if ($layout = $app->input->get('layout')) {
			$this->context .= '.' . $layout;
		}

		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$state = $this->getUserStateFromRequest($this->context . '.filter.state', 'filter_state', '', 'int');
		$this->setState('filter.state', $state);

		// List state information.
		parent::populateState('a.invt_id', 'asc');

	}
	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return	string	An SQL query
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		// Select some fields
		$query->select('*');
		// From the products table
		$query->from('#__bkaccessories AS a');

		// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			$search = $db->Quote('%'.$db->escape($search, true).'%');
			$query->where('(a.title LIKE '.$search.' OR a.invt_id LIKE '.$search.')');
		}

		// if state is set filter by it, otherwise include all states
		$state = $this->getState('filter.state');
		if (is_numeric($state)) {
			$query->where('a.state = ' . (int) $state);
		}
		elseif ($state === '') {
			$query->where('(a.state = 0 OR a.state = 1 OR a.state = 2 OR a.state = -1)');
		}

		$listOrdering	= $this->getState('list.ordering');
		$listDirn		= $db->escape($this->getState('list.direction'));
		if($listOrdering == 'a.invt_id') {
			$query->order('a.invt_id '.$listDirn.', a.state '.$listDirn);
		} else {
			$query->order($db->escape($listOrdering).' '.$listDirn);
		}

		return $query;
	}
}