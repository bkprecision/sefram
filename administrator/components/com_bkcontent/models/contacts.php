<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import the Joomla modellist library
jimport('joomla.application.component.modellist');
/**
 * Contact Us Requests List Model
 */
class BkcontentModelContacts extends JModelList
{
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param	string	An optional ordering field.
	 * @param	string	An optional direction (asc|desc).
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function populateState($ordering = null, $direction = null) {
		
		$app		= JFactory::getApplication();
		$context	= $this->context;
		
		$search = JRequest::getCmd('filter_search');
		$this->setState('filter.search', $search);
		
		$orderCol   = JRequest::getCmd('filter_order', 'c.timestamp');
		$this->setState('list.ordering', $orderCol);

		$listOrder   =  JRequest::getCmd('filter_order_Dir', 'ASC');
		$this->setState('list.direction', $listOrder);
		
		if($this->context)
		{
			$value = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'));
			$limit = $value;
			$this->setState('list.limit', $limit);

			$value = $app->getUserStateFromRequest($this->context . '.limitstart', 'limitstart', 0);
			$limitstart = ($limit != 0 ? (floor($value / $limit) * $limit) : 0);
			$this->setState('list.start', $limitstart);
		}
		else
		{
			$this->setState('list.start', 0);
			$this->state->set('list.limit', 0);
		}
		
		
	}
	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return	string	An SQL query
	 */
	protected function getListQuery()
	{
		// Create a new query object.		
		$db 	= JFactory::getDBO();
		$query 	= $db->getQuery(true);
		
		// Select some fields
		$query->select(
			$this->getState(
				'list.select', 
				'*'
			)
		);
		
		// From the contact requests table
		$query->from('#__contact_requests AS c');
		
		// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			$search = $db->Quote('%'.$db->escape($search, true).'%');
			$query->where('(c.name LIKE '.$search.' OR c.email LIKE '.$search.' OR c.subject LIKE '.$search.')');
		}
		
		$listOrdering	= $this->getState('list.ordering');
		$listDirn		= $db->escape($this->getState('list.direction'));
		if($listOrdering == 'c.timestamp') {
			$query->order('c.timestamp '.$listDirn);
		} else {
			$query->order($db->escape($listOrdering).' '.$listDirn);
		}
		
		return $query;
	}
	
}