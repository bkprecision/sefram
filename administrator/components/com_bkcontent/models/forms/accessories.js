jQuery(document).ready(function ($) {
	
	$('#crossType').change(function () {
		
		var crossType 	= this.value,
			rndInt		= Math.floor(Math.random()*1000),
			$newSel		= $('#'+crossType+'_tmpl').clone(true).attr('id', crossType+'_tmpl_'+rndInt),
			$newLI		= $(document.createElement("LI")).append( $(document.createElement("LABEL")).append(document.createTextNode($(':selected',this).text())) ).append($newSel).append('<button class="delCross">Delete</button>'),
			$dest		= $('#itemCrosses');
		
		if(crossType) {
			
			$('option', this).attr('selected', false).eq(0).attr('selected', true);
			
			$dest.prepend($newLI);

			$('.delCross')
				.button({
					icons:{
						primary:"ui-icon-close"
					},
					text:false
				})
				.click(function (e) {

					$(this).parent().remove();

					e.preventDefault();
				});
		}
		
		
		
	});
	
	$('.delCross')
		.button({
			icons:{
				primary:"ui-icon-close"
			},
			text:false
		})
		.click(function (e) {
			
			$(this).parent().remove();
			
			e.preventDefault();
		});
	
});