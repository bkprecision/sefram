<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelform library
jimport('joomla.application.component.modeladmin');

use Aws\S3\S3Client;

/**
 * BKContent Product Model
 */
class BkcontentModelBkproduct extends JModelAdmin {


	protected $_photos			= null;

	/*
	 * AWS settings
	*/
	private $bucket;

	private $client;

	public function __construct() {
		$params 			= JComponentHelper::getParams('com_bkcontent');

		$this->client 		= S3Client::factory(array(
				"key"		=> $params->get("accessKey"),
				"secret"	=> $params->get("secretKey")
		));
		$this->bucket 		= $params->get("s3bucket");
		$this->_docTypes   	= explode(" ", $params->get("docTypes"));
		$this->_photoTypes 	= explode(" ", $params->get("photoTypes"));
		$this->_photoDir    = $params->get("photoDir");

		parent::__construct(array());
	}

	/**
	 * @var        string    The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_BKCONTENT';

	/**
	 * The type alias for this content type (for example, 'com_content.article').
	 *
	 * @var      string
	 * @since    3.2
	 */
	public $typeAlias = 'com_bkcontent.product';

	/**
	 * Stock method to auto-populate the model state.
	 *
	 * @return  void
	 *
	 * @since   11.1
	 */
	protected function populateState() {
		// Initialise variables.
		$input 	= JFactory::getApplication()->input;
		$table 	= $this->getTable();
		$key 	= $table->getKeyName();

		// Get the pk of the record from the request.
		$pk = $input->getInt($key);
		$this->setState($this->getName() . '.id', $pk);

		// Load the parameters.
		$value = JComponentHelper::getParams($this->option);
		$this->setState('params', $value);

		// load the product id
		$id = $input->getInt("id", "");
		$this->setState('product.id', $id);

		parent::populateState();
	}
	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */

	public function save($data)
	{
		$params 	 	 = JComponentHelper::getParams('com_bkcontent');
		$allowedTags 	 = "<" . str_replace(" ", "><", $params->get("allowedTags")) . ">";
		$input 		 	 = JFactory::getApplication()->input;
		$this->_saveData = $input->get("jform", null, "array");
		// remove any elements not approved
		$data['long_desc'] = strip_tags($data['long_desc'], $allowedTags);
		// Remove any 'styling' that might be trying to be applied
		$data['long_desc'] = preg_replace('/\s+style="[^"]*"/im', '', $data['long_desc']);

		$trueBit = parent::save($data);

		$db = JFactory::getDBO();
		$query = "DELETE FROM #__xref_prods_filters WHERE invt_id = '".$this->_saveData['invt_id']."'";
		$trueBit = $trueBit ? $db->setQuery($query)->query() : $trueBit;
		$trueBit = $trueBit ? $this->saveFilters() : $trueBit;
		$trueBit = $trueBit ? $this->saveListPrice($this->_saveData) : $trueBit;
		if (isset($this->_saveData['replacement']))
		{
			$trueBit = $trueBit ? $this->saveReplacement(array_filter($this->_saveData['replacement'])) : $trueBit;
		}
		$trueBit = $trueBit ? $this->savePhotos() : $trueBit;
		$trueBit = $trueBit ? $this->saveDocs($this->_saveData) : $trueBit;


		//$data['state']
		if ($trueBit === true && $data['state'] == 1) {

            require_once $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';

            $es = new Elasticsearch\Client(['hosts'=> ['52.208.30.60:9200']]);

            $params = [
                'index' => 'products',
                'body' => [
                    'query' => [
                        'match' => [
                            'id' => $data['id']
                        ]
                    ]
                ]
            ];
            $query = $es->search($params);

            $hits = $query['hits']['hits'];

            if (!empty($hits)) {
                foreach ($hits as $hit) {
                    if ($hit['_source']['id'] === $data['id']) {
                        $params = [
                            'index' => $hit['_index'],
                            'type' => $hit['_type'],
                            'id' => $hit['_id']
                        ];

                        $es->delete($params);
                    }
                }
            }

            $params = [
                'index' => 'products',
                'type' => 'product',
                'body' => [
                    'alias' => $data['alias'],
                    'cat_id' => intval($data['cat_id']),
                    'file_name' => $data['file_name'],
                    'id' => $data['id'],
                    'product' => $data['invt_id'],
                    'description' => strip_tags($data['long_desc']),
                    'keywords' => [
                        0 => ''
                    ]
                ]
            ];

            $es->index($params);
        }

		return $trueBit;
	}

	private function saveListPrice($data)
	{
		$invtId 	= $data['invt_id'];
		$listPrice 	= $data['list_price'];
		$db 		= JFactory::getDBO();
		$query 		= $db->getQuery(true);

		// delete anything that might be lingering
		$query->delete("#__xref_products_price")->where("invt_id = '{$invtId}'");
		$db->setQuery($query)->execute();

		if (empty($listPrice))
		{
			return true;
		}

		$query = $db->getQuery(true);
		$query
			->insert("#__xref_products_price")
			->columns("list_price, invt_id")
			->values("'{$listPrice}', '{$invtId}'");

		$db->setQuery($query)->execute();

		return true;
	}

	public function saveFilters() {
		$trueBit = false;

		if( ! empty($this->_saveData)) {
			$db = JFactory::getDBO();
			$query = "SELECT * FROM #__filters";
			$trueBit = $db->setQuery($query)->query();
			$filters = $db->loadObjectList();

			foreach($filters as $filterObj) {

				if(isset($this->_saveData[$filterObj->name]) AND ! empty($this->_saveData[$filterObj->name])) {

					foreach($this->_saveData[$filterObj->name] as $filterVal) {
						if(!empty($filterVal)) {
							$qry = 'INSERT INTO #__xref_prods_filters (invt_id, filter_id, value) VALUES (\''.$this->_saveData['invt_id'].'\', '.$filterObj->id.', \''.htmlentities($filterVal, ENT_QUOTES | ENT_IGNORE, "UTF-8").'\')';
							$trueBit = $db->setQuery($qry)->query();
						}
					}
				}
			}
		}

		return $trueBit;
	}

	private function saveDocs($item)
	{
		$catId  = $item['cat_id'];
		$db 	= JFactory::getDbo();
		$query  = $db->getQuery(true)
			->select("language")
			->from("#__categories")
			->where("id={$catId}");

		$db->setQuery($query);

		$item['lang'] = strtolower($db->loadResult());
		if ($item['lang'] == 'en-gb')
		{
			$item['lang'] = 'en-us';
		}

		foreach ($this->_docTypes as $docType)
		{
			$input = "Proddocs-" . $docType;
			$file  = $_FILES[$input];

			if (empty($file['name']))
			{
				continue;
			}

			$fileName = "downloads/{$docType}s/" . $item['lang'] . "/" . $file['name'];
			$fileSrc  = $file['tmp_name'];

			if ($this->client->doesObjectExist($this->bucket, $fileName))
			{
				$this->client->deleteObject(array(
						'Bucket'       => $this->bucket,
						'Key'          => $fileName
				));
			}

			$result = $this->client->putObject(array(
					'Bucket'       => $this->bucket,
					'Key'          => $fileName,
					'SourceFile'   => $fileSrc,
					'ContentType'  => $file['type'],
					'ACL'          => 'public-read'
			));
		}

		return true;
	}

	private function savePhotos()
	{
		$params 			= JComponentHelper::getParams('com_bkcontent');
		$photoDir    		= $params->get("photoDir") . "/";
		$iconsDir 		 	= "icons/";

		$largePhotos = $_FILES['Largephotos'];
		$newProdIcon = $_FILES['Newprodicon'];
		$productIcon = $_FILES['Producticon'];

		// Upload all the large images
		if (!empty($largePhotos['name']))
		{
			foreach ($largePhotos['name'] as $key => $file)
			{
				if (!empty($file))
				{
					if ($this->client->doesObjectExist($this->bucket, $photoDir . $file))
					{
						$this->client->deleteObject(array(
							'Bucket'       => $this->bucket,
							'Key'          => $photoDir . $file
						));
					}

					$result = $this->client->putObject(array(
							'Bucket'       => $this->bucket,
							'Key'          => $photoDir . $file,
							'SourceFile'   => $largePhotos['tmp_name'][$key],
							'ContentType'  => $largePhotos['type'][$key],
							'ACL'          => 'public-read'
					));
				}
			}
		}

		// Upload New Product Icon
		if (!empty($newProdIcon['name']))
		{
			if ($this->client->doesObjectExist($this->bucket, $iconsDir . $newProdIcon['name']))
			{
				$this->client->deleteObject(array(
						'Bucket'       => $this->bucket,
						'Key'          => $iconsDir . $newProdIcon['name']
				));
			}

			$result = $this->client->putObject(array(
					'Bucket'       => $this->bucket,
					'Key'          => $iconsDir . $newProdIcon['name'],
					'SourceFile'   => $newProdIcon['tmp_name'],
					'ContentType'  => $newProdIcon['type'],
					'ACL'          => 'public-read'
			));
		}

		// Upload Product Icon
		if (!empty($productIcon['name']))
		{
			if ($this->client->doesObjectExist($this->bucket, $iconsDir . $productIcon['name']))
			{
				$this->client->deleteObject(array(
						'Bucket'       => $this->bucket,
						'Key'          => $iconsDir . $productIcon['name']
				));
			}

			$result = $this->client->putObject(array(
					'Bucket'       => $this->bucket,
					'Key'          => $iconsDir . $productIcon['name'],
					'SourceFile'   => $productIcon['tmp_name'],
					'ContentType'  => $productIcon['type'],
					'ACL'          => 'public-read'
			));
		}

		return true;
	}

	public function saveReplacement($replacements = null) {
		$trueBit = false;
		// clear out all replacements in the db
		$trueBit = $this->_db->setQuery("DELETE FROM #__xref_prods_replacements WHERE invt_id = '{$this->_saveData['invt_id']}'")->query();

		if(is_array($replacements)) {

			foreach($replacements as $replacement) {

				$qry = 'INSERT INTO #__xref_prods_replacements (invt_id, replacement) VALUES (\''.$this->_saveData['invt_id'].'\', \''.htmlentities($replacement, ENT_QUOTES | ENT_IGNORE, "UTF-8").'\')';
				$trueBit = $this->_db->setQuery($qry)->query();
			}
		}

		return $trueBit;
	}

	protected function allowEdit($data = array(), $key = 'id') {
		// Check specific edit permission then general edit permission.
		return JFactory::getUser()->authorise('core.edit', 'com_bkcontent.message.'.((int) isset($data[$key]) ? $data[$key] : 0)) or parent::allowEdit($data, $key);
	}

	/**
	 * Returns a Table object, always creating it.
	 *
	 * @param   type      The table type to instantiate
	 * @param   string    A prefix for the table class name. Optional.
	 * @param   array     Configuration array for model. Optional.
	 *
	 * @return  JTable    A database object
	 */
	public function getTable($type = 'Bkproduct', $prefix = 'JTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		Data for the form.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	mixed	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true) {
		// Get the form.
		$form = $this->loadForm('com_bkcontent.bkproduct', 'bkproduct',
		                        array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		return $form;
	}

	public function getItemFilters() {

		$productId = $this->getState('product.id');

		if (empty($productId)) {
			return;
		}

		$query = $this->_db->getQuery(true);
		$query->select('f.id, x.filter_id, x.value, f.name, f.title, p.id as itemid, p.invt_id');
		$query->from('#__xref_prods_filters AS x');
		$query->leftJoin('#__filters AS f ON f.id=x.filter_id');
		$query->leftJoin('#__bkproducts AS p USING(invt_id)');
		$query->where('p.id='.$this->getState('product.id'));
		$this->_db->setQuery($query);

		return $this->_db->loadObjectList();
	}

	public function getItemReplacements() {

		$productId = $this->getState('product.id');

		if (empty($productId)) {
			return;
		}

		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('x.replacement')
				->from('#__xref_prods_replacements AS x')
				->leftJoin('#__bkproducts AS p ON p.invt_id=x.invt_id')
				->where('p.id='.$this->getState('product.id'));

		$db->setQuery($query);

		return $db->loadColumn();
	}

	public function getFilters() {
		$db = JFactory::getDBO();
		$db->setQuery( "select * from #__filters");
		return $db->loadObjectList();

	}

	public function getProducts() {
		$db = JFactory::getDBO();
		$db->setQuery($db->getQuery(true)
						->select('invt_id')
						->from('#__bkproducts AS p')
						->where('p.state=1')
						->group('p.invt_id'));

		return $db->loadColumn();
	}

	public function getPricing() {

		$productId = $this->getState('product.id');

		if (empty($productId)) {
			return;
		}

		$query = $this->_db->getQuery(true);

		$query->select('xp.calibration_cost, xp.calib_data_cost, xp.calib_data_cost*1.2 as calib_badata_cost,
						xp.list_price, xp.repair_cost, xp.ship_cost, xp.upgrade_price');
		$query->from('#__bkproducts AS p');
		$query->join('LEFT OUTER', $this->_db->quoteName('#__xref_products_price').' AS xp USING(invt_id)');
		$query->where('p.id='.$this->getState('product.id'));
		$this->_db->setQuery($query);

		return $this->_db->loadObject();
	}

	public function getAccessories() {

		$productId = $this->getState('product.id');

		if (empty($productId)) {
			return;
		}
		// get cat id and cat parent id
		$catId = $this->getItem()->cat_id;
		$cat_tbl = JTable::getInstance('Category', 'JTable');
		$cat_tbl->load($catId);
		$parentId = $cat_tbl->parent_id;

		$query = $this->_db->getQuery(true);
		$query->select('a.id, a.invt_id, a.title
		from #__bkproducts AS p
		left outer join #__xref_accessories AS x ON x.xref_id=p.id
		left join #__bkaccessories AS a ON a.id=x.accessory_id
		where p.id = '.$this->getState('product.id').' AND x.type="invt_id" AND a.state=1

		union distinct

		select a.id, a.invt_id, a.title
		from #__bkproducts AS p
		left outer join #__xref_accessories AS x ON x.xref_id=p.series
		left join #__bkaccessories AS a ON a.id=x.accessory_id
		where p.id = '.$this->getState('product.id').' and type="series" AND a.state=1

		union distinct

		select a.id, a.invt_id, a.title
		from #__categories AS c
		left outer join #__xref_accessories AS x ON x.xref_id=c.id
		left join #__bkaccessories AS a ON a.id=x.accessory_id
		where c.id IN ('.$catId.','.$parentId.') and type="cat_id" AND a.state=1');
		$this->_db->setQuery($query);



		return $this->_db->loadObjectList();
	}

	public function getPhotos()
	{
		$item 			= $this->getItem();
		$this->_photos	= new stdClass();

		// Make sure we have the product informaiton
		if (isset($item) && isset($item->file_name)) {

			// Check for cached copy
			if ( !isset($this->cachedPhotos[$item->file_name]) ) {

				// Check for each suffix, and if not available, include placeholder
				foreach($this->_photoTypes as $suffix) {

					$photo 			= new stdClass;
					$largeMissing 	= "https://bkpmedia.s3.amazonaws.com/photos/missing/large-missing.png";
					$fileNameSrc 	= "/{$this->_photoDir}/{$item->file_name}{$suffix}";
					$otherIdSrc 	= "/{$this->_photoDir}/" . substr($item->invt_id, 2) . $suffix;
					$invtIdSrc 		= "/{$this->_photoDir}/{$item->invt_id}{$suffix}";

					if ($this->client->doesObjectExist($this->bucket, $fileNameSrc)) {
						$photo->src = $this->client->getObjectUrl($this->bucket, $fileNameSrc);
					}
					else if ($this->client->doesObjectExist($this->bucket, $invtIdSrc)) {
						$photo->src = $this->client->getObjectUrl($this->bucket, $invtIdSrc);
					}
					else if ($this->client->doesObjectExist($this->bucket, $otherIdSrc)) {
						$photo->src = $this->client->getObjectUrl($this->bucket, $otherIdSrc);
					}

					if( !empty($photo->src) ) {
						$photo->title	  = JTEXT::_('COM_BK_MODEL') . " {$item->invt_id} " . ucwords(preg_replace("/^.([^_]+)_.+$/i", "$1", $suffix));
						$photo->altTag	  = JTEXT::_('COM_BK_MODEL') . " {$item->invt_id} " . ucwords(preg_replace("/^.([^_]+)_.+$/i", "$1", $suffix));

						$this->cachedPhotos[$item->file_name][] = $photo;
					}
					else {
						$photo->src = $largeMissing;
						$photo->title	  = "<span class='alert alert-error'>" . $item->file_name . $suffix . " missing!</span>";
						$photo->altTag	  = JTEXT::_('COM_BK_MODEL') . " {$item->invt_id} " . ucwords(preg_replace("/^.([^_]+)_.+$/i", "$1", $suffix));
						$this->cachedPhotos[$item->file_name][] = $photo;
					}
				}

			}

			if (isset($this->cachedPhotos[$item->file_name])) {
				$this->_photos->large = $this->cachedPhotos[$item->file_name];
			}

			// Get New Product Icon
			$photo 				= new stdClass;
			$newProdIconMissing = "https://bkpmedia.s3.amazonaws.com/photos/missing/new_prod_icon.png";
			$fileNameSrc 		= "/icons/{$item->file_name}.gif";
			$otherIdSrc 		= "/icons/" . substr($item->invt_id, 2) . ".gif";
			$invtIdSrc 			= "/icons/{$item->invt_id}.gif";

			if ($this->client->doesObjectExist($this->bucket, $fileNameSrc)) {
				$photo->src = $this->client->getObjectUrl($this->bucket, $fileNameSrc);
			}
			else if ($this->client->doesObjectExist($this->bucket, $invtIdSrc)) {
				$photo->src = $this->client->getObjectUrl($this->bucket, $invtIdSrc);
			}
			else if ($this->client->doesObjectExist($this->bucket, $otherIdSrc)) {
				$photo->src = $this->client->getObjectUrl($this->bucket, $otherIdSrc);
			}

			if( !empty($photo->src) ) {
				$photo->title	  = JTEXT::_('COM_BK_MODEL') . " {$item->invt_id} New Product Icon";
				$photo->altTag	  = JTEXT::_('COM_BK_MODEL') . " {$item->invt_id}";
			}
			else {
				$photo->src = $newProdIconMissing;
				$photo->title	  = "<span class='alert alert-error'>New Product Icon " . $item->file_name . ".gif missing!</span>";
				$photo->altTag	  = JTEXT::_('COM_BK_MODEL') . " {$item->invt_id}";
			}

			$this->_photos->newProdIcon = $photo;

			// Get Product Preview Icon
			$photo 			 = new stdClass;
			$prodIconMissing = "https://bkpmedia.s3.amazonaws.com/photos/missing/product_icon_cats.png";
			$fileNameSrc 	 = "/icons/{$item->file_name}_icon.jpg";
			$otherIdSrc 	 = "/icons/" . substr($item->invt_id, 2) . "_icon.jpg";
			$invtIdSrc 		 = "/icons/{$item->invt_id}_icon.jpg";

			if ($this->client->doesObjectExist($this->bucket, $fileNameSrc)) {
				$photo->src = $this->client->getObjectUrl($this->bucket, $fileNameSrc);
			}
			else if ($this->client->doesObjectExist($this->bucket, $invtIdSrc)) {
				$photo->src = $this->client->getObjectUrl($this->bucket, $invtIdSrc);
			}
			else if ($this->client->doesObjectExist($this->bucket, $otherIdSrc)) {
				$photo->src = $this->client->getObjectUrl($this->bucket, $otherIdSrc);
			}

			if( !empty($photo->src) ) {
				$photo->title	  = JTEXT::_('COM_BK_MODEL') . " {$item->invt_id} Product Icon";
				$photo->altTag	  = JTEXT::_('COM_BK_MODEL') . " {$item->invt_id}";
			}
			else {
				$photo->src = $prodIconMissing;
				$photo->title	  = "<span class='alert alert-error'>Product Icon " . $item->file_name . "_icon.jpg missing!</span>";
				$photo->altTag	  = JTEXT::_('COM_BK_MODEL') . " {$item->invt_id}";
			}

			$this->_photos->prodIcon = $photo;

		}

		return $this->_photos;
	}

	public function getDocumentation()
	{
		$this->_docs = array();
		$item = $this->getItem();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true)
			->select("language")
			->from("#__categories")
			->where("id={$item->cat_id}");

		$db->setQuery($query);

		$item->lang = strtolower($db->loadResult());

		if( isset($item) ) {

			foreach($this->_docTypes as $docType) {

				$tmp  		= new stdClass();
				$tmp->title = "COM_BKPRODUCT_" . strtoupper($docType);
				$tmp->type	= $docType;
				$item->lang = str_replace("en-gb", "en-us", $item->lang);
				if (isset($item->file_name)) {
					$src  		= "/downloads/{$docType}s/{$item->lang}/{$item->file_name}_{$docType}.pdf";
				}
				if (isset($item->invt_id)) {
					$altSrc		= "/downloads/{$docType}s/{$item->lang}/{$item->invt_id}_{$docType}.pdf";
					$bkProduct 	= substr($item->invt_id, 2);
					$altSrc2	= "/downloads/{$docType}s/{$item->lang}/{$bkProduct}_{$docType}.pdf";
				}

				if (!empty($src) && $this->client->doesObjectExist($this->bucket, $src)) {
					$tmp->src = "https://{$this->bucket}.s3.amazonaws.com{$src}";

				}
				else if (!empty($altSrc) && $this->client->doesObjectExist($this->bucket, $altSrc)) {
					$tmp->src = "https://{$this->bucket}.s3.amazonaws.com{$altSrc}";
				}
				// try the luck at something that has BK in front of the model number
				else if ( !isset($tmp->src)
						&& !empty($altSrc2)
						&& $this->client->doesObjectExist($this->bucket, $altSrc2)) {
					$tmp->src = "https://{$this->bucket}.s3.amazonaws.com{$altSrc2}";
				}

				if( isset($tmp->src) ) {
					$this->_docs[] = $tmp;
				}
				else {
					$tmp->class = "alert alert-error";
					$tmp->title = JText::_($tmp->title) . " missing!";
					$this->_docs[] = $tmp;
				}
			}

		}

		return $this->_docs;
	}
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData() {
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_bkcontent.edit.bkproduct.data', array());
		if (empty($data)) {
			$data = $this->getItem();
		}
		return $data;
	}
}
