<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelform library
jimport('joomla.application.component.modeladmin');

class BkcontentModelAccessorieslist extends JModelAdmin {

	/**
	 * @var        string    The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_BKCONTENT';

	/**
	 * The type alias for this content type (for example, 'com_content.article').
	 *
	 * @var      string
	 * @since    3.2
	 */
	public $typeAlias = 'com_bkcontent.accessory';

	protected $_saveData = null;

	protected $_products = null;

	public function save($data) {

		// get form data
		$input = JFactory::getApplication()->input;
		$jform = $input->get("jform", array(), "array");
		//$jform = JRequest::getVar('jform');
		$this->setState('form.data', $jform);

		// get current user info
		$user = JFactory::getUser();
		$currentUser = $user->get( 'id' );
		$this->setState('form.userId', $currentUser);

		$this->_saveData = $this->getState('form.data');

		if(empty($data["created_by"]))
		{
			$data["created_by"] = $this->getState('form.userId');
			$data["created"] 	= date('Y-m-d H:m:s');
		}

		$data["modified_by"] 	= $this->getState('form.userId');
		$data["modified"] 		= date('Y-m-d H:m:s');

		$data['long_desc'] = strip_tags($data['description'], '<p><ul><li>');

		$trueBit = $this->saveCrosses(); // save the item crosses

		$trueBit = parent::save($data); // save the item


		return $trueBit;

	}

	protected function saveCrosses() {

		$db 		= JFactory::getDBO();
		$formData 	= $this->getState('form.data');

		if (empty($formData["id"])) {
			return true;
		}

		$crosses 	= $formData["acc_xref"];
		$delQry 	= $db->getQuery(true)
							->delete('#__xref_accessories')
							->where('accessory_id='.$formData["id"]);
		$success 	= $db->setQuery($delQry)->query();

		foreach ($crosses as $crossType=>$itemCrosses) {
			foreach ($itemCrosses as $xref_id) {
				if ( ! empty($xref_id)) {
					$crossQry 	= 'INSERT INTO #__xref_accessories (accessory_id, type, xref_id) VALUES ('.$formData["id"].', "'.$crossType.'", "'.$xref_id.'")';
					$success 	= $db->setQuery($crossQry)->query();
				}
			}
		}

		return $success;
	}

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */

	public function getTable($type = 'Accessorieslist', $prefix = 'BkcontentTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		Data for the form.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	mixed	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_bkcontent.accessorieslist', 'accessorieslist',
		                        array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form))
		{
			return false;
		}
		return $form;
	}
	/**
	* Method to get the scripts that have to be included on the form
	*
	* @return string	Script files
	*/
	public function getScript() {
		return 'administrator/components/com_bkcontent/models/forms/accessories.js';
	}

	public function getItemCross() {
		$id = JRequest::getVar('id');
		if (empty($id)) {
			return array();
		}
		$db = JFactory::getDBO();
		$query = $db->getQuery(true)
						->select('type, xref_id')
						->from('#__bkaccessories as a')
						->join("INNER",'#__xref_accessories as x ON x.accessory_id=a.id')
						->where('a.id='.$id);

		$db->setQuery( $query );
		$data = $db->loadObjectList();

		return $data;
	}
	public function getSeries()
	{
		$db = JFactory::getDBO();
		$db->setQuery( "select * from #__bkseries");
		return $db->loadObjectList();
	}

	public function getProducts()
	{
		if(!isset($this->_products))
		{
			$db = JFactory::getDBO();
			$query = $db->getQuery(true)
							->select('p.id, p.invt_id, c.title')
							->from('#__bkproducts AS p')
							->leftJoin('#__categories AS c ON c.id=p.cat_id')
							->order('p.invt_id');
			$db->setQuery($query);

			$this->_products = $db->loadObjectList();
		}

		return $this->_products;
	}

	public function getCategories()
	{
		$db = JFactory::getDBO();
		$db->setQuery("select a.id, a.title from #__categories AS a where extension ='com_bkcontent' AND a.published in (0,1) ORDER BY a.lft");
		return $db->loadObjectList();
	}
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_bkcontent.edit.accessorieslist.data', array());
		if (empty($data))
		{
			$data = $this->getItem();
		}
		return $data;
	}
}