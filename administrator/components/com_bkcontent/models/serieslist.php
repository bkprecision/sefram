<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Series Model
 */
class BkcontentModelSerieslist extends JModelAdmin {

	/**
	 * @var        string    The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_BKCONTENT';

	/**
	 * The type alias for this content type (for example, 'com_content.article').
	 *
	 * @var      string
	 * @since    3.2
	 */
	public $typeAlias = 'com_bkcontent.series';

	protected $products;

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'Serieslist', $prefix = 'BkcontentTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		Data for the form.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	mixed	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true) {

		// Get the form.
		$form = $this->loadForm('com_bkcontent.serieslist', 'serieslist', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form)) {
			return false;
		}

		return $form;
	}

	public function getProducts() {

		$id = $this->getItem()->id;
		if (empty($id)) {
			return;
		}

		$db = JFactory::getDbo();
		$db->setQuery($db->getQuery(true)
				->select("id, invt_id")
				->from("#__bkproducts")
				->where("series = {$id}"));

		return $db->loadObjectList();
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData() {

		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_bkcontent.edit.serieslist.data', array());

		if (empty($data)) {
			$data = $this->getItem();
		}

		return $data;
	}
}