<?php
// No direct access to this file
defined('_JEXEC') or die;
 
// import the list field type
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');
 
/**
 * BKProduct Form Field class for the BKContent component
 */
class JFormFieldBkproduct extends JFormFieldList
{
	/**
	 * The field type.
	 *
	 * @var		string
	 */
	protected $type = 'Bkproduct';
	
	/**
	 * Method to get a list of options for a list input.
	 *
	 * @return	array		An array of JHtml options.
	 */
	protected function getOptions() 
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('#__bkproducts.id as id, invt_id, #__categories.title as category, cat_id');
		$query->from('#__bkproducts');
		$query->leftJoin('#__categories on cat_id=#__categories.id');
		$db->setQuery((string)$query);
		$products = $db->loadObjectList();
		$options = array();
		if ($products)
		{
			foreach($products as $product) 
			{
				$options[] = JHtml::_('select.option', $product->id, $product->invt_id . ($product->cat_id ? ' (' . $product->category . ')' : ''));
			}
		}
		$options = array_merge(parent::getOptions(), $options);
		return $options;
	}
}