<?php
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Where to Buy table
 */
class BkcontentTableWheretobuy extends JTable {

	/**
	 * Constructor
	 *
	 * @param object Database connector object
	 */
	function __construct(JDatabaseDriver $db) {
		parent::__construct('#__bkdistribution_intl', 'id', $db);
	}
}