<?php
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * BK Content Table class
 */
class BkcontentTableSerieslist extends JTable {

	/**
	 * Constructor
	 *
	 * @param object Database connector object
	 */
	function __construct(JDatabaseDriver $db) {
		parent::__construct('#__bkseries', 'id', $db);
	}

	/**
	* Overloaded bind function
	*
	* @param	array 	named array
	* @return 	null|string 	null is operation was satisfactory, otherwise returns an error
	* @see JTable::bind
	* @since 1.5
	*/
	public function bind($array, $ignore = '') {

		$attributes = JFactory::getApplication()->input->get("jform", array(), "array");
		if (isset($attributes["attribs"])) {
			$attributes = $attributes["attribs"];
			foreach($attributes as $title => $models) {
				$filteredModels = array_filter($models);
				if (empty($filteredModels)) {
					unset($attributes[$title]);
				}
			}
		}

		// check if we are saving new attributes
		if (!empty($attributes)) {
			$params = new JRegistry();
			$params->loadArray(array("attribs"=>$attributes));
			$array["params"] = $params->toString();
		}

		return parent::bind($array, $ignore);
	}
	/**
	* Overloaded check function
	*
	* @return 	boolean 	true if object is ok else false
	* @see JTable::check
	*/
	public function check() {

		if (trim($this->title) == '') {
			$this->setError(JText::_('COM_CONTENT_WARNING_PROVIDE_VALID_NAME'));

			return false;
		}

		//check to ensure the alias is set and is URL safe
		$this->alias = JApplication::stringURLSafe($this->alias);

		if (trim(str_replace('-', '', $this->alias)) == '') {
			$this->alias = JApplication::stringURLSafe($this->title);
		}

		/* Any other check */
		return true;
	}

	/**
	 * Overrides JTable::store to set modified data and user id.
	 *
	 * @param   boolean  $updateNulls  True to update fields even if they are null.
	 *
	 * @return  boolean  True on success.
	 *
	 * @since   11.1
	 */
	public function store($updateNulls = false) {
		$date = JFactory::getDate();
		$user = JFactory::getUser();

		if ($this->id) {
			// Existing item
			$this->modified = $date->toSql();
			$this->modified_by = $user->get('id');
		}

		if (!(int) $this->created) {
			$this->created = $date->toSql();
		}

		if (empty($this->created_by)) {
			$this->created_by = $user->get('id');
		}

		// Verify that the alias is unique
		$table = JTable::getInstance('Serieslist', 'BkcontentTable');

		if ($table->load(array('alias' => $this->alias)) && ($table->id != $this->id || $this->id == 0)) {
			$this->setError(JText::_('JLIB_DATABASE_ERROR_ARTICLE_UNIQUE_ALIAS'));

			return false;
		}

		return parent::store($updateNulls);
	}

	/**
	* Overloaded load function
	*
	* @param 	int $pk primary key
	* @param 	boolean $reset reset data
	* @return 	boolean
	* @see JTable::load
	*/
	public function load($pk = null, $reset = true) {
		if (parent::load($pk, $reset)) {
			// Convert the params field to a registry.
			$params = new JRegistry;
			$params->loadString($this->params);
			$this->params = $params;

			return true;
		}
		else {
			return false;
		}
	}
}