<?php
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * BK Content Table class
 */
class BkcontentTableSoftware extends JTable {

	/**
	 * Constructor
	 *
	 * @param object Database connector object
	 */
	function __construct(JDatabaseDriver $db) {
		parent::__construct('#__bksoftware', 'id', $db);
	}

	/**
	* Overloaded bind function
	*
	* @param	array 	named array
	* @return 	null|string 	null is operation was satisfactory, otherwise returns an error
	* @see JTable::bind
	* @since 1.5
	*/
	public function bind($array, $ignore = '') {

		if(isset($array['params']) AND is_array($array['params'])) {

			// Convert the params field to a string
			$parameter = new JRegistry;
			$parameter->loadArray($array['params']);
			$array['params'] = (string)$parameter;
		}

		return parent::bind($array, $ignore);
	}

	/**
	* Overloaded load function
	*
	* @param 	int $pk primary key
	* @param 	boolean $reset reset data
	* @return 	boolean
	* @see JTable::load
	*/
	public function load($pk = null, $reset = true) {

		if (parent::load($pk, $reset)) {

			// Convert the params field to a registry.
			$params = new JRegistry;
			if (property_exists($this, $params)) {
				$params->loadString($this->params);
			}
			$this->params = $params;
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Overrides JTable::store to set modified data and user id.
	 *
	 * @param   boolean  $updateNulls  True to update fields even if they are null.
	 *
	 * @return  boolean  True on success.
	 *
	 * @since   11.1
	 */
	public function store($updateNulls = false) {
		$date = JFactory::getDate();
		$user = JFactory::getUser();

		if ($this->id) {
			// Existing item
			$this->modified = $date->toSql();
			$this->modified_by = $user->get('id');
		}

		if (!(int) $this->created) {
			$this->created = $date->toSql();
		}

		if (empty($this->created_by)) {
			$this->created_by = $user->get('id');
		}

		return parent::store($updateNulls);
	}
}