<?php
// No direct access
defined('_JEXEC') or die('Restricted access');

// import Joomla table library
jimport('joomla.database.table');

/**
 * BK Content Table class
 */
class BkcontentTableAccessorieslist extends JTable {
	/**
	 * Constructor
	 *
	 * @param object Database connector object
	 */
	function __construct(&$db) {
		parent::__construct('#__bkaccessories', 'id', $db);
	}
	/**
	* Overloaded bind function
	*
	* @param	array 	named array
	* @return 	null|string 	null is operation was satisfactory, otherwise returns an error
	* @see JTable::bind
	* @since 1.5
	*/
	public function bind($array, $ignore = '')
	{
		if(isset($array['params']) AND is_array($array['params']))
		{
			// Convert the params field to a string
			$parameter = new JRegistry;
			$parameter->loadArray($array['params']);
			$array['params'] = (string)$parameter;
		}
		return parent::bind($array, $ignore);
	}
	/**
	* Overloaded check function
	*
	* @return 	boolean 	true if object is ok else false
	* @see JTable::check
	*/
	public function check()
	{
		return true;
	}
	/**
	* Overloaded load function
	*
	* @param 	int $pk primary key
	* @param 	boolean $reset reset data
	* @return 	boolean
	* @see JTable::load
	*/
	public function load($pk = null, $reset = true)
	{
		if(parent::load($pk, $reset))
		{
			// Convert the params field to a registry.
			$params = new JRegistry;
			$params->loadString($this->params);
			$this->params = $params;
			return true;
		}
		else
		{
			return false;
		}
	}

	public function delete($pk = null)
	{
		// Initialise variables.
		$k = $this->_tbl_key;
		$pk = (is_null($pk)) ? $this->$k : $pk;

		// If no primary key is given, return false.
		if ($pk === null)
		{
			$e = new JException(JText::_('JLIB_DATABASE_ERROR_NULL_PRIMARY_KEY'));
			$this->setError($e);
			return false;
		}

		// Update the row by primary key to "disabled"
		$query = $this->_db->getQuery(true);
		$query->update($this->_tbl)->set('state = -1');
		$query->where($this->_tbl_key . ' = ' . $this->_db->quote($pk));
		$this->_db->setQuery($query);

		// Check for a database error.
		if (!$this->_db->query())
		{
			$e = new JException(JText::sprintf('JLIB_DATABASE_ERROR_DELETE_FAILED', get_class($this), $this->_db->getErrorMsg()));
			$this->setError($e);
			return false;
		}

		return true;
	}
}