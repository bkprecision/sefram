<?php
// No direct access
defined('_JEXEC') or die('Restricted access');

// import Joomla table library
jimport('joomla.database.table');

/**
 * BK Content Table class
 */
class BkcontentTableBkproductfilter extends JTable
{
	/**
	 * Constructor
	 *
	 * @param object Database connector object
	 */
	function __construct(&$db)
	{
		parent::__construct('#__filters', 'id', $db);
	}
	/**
	* Overloaded bind function
	*
	* @param	array 	named array
	* @return 	null|string 	null is operation was satisfactory, otherwise returns an error
	* @see JTable::bind
	* @since 1.5
	*/
	public function bind($array, $ignore = '')
	{
		if(isset($array['params']) AND is_array($array['params']))
		{
			// Convert the params field to a string
			$parameter = new JRegistry;
			$parameter->loadArray($array['params']);
			$array['params'] = (string)$parameter;
		}
		return parent::bind($array, $ignore);
	}
	/**
	* Overloaded check function
	*
	* @return 	boolean 	true if object is ok else false
	* @see JTable::check
	*/
	public function check()
	{

		/* Any other check */
		return true;
	}
	/**
	* Overloaded load function
	*
	* @param 	int $pk primary key
	* @param 	boolean $reset reset data
	* @return 	boolean
	* @see JTable::load
	*/
	public function load($pk = null, $reset = true)
	{
		if(parent::load($pk, $reset))
		{
			// Convert the params field to a registry.
			$params = new JRegistry;
			$params->loadString($this->params);
			$this->params = $params;
			return true;
		}
		else
		{
			return false;
		}
	}
}