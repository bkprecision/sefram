<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

if (!JFactory::getUser()->authorise('core.manage', 'com_bkcontent')) {
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

// Require helper file
JLoader::register('BkcontentHelper', __DIR__ . '/helpers/bkcontent.php');

// load the AWS PHP SDK
require JPATH_LIBRARIES.'/aws/aws.phar';

// set some global properties
$document = JFactory::getDocument();
$document->addStyleDeclaration('.icon-48-BK {background-image: url(../media/com_bkcontent/images/bk48x48.png);}');

// Get an instance of the controller prefixed by Bkcontent
$controller = JControllerLegacy::getInstance('Bkcontent');

// Perform the Request task
$controller->execute(JFactory::getApplication()->input->get('task'));

// Redirect if set by the controller
$controller->redirect();