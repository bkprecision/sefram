<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * General Controller of Bkcontent component
 */
class BkcontentController extends JControllerLegacy {
	/**
	 * display task
	 *
	 * @return void
	 */
	function display($cachable = false, $urlparams = false) {

		// set default view if not set
		JFactory::getApplication()->input->set('view', JFactory::getApplication()->input->get("view", "Bkproducts"));

		// call parent behavior
		parent::display($cachable);

		// Set the submenu
		BkcontentHelper::addSubMenu(JFactory::getApplication()->input->get('view'));
	}
}