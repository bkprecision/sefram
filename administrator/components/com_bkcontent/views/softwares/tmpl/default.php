<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

// load tooltip behavior
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));

$sortFields = $this->getSortFields();
?>

<form action="<?php echo JRoute::_('index.php?option=com_bkcontent&view=softwares');?>" method="post" name="adminForm" id="adminForm">
<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
		<?php
		// Search tools bar
		echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
		?>
		<?php if (empty($this->items)): ?>
			<div class="alert alert-no-items">
				<?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
			</div>
		<?php else: ?>

	<table class="table table-striped" id="productList">
		<thead>
			<tr>
				<th width="1%" class="hidden-phone">
					&nbsp;
				</th>
				<th>
					<?php echo JHtml::_('searchtools.sort',  'COM_BKCONTENT_SOFTWARE_FILENAME', 's.file_name', $listDirn, $listOrder); ?>
				</th>
				<th>
					<?php echo JText::_('COM_BKCONTENT_BKPRODUCT_DESCRIPTION_LABEL'); ?>
				</th>
				<th>
					<?php echo JText::_('COM_BKCONTENT_BKPRODUCT_VERSION_LABEL'); ?>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($this->items as $i => $item): ?>
				<tr class="row<?php echo $i % 2; ?>">
					<td class="center hidden-phone">
						<?php echo JHtml::_('grid.id', $i, $item->id); ?>
					</td>
					<td>
						<a href="<?php echo JRoute::_('index.php?option=com_bkcontent&task=software.edit&id=' . $item->id); ?>">
							<?php echo JText::_($item->file_name); ?>
						</a>
					</td>
					<td>
						<?php echo $item->description; ?>
					</td>
					<td>
						<?php echo $item->version; ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<?php endif; ?>
	<?php echo $this->pagination->getListFooter(); ?>


		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
