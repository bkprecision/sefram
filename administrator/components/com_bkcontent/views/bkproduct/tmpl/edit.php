<?php
// No direct access
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('bootstrap.framework');

$app = JFactory::getApplication();
$input = $app->input;
?>
<script type="text/javascript">
	var BKP = BKP || {};
	BKP.models = [<?php echo "'".implode("','", $this->products)."'"; ?>];
</script>
<script type="text/javascript" data-main="/media/com_bkcontent/js/admin/product/main.js" src="/media/com_bkcontent/js/lib/require/require.js"></script>
<script type="text/javascript">
	Joomla.submitbutton = function(task) {
		if (task == 'bkproduct.cancel' || document.formvalidator.isValid(document.id('product-form'))) {
			<?php
				echo $this->form->getField('long_desc')->save();
			?>
			Joomla.submitform(task, document.getElementById('product-form'));
		}
	}
</script>

<form enctype="multipart/form-data" action="<?php echo JRoute::_('index.php?option=com_bkcontent&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="product-form" class="form-validate">

	<div class="form-inline form-inline-header">
	<?php
	echo $this->form->getControlGroup('invt_id');
	echo $this->form->getControlGroup('title');
	echo $this->form->getControlGroup('alias');
	?>
	</div>

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_BKCONTENT_BKPRODUCT_DETAILS', true)); ?>

			<div class="row-fluid">
				<div class="span3">
					<fieldset class="form-vertical">
						<div class="control-group">
							<div class="control-label">
								<label>MSRP</label>
							</div>
							<div class="controls">
					<?php if (!empty($this->item->id) && !empty($this->item->pricing->list_price)): ?>
								<input name="jform[list_price]" type="text" value="<?php echo number_format($this->item->pricing->list_price, 0, '.', ''); ?>">
					<?php else: ?>
								<input name="jform[list_price]" type="text">
					<?php endif; ?>
							</div>
						</div>
						<?php foreach ($this->form->getFieldset('details') as $field) : ?>
							<div class="control-group">
								<div class="control-label">
									<?php echo $field->label; ?>
								</div>
								<div class="controls">
									<?php echo $field->input; ?>
								</div>
							</div>
						<?php endforeach; ?>
						<?php
						if($this->item->state == 2):
							if(is_array($this->item->replacements) AND count($this->item->replacements) > 0 AND ! empty($this->item->replacements[0])):
								$i = 1;
								foreach($this->item->replacements as $replacement):
						?>
							<div class="control-group">
								<div class="control-label">
									<label
										id="jform_replacement-lbl<?='_'.$i?>"
										for="jform_replacement<?='_'.$i?>"
										class="hasTip"
										title="<?=JText::_('COM_BKCONTENT_BKPRODUCT_REPLACEMENT_TIP')?>">Replacement</label>
								</div>
								<div class="controls">
									<input
										type="text"
										name="jform[replacement][]"
										id="jform_replacement<?='_'.$i?>"
										class="inputbox autocomplete models"
										value="<?=$replacement?>" />
									<button class="btn btn-danger btn-mini remove-replacement">Remove</button>
								</div>
							</div>
							<?php

									$i++;
									endforeach;
							?>
							<div class="control-group">
									<div class="control-label">
										<label
											id="jform_replacement-lbl_<?php echo $i; ?>"
											for="jform_replacement_<?php echo $i; ?>"
											class="hasTip"
											title="<?=JText::_('COM_BKCONTENT_BKPRODUCT_REPLACEMENT_TIP')?>">Replacement</label>
									</div>
									<div class="controls">
										<input
											type="text"
											name="jform[replacement][]"
											id="jform_replacement_<?php echo $i; ?>"
											class="inputbox typeahead"
											autocomplete="off"
											data-provide="typeahead"
											data-source="BKP.models">
										<button class="btn btn-success btn-mini add-replacement">Add</button>
									</div>
								</div>
							<?php else:	?>
								<div class="control-group">
									<div class="control-label">
										<label
											id="jform_replacement-lbl_1"
											for="jform_replacement_1"
											class="hasTip"
											title="<?=JText::_('COM_BKCONTENT_BKPRODUCT_REPLACEMENT_TIP')?>">Replacement</label>
									</div>
									<div class="controls">
										<input
											type="text"
											name="jform[replacement][]"
											id="jform_replacement_1"
											class="inputbox typeahead"
											autocomplete="off"
											data-provide="typeahead"
											data-source="BKP.models">
										<i class="icon-plus-circle add-replacement"></i>
									</div>
								</div>
							<?php
								endif;
							endif; ?>
					</fieldset>
				</div>
				<div class="span9">
					<fieldset class="adminform">
						<?php echo $this->form->getLabel('long_desc') . $this->form->getInput('long_desc'); ?>
					</fieldset>
				</div>
			</div>

		<?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'information-sliders-'.$this->item->id, JText::_('COM_BKCONTENT_BKPRODUCT_INFORMATION', true)); ?>
			<div class="row-fluid form-horizontal-desktop">
				<div class="span6">
					<?php foreach ($this->form->getFieldset('information') as $field) : ?>
						<div class="control-group">
							<div class="control-label">
								<?php echo $field->label; ?>
							</div>
							<div class="controls">
								<?php echo $field->input; ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
				<div class="span6">
					<?php foreach ($this->form->getFieldset('params') as $field) : ?>
						<div class="control-group">
							<div class="control-label">
								<?php echo $field->label; ?>
							</div>
							<div class="controls">
								<?php echo $field->input; ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'filter-accessory-details', JText::_('COM_BKCONTENT_BKPRODUCT_FILTERS', true) . " & " . JText::_('COM_BKCONTENT_BKPRODUCT_ACCESSORIES', true)); ?>
			<div class="row-fluid form-horizontal-desktop">
				<?php if (!empty($this->item->id)): ?>
				<div class="span6">
					<? foreach($this->item->filters as $itemFilter): ?>
						<? if ($itemFilter->invt_id != NULL): ?>
							<div class="control-group">
							<? foreach($this->form->filteroptions as $filterObj): ?>
								<? if($itemFilter->id == $filterObj->id): ?>
									<div class="control-label">
										<label for="jform[<?=$itemFilter->name?>]" id="jform_<?= strtolower($itemFilter->title); ?>-lbl"><?=JText::_($itemFilter->title)?></label>
									</div>
									<div class="controls">
										<input type="text" id="jform_<?=$filterObj->name?>" class="inputbox" name="jform[<?=$filterObj->name?>][]" value="<?=$itemFilter->value?>" />
										<button class="btn btn-danger btn-mini remove-filter">Remove</button>
									</div>
								<? endif; ?>
							<? endforeach; ?>
							</div>
						<? endif; ?>
					<? endforeach; ?>

					<? if ($this->form->filteroptions): ?>
						<div class="control-group">
							<div class="control-label">
								<label for="jform_name">Filter Name</label>
							</div>
							<div class="controls">
								<select id="jform_newFilter" class="inputbox required" name="jform[name]" />
									<option value="0">- Select Filter -</option>
									<? foreach($this->form->filteroptions as $filterObj): ?>
									<option value="<?=$filterObj->name?>"><?=JTEXT::_($filterObj->title)?></option>
									<? endforeach; ?>
								</select>
							</div>
						</div>
					<? endif; ?>
				</div>
				<div class="span6">
					<ul class="unstyled">
					<?php foreach ($this->item->accessories as $accessory) : ?>
						<li><a href="<?php echo JRoute::_('index.php?option=com_bkcontent&task=accessorieslist.edit&id=' . $accessory->id); ?>"><?php echo $accessory->invt_id; echo ' - '.$accessory->title; ?></a></li>
					<?php endforeach; ?>
					</ul>
				</div>
				<?php endif; ?>
			</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'photos-tab', JText::_('COM_BKCONTENT_BKPRODUCT_PHOTOS', true)); ?>
		<div class="row-fluid">
			<section class="span6 large-photos">
				<div>
					<label for="upload-large-photos" class="control-label"><?php echo JText::_('COM_BKCONTENT_UPLOAD_LARGE_PHOTOS'); ?></label>
					<input type="file" id="upload-large-photos" name="Largephotos[]" multiple />
				</div>
				<?php if (!empty($this->item->id)): ?>
				<ul class="unstyled">
				<?php foreach ($this->item->photos->large as $largePhoto): ?>
				<li>
					<h4><?php echo $largePhoto->title; ?></h4>
					<div class="thumbnail">
						<img src="<?php echo $largePhoto->src; ?>" alt="<?php echo $largePhoto->altTag; ?>" />
					</div>
				</li>
				<?php endforeach; ?>
				</ul>
				<?php endif; ?>
			</section>
			<section class="span6 icons-docs">
				<div class="row-fluid">
					<div>
						<label for="upload-new-prod-icon" class="control-label"><?php echo JText::_('COM_BKCONTENT_UPLOAD_NEW_PROD_ICON'); ?></label>
						<input type="file" id="upload-new-prod-icon" name="Newprodicon" />
					</div>
					<?php if (!empty($this->item->id)): ?>
					<h4><?php echo $this->item->photos->newProdIcon->title; ?></h4>
					<div class="thumbnail">
						<img src="<?php echo $this->item->photos->newProdIcon->src; ?>" alt="<?php echo $this->item->photos->newProdIcon->altTag; ?>" />
					</div>
					<?php endif; ?>
				</div>
				<div class="row-fluid">
					<div>
						<label for="upload-product-icon" class="control-label"><?php echo JText::_('COM_BKCONTENT_UPLOAD_PRODUCT_ICON'); ?></label>
						<input type="file" id="upload-product-icon" name="Producticon" />
					</div>
					<?php if (!empty($this->item->id)): ?>
					<h4><?php echo $this->item->photos->prodIcon->title; ?></h4>
					<div class="thumbnail">
						<img src="<?php echo $this->item->photos->prodIcon->src; ?>" alt="<?php echo $this->item->photos->prodIcon->altTag; ?>" />
					</div>
					<?php endif; ?>
				</div>
				<div class="row-fluid">
					<?php if (!empty($this->item->documentation) && !empty($this->item->id)): ?>
					<h4>Documentation</h4>
					<table class="table table-bordered">
						<tbody>
							<?php foreach ($this->item->documentation as $doc): ?>
							<tr>
								<?php if (isset($doc->class)): ?>
								<td class="<?php echo $doc->class; ?>"><?php echo JText::_($doc->title); ?></td>
								<?php else: ?>
								<td><?php echo JText::_($doc->title); ?></td>
								<?php endif; ?>
								<?php if (isset($doc->src)): ?>
								<td>
									<a href="<?php echo $doc->src; ?>"><?php echo $doc->src; ?></a>
									<label for="upload-product-docs-<?php echo $doc->type; ?>" class="control-label"><?php echo JText::_('COM_BKCONTENT_UPLOAD_PRODUCT_DOCS_' . strtoupper($doc->type)); ?></label>
									<input type="file" id="upload-product-docs-<?php echo $doc->type; ?>" name="Proddocs-<?php echo $doc->type; ?>" />
								</td>
								<?php else: ?>
								<td>
									<label for="upload-product-docs-<?php echo $doc->type; ?>" class="control-label"><?php echo JText::_('COM_BKCONTENT_UPLOAD_PRODUCT_DOCS_' . strtoupper($doc->type)); ?></label>
									<input type="file" id="upload-product-docs-<?php echo $doc->type; ?>" name="Proddocs-<?php echo $doc->type; ?>" />
								</td>
								<?php endif; ?>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
					<?php endif; ?>
				</div>
			</section>
		</div>

		<?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="return" value="<?php echo $input->getCmd('return'); ?>" />
		<?php echo JHtml::_('form.token'); ?>


	</div>
</form>
<script id="new-filter-tmpl" type="text/x-handlebars-template">
	<div class="control-group">
		<div class="control-label">
			<label for="jform[{{type}}]" id="jform_{{type}}-lbl">{{label}}</label>
		</div>
		<div class="controls">
			<input type="text" id="jform_{{type}}" class="inputbox" name="jform[{{type}}][]" />
		</div>
	</div>
</script>