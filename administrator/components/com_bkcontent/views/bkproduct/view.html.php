<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Bkproduct View
 */
class BkcontentViewBkproduct extends JViewLegacy {

	protected $form;

	protected $item;

	protected $state;

	protected $products;

	protected $script;

	/**
	 * display method of Bkproduct
	 * @return void
	 */
	public function display($tpl = null) {
		// get the Data
		$this->form 				= $this->get('Form');
		$this->form->filteroptions  = $this->get('Filters');
		$this->item 				= $this->get('Item');
		if (!empty($this->item->id))
		{
			$this->item->accessories 	= $this->get('Accessories');
			$this->item->filters 		= $this->get('ItemFilters');
			$this->item->replacements 	= $this->get('ItemReplacements');
			$this->item->pricing 		= $this->get('Pricing');
			$this->item->photos 		= $this->get('Photos');
			$this->item->documentation	= $this->get('Documentation');
		}
		$this->state				= $this->get('State');
		$this->products				= $this->get('Products');
		$this->script 				= $this->get('Script');
		$this->params 				= JComponentHelper::getParams('com_bkcontent');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Set the toolbar
		$this->addToolBar();

		// Set the document
		$this->setDocument();

		// Display the template
		parent::display($tpl);
	}

	/**
	 * Setting the toolbar
	 */


	protected function addToolBar() {
		JFactory::getApplication()->input->set('hidemainmenu', true);
		$user = JFactory::getUser();
		$userId = $user->id;
		$isNew = $this->item->id == 0;
		$canDo = BkcontentHelper::getActions($this->item->id);
		JToolBarHelper::title($isNew ? JText::_('COM_BKCONTENT_MANAGER_BKPRODUCT_NEW')
											 : JText::_('COM_BKCONTENT_MANAGER_BKPRODUCT_EDIT'), 'bkproduct');
		// Built the actions for new and existing records.
		if ($isNew) {
					 // For new records, check the create permission.
			if ($canDo->get('core.create')) {
				JToolBarHelper::apply('bkproduct.apply', 'JTOOLBAR_APPLY');
				JToolBarHelper::save('bkproduct.save', 'JTOOLBAR_SAVE');
				JToolBarHelper::custom('bkproduct.save2new', 'save-new.png',
							 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
			}
			JToolBarHelper::cancel('bkproduct.cancel', 'JTOOLBAR_CANCEL');
		}
		else {
			if ($canDo->get('core.edit')) {
				// We can save the new record
				JToolBarHelper::apply('bkproduct.apply', 'JTOOLBAR_APPLY');
				JToolBarHelper::save('bkproduct.save', 'JTOOLBAR_SAVE');

				// We can save this record, but check the create permission to
				// see if we can return to make a new one.
				if ($canDo->get('core.create')) {
					JToolBarHelper::custom('bkproduct.save2new', 'save-new.png',
								 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
				}
			}
			if ($canDo->get('core.create')) {
				JToolBarHelper::custom('bkproduct.save2copy', 'save-copy.png',
										'save-copy_f2.png', 'JTOOLBAR_SAVE_AS_COPY', false);
			}
			JToolBarHelper::cancel('bkproduct.cancel', 'JTOOLBAR_CLOSE');
		}
	}

	/**
	* Method to set up the document properties
	*
	* @return void
	*/
	protected function setDocument() {
		$isNew	= ($this->item->id < 1);
		$doc	= JFactory::getDocument();
		$doc->setTitle($isNew ? JText::_('COM_BKCONTENT_BKPRODUCT_CREATING')
							  : JText::_('COM_BKCONTENT_BKPRODUCT_EDITING') . " " . $this->item->invt_id);

		JText::script('COM_BKCONTENT_BKPRODUCT_ERROR_UNACCEPTABLE');
		$models = implode('","', $this->products);
		$script = <<<"SCRIPT"
var bkp = bkp || {};
bkp.models = ["$models"];
SCRIPT;

		$doc->addScriptDeclaration($script);

		$style = <<<"STYLE"
.chzn-color.chzn-single[rel="value_2"],
.chzn-color-state.chzn-single[rel="value_2"] {
	color: #ffffff;
	text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
	background-color: #faa732;
	*background-color: #f89406;
	background-image: -moz-linear-gradient(top, #fbb450, #f89406);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#fbb450), to(#f89406));
	background-image: -webkit-linear-gradient(top, #fbb450, #f89406);
	background-image: -o-linear-gradient(top, #fbb450, #f89406);
	background-image: linear-gradient(to bottom, #fbb450, #f89406);
	background-repeat: repeat-x;
	border-color: #f89406 #f89406 #ad6704;
	border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fffbb450', endColorstr='#fff89406', GradientType=0);
	filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}
STYLE;
		$doc->addStyleDeclaration($style);

	}
}
