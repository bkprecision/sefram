<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Bkproduct View
 */
class BkcontentViewSoftware extends JViewLegacy {

	/**
	 * display method of Bkproduct
	 * @return void
	 */
	public function display($tpl = null) {

		// get the Data
		$model 				= $this->getModel();
		$this->form 		= $this->get("Form");
		$this->item 		= $this->get("Item");
		$this->item->models = $model->getModels($this->item);
		$this->modelList	= $this->get("ModelList");
		$this->software 	= $this->get("Software");
		//var_export($this->item->models);die;

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Set the toolbar
		$this->addToolBar();

		// Display the template
		parent::display($tpl);

		// Set the document
		$this->setDocument();
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar() {

		JFactory::getApplication()->input->set("hidemainmenu", TRUE);
		$isNew = ($this->item->id == 0);
		JToolBarHelper::title($isNew ? JText::_('COM_BKCONTENT_MANAGER_SOFTWARE_NEW')
									 : JText::_('COM_BKCONTENT_MANAGER_SOFTWARE_EDIT'), 'software');
		JToolBarHelper::apply('software.apply', 'JTOOLBAR_APPLY');
		JToolBarHelper::save('software.save');
        JToolBarHelper::custom('software.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
		JToolBarHelper::cancel('software.cancel', $isNew ? 'JTOOLBAR_CANCEL'
														 : 'JTOOLBAR_CLOSE');
	}

	/**
	* Method to set up the document properties
	*
	* @return void
	*/
	protected function setDocument() {

		$isNew 		= ($this->item->id == 0);
		$document 	= JFactory::getDocument();
		$document->setTitle($isNew ? JText::_('COM_BKCONTENT_SOFTWARE_CREATING')
								   : JText::_('COM_BKCONTENT_SOFTWARE_EDITING'));
	}
}