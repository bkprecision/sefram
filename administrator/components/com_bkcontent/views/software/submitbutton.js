Joomla.submitbutton = function (task) {
	if(task === '') {
		return false;
	}
	else {
		var isValid = true,
			action 	= task.split('.'),
			forms,
			i;
		
		if(action[1] !== 'cancel' && action[1] !== 'close') {
			forms = $$('form.form-validate');
			
			for(i = 0; i<forms.length; i += 1) {
				if(!document.formvalidator.isValid(forms[i])) {
					isValid = false;
					break;
				}
			}
		}
		
		if(isValid) {
			Joomla.submitform(task);
			return true;
		}
		else {
			alert(Joomla.JText._('COM_BKCONTENT_BKPRODUCT_ERROR_UNACCEPTABLE', 'Some values are unacceptable'));
			return false;
		}
	}
}