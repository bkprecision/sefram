<?php
// No direct access
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('bootstrap.framework');

?>
<script type="text/javascript" data-main="/media/com_bkcontent/js/admin/software/main.js" src="/media/com_bkcontent/js/lib/require/require.js"></script>
<script type="text/javascript">
Joomla.submitbutton = function(task) {
	if (task == 'software.cancel' || document.formvalidator.isValid(document.id('software-form'))) {
		Joomla.submitform(task, document.getElementById('software-form'));
	}
}
</script>
<form action="<?php echo JRoute::_('index.php?option=com_bkcontent&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="software-form" class="form-validate">
	<div class="form-horizontal">
		<div class="row-fluid">
			<div class="span6">
				<fieldset>
					<legend><?php echo JText::_( 'COM_BKCONTENT_SOFTWARE_DETAILS' ); ?></legend>
					<div class="control-group">
						<div class="control-label">
							<label for="jform_software_list">File Name</label>
						</div>
						<div class="controls">
							<?php if (empty($this->item->id)): ?>

							<select id="jform_software_list" name="jform[file_name]" class="span10">
								<?php foreach ($this->software as $file_name): ?>

								<option value="<?php echo $file_name; ?>" <?php echo (($file_name == $this->item->file_name) ? "selected='selected'" : ""); ?>><?php echo $file_name; ?></option>

								<?php endforeach; ?>

							</select>

							<?php else: ?>

								<input type="text" value="<?php echo $this->item->file_name; ?>" name="jform[file_name]" class="inputbox span10" readonly>
							<?php endif; ?>

						</div>
					</div>
					<?php
						echo $this->form->getControlGroup("description");
						echo $this->form->getControlGroup("version");
					?>
					<div class="control-group">
						<div class="control-label">
							<label for="jform_models_list">Models List</label>
						</div>
						<div class="controls">
							<select id="jform_models_list" name="newSoftwareCross" class="span10">
								<option>Select a new model cross</option>
								<?php foreach ($this->modelList as $model): ?>

								<option value="<?php echo $model; ?>"><?php echo $model; ?></option>

								<?php endforeach; ?>

							</select>
						</div>

					</div>
					<?php foreach ($this->item->models as $model): ?>
					<div class="control-group">
						<div class="controls">

							<input class="inputbox span9" type="text" value="<?php echo $model; ?>" name="jform[modelList][]" readonly>
							<button class="btn btn-mini btn-danger remove-software-model">Remove</button>
						</div>
					</div>
					<?php endforeach; ?>
				</fieldset>
			</div>
			<div class="span6">
				<fieldset>
					<legend><?php echo JText::_( 'COM_BKCONTENT_SOFTWARE_INFORMATION' ); ?></legend>
					<?php foreach($this->form->getFieldset('information') as $field): ?>

						<div class="control-group">
							<div class="control-label">
								<?php echo $field->label; ?>

							</div>
							<div class="controls">
								<?php echo $field->input; ?>

							</div>
						</div>
					<?php endforeach; ?>
				</fieldset>
			</div>
		</div>
	</div>
	<input type="hidden" name="task" value="software.edit">
	<input type="hidden" name="jform[oldVersion]" value="<?php echo $this->item->version; ?>">
	<?php
		echo $this->form->getControlGroup('id');
		echo $this->form->getControlGroup('invt_id');
		echo JHtml::_('form.token');
	?>
</form>