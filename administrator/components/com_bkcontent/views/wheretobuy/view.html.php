<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Bkproduct View
 */
class BkcontentViewWheretobuy extends JViewLegacy {

	/**
	* View form
	*
	* @var		form
	*/
	protected $form = null;

	/**
	 * display method of Bkproduct
	 * @return void
	 */
	public function display($tpl = null) {
		// get the Data
		$form 				= $this->get('Form');
		$item 				= $this->get('Item');
		$script 			= $this->get('Script');
 		$itemWheretobuys 	= $this->get('ItemWheretobuys');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Assign the Data
		$this->form 				= $form;
		$this->item 				= $item;
		$this->item->wheretobuys 	= $itemWheretobuys;
		$this->script 				= $script;

		// Set the toolbar
		$this->addToolBar();

		// Display the template
		parent::display($tpl);

		// Set the document
		$this->setDocument();
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar() {

		JFactory::getApplication()->input->set("hidemainmenu", TRUE);
		$isNew = ($this->item->id == 0);
		JToolBarHelper::title($isNew 	? JText::_('COM_BKCONTENT_MANAGER_WHERETOBUY_NEW')
										: JText::_('COM_BKCONTENT_MANAGER_WHERETOBUY_EDIT'), 'wheretobuy');
		JToolBarHelper::apply('wheretobuy.apply', 'JTOOLBAR_APPLY');

		JToolBarHelper::save('wheretobuy.save');
		JToolBarHelper::cancel('wheretobuy.cancel', $isNew 	? 'JTOOLBAR_CANCEL'
															: 'JTOOLBAR_CLOSE');
	}

	/**
	* Method to set up the document properties
	*
	* @return void
	*/
	protected function setDocument() {

		$isNew 		= ($this->item->id < 1);
		$document 	= JFactory::getDocument();
		$document->setTitle($isNew 	? JText::_('COM_BKCONTENT_WHERETOBUY_CREATING')
									: JText::_('COM_BKCONTENT_WHERETOBUY_EDITING'));
		$document->addScript(JURI::root() . $this->script);
		$document->addScript(JURI::root() . "/administrator/components/com_bkcontent/views/bkproduct/submitbutton.js");
		JText::script('COM_BKCONTENT_BKPRODUCT_ERROR_UNACCEPTABLE');
	}
}