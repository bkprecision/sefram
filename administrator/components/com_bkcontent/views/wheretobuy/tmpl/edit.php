<?php
// No direct access
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('bootstrap.framework');
?>

<script type="text/javascript">
Joomla.submitbutton = function(task) {
	if (document.formvalidator.isValid(document.id('wheretobuy-form'))) {
		Joomla.submitform(task, document.getElementById('wheretobuy-form'));
	}
}
</script>
<form action="<?php echo JRoute::_('index.php?option=com_bkcontent&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="wheretobuy-form" class="form-validate">
	<div class="form-horizontal">
		<div class="row-fluid">
			<div class="span6 offset3">
				<fieldset>
					<legend><?php echo JText::_( 'COM_BKCONTENT_WHERETOBUY_DETAILS' ); ?></legend>
					<?php foreach($this->form->getFieldset('details') as $field): ?>

						<div class="control-group">
							<div class="control-label">
								<?php echo $field->label; ?>

							</div>
							<div class="controls">
								<?php echo $field->input; ?>

							</div>
						</div>
					<?php endforeach; ?>

				</fieldset>
			</div>
		</div>
	</div>
	<input type="hidden" name="task" value="wheretobuy.edit">
	<?php
		echo $this->form->getControlGroup('id');
		echo JHtml::_('form.token');
	?>
</form>