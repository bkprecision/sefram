<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

// load tooltip behavior
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));

$sortFields = $this->getSortFields();
?>

<form action="<?php echo JRoute::_('index.php?option=com_bkcontent&view=wheretobuys');?>" method="post" name="adminForm" id="adminForm">
<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
		<?php
		// Search tools bar
		echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
		?>
		<?php if (empty($this->items)): ?>
			<div class="alert alert-no-items">
				<?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
			</div>
		<?php else: ?>

	<table class="table table-striped" id="productList">
		<thead>
			<tr>
				<th width="1%" class="hidden-phone">
					&nbsp;
				</th>
				<th>
					<?php echo JHtml::_('searchtools.sort', 'Company Name', 'name', $listDirn, $listOrder); ?>
				</th>
				<th>
					<?php echo JHtml::_('searchtools.sort',  'Country', 'country', $listDirn, $listOrder); ?>
				</th>
				<th>
					<?php echo JHtml::_('searchtools.sort',  'City', 'city', $listDirn, $listOrder); ?>
				</th>
				<th>
					Phone
				</th>
				<th>
					<?php echo JHtml::_('searchtools.sort',  'Email', 'email', $listDirn, $listOrder); ?>
				</th>
				<th width="3%">
					<?php echo JText::_('COM_BKCONTENT_BKPRODUCTS_HEADING_ID'); ?>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($this->items as $i => $item): ?>
				<tr class="row<?php echo $i % 2; ?>">
					<td class="center hidden-phone">
						<?php echo JHtml::_('grid.id', $i, $item->id); ?>
					</td>
					<td>
						<a href="<?php echo JRoute::_('index.php?option=com_bkcontent&task=wheretobuy.edit&id=' . $item->id); ?>">
							<?php echo $item->name; ?>
						</a>
					</td>
					<td>
						<a href="<?php echo JRoute::_('index.php?option=com_bkcontent&task=wheretobuy.edit&id=' . $item->id); ?>">
							<?php echo $item->country; ?>
						</a>
					</td>
					<td>
						<a href="<?php echo JRoute::_('index.php?option=com_bkcontent&task=wheretobuy.edit&id=' . $item->id); ?>">
							<?php echo $item->city; ?>
						</a>
					</td>
					<td>
						<a href="<?php echo JRoute::_('index.php?option=com_bkcontent&task=wheretobuy.edit&id=' . $item->id); ?>">
							<?php echo $item->phone; ?>
						</a>
					</td>
					<td>
						<a href="<?php echo JRoute::_('index.php?option=com_bkcontent&task=wheretobuy.edit&id=' . $item->id); ?>">
							<?php echo $item->email; ?>
						</a>
					</td>
					<td class="center">
						<?php echo $item->id; ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<?php endif; ?>
	<?php echo $this->pagination->getListFooter(); ?>


		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>