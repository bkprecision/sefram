<?php
// No direct access
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task) {
		if (task == 'download.cancel' || document.formvalidator.isValid(document.id('software-form'))) {
			Joomla.submitform(task, document.getElementById('software-form'));
		}
	}
</script>
<form action="<?php echo JRoute::_('index.php?option=com_bkcontent&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="software-form" class="form-validate">
	<div class="width-100 fltlft">
		<fieldset class="adminform">
			<legend><?php echo JText::_( 'COM_BKCONTENT_SOFTWARE_DETAILS' ); ?></legend>
			<ul class="adminformlist">
			<?php foreach($this->form->getFieldset('name') as $field): ?>
				<li><?php echo $field->label; echo $field->input; ?></li>
			<?php endforeach; ?>
			</ul>
		</fieldset>
	</div>
	<div class="width-100 fltlft">
		<fieldset class="adminform">
			<ul class="adminformlist">
			<?php foreach($this->form->getFieldset('description') as $field): ?>
				<li><?php echo $field->label; echo $field->input; ?></li>
			<?php endforeach; ?>
			</ul>
		</fieldset>
	</div>
	<div>
		<input type="hidden" name="task" value="download.edit" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>