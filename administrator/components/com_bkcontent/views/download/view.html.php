<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Bkproduct View
 */
class BkcontentViewDownload extends JViewLegacy {
	/**
	* View form
	*
	* @var		form
	*/
	protected $form = null;

	/**
	 * display method of Bkproduct
	 * @return void
	 */
	public function display($tpl = null) {
		// get the Data
		$form 	= $this->get('Form');
		$item 	= $this->get('Item');
		$script = $this->get('Script');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Assign the Data
		$this->form 	= $form;
		$this->item 	= $item;
		$this->script 	= $script;

		// Set the toolbar
		$this->addToolBar();

		// Display the template
		parent::display($tpl);

		// Set the document
		$this->setDocument();
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar() {

		JFactory::getApplication()->input->set("hidemainmenu", TRUE);
		$isNew = ((int)$this->item->id < 1);
		$canDo = BkcontentHelper::getActions($this->item->id);
		JToolBarHelper::title($isNew 	? JText::_('COM_BKCONTENT_MANAGER_SOFTWARE_NEW')
										: JText::_('COM_BKCONTENT_MANAGER_SOFTWARE_EDIT'), 'download');
		JToolBarHelper::apply('download.apply', 'JTOOLBAR_APPLY');

		JToolBarHelper::save('download.save');
		JToolBarHelper::save2new('download.save2new');
		if ($canDo->get('core.create')) {
                 JToolBarHelper::custom('bkproduct.save2copy', 'save-copy.png',
                         'save-copy_f2.png', 'JTOOLBAR_SAVE_AS_COPY', false);
        }
		JToolBarHelper::cancel('download.cancel', $isNew 	? 'JTOOLBAR_CANCEL'
															: 'JTOOLBAR_CLOSE');
	}

	/**
	* Method to set up the document properties
	*
	* @return void
	*/
	protected function setDocument() {

		$isNew 		= ((int)$this->item->id < 1);
		$document 	= JFactory::getDocument();
		$document->setTitle($isNew 	? JText::_('COM_BKCONTENT_SOFTWARE_CREATING')
									: JText::_('COM_BKCONTENT_SOFTWARE_EDITING'));
		JText::script('COM_BKCONTENT_BKPRODUCT_ERROR_UNACCEPTABLE');
	}
}