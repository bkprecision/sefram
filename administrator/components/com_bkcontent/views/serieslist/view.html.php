<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Bkproduct View
 */
class BkcontentViewSerieslist extends JViewLegacy {

	/**
	 * display method of Bkproduct
	 * @return void
	 */
	public function display($tpl = null) {

		// get the Data
		$this->form 		  = $this->get("Form");
		$this->item 		  = $this->get("Item");
		$this->item->products = $this->get("Products");

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Set the toolbar
		$this->addToolBar();

		// Set the document
		$this->setDocument();

		// Display the template
		parent::display($tpl);
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar() {

		JFactory::getApplication()->input->set("hidemainmenu", TRUE);
		$isNew = ($this->item->id == 0);
		JToolBarHelper::title($isNew 	? JText::_('COM_BKCONTENT_MANAGER_SERIESLIST_NEW')
										: JText::_('COM_BKCONTENT_MANAGER_SERIESLIST_EDIT'), 'serieslist');
		JToolBarHelper::apply('serieslist.apply', 'JTOOLBAR_APPLY');
		JToolBarHelper::save('serieslist.save');
		JToolBarHelper::custom('serieslist.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
		JToolBarHelper::cancel('serieslist.cancel', $isNew 	? 'JTOOLBAR_CANCEL'
															: 'JTOOLBAR_CLOSE');
	}

	/**
	* Method to set up the document properties
	*
	* @return void
	*/
	protected function setDocument() {
		$isNew 		= ($this->item->id == 0);
		$document 	= JFactory::getDocument();
		$document->setTitle($isNew 	? JText::_('COM_BKCONTENT_SERIESLIST_CREATING')
									: JText::_('COM_BKCONTENT_SERIESLIST_EDITING') . " " . $this->item->name);
	}
}