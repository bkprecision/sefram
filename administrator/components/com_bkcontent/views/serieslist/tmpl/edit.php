<?php
// No direct access
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('bootstrap.framework');

$app = JFactory::getApplication();
$input = $app->input;

$invtIds = array_map(function ($product) { return $product->invt_id; }, $this->item->products);
?>
<script type="text/javascript">
var BK = BK || {};
BK.products = ["<?php echo implode('","', $invtIds); ?>"];
</script>
<script type="text/javascript" data-main="/media/com_bkcontent/js/admin/series/main.js" src="/media/com_bkcontent/js/lib/require/require.js"></script>
<script type="text/javascript">
	Joomla.submitbutton = function(task) {
		if (task == 'serieslist.cancel' || document.formvalidator.isValid(document.id('series-form'))) {
			<?php echo $this->form->getField('description')->save(); ?>
			Joomla.submitform(task, document.getElementById('series-form'));
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_bkcontent&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="series-form" class="form-validate">

	<div class="form-inline form-inline-header">
	<?php
	echo $this->form->getControlGroup('name');
	echo $this->form->getControlGroup('title');
	echo $this->form->getControlGroup('alias');
	?>
	</div>

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_BKCONTENT_SERIESLIST_DETAILS', true)); ?>

			<div class="row-fluid">
				<div class="span3">
					<fieldset class="form-vertical">
						<legend>Models</legend>
						<?php foreach ($this->item->products as $product) : ?>
							<div class="control-group">
								<div class="control-label">
									<?php echo $product->invt_id; ?>
								</div>
							</div>
						<?php endforeach; ?>
					</fieldset>
				</div>
				<div class="span9">
					<fieldset class="adminform">
						<?php echo $this->form->getLabel('description') . $this->form->getInput('description'); ?>
					</fieldset>
				</div>
			</div>

		<?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'attributes', JText::_('COM_BKCONTENT_SERIESLIST_ATTRIBS', true)); ?>
		<div class="row-fluid">
			<?php
			if (isset($this->item->params["attribs"])):

			foreach($this->item->params["attribs"] as $title => $models): ?>

				<div class="span3 attribute-container">
					<fieldset class="form-vertical">
						<div class="control-group">
							<div class="control-label">
								<label>Attribute Title</label>
							</div>
							<div class="controls">
								<input type="text" name="jform[attribs][<?php echo $title; ?>]" value="<?php echo $title; ?>" class="attrib-title">
							</div>
						</div>
						<?php foreach($models as $model => $value): ?>

						<div class="control-group">
							<div class="control-label">
								<label><?php echo $model; ?></label>
							</div>
							<div class="controls">
								<input type="text" name="jform[attribs][<?php echo $title; ?>][<?php echo $model; ?>]" value="<?php echo $value; ?>">
							</div>
						</div>
						<?php endforeach; ?>

						<div class="control-group">
							<div class="controls">
								<button class="btn btn-danger attrib-remove" type="button">Remove Attribute</button>
							</div>
						</div>
					</fieldset>
				</div>
			<?php endforeach; endif; ?>

			<div class="span3 attribute-container">
				<fieldset class="form-vertical">
					<div class="control-group">
						<div class="control-label">
							<label>Attribute Title</label>
						</div>
						<div class="controls">
							<input name="jform[attribs][title][]" type="text" class="attrib-title">
						</div>
					</div>
					<?php foreach($this->item->products as $product): ?>

					<div class="control-group">
						<div class="control-label">
							<label><?php echo $product->invt_id; ?></label>
						</div>
						<div class="controls">
							<input type="text">
						</div>
					</div>
					<?php endforeach; ?>

				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'information-sliders-'.$this->item->id, JText::_('COM_BKCONTENT_BKPRODUCT_INFORMATION', true)); ?>
			<div class="row-fluid form-horizontal-desktop">
				<?php foreach ($this->form->getFieldset('information') as $field) : ?>
					<div class="control-group">
						<div class="control-label">
							<?php echo $field->label; ?>
						</div>
						<div class="controls">
							<?php echo $field->input; ?>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>


		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value="serieslist.edit" />
		<?php
			echo $this->form->getControlGroup('id');
			echo JHtml::_('form.token');
		?>
	</div>
</form>