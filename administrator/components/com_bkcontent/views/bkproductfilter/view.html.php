<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Bkproductfilter View
 */
class BkcontentViewBkproductfilter extends JViewLegacy {
	/**
	* View form
	*
	* @var		form
	*/
	protected $form = null;

	/**
	 * display method of Bkproductfilter
	 * @return void
	 */
	public function display($tpl = null) {
		// get the Data
		$this->form 			= $this->get('Form');
		$this->item 			= $this->get('Item');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Set the toolbar
		$this->addToolBar();

		// Display the template
		parent::display($tpl);

		// Set the document
		$this->setDocument();
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar() {
		JFactory::getApplication()->input->set("hidemainmenu", TRUE);
		$isNew = ($this->item->id == 0);
		JToolBarHelper::title($isNew ? JText::_('COM_BKCONTENT_MANAGER_BKPRODUCTFILTER_NEW')
									 : JText::_('COM_BKCONTENT_MANAGER_BKPRODUCTFILTER_EDIT'), 'bkproductfilter');

		JToolBarHelper::apply('bkproductfilter.apply', 'JTOOLBAR_APPLY');
		JToolBarHelper::save('bkproductfilter.save', 'JTOOLBAR_SAVE');
		JToolBarHelper::custom('bkproductfilter.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);

		if ($isNew) {
			JToolBarHelper::cancel('bkproductfilter.cancel', 'JTOOLBAR_CANCEL');
		}
		else {
			JToolBarHelper::cancel('bkproductfilter.cancel', 'JTOOLBAR_CLOSE');
		}
	}

	/**
	* Method to set up the document properties
	*
	* @return void
	*/
	protected function setDocument() {
		$isNew 		= ($this->item->id < 1);
		$document 	= JFactory::getDocument();
		$document->setTitle($isNew 	? JText::_('COM_BKCONTENT_BKPRODUCTFILTER_CREATING')
									: JText::_('COM_BKCONTENT_BKPRODUCTFILTER_EDITING'));
	}
}