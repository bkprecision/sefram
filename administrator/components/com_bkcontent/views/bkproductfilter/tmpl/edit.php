<?php
// No direct access
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('bootstrap.framework');
?>

<script type="text/javascript">
	Joomla.submitbutton = function(task) {
		if (task == 'article.cancel' || document.formvalidator.isValid(document.id('filter-form'))) {
			Joomla.submitform(task, document.getElementById('filter-form'));
		}
	}
</script>
<form action="<?php echo JRoute::_('index.php?option=com_bkcontent&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="filter-form" class="form-validate">
	<fieldset>
		<legend><?php echo JText::_( 'COM_BKCONTENT_BKPRODUCTFILTER_DETAILS' ); ?></legend>
		<div class="form-inline form-inline-header">
		<?php
		echo $this->form->getControlGroup('name');
		echo $this->form->getControlGroup('title');
		?>

		</div>
		<input type="hidden" name="task" value="bkproductfilter.edit" />
		<?php
			echo $this->form->getControlGroup('id');
			echo JHtml::_('form.token');
		?>
	</fieldset>
</form>