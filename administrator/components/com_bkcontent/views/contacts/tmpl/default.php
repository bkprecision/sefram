<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
// load tooltip behavior

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

$doc = JFactory::getDocument();
$doc->addScript( '/media/system/js/jquery-1.7.2.min.js');
$doc->addScript( '/media/system/js/jquery-ui-1.8.18.min.js');

$titleText 			= JText::_('COM_BKCONTENT_CONTACT_TITLE_LABEL');
$timestampLabelText	= JText::_('COM_BKCONTENTN_TIMESTAMP_LABEL');
$emailLabelText 	= JText::_('COM_CONTACT_EMAIL_LABEL');
$nameLabelText 		= JText::_('COM_CONTACT_CONTACT_EMAIL_NAME_LABEL');
$subjectLabelText 	= JText::_('COM_CONTACT_CONTACT_MESSAGE_SUBJECT_LABEL');
$messageLabelText 	= JText::_('COM_CONTACT_CONTACT_ENTER_MESSAGE_LABEL');

$script = <<<EOT
jQuery(document).ready(function ($) {
	
	var dialog 	= $( document.createElement("DIV") )
							.dialog({
								modal	:true,
								autoOpen:false,
								width	:620,
								buttons	:{
								
										Ok	: function() { 
												$( this ).dialog( "close" );
												}
										}
							}),
		list		= $( document.createElement("UL") ),
		listItem 	= $( document.createElement("LI") );
	
	$('.contact-details')
		.on('click', function (e) {
			e.preventDefault();
			
			$.getJSON(this.href, function (contact) {
				
				var details 	= list.clone(),
					name 		= listItem.clone().text("$nameLabelText: " + contact.name).appendTo(details),
					email 		= listItem.clone().text("$emailLabelText: " + contact.email).appendTo(details),
					subject 	= listItem.clone().text("$subjectLabelText: " + contact.subject).appendTo(details),
					timestamp 	= listItem.clone().text("$timestampLabelText: " + contact.timestamp).appendTo(details),
					message 	= listItem.clone().append("$messageLabelText: " + contact.message).appendTo(details);
					
				dialog
					.dialog( "option", "title", "$titleText " + contact.name )
					.html(details)
					.dialog( "open" );
				
			});
			
		});
		
});
EOT;
$doc->addScriptDeclaration($script);
$doc->addStyleSheet('/media/system/css/jquery-ui-1.8.18.css');
?>
<form action="<?php echo JRoute::_('index.php?option=com_bkcontent&view=contacts'); ?>" method="post" name="adminForm" id="adminForm">
<table class="adminlist">
	<thead>
		<tr>
			<th width="20" class="nowrap">
				<?php echo JText::_('id'); ?>
			</th>			
			<th class="nowrap">
				<?php echo JText::_('Name'); ?>
			</th>
			<th class="nowrap">
				<?php echo JText::_('Email'); ?>
			</th>
			<th class="nowrap">
				<?php echo JText::_('Subject'); ?>
			</th>
			<th class="nowrap">
				<?php echo JText::_('Timestamp'); ?>
			</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="5"><?php echo $this->pagination->getListFooter(); ?></td>
		</tr>
	</tfoot>
	<tbody>
		<?php foreach($this->items as $i => $item): ?>
			<tr class="row<?php echo $i % 2; ?>">
				<td class="nowrap" width="5%">
					<center><?php echo $item->id; ?></center>
				</td>
				<td width="45%" class="nowrap">
					<a class="contact-details" href="<?php echo JRoute::_('index.php?format=json&option=com_bkcontent&view=contact&id=' . $item->id); ?>">
						<?php echo $item->name; ?>
					</a>
				</td>
				<td>
					<?php echo $item->email; ?>
				</td>
				<td>
					<?php echo $item->subject; ?>
				</td>
				<td width="13%">
					<?php echo date('Y-m-d h:i:s',	strtotime($item->timestamp)); ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<div>
                <input type="hidden" name="task" value="" />
                <input type="hidden" name="boxchecked" value="0" />
                <?php echo JHtml::_('form.token'); ?>
        </div>
</form>
