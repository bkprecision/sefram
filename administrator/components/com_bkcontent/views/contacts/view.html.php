<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Bkcontent View
 */
class BkcontentViewContacts extends JViewLegacy {
	/**
	 * Bkcontent view display method
	 * @return void
	 */
	function display($tpl = null) {

		BkcontentHelper::addSubmenu('bkproducts');

		// Get data from the model
		$items 		= $this->get('Items');
		$pagination = $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		// Assign data to the view
		$this->items 		= $items;
		$this->pagination 	= $pagination;

		// Set the toolbar
		$this->addToolBar();

		// Display the template
		parent::display($tpl);

		// Set the document
		$this->setDocument();
	}

	/**
	* Setting the toolbar
	*/
	protected function addToolBar() {
		JToolBarHelper::title(JText::_('COM_BKCONTENT_MANAGER_CONTACTS'), 'Contact Us Requests');
		JToolBarHelper::preferences('com_bkcontent');
	}


	/**
	* Method to set up the document properties
	*
	* @return void
	*/
	protected function setDocument() {
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_BKCONTENT_ADMINISTRATION'));
	}

}