<?php
// No direct access
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('bootstrap.framework');

$app = JFactory::getApplication();
$input = $app->input;
?>
<script type="text/javascript" data-main="/media/com_bkcontent/js/admin/accessory/main.js" src="/media/com_bkcontent/js/lib/require/require.js"></script>
<script type="text/javascript">
	Joomla.submitbutton = function(task) {
		if (task == 'accessorieslist.cancel' || document.formvalidator.isValid(document.id('accessory-form'))) {
			Joomla.submitform(task, document.getElementById('accessory-form'));
		}
	}
</script>
<form action="<?php echo JRoute::_('index.php?option=com_bkcontent&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="accessory-form" class="form-validate">

	<div class="form-inline form-inline-header">
	<?php
	echo $this->form->getControlGroup('invt_id');
	echo $this->form->getControlGroup('title');
	echo $this->form->getControlGroup('alias');
	?>
	</div>

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'overview')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'overview', JText::_('COM_BKCONTENT_ACCESSORIESLIST_DETAILS', true)); ?>

			<div class="row-fluid">
				<div class="span3">
					<fieldset class="form-vertical">
						<?php foreach ($this->form->getFieldset('details') as $field) : ?>

							<div class="control-group">
								<div class="control-label">
									<?php echo $field->label; ?>

								</div>
								<div class="controls">
									<?php echo $field->input; ?>

								</div>
							</div>
						<?php endforeach; ?>
					</fieldset>
					<?php if (!empty($this->item->crosses)): ?>
					<fieldset class="form-vertical">
						<legend><?php echo JText::_( 'COM_BKCONTENT_ACCESSORIES_CROSS' ); ?></legend>
						<?php foreach($this->item->crosses as $itemCross): ?>

						<div class="control-group">
							<div class="control-label">
								<?php if ($itemCross->type == "cat_id"): ?>
								Category
								<?php elseif ($itemCross->type == "invt_id"): ?>
								Model
								<?php elseif ($itemCross->type == "series"): ?>
								Series
								<?php endif; ?>
							</div>
							<div class="controls">
								<select id="<?= $itemCross->type; ?>_tmpl_<?= $itemCross->xref_id; ?>" class="inputbox required" name="jform[acc_xref][<?= $itemCross->type; ?>][]">
								<?php if ($itemCross->type == "cat_id"): ?>
									<?php foreach($this->form->categories as $category): ?>

									<option value="<?= $category->id; ?>" <?= ($category->id == $itemCross->xref_id) ? "selected='selected'" : ""; ?>><?= $category->title; ?></option>
									<?php endforeach; ?>
								<?php elseif ($itemCross->type == "invt_id"): ?>
									<?php foreach($this->form->products as $product): ?>

									<option value="<?= $product->id; ?>" <?= ($product->id == $itemCross->xref_id) ? "selected='selected'" : ""; ?>><?= $product->invt_id . " " . $product->title; ?></option>
									<?php endforeach; ?>
								<?php elseif ($itemCross->type == "series"): ?>
									<?php foreach($this->form->series as $series): ?>

									<option value="<?= $series->id; ?>" <?= ($series->id == $itemCross->xref_id) ? "selected='selected'" : ""; ?>><?= $series->name; ?></option>
									<?php endforeach; ?>
								<?php endif; ?>
								</select>
								<button type="button" class="btn btn-remove-item-cross"><i class="icon-remove item-cross"></i></button>

							</div>
						</div>
						<?php endforeach; ?>

					</fieldset>
					<?php endif; ?>
					<fieldset class="form-vertical">
					<legend><?php echo JText::_( 'COM_BKCONTENT_ACCESSORIESLIST_TYPE' ); ?></legend>
						<div class="control-group">
							<div class="control-label">
								<label for="crossType">Type</label>
							</div>
							<div class="controls">
								<select id="crossType" class="inputbox required" name="crossType" />
									<option value="0">- Select Type -</option>
									<option value="cat_id">Category</option>
									<option value="invt_id">Model</option>
									<option value="series">Series</option>
								</select>
							</div>
						</div>
						<? if ($this->form->series): ?>

						<div class="control-group hide">
							<div class="control-label">
								<label for="series_tmpl">Series Value</label>
							</div>
							<div class="controls">
								<select id="series_tmpl" class="inputbox required" name="jform[acc_xref][series][]" />
									<option value="0">- Select Series -</option>
								<? foreach($this->form->series as $series): ?>

									<option value="<?=$series->id?>"><?=$series->name?></option>
								<? endforeach; ?>

								</select>
							</div>
						</div>
						<? endif; ?>

						<? if ($this->form->products): ?>

						<div class="control-group hide">
							<div class="control-label">
								<label for="invt_id_tmpl">Model #</label>
							</div>
							<div class="controls">
								<select id="invt_id_tmpl" class="inputbox required" name="jform[acc_xref][invt_id][]" />
									<option value="0">- Select Model # -</option>
								<? foreach($this->form->products as $product): ?>

									<option value="<?=$product->id?>"><?=$product->invt_id.' '.$product->title?></option>
								<? endforeach; ?>

								</select>
							</div>
						</div>
						<? endif; ?>

						<? if ($this->form->categories): ?>

						<div class="control-group hide">
							<div class="control-label">
								<label for="cat_id_tmpl">Categories</label>
							</div>
							<div class="controls">
								<select id="cat_id_tmpl" class="inputbox required" name="jform[acc_xref][cat_id][]" />
									<option value="0">- Select Category -</option>
								<? foreach($this->form->categories as $category): ?>

									<option value="<?=$category->id?>"><?=$category->title?></option>
								<? endforeach; ?>

								</select>
							</div>
						</div>
						<? endif; ?>

					</fieldset>
				</div>
				<div class="span9">
					<fieldset class="adminform">
						<?php echo $this->form->getLabel('description') . $this->form->getInput('description'); ?>
					</fieldset>
				</div>
			</div>

		<?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'information-'.$this->item->id, JText::_('COM_BKCONTENT_ACCESSORIESLIST_INFORMATION', true)); ?>
			<div class="row-fluid form-horizontal-desktop">
				<fieldset class="form-vertical">
					<?php foreach ($this->form->getFieldset('information') as $field) : ?>

						<div class="control-group">
							<div class="control-label">
								<?php echo $field->label; ?>

							</div>
							<div class="controls">
								<?php echo $field->input; ?>

							</div>
						</div>
					<?php endforeach; ?>

				</fieldset>
			</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="return" value="<?php echo $input->getCmd('return'); ?>" />
		<?php
			echo $this->form->getControlGroup('id');
			echo JHtml::_('form.token');
		?>

	</div>
</form>