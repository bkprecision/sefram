<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Bkcontent View
 */
class BkcontentViewBkproductfilters extends JViewLegacy {
	/**
	 * Bkcontent view display method
	 * @return void
	 */
	function display($tpl = null) {

		BkcontentHelper::addSubmenu("bkproductfilters");

		// Get data from the model
		// Get data from the model
		$this->state 		 = $this->get('State');
		$this->items 		 = $this->get('Items');
		$this->pagination 	 = $this->get('Pagination');
		$this->filterForm  	 = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Set the toolbar
		$this->addToolBar();
		$this->sidebar = JHtmlSidebar::render();

		// Display the template
		parent::display($tpl);

		// Set the document
		$this->setDocument();
	}

	/**
	* Setting the toolbar
	*/
	protected function addToolBar() {
		JToolBarHelper::title(JText::_('COM_BKCONTENT_MANAGER_BKPRODUCTFILTERS'), 'BK Product Filters');
		JToolBarHelper::deleteList('', 'bkproductfilters.delete');
		JToolBarHelper::editList('bkproductfilter.edit');
		JToolBarHelper::addNew('bkproductfilter.add');
		JToolBarHelper::preferences('com_bkcontent');
	}


	/**
	* Method to set up the document properties
	*
	* @return void
	*/
	protected function setDocument() {
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_BKCONTENT_ADMINISTRATION'));
	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields() {
		return array(
				'f.name'      => JText::_('COM_BKCONTENT_BKPRODUCTS_HEADING_INVTID'),
				'f.title'     => JText::_('JGLOBAL_TITLE')
		);
	}

}