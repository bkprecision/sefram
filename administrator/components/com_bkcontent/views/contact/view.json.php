<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Bkproduct View
 */
class BkcontentViewContact extends JViewLegacy {
	/**
	 * display method of Contact Us Request
	 * @return void
	 */
	public function display($tpl = null) {
		// get the Data
		$item 		= $this->get('Item');

		if(isset($item->message) && !empty($item->message)) {
			$item->message = nl2br($item->message);
		}

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Assign the Data
		$this->item = $item;

		$this->setLayout('json');

		// Display the template
		parent::display($tpl);
	}
}