<?php
// No direct access
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('bootstrap.framework');

$invt_ids = array_filter(explode(",", $this->form->getValue("invt_ids")));
?>
<script type="text/javascript">
	var BKP = BKP || {};
	BKP.models = [<?php echo "'".implode("','", $this->models)."'"; ?>];
</script>
<script type="text/javascript" data-main="/media/com_bkcontent/js/admin/video/main.js" src="/media/com_bkcontent/js/lib/require/require.js"></script>
<script type="text/javascript">
Joomla.submitbutton = function(task) {
	if (task == 'video.cancel' || document.formvalidator.isValid(document.id('video-form'))) {
		Joomla.submitform(task, document.getElementById('video-form'));
	}
}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_bkcontent&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="video-form" class="form-validate">
	<div class="form-horizontal">
		<div class="row-fluid">
			<div class="span6 offset3">
				<fieldset>
					<legend><?php echo JText::_( 'COM_BKCONTENT_VIDEO_DETAILS' ); ?></legend>
						<div class="control-group">
							<div class="control-label">
								<?php echo $this->form->getLabel('playlist_id'); ?>

							</div>
							<div class="controls">
								<?php echo $this->form->getInput('playlist_id'); ?>

							</div>
						</div>
						<?php
							$key = 0;
							if (!empty($invt_ids)):
								foreach ($invt_ids as $key => $invt_id):
						?>

						<div class="control-group">
							<div class="control-label">
								<label
									id="jform_invt_ids_<?php echo $key; ?>-lbl"
									for=""
									class="required"
									title data-original-title="<strong>Model/File Name</strong><br />Model or file_name of a series."
									aria-invalid="false">Model/File Name</label>
							</div>
							<div class="controls">
								<input
									type="text"
									name="jform[invt_ids][]"
									id="jform_invt_ids_<?php echo $key; ?>"
									value="<?php echo $invt_id; ?>"
									class="required"
									required="required"
									aria-required="true"
									aria-invalid="false">
								<button type="button" class="btn btn-danger remove-invt-id">X</button>
							</div>
						</div>
						<?php
								endforeach;
							endif;
						?>

						<div class="control-group">
							<div class="control-label">
								<?php if (!empty($invt_ids)): ?>

								<label
									id="jform_invt_ids_<?php echo ++$key; ?>-lbl"
									for="jform_invt_ids_<?php echo $key; ?>"
									title
									aria-invalid="false">New Link</label>
								<?php else: ?>

								<label
									id="jform_invt_ids_<?php echo $key; ?>-lbl"
									for="jform_invt_ids_<?php echo $key; ?>"
									class="required inputbox typeahead"
									title data-original-title="<strong>Model/File Name</strong><br />Model or file_name of a series."
									aria-invalid="false">Model/File Name</label>
								<?php endif; ?>

							</div>
							<div class="controls">
								<input
									type="text"
									name="jform[invt_ids][]"
									id="jform_invt_ids_<?php echo $key; ?>"
									value=""
									class="inputbox typeahead"
									autocomplete="off"
									data-provide="typeahead"
									data-source="BKP.models"
									aria-invalid="false">
							</div>
						</div>

				</fieldset>
			</div>
		</div>
	</div>
	<input type="hidden" name="task" value="video.edit">
<?php
	echo $this->form->getControlGroup('id');
	echo JHtml::_('form.token');
?>

</form>