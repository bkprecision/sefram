<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Bkproduct View
 */
class BkcontentViewVideo extends JViewLegacy {

	protected $form;

	protected $item;

	protected $state;

	protected $products;

	protected $script;

	/**
	 * display method of Bkproduct
	 * @return void
	 */
	public function display($tpl = null) {
		// get the Data
		$this->form 				= $this->get('Form');
		$this->form->filteroptions  = $this->get('Filters');
		$this->item 				= $this->get('Item');
		$this->state				= $this->get('State');
		$this->script 				= $this->get('Script');
		$this->models				= $this->get('Models');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Set the toolbar
		$this->addToolBar();

		// Set the document
		$this->setDocument();

		// Display the template
		parent::display($tpl);
	}

	/**
	 * Setting the toolbar
	 */


	protected function addToolBar() {
		JFactory::getApplication()->input->set('hidemainmenu', true);
		$user 	= JFactory::getUser();
		$userId = $user->id;
		$isNew 	= $this->item->id == 0;
		$canDo 	= BkcontentHelper::getActions($this->item->id);
		JToolBarHelper::title($isNew ? JText::_('COM_BKCONTENT_MANAGER_BKPRODUCT_NEW')
											 : JText::_('COM_BKCONTENT_MANAGER_BKPRODUCT_EDIT'), 'video');
		// Built the actions for new and existing records.
		if ($isNew) {
					 // For new records, check the create permission.
			if ($canDo->get('core.create')) {
				JToolBarHelper::apply('video.apply', 'JTOOLBAR_APPLY');
				JToolBarHelper::save('video.save', 'JTOOLBAR_SAVE');
				JToolBarHelper::custom('video.save2new', 'save-new.png',
							 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
			}
			JToolBarHelper::cancel('video.cancel', 'JTOOLBAR_CANCEL');
		}
		else {
			if ($canDo->get('core.edit')) {
				// We can save the new record
				JToolBarHelper::apply('video.apply', 'JTOOLBAR_APPLY');
				JToolBarHelper::save('video.save', 'JTOOLBAR_SAVE');

				// We can save this record, but check the create permission to
				// see if we can return to make a new one.
				if ($canDo->get('core.create')) {
					JToolBarHelper::custom('video.save2new', 'save-new.png',
								 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
				}
			}
			if ($canDo->get('core.create')) {
				JToolBarHelper::custom('video.save2copy', 'save-copy.png',
										'save-copy_f2.png', 'JTOOLBAR_SAVE_AS_COPY', false);
			}
			JToolBarHelper::cancel('video.cancel', 'JTOOLBAR_CLOSE');
		}
	}

	/**
	* Method to set up the document properties
	*
	* @return void
	*/
	protected function setDocument() {
		$isNew	= ($this->item->id < 1);
		$doc	= JFactory::getDocument();
		$doc->setTitle($isNew ? JText::_('COM_BKCONTENT_BKPRODUCT_CREATING')
							  : JText::_('COM_BKCONTENT_BKPRODUCT_EDITING') . " " . $this->item->playlist_id);

	}
}
