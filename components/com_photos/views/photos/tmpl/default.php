<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
$document = JFactory::getDocument();

$document->_generator = null;
$size = JRequest::getVar('size');
$maxWidth = JRequest::getVar('maxWidth');
switch($size) {
	case "smallest":
		$percent = 0.1;
		break;
	case "thumb":
		$percent = 0.2;
		break;
	case "small":
		$percent = 0.4;
		break;
	case "medium":
		$percent = 0.6;
		break;
	case "large":
		$percent = 0.8;
		break;
	default:
		$percent = 1;
}

$src = JRequest::getVar('src');
$extension = preg_replace('/^.*\.(.+)$/i', '$1', $src);

if( ! file_exists( $src ) )
{
	$src = 'images/noimage.jpg';
	$extention = 'jpg';
} 

// set the mime type of the output
if(strcasecmp($extension, 'jpg') == 0 OR strcasecmp($extension, 'jpeg') == 0)
{
	$document->_mime = 'image/jpeg';
}
else if(strcasecmp($extension, 'png') == 0)
{
	$document->_mime = 'image/png';
}

list($width, $height) = getimagesize($src);

// given a maximum width in pixels, get the percent needed to properly scale the height
// new width/current width = percent
if(!empty($maxWidth) AND (int)$maxWidth < $width)
{
	$maxWidthPercent = (int)$maxWidth/$width;
	if($maxWidthPercent*$width < $width * $percent)
	{
		$percent = $maxWidthPercent;
	}
}


$new_width = $width * $percent;
$new_height = $height * $percent;
if(strcasecmp($extension, 'jpg') == 0 OR strcasecmp($extension, 'jpeg') == 0)
{
	$image = imagecreatefromjpeg($src);
}
else if(strcasecmp($extension, 'png') == 0)
{
	$image = imagecreatefrompng($src);
}
$image_new = imagecreatetruecolor($new_width, $new_height);

if(strcasecmp($extension, 'jpg') == 0 OR strcasecmp($extension, 'jpeg') == 0)
{
	imagecopyresampled($image_new, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
	imagejpeg($image_new, null, 90);
	
}
else if(strcasecmp($extension, 'png') == 0)
{
	
	// Turn off alpha blending and set alpha flag
	imagealphablending($image_new, false);
	imagesavealpha($image_new, true);
	
	$transparency = imagecolorallocatealpha($image_new, 255, 255, 255, 127);
	imagefilledrectangle($image_new, 0, 0, $nWidth, $nHeight, $transparent);
	
	imagecopyresampled($image_new, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
	
	imagepng($image_new, null, 5);
	
}
//destroy image
imagedestroy($image_new);
imagedestroy($image);