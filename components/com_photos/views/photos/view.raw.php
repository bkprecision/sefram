<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

class PhotosViewPhotos extends JView {

	function display($tpl = null) {

		// Assign data to the view
		$this->raw = new stdClass();

		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		parent::display($tpl);

	}

}