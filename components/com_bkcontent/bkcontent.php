<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

if(!defined('DS')) {
	define('DS', DIRECTORY_SEPARATOR);
}


require JPATH_LIBRARIES.'/aws/aws.phar';

// Get an instance of the controller prefixed by bkcontent
$controller = JControllerLegacy::getInstance('Bkcontent');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();