<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * BK Content Component Controller
 */
class BkcontentController extends JControllerLegacy {

	public function display($cachable = false, $urlparams = array()) {

		// get the current view
		$viewName 	= $this->input->get('view', $this->default_view);
		// execute any function for that view
		if (method_exists($this, $viewName)) {
			$this->$viewName();
		}
		// continue loading
		parent::display($cachable, $urlparams);

		return $this;
	}

	private function support() {

		$customersModel = JModelLegacy::getInstance("Customers", "BkcontentModel");
		$session	  	= JFactory::getSession();
		$this->user 	= $session->get("customer.user");
		$this->params	= JFactory::getApplication()->getParams();
		$showLogin		= (bool)$this->params->get("show_user_login");

		if (empty($this->user)) {
			$this->user = $customersModel->getUser();
			$session->set("customer.user", $this->user);
		}

		$isGuest = $this->user->guest ? TRUE : FALSE;

		if ($isGuest && $showLogin) {
			$this->input->set("layout", "login");
		}
	}

	// support login
	public function login() {

		$input 		= JFactory::getApplication()->input;
		$customerId = $input->get("customer_id", "", "string");
		$password 	= $input->get("passwd", "", "string");

		if (empty($customerId) || empty($password)) {
			$this->display(false, array());
			return;
		}

		var_export($password);die;
	}

}