<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$doc  = JFactory::getDocument();
$doc->setMetaData("data-main", "/media/com_bkcontent/js/pages/category/main.js");
$doc->addStyleSheet("/media/com_bkcontent/css/pages/category/styles.css");

$page = $this->state->get("google._escaped_fragment_");

if ( ! empty($page)) {
	$page = explode(":", $page);
	$page = $page[1];
	$doc->setTitle($doc->getTitle() . " - Page " . $page);
}

$app = JFactory::getApplication(); // get the application

// Get the category photo src
$imgUrl = $this->parent->params->get('image');

// Check for photo exists
// if it does, add it to the meta-data
if( file_exists(getcwd().DS.$imgUrl) ) {
	$doc->setMetaData('product-image', "/".$imgUrl);
}

// read more text for category description
$description = strip_tags($this->parent->description);

if (strlen($description) > 155) {

	// truncate string
	$stringCut = substr($description, 0, 155);

	// make sure it ends in a word so assassinate doesn't become ass...
	$description = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a href="#full-category-description" role="button" data-toggle="modal">' . JText::_("COM_BKCONTENT_CATEGORY_READ_MORE") . '</a>';
}
?>

<div class="row-fluid">
	<h2 class="category-title hidden"><?=$this->parent->title?></h2>
	<h1 class="hide">Sefram</h1>
</div>

<div class="row-fluid">


		<?php
		if ( !empty($this->filters) ):
			$productsClass = "span9";
			$interator = 0;
		?>

	<section class="filters span3">
		<div class="filters-list">
			<h4><?php echo JText::_("COM_BKCONTENT_CATEGORY_FILTER_SORT_OPTIONS_TITLE"); ?></h4>
			<div class="sort-options-container">
				<label><?php echo JText::_("COM_BKCONTENT_CATEGORY_FILTER_BY_TITLE"); ?></label>
			</div>
		<?php foreach ($this->filters as $filter): ?>

			<div class="accordion-group">
				<div class="accordion-heading">
					<h5>
						<a class="btn btn-link" data-toggle="collapse" data-target="#<?= $filter->name; ?>"><?= JText::_($filter->title); ?></a>
					</h5>
				</div>
				<div class="collapse accordion-body <?= ($interator > 2) ? "" : "in"; ?>" id="<?=$filter->name?>">
					<ul class="unstyled">

	<?php foreach ($filter->values as $value): ?>

							<li>
								<label class="checkbox inline category-filter" for="<?php echo $filter->name . $value; ?>">
									<input type="checkbox" id="<?php echo $filter->name . $value; ?>" name="<?php echo $filter->name; ?>" value="<?php echo $value; ?>" />
									<?php echo JText::_($value); ?>
								</label>
							</li>
	<?php endforeach; ?>

					</ul>
				</div>
			</div>
		<?php
			$interator++;
		endforeach;
		?>

		</div>
	</section><!-- end category filters -->
	<?php
		else:
			$productsClass = "span12";

		endif;

	?>

	<section class="products-container <?php echo $productsClass; ?>">

	<div class="products-list">
		<?php
			$i = 1;
			foreach($this->products as $product):

				if ($i == 1):
		?>
		<div class="row-fluid">
			<ul class="inline">

		<?php endif; ?>

		<li class="product-tile span3" itemscope itemtype="http://schema.org/Product">
			<a class="img-link" href="<?= $product->href; ?>" itemprop="url">
				<div class="intro-img-container">
					<img class="intro-img" src="<?=$product->icon?>" alt="<?=$product->title?>" itemprop="image">
				</div>
				<ul class="unstyled product-details">
					<li class="title" itemprop="description"><?= $product->title; ?></li>
					<li class="model"><strong itemprop="name"><?= JTEXT::_('COM_BK_MODEL').' '.$product->invt_id; ?></strong></li>
				</ul>
			</a>
		</li>


		<?php
				if ($i >= 4):
					$i = 1;
		?>

			</ul>
		</div>
		<?php

				else :
					$i++;

				endif;
			endforeach;
			// verify that we close out the ul and the div container
			if ($i < 4 && $i > 1) :
			?>

					</ul>
			</div>
			<?php endif; ?>
		</div><!-- end products-list -->

		<?php  $this->pagination->setAdditionalUrlParam('layout', 'filter'); ?>

			<? if($this->pagination->total > $this->limit): ?>
			<div class="pull-right pagination-limit">
				<label for="pgLimit"><?=JTEXT::_('COM_BK_PAGINATIONSHOW')?>
				<select name="limit" id="pgLimit" class="span6">
					<option value="12"<?=(($this->limit == '12') ? ' selected="selected"' : '' )?>>12</option>
					<option value="*"<?=(($this->limit == '1000') ? ' selected="selected"' : '' )?>><?=JTEXT::_('COM_BK_PAGINATIONSHOWALL')?></option>
				</select>
				</label>
			</div>
			<? endif; ?>

			<?= $this->pagination->getPagesLinks(); ?>

		<div class="loading hide">
			<div class="alert alert-error">
				<p>Loading products, please wait...</p>
			</div>
			<div class="progress progress-striped active">
			  <div class="bar"></div>
			</div>
		</div>


		<input type="hidden" id="state" name="state" value="<?= $this->state->state; ?>" />
		<input type="hidden" id="parentId" name="parentId" value="<?=$this->parent->id?>" />
		<input type="hidden" id="limitStart" name="limitStart" value="<?=$this->limitStart?>" />
		<input type="hidden" value="<?=$this->pagination->total?>" />

	</section><!-- end category products list -->
</div>

<div class="category-header-img hide">
	<img src="/images/products/categories/<?php echo $this->parent->alias; ?>.jpg" alt="<?=$this->parent->title?>" />
	<div class="title">
		<h3><?php echo $this->parent->title; ?></h3>
		<p class="category-desc hidden-phone"><?php echo $description; ?></p>
	</div>
	<div class="opacity"></div>
</div>

<div id="search-wait" class="modal hidden" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-header">
    <h3 id="myModalLabel"><?= JText::_("COM_BKCATEGORY_SEARCH_WAIT"); ?></h3>
  </div>
  <div class="modal-body">
	<div class="progress progress-info progress-striped active">
	  <div class="bar" style="width: 100%"></div>
	</div>
  </div>
</div><!-- end model template -->

<div id="full-category-description" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="full-category-title" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="full-category-title"><?php echo $this->parent->title; ?></h3>
	</div>
	<div class="modal-body">
		<?php echo $this->parent->description; ?>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo JText::_("COM_BKCONTENT_MODAL_CLOSE_TITLE"); ?></button>
	</div>
</div>

<script id="products-list-template" type="text/x-handlebars-template">
	{{#productEach products}}

	<li class="product-tile span3" itemscope itemtype="http://schema.org/Product">
		<a class="img-link" href="{{href}}" itemprop="url">
			<div class="intro-img-container">
				{{imgTile}}
			</div>
			<ul class="unstyled product-details">
				<li class="title" itemprop="description">{{title}}</li>
				<li class="model"><strong itemprop="name"><?php echo JTEXT::_('COM_BK_MODEL').' '; ?>{{invt_id}}</strong></li>
			</ul>
		</a>
	</li>


	{{/productEach}}
</script>