<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$doc = JFactory::getDocument(); //get the overall document

$imgUrl = $this->parent->params->get('image');
$doc->setMetaData('product-image', "/".$imgUrl);
?>
<h1><?=$this->parent->title?></h1>

<div class="container categories">
	<section class="leftPane width-24">
		<?php if(isset($imgUrl) AND file_exists($imgUrl)): ?>
			<img src="<?=$imgUrl?>" alt="<?=$this->parent->title?>" />
		<?php endif; ?>
		<ul class="categories subcats">
			<?php foreach($this->categories as $category): ?>
				<li>
					<a href="<?=preg_replace('/^(.+)(\/.+)\.html$/i', '$1.html#!$2',JRoute::_('index.php?view=products&cat_id='.$category->id.':'.$category->alias))?>"><span class="title"><?=$category->title?></span></a>
				</li>
			<?php endforeach; ?>
		</ul>
	</section>
	<section class="rightPane width-72">
		<div class="tabs">
			<ul>
		        <li><a href="#products"><?=JTEXT::_('COM_BK_CATPRODS')?></a></li>
		        <li><a href="#overview"><?=JTEXT::_('COM_BKPRODUCT_OVERVIEW')?></a></li>
		    </ul>
		    <div id="products">
		        <div class="loadWait hidden ui-widget-overlay ui-corner-all alignCenter"><img src="images/ajax-load.gif" border="0" alt="Please, wait..." /></div>
				<ul class="products hidden no-js">
				<?php foreach($this->products as $product): ?>
					<li>
						<ul>
							<li><a href="/<?=$product->path.'/'.$product->invt_id.'-'.$product->alias.'.html'?>"><?=$product->title?></a></li>
							<li class="modelNum"><?=JTEXT::_('COM_BK_MODEL').' '.$product->invt_id?></li>
							<li class="shortDesc"><p><?=$product->short_desc?></p></li>
						</ul>
					</li>
				<?php endforeach; ?>
				</ul>
		    </div>
			<div id="overview">
			        <?=$this->parent->description?>
			</div>
		</div>
	</section>
	<div class="clr"></div>
</div>
<script type="text/javascript">

 // Add a script element as a child of the body
bkp.loadJsOnLoad = function () {
	var element = document.createElement("script");
	element.src = "/templates/bkpre2011/js/categorySubcats.js";
	document.body.appendChild(element.cloneNode(false));
}

// Check for browser support of event handling capability
if (window.addEventListener) {
	window.addEventListener("load", bkp.loadJsOnLoad, false);
}
else if (window.attachEvent) {
	window.attachEvent("onload", bkp.loadJsOnLoad);
}
else {
	window.onload = bkp.loadJsOnLoad;
}

</script>