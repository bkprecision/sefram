<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$doc  = JFactory::getDocument();
$doc->setMetaData("data-main", "media/com_bkcontent/js/pages/products/main.js");
$doc->addStyleSheet("/media/com_bkcontent/css/pages/products/styles.css");
?>
<h2><?= $this->parent->title; ?></h2>
<h1 class="hidden">B&amp;K Precision</h1>

<div class="row-fluid">
	<div class="span9">
	<div class="category-description">
		<?php echo $this->parent->description; ?>
	</div>

<?php
// we are cylcling through each array of an array of categories.
// the model send back a "chunked" array, split into 4
foreach ($this->categories as $categories): ?>

	<ul class="inline product-category-list <? echo $this->state->params->get("pageclass_sfx"); ?>">
		<?php foreach ($categories as $category): ?>

			<li class="<?php echo $category->class; ?> span3">
				<a href="<?php echo $category->link; ?>">
					<img src="<?php echo $category->image; ?>" />
					<div class="title">
						<h4><?php echo $category->title; ?></h4>
					</div>
					<div class="opacity"></div>
				</a>
			</li>
		<?php endforeach;?>

	</ul>
<?php endforeach;?>

	</div>
<?php if (!empty($this->newProducts)): ?>
	<div class="span3 new-products-list">
		<h4><?php echo JText::_("COM_BKCONTENT_CATEGORY_NEW_PRODUCTS_TITLE"); ?></h4>
		<ul class="unstyled">
			<?php
				foreach($this->newProducts as $product):
					$product->invt_id = str_replace("BK", "", $product->invt_id);
			?>

			<li class="row-fluid">
				<a href="/<?php echo $product->link; ?>.html">
					<div class="product-photo span4">
						<img src="https://bkpmedia.s3.amazonaws.com/icons/<?php echo $product->invt_id; ?>.gif" />
					</div>
					<div class="product-details span8">

					<?php if ($product->series > 0): ?>

						<h6><?php echo $product->name; ?></h6>
						<p><?php echo $product->seriesTitle; ?></p>
					<?php else: ?>

						<h6><?php echo $product->invt_id; ?></h6>
						<p><?php echo $product->productTitle; ?></p>
					<?php endif; ?>

					</div>
				</a>
			</li>
			<?php endforeach; ?>

		</ul>
	</div>
<?php endif; ?>
</div>