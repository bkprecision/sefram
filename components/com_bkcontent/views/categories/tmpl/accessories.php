<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$doc  = JFactory::getDocument();
$doc->setMetaData("data-main", "/media/com_bkcontent/js/pages/category/accessories.js");
$doc->addStyleSheet("/media/com_bkcontent/css/pages/category/styles.css");
$doc->addStyleSheet("/media/com_bkcontent/css/pages/category/accessories.css");

$page = $this->state->get("google._escaped_fragment_");

if ( ! empty($page)) {
	$page = explode(":", $page);
	$page = $page[1];
	$doc->setTitle($doc->getTitle() . " - Page " . $page);
}

// read more text for category description
$description = strip_tags($this->parent->description);

if (strlen($description) > 155) {

	// truncate string
	$stringCut = substr($description, 0, 155);

	// make sure it ends in a word so assassinate doesn't become ass...
	$description = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a href="#full-category-description" role="button" data-toggle="modal">Read More</a>';
}
?>
<div class="row-fluid">
	<h1 class="hide"><?=$this->parent->title?></h1>
</div>

<div class="row-fluid">
	<section class="filters span3">
		<div class="filters-list">
			<h4>Filter Options</h4>
			<div class="sort-options-container">
				<select id="sort-options" name="subcategory">
					<option value="<?php echo $this->parent->id; ?>">--Select a Category--</option>
					<?php foreach ($this->categories as $category): ?>

					<option value="<?php echo $category->id; ?>"><?php echo $category->title; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
	</section>
	<section class="span9 products-container">
		<div class="products-list">
	<?php
			$i = 1;
			foreach($this->accessories as $accessory):

				if ($i == 1):
		?>
		<div class="row-fluid">
			<ul class="inline">

		<?php endif; ?>

		<li class="product-tile span3">
			<a class="img-link" data-toggle="modal" data-target="#<?php echo str_replace(" ", "-", $accessory->invt_id); ?>" href="#">
				<div class="intro-img-container">
					<img class="intro-img" src="<?=$accessory->icon?>" alt="<?=$accessory->title?>" />
				</div>
				<ul class="unstyled product-details">
					<li class="title"><?= $accessory->title; ?></li>
					<li class="model"><strong><?= JTEXT::_('COM_BK_MODEL').' '.$accessory->invt_id; ?></strong></li>
					<?php if ($accessory->list_price > 0): ?><li>MSRP: $<?= number_format($accessory->list_price, 0); ?></li><?php endif; ?>
				</ul>
			</a>
			<div
				id="<?php echo str_replace(" ", "-", $accessory->invt_id); ?>"
				class="modal hide fade"
				tabindex="-1"
				role="dialog"
				aria-labelledby="<?php echo str_replace(" ", "-", $accessory->invt_id); ?>Label"
				aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h3 id="<?php echo str_replace(" ", "-", $accessory->invt_id); ?>Label"><?php echo JTEXT::_('COM_BK_MODEL').' '.$accessory->invt_id; ?></h3>
					<h5><?php echo $accessory->title; ?></h5>
  				</div>
				<div class="modal-body">
					<div class="row-fluid">
						<div class="accessory-img span6">
							<img src="<?php echo $accessory->photo; ?>">
						</div>
						<div class="span6">
							<?php echo $accessory->description; ?>

						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
				</div>
			</div>
		</li>


		<?php
				if ($i >= 4):
					$i = 1;
		?>

			</ul>
		</div>
		<?php

				else :
					$i++;

				endif;
			endforeach;
			// verify that we close out the ul and the div container
			if ($i < 4 && $i > 1) :
			?>

					</ul>
			</div>
			<?php endif; ?>
		</div>

		<?php  $this->pagination->setAdditionalUrlParam("layout", "accessories"); ?>

		<?php if($this->pagination->total > $this->limit): ?>
		<div class="pull-right pagination-limit">
			<label for="pgLimit"><?php echo JText::_('COM_BK_PAGINATIONSHOW'); ?>
			<select name="limit" id="pgLimit" class="span6">
				<option value="12"<?=(($this->limit == '12') ? ' selected="selected"' : '' )?>>12</option>
				<option value="*"<?=(($this->limit == '1000') ? ' selected="selected"' : '' )?>><?=JText::_('COM_BK_PAGINATIONSHOWALL')?></option>
			</select>
			</label>
		</div>
		<?php endif; ?>

		<?php echo $this->pagination->getPagesLinks(); ?>


		<div class="loading hide">
			<div class="alert alert-error">
				<p>Loading products, please wait...</p>
			</div>
			<div class="progress progress-striped active">
			  <div class="bar"></div>
			</div>
		</div>
	</section>

	<input type="hidden" id="parentId" name="parentId" value="<?php echo $this->parent->id; ?>" />
	<input type="hidden" id="limitStart" name="limitStart" value="<?php echo $this->limitStart; ?>" />
	<input type="hidden" value="<?php echo $this->pagination->total; ?>" />

</div>

<div class="category-header-img hide">
	<img src="/images/products/categories/<?php echo $this->parent->alias; ?>.jpg" alt="<?php echo $this->parent->title; ?>" />
	<div class="title">
		<h3><?php echo $this->parent->title; ?></h3>
		<p class="category-desc hidden-phone"><?php echo $description; ?></p>
	</div>
	<div class="opacity"></div>
</div>
<div id="full-category-description" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="full-category-title" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="full-category-title"><?php echo $this->parent->title; ?></h3>
	</div>
	<div class="modal-body">
		<?php echo $this->parent->description; ?>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	</div>
</div>

<script id="accessory-list-template" type="text/x-handlebars-template">
	{{#each accessories}}

	<li class="product-tile span3">
		<a class="img-link" href="#" data-toggle="modal" data-target="#{{file_name}}">
			<div class="intro-img-container">
				{{imgTile}}
			</div>
			<ul class="unstyled product-details">
				<li class="title">{{title}}</li>
				<li class="model"><strong><?php echo JTEXT::_('COM_BK_MODEL').' '; ?>{{invt_id}}</strong></li>
				{{#if list_price}}<li>MSRP: ${{msrp}}</li>{{/if}}
			</ul>
		</a>
		<div
			id="{{file_name}}"
			class="modal hide fade"
			tabindex="-1"
			role="dialog"
			aria-labelledby="{{accessoryId}}Label"
			aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 id="{{file_name}}Label"><?php echo JTEXT::_('COM_BK_MODEL').' '; ?>{{invt_id}}</h3>
				<h5>{{title}}</h5>
  			</div>
			<div class="modal-body">
				<div class="row-fluid">
					<div class="accessory-img span6">
						{{photo}}
					</div>
					<div class="span6">
						{{description}}
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			</div>
		</div>
	</li>


	{{/each}}
</script>