<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for Categories in the BKContent Component
 */
class BkcontentViewCategories extends JViewLegacy {

	// Overwriting JViewLegacy display method
	function display($tpl = null) {

		// Assign data to the view
		$this->parent 	= $this->get("Parent");
		$this->state 	= $this->get("state");

		if ($this->getLayout() !== "accessories" AND $this->getLayout() !== "default") {

			$this->setLayout("filter");
			$model 				= JModelLegacy::getInstance("Products","BkcontentModel");

			$model->getParent();
			// setup document variables
			$this->categories 	= $this->get("Subcats");
			$this->doc 			= JFactory::getDocument();
			$this->products 	= $model->getProducts();
			$this->filters 		= $model->getFilters();
			$this->total 		= $model->getTotal();
			$this->pagination 	= $model->getPagination();
			$this->limitStart 	= $model->getState("limitstart");
			$this->limit 		= $model->getState("limit");
			$this->sortOptions  = $model->getState("sortOptions");
		}
		elseif ($this->getLayout() === "accessories") {

			$model 			   = JModelLegacy::getInstance("Accessories","BkcontentModel");
			$this->total 	   = $model->getTotal();
			$this->pagination  = $model->getPagination();
			$this->limitStart  = $model->getState("limitstart");
			$this->limit 	   = $model->getState("limit");
			$this->accessories = $this->get("Accessories");
			$this->categories  = $this->get("Subcats");
		}
		else {

			$this->categories 	= $this->get("ProductCategories");
			$this->newProducts 	= $this->get("NewProducts");
		}

		// Check for errors.
		if (count($errors = $this->get("Errors"))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Display the view
		parent::display($tpl);

	}
}