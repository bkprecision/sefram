<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for Where to Buy
 */
class BkcontentViewWtb extends JViewLegacy {

	// Overwriting JView display method
	function display($tpl = null) {

		$doc = JFactory::getDocument(); //get the overall document
		$doc->setMetaData("data-main", "/media/com_bkcontent/js/pages/where-to-buy/main.js");
		$doc->addStyleSheet("/media/com_bkcontent/css/pages/where-to-buy/style.css");

		$this->distributors = $this->get("IntDistributors");
		$this->countries	= $this->get("Countries");
		$this->products 	= $this->get("Products");
		$this->params		= JFactory::getApplication()->getParams();
		$this->search		= JFactory::getApplication()->input->get("invt_id", "", "string");
		$this->results 		= $this->get("Results");

		// Check for errors.
		if (count($errors = $this->get("Errors"))) {
			JError::raiseError(500, implode("<br />", $errors));
			return false;
		}

		// Display the view
		parent::display($tpl);
	}

}