<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for Where to Buy
 */
class BkcontentViewWtb extends JViewLegacy
{
	// Overwriting JView display method
	function display($tpl = null)
	{
		$this->json = $this->get('Results');
		//$this->json->products 		= $this->get('Products');
		//$this->json->distributors 	= $this->get('IntDistributors');
		//$this->json->country		= $this->get('Country');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Change the layout from default to json
		$this->setLayout('json');

		// Display the view
		parent::display($tpl);
	}

}