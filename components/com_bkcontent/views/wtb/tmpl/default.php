<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$doc = JFactory::getDocument(); //get the overall document
$uri = JFactory::getURI(); // get the URI
$app = JFactory::getApplication(); // get the application

?>
<div class="row-fluid">
	<h1><?php echo $doc->title; ?></h1>
</div>
<div class="row-fluid">
	<div id="intDistys">
		<div class="control-group">
			<div class="control-label">
				<label for="filter-country"><?= JText::_("COM_BKCONTENT_WTB_FILTER_COUNTRY_SELECT_TITLE")?></label>
			</div>
			<div class="controls">
				<select id="filter-country">
					<option value><?= JText::_("COM_BKCONTENT_WTB_FILTER_COUNTRY_SELECT_DEFAULT")?></option>
					<? foreach($this->countries as $country): ?>

					<option value="distibutor-<?= preg_replace("/\s+/", "-", strtolower($country)); ?>"><?= $country; ?></option>
					<? endforeach; ?>

				</select>
			</div>
		</div>
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
					<th><?php echo JText::_("COM_BKCONTENT_WTB_COUNTRY_TITLE"); ?></th>
					<th><?php echo JText::_("COM_BKCONTENT_WTB_CITY_TITLE"); ?></th>
					<th><?php echo JText::_("COM_BKCONTENT_WTB_COMPANY_TITLE"); ?></th>
					<th><?php echo JText::_("COM_BKCONTENT_WTB_PHONE_TITLE"); ?></th>
					<th><?php echo JText::_("COM_BKCONTENT_WTB_EMAIL_TITLE"); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($this->distributors as $distributor): ?>

				<tr class="distibutor-<?php echo preg_replace("/\s+/", "-", strtolower($distributor->country)); ?>">
					<td><?php echo $distributor->country; ?></td>
					<td><?php echo $distributor->city; ?></td>
					<td><?php echo $distributor->name; ?></td>
					<td><?php echo $distributor->phone; ?></td>
					<td><a href="mailto:<?php echo $distributor->email; ?>"><?php echo $distributor->email; ?></a></td>
				</tr>
				<?php endforeach; ?>

			</tbody>
		</table>
	</div>
</div>