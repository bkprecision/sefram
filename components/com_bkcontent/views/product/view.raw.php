<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for Categories in the BKContent Component
 */
class BkcontentViewProduct extends JViewLegacy {
	// Overwriting JView display method
	function display($tpl = null) {

		// Assign data to the view
		$this->product					= $this->get('Product');
		if(!empty($this->product)) //check to see if this is an individual product or series
		{
			$this->product->software		= $this->get('Software');
			$this->product->results			= $this->get('Stock'); // get WTB results
			$this->product->videos			= $this->get('Videos');
		}
		else // this is part of a series and we need get information about that series from the menu item
		{
			$app 	= JFactory::getApplication();
			$menu 	= $app->getMenu();
			$aMenu 	= $menu->getActive();
			if ($aMenu->query["layout"] == "series") {
				$model 						= $this->getModel();
				$this->product->seriesInfo 	= $model->getSeries($aMenu->query["series"]);
				$this->product->state		= $this->product->seriesInfo->products[0]->state;
				$this->product->software	= $model->getSoftware($aMenu->query["series"]);
				$this->product->file_name 	= $this->product->seriesInfo->file_name;
				$this->product->warranty	= $this->product->seriesInfo->warranty;
			}
		}

		$this->product->accessories		= $this->get('Accessories');
		// If the product is part of a series, we need to display it appropriately
		// and gather series information for the rendering
		if(!empty($this->product->series)) {
			$this->setLayout('series');
			$model 						= $this->getModel();
			$this->product->seriesInfo 	= $model->getSeries($this->product->series);
			$this->product->file_name 	= $this->product->seriesInfo->file_name;
		}
		if($this->product->state == 2) {
			$this->setLayout('discontinued');
		}


		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Display the view
		parent::display($tpl);
	}

	// transform bytes to human readable format
	// this is used in the template to convert software file sizes to human readable format
	function byteConvert($bytes) {
		$s = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
		$e = floor(log($bytes)/log(1024));

		return sprintf('%.2f '.$s[$e], ($bytes/pow(1024, floor($e))));
	}
}