<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$doc = JFactory::getDocument(); //get the overall document
$doc->addStyleSheet("/templates/bkpre2011/css/jquery.fancybox-1.3.4.css");

// search for the manual, datasheet
// first, by filename
// then by the invt_id
$docTab = false;
if(file_exists(getcwd().DS.'downloads'.DS.'manuals'.DS.'en'.DS.$this->product->file_name.'_manual.pdf'))
{
	$docTab = true;
	$manuelSrc = '/downloads/manuals/en/'.$this->product->file_name.'_manual.pdf';
}
else if(file_exists(getcwd().DS.'downloads'.DS.'manuals'.DS.'en'.DS.$this->product->invt_id.'_manual.pdf'))
{
	$docTab = true;
	$manuelSrc = '/downloads/manuals/en/'.$this->product->invt_id.'_manual.pdf';
}
if(file_exists(getcwd().DS.'downloads'.DS.'datasheets'.DS.$this->product->file_name.'_datasheet.pdf'))
{
	$docTab = true;
	$datasheetSrc = '/downloads/datasheets/'.$this->product->file_name.'_datasheet.pdf';
}
else if(file_exists(getcwd().DS.'downloads'.DS.'datasheets'.DS.$this->product->invt_id.'_datasheet.pdf'))
{
	$docTab = true;
	$datasheetSrc = '/downloads/datasheets/'.$this->product->invt_id.'_datasheet.pdf';
}
//Programming Guide
if(file_exists(getcwd().DS.'downloads'.DS.'pdf'.DS.$this->product->file_name.'_programming_manual.pdf'))
{
	$docTab = true;
	$progguideSrc = '/downloads/pdf/'.$this->product->file_name.'_programming_manual.pdf';
}
//Comparison Guide
if(file_exists(getcwd().DS.'downloads'.DS.'pdf'.DS.$this->product->file_name.'_comparison_guide.pdf'))
{
	$docTab = true;
	$compguideSrc = '/downloads/pdf/'.$this->product->file_name.'_comparison_guide.pdf';
}
//Spanish Datasheets
if(file_exists(getcwd().DS.'downloads'.DS.'datasheets'.DS.'es'.DS.$this->product->file_name.'_hoja_de_datos.pdf'))
{
	$docTab = true;
	$hojadedatosSrc = '/downloads/datasheets/es/'.$this->product->file_name.'_hoja_de_datos.pdf';
}
else if(file_exists(getcwd().DS.'downloads'.DS.'datasheets'.DS.'es'.DS.$this->product->invt_id.'_datasheet.pdf'))
{
	$docTab = true;
	$hojadedatosSrc = '/downloads/datasheets/es/'.$this->product->invt_id.'_hoja_de_datos.pdf';
}

// setup product photo slide show images
$photos = array();
$photoPrefix = 'images/products/photos/large/';
$photoSuffixs = array('_front_lrg.jpg','_left_lrg.jpg','_right_lrg.jpg','_rear_lrg.jpg');
$pGalleryWidth = 0;
$pGalleryHeight = 0;
$pMenuHeight = 0;
$pMenuWidth = 0;
$pMenuItemWidth = 0;

foreach($photoSuffixs as $suffix)
{
	if(file_exists(getcwd().DS.'images'.DS.'products'.DS.'photos'.DS.'large'.DS.$this->product->file_name.$suffix))
	{
		$obj = new stdClass();
		$obj->src = $photoPrefix.$this->product->file_name.$suffix;
		list($width, $height) = getimagesize(getcwd().DS.$obj->src);

		if( ($width*0.6) > 320 )
		{
			$largeMultiplier = 320/(int)$width;
		}
		else
		{
			$largeMultiplier = 0.6;
		}

		if( ($width*0.1) > 65 )
		{
			$smallMultiplier = 65/(int)$width;
		}
		else
		{
			$smallMultiplier = 0.1;
		}


		$obj->width = $width;
		$pGalleryWidth = max($pGalleryWidth, ($width*$largeMultiplier));
		$obj->height = $height;
		$pGalleryHeight = max($pGalleryHeight, ($height*$largeMultiplier));
		$pMenuHeight = max($pMenuHeight, ($height*$smallMultiplier));
		$pMenuItemWidth = max($pMenuItemWidth, ($width*$smallMultiplier)+7);
		$pMenuWidth = $pMenuWidth + ($width*$smallMultiplier) + 7;
		$photos[] = $obj;
	}
	else if(file_exists(getcwd().DS.'images'.DS.'products'.DS.'photos'.DS.'large'.DS.$this->product->invt_id.$suffix))
	{
		$obj = new stdClass();
		$obj->src = $photoPrefix.$this->product->invt_id.$suffix;
		list($width, $height) = getimagesize(getcwd().DS.$obj->src);

		if( ($width*0.6) > 320 )
		{
			$largeMultiplier = 320/(int)$width;
		}
		else
		{
			$largeMultiplier = 0.6;
		}

		if( ($width*0.1) > 65 )
		{
			$smallMultiplier = 65/(int)$width;
		}
		else
		{
			$smallMultiplier = 0.1;
		}

		$obj->width = $width;
		$pGalleryWidth = max($pGalleryWidth, ($width*$largeMultiplier));
		$obj->height = $height;
		$pGalleryHeight = max($pGalleryHeight, ($height*$largeMultiplier));
		$pMenuHeight = max($pMenuHeight, ($height*$smallMultiplier));

		$pMenuItemWidth = max($pMenuItemWidth, ($width*$smallMultiplier)+7);
		$pMenuWidth = $pMenuWidth + ($width*$smallMultiplier) + 7;
		$photos[] = $obj;
	}
}

$doc->setMetaData('product-image', '/'.$photos[0]->src);

if(is_object($this->product->seriesInfo->params))
{
	$promoLink = $this->product->seriesInfo->params->getValue('promo-link');
	if(empty($promoLink))
	{
		$promoLink = $this->product->params->getValue('promo-link');
	}
}

if($this->product->seriesInfo->individualProduct == true)
{
	$doc->setTitle("Model ".$this->product->invt_id.", ".$this->product->seriesInfo->title);
}
else
{
	$doc->setTitle($this->product->seriesInfo->name.', '.$this->product->seriesInfo->title);
}
?>
<h1 style="border: none; margin-bottom: 0.25em;"><?php echo $this->product->seriesInfo->title; ?></h1>
<h2 class="hidden">B&amp;K Precision</h2>
<section class="modelInfo" style="border-bottom: 1px solid #036; margin-left: 8px; margin-right: 8px;">
	<h2 id="model">
		<?php if($this->product->seriesInfo->individualProduct == true): ?>
			<span><?=$this->product->seriesInfo->name?>: Model <?=$this->product->invt_id?></span>
		<?php else: ?>
			<?php echo $this->product->seriesInfo->name; ?>
		<?php endif; ?>
	</h2>
	<ul class="inline alignRight">
		<?php if(!empty($this->product->warranty)): ?>
		<li class="modelWarranty"><h4><?php echo $this->product->warranty; ?> Year Warranty</h4></li>
		<?php endif; ?>
	</ul>
	<div class="clr"></div>
</section>
<div class="container">

	<section class="rightPane" style="width: 58.4em;">
		<div class="tabs ui-tabs ui-widget ui-widget-content ui-corner-all">

			<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
				<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active"><a href="#overview">Overview</a></li>
				<?php if($docTab == true): ?>
				<li class="ui-state-default ui-corner-top"><a href="#documentation">Documentation</a></li>
				<?php endif; ?>
				<?php if(count($this->product->accessories)): ?>
				<li class="ui-state-default ui-corner-top"><a href="#accessories">Accessories</a></li>
				<?php endif; ?>
				<?php if(count($this->product->software)): ?>
				<li class="ui-state-default ui-corner-top"><a href="#software">Software</a></li>
				<?php endif; ?>
			</ul>

			<div id="overview" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
				<?php echo $this->product->seriesInfo->description; ?>
			</div>

			<?php if($docTab == true): ?>
			<div id="documentation" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
				<ul class="bltlistbl alignLeft">
					<?php if(isset($datasheetSrc)): ?>
					<li><a href="<?php echo $datasheetSrc; ?>" target="_blank">Datasheet</a> (PDF)</li>
					<?php endif; ?>
					<?php if(isset($hojadedatosSrc)): ?>
					<li class="indent"><a href="<?php echo $hojadedatosSrc; ?>" target="_blank">Hoja de Datos</a> (PDF)</li>
					<?php endif; ?>
					<?php if(isset($manuelSrc)): ?>
					<li><a href="<?php echo $manuelSrc; ?>" target="_blank">Manual</a> (PDF)</li>
					<?php endif; ?>
					<?php if(isset($compguideSrc)): ?>
					<li><a href="<?php echo $compguideSrc; ?>" target="_blank">Comparison Guide</a> (PDF)</li>
					<?php endif; ?>
					<?php if(isset($progguideSrc)): ?>
					<li><a href="<?php echo $progguideSrc; ?>" target="_blank">Programming Manual</a> (PDF)</li>
					<?php endif; ?>
				</ul>
			</div>
			<?php endif; ?>
			
			<?php if(count($this->product->accessories)): ?>
			<div id="accessories" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
				<ul class="accessories">
					<?php foreach($this->product->accessories as $accessory): ?>
						<li>
							<section>
								<ul>
									<li><?php echo $accessory->title; ?></li>
									<li class="modelNum">Model# <?php echo $accessory->invt_id; ?></li>

									<? if(file_exists(getcwd().DS.'downloads'.DS.'manuals'.DS.'en'.DS.$accessory->invt_id.'_manual.pdf')): ?>
									<? $accmanual = '/downloads/manuals/en/'.$accessory->invt_id.'_manual.pdf';?>
									<li><a href="<?php echo $accmanual; ?>">Download <?php echo "$accessory->invt_id"; ?> Manual</a></li>
									<? endif; ?>
									
									<li class="shortDesc"><p><?php echo $accessory->description; ?></p></li>
								</ul>
							</section>
							<section class="alignCenter" style="float:right; width: 27%;">
								<ul>
									<li class="photoPreview">
										<a href="/images/products/photos/accs/<?php echo $accessory->invt_id; ?>.gif" class="productThumb alignCenter fancybox" id="<?php echo $accessory->invt_id; ?>"><img src="/images/products/photos/icons/<?php echo $accessory->invt_id; ?>.gif" alt="<?php echo $accessory->title; ?>" /></a>
									</li>
									<li class="alignCenter listPrice"><a href="<?=JRoute::_('index.php?option=com_bkcontent&view=wtb&Itemid=120&invt_id='.$accessory->invt_id)?>" title="Where to Buy">MSRP $<?php echo number_format($accessory->list_price, 2); ?></a></li>
								</ul>
							</section>
							<div class="clr"></div>
						</li>
					<?php endforeach; ?>
				</ul>
				<div class="clr"></div>
			</div>
			<?php endif; ?>
			
			<?php if(count($this->product->software)): ?>
			<div id="software" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
				<table>
					<tr><th>Version</th><th>Note/Description</th><th>File Size</th><th>File Link</th></tr>
					<?php foreach($this->product->software as $software): ?>
					<?php $softwarePath = getcwd().DS.'downloads'.DS.'software'.DS.$software->file_name; ?>
					<tr><td class="col1"><?=$software->version?></td><td class="col2"><?=$software->description?></td><td class="col3"><?=$this->byteConvert(filesize($softwarePath))?></td><td class="col4"><a href="/downloads/software/<?=$software->file_name?>"><?=$software->file_name?></a></td></tr>
					<?php endforeach; ?>
				</table>
			</div>
			<?php endif; ?>

		</div>
	</section>

	<section class="leftPane alignCenter" style="width: 20.25em;">

		<div id="photoGallery" class="ui-corner-all" style="border: 1px solid #DDD;">

			<div id="slides" style="height: <?php echo $pGalleryHeight; ?>px; background-color: #fff;">
			<?php foreach($photos as $photoObj): ?>
				<div class="slide"><a href="/photos/?src=<?php echo $photoObj->src?>" rel="gallary" title="Series Information — <?=$this->product->seriesInfo->title?>"><img src="/photos/?src=<?php echo $photoObj->src.'&size=medium&maxWidth=320'?>" width="<?php echo (($photoObj->width*0.6) > 320 ? '320' : round($photoObj->width*0.6)); ?>" height="<?php echo (($photoObj->width*0.6) > 320 ? $photoObj->height*(320/(int)$photoObj->width) : $photoObj->height*0.6); ?>" border="0" /></a></div>
			<?php endforeach; ?>
			</div>

			<div class="ui-widget-content slide-menu" id="slideMenu" style="border-bottom: none; border-left: none; border-right: none;">
				<ul class="slide-menu-list" style="height: <?php echo $pMenuHeight+5; ?>px;">
					<?php for($i=(4-count($photos)); $i>0; $i--): ?>
						<li class="menuItem placeholder<?=(((4-count($photos))>2 AND $i==1)?' small':'')?>"></li>
					<?php endfor; ?>
					<?php $i = 1; foreach($photos as $photoObj): ?>
						<li class="menuItem <?=(($i == count($photos))?'last':'')?>">
							<a><img src="/photos/?src=<?php echo $photoObj->src.'&size=smallest&maxWidth=70'; ?>" width="<?php echo (($photoObj->width*0.1) > 65 ? '65' : ($photoObj->width*0.1)); ?>" border="0" /></a>
						</li>
					<?php $i++; endforeach; ?>
				</ul>
			</div>

		</div>

		<?php if(count($this->product->videos) > 0): ?>
		<p class="alignCenter">
			<?php $i = 0; ?>
			<?php foreach($this->product->videos as $playlistId): ?>

			<a class="fancybox iframe<?=(($i > 0) ? ' hidden': '')?>" rel="videoGroup" href="http://www.youtube.com/embed?listType=playlist&list=<?=$playlistId?>"><strong>Product Video</strong></a>
			<?php $i++; ?>
			<?php endforeach; ?>
		</p>
		<?php endif; ?>

		<div id="seriesInfo">
			<ul>
				<li><span class="tblTitle"><strong>Where to Buy</strong></span><span class="tblTitle">MSRP</span></li>
			<?php foreach($this->product->seriesInfo->products as $seriesProduct): ?>
				<li><span class="<?=((isset($this->product->invt_id)) ? ($seriesProduct->invt_id == $this->product->invt_id) ? 'selectedModel':'' : '')?>"><span class="modelNumber"><a href="<?=JRoute::_('index.php?option=com_bkcontent&view=wtb&Itemid=120&invt_id='.$seriesProduct->invt_id)?>"><?=$seriesProduct->invt_id?></a></span></span><span class="<?=((isset($this->product->invt_id)) ? ($seriesProduct->invt_id == $this->product->invt_id) ? 'selectedModel':'': '')?>">$<?=number_format($seriesProduct->list_price)?></span></li>
			<?php endforeach; ?>
			</ul>
		</div>

	</section>

	<div class="clr"></div>
</div>
<?php if(!empty($promoLink)): ?>
<div class="promoDiv">
	<a href="<?=$promoLink?>">
		<img src="/images/promo_badge_45.png" />
	</a>
</div>
<?php endif; ?>
<script type="text/javascript">

 // Add a script element as a child of the body
bkp.loadJsOnLoad = function () {
	var element = document.createElement("script");
	element.src = "/templates/bkpre2011/js/product.js";
	document.body.appendChild(element.cloneNode(false));
	element.src = "/templates/bkpre2011/js/series.js";
	document.body.appendChild(element.cloneNode(false));
}

// Check for browser support of event handling capability
if (window.addEventListener) {
	window.addEventListener("load", bkp.loadJsOnLoad, false);
}
else if (window.attachEvent) {
	window.attachEvent("onload", bkp.loadJsOnLoad);
}
else {
	window.onload = bkp.loadJsOnLoad;
}

</script>