<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication(); //get the app

if( isset($this->product->params) ) {
	$promoLink = $this->product->params->get('promo-link');
}
?>
<div itemscope itemtype="http://schema.org/ProductModel" class="row-fluid">
<meta itemprop="brand" content="B&K Precision">
<meta itemprop="sku" content="<?= $this->product->invt_id; ?>">
<meta itemprop="image" content="<?= $this->product->photos[0]->src; ?>">
<div class="row-fluid">

<?php if ($this->product->series > 0): ?>
<h3 class="series name" itemprop="name"><?= $this->product->seriesInfo->name; ?></h3>
<?php endif; ?>
<h2 class="description" itemprop="description"><?= $this->product->title; ?></h2>
<h1 class="model" itemprop="model">
<?php
// apply correct discontinued title
if ($this->product->state == 2) {
	echo JText::_('COM_BKPRODUCT_DISCONTINUED') . " ";
}
echo JText::_("COM_BK_MODEL") . " " . $this->product->invt_id;
?>
</h1>

<?php if ( (int)$this->product->warranty > 0 ): ?>
<h5 class="warranty-info">
	<?php
		if ( $this->product->state == 2 ) {
			echo JText::_('COM_BKPRODUCT_DISCONTINUED') . ": " . date('F j, Y', strtotime($this->product->obsolted_date)) . " ";
		}
		echo $this->product->warranty . ' ' . JTEXT::_('COM_BKPRODUCT_WARRANTY');
	?>

<?php endif; ?>

</h5>
<?php if ($this->product->state != 2 && $this->params->get("show_wtb_btn")): ?>
	<a class="btn btn-success btn-mini btn-wtb-results hidden-desktop" href="/where-to-buy.html?invt_id=<?php echo $this->product->invt_id; ?>">Where to Buy</a>
<?php endif; ?>
</div>
<div class="row-fluid">

	<section class="span4 photos-block pull-right" id="photos-block">

		<div class="row-fluid">

			<div id="photoGallery" class="photo-gallery carousel slide" data-interval="">
				<!-- Carousel items -->
				<div class="slide-show carousel-inner hidden-phone">
					<?php
						// we only want to show 4 product photos here
						// and if we have more show an elipses "..." for the icons
						if (count($this->product->photos) > 4) :
							for ($i = 0; $i < 4; $i++) :
							$photo = $this->product->photos[$i];
						?>

						<div class="item<?= (($i == 0) ? " active" : ""); ?>">
							<a href="#galleryLargeModal" data-toggle="lightbox" class="visible-desktop">
								<img src="<?php echo $photo->src; ?>" title="<?php echo $photo->slideTitle; ?>" alt="<?php echo $photo->slideAltTag; ?>">
							</a>
							<img src="<?php echo $photo->src; ?>" title="<?php echo $photo->slideTitle; ?>" alt="<?php echo $photo->slideAltTag; ?>" class="hidden-desktop">
						</div>

					<?php
							endfor;
						// only 4 photos are present
						else :
							foreach($this->product->photos as $photo) :
					?>

						<div class="item<?= (($photo->src === $this->product->photos[0]->src) ? " active" : ""); ?>">
							<a href="#galleryLargeModal" data-toggle="lightbox" class="visible-desktop">
								<img src="<?php echo $photo->src; ?>" title="<?php echo $photo->slideTitle; ?>" alt="<?php echo $photo->slideAltTag; ?>">
							</a>
							<img src="<?php echo $photo->src; ?>" title="<?php echo $photo->slideTitle; ?>" alt="<?php echo $photo->slideAltTag; ?>" class="hidden-desktop">
						</div>
					<?php
							endforeach;
						endif;
					?>

				</div>
				<div class="carousel-footer fluid-row">
		        	<ul class="inline">
		        		<?php
							// we only want to show 4 product photos here
							// and if we have more show an elipses "..." for the icons
							if (count($this->product->photos) > 4) :
								for ($i = 0; $i < 4; $i++) :
								$photo = $this->product->photos[$i];
						?>

						<li class="span2<?= (($i == 0) ? " offset1" : ""); ?>">
							<a data-target="#photoGallery" href="#" data-slide-to="<?= $i; ?>" class="visible-desktop">
								<img src="<?php echo $photo->src; ?>">
							</a>
							<img src="<?php echo $photo->src; ?>" title="<?php echo $photo->slideTitle; ?>" alt="<?php echo $photo->slideAltTag; ?>" class="hidden-desktop">
						</li>
						<?php endfor; ?>

						<li class="span2">
							<a href="#galleryLargeModal" data-toggle="lightbox" class="visible-desktop">
								<img src="https://bkpmedia.s3.amazonaws.com/images/dotdotdot.png">
							</a>
						</li>
						<?php
								// only 4 photos are present
							else :
								foreach($this->product->photos as $i => $photo) :
						?>

								<li class="span3">
									<a data-target="#photoGallery" href="#" data-slide-to="<?= $i; ?>" class="visible-desktop">
										<img src="<?php echo $photo->src; ?>">
									</a>
									<img src="<?php echo $photo->src; ?>" title="<?php echo $photo->slideTitle; ?>" alt="<?php echo $photo->slideAltTag; ?>" class="hidden-desktop">
								</li>
						<?php

								endforeach;
							endif;
						?>

		        	</ul>
			    </div>
			</div>

			<!-- Modal -->
			<div id="galleryLargeModal" class="lightbox fade hide hidden-phone" tabindex="-1" role="dialog" aria-hidden="true">
				<div class='lightbox-dialog'>
			        <div class='lightbox-content'>
			        	<div id="photoGalleryLarge" class="photo-gallery-large carousel slide" data-interval="">
							<!-- Carousel items -->
							<div class="slide-show carousel-inner">
								<?php foreach($this->product->photos as $photo): ?>

									<div class="item<?= (($photo->src === $this->product->photos[0]->src) ? " active" : ""); ?>">
										<img src="<?php echo $photo->src; ?>" title="<?php echo $photo->slideTitle; ?>" alt="<?php echo $photo->slideAltTag; ?>">
									</div>
								<?php endforeach; ?>

							</div>

							<!-- Carousel nav -->
							<a class="carousel-control left" href="#photoGalleryLarge" data-slide="prev">&lsaquo;</a>
							<a class="carousel-control right" href="#photoGalleryLarge" data-slide="next">&rsaquo;</a>
						</div>
			        </div>
			    </div>
			</div>

			<?php if (!empty($this->product->video)): ?>

			<iframe
				id="productVideo"
				class="span12"
				height="360px"
				type="text/html"
				src="https://www.youtube.com/embed?listType=playlist&origin=http://bkprecision.com&list=<?= $this->product->video; ?>"
				frameborder="0">
			</iframe>
			<?php endif; ?>

		</div>

	</section>


	<section class="span8 support-info-block pull-left" id="support-info-block">

		<ul class="nav nav-tabs">
			<?php if( !empty($this->product->replacements) ): ?>
			<li class="active" ><a href="#replacements" data-toggle="tab"><?= JTEXT::_('COM_BKPRODUCT_REPLACEMENTS'); ?></a></li>
			<?php endif; ?>
			<li<?=( !empty($this->product->replacements) ? "" : " class=\"active\"" )?>><a href="#overview" data-toggle="tab"><?= JTEXT::_('COM_BKPRODUCT_OVERVIEW'); ?></a></li>
			<?php if ($this->product->series > 0): ?>
			<li><a href="#models" data-toggle="tab"><?= JTEXT::_('COM_BKPRODUCT_MODELS'); ?></a></li>
			<?php endif; ?>
			<?php if ( !empty($this->product->documentation) || !empty($this->product->software) ): ?>
			<li><a href="#docsoft" data-toggle="tab"><?= JTEXT::_('COM_BKPRODUCT_DOCS_SOFT'); ?></a></li>
			<?php endif; ?>
			<?php if ( !empty($this->product->accessories) ): ?>
			<li><a href="#product-accessories" data-toggle="tab"><?= JTEXT::_('COM_BKPRODUCT_ACCESSORIES'); ?></a></li>
			<?php endif; ?>

		<!-- More Information Button -->
			<div class="visible-desktop pull-right">
				<a href=
					"/<?php if ($this->product->lang === 'fr-fr') {
						echo 'contactez-sefram.html';
					} else {
						echo 'en/contact-us.html';
					}
					?>" type="button" class="btn btn-success">
					<?php if ($this->product->lang === 'fr-fr') {
						echo 'Plus d\'information';
					} else {
						echo 'More Information';
					}
					?>
				</a>
			</div>
			<?php if ($this->product->state != 2 && $this->params->get("show_wtb_btn")): ?>
<?php endif; ?>
		</ul>

		<div class="tab-content">

			<?php if ( !empty($this->product->replacements) ): ?>
			<div id="replacements" class="tab-pane active replacements">
				<ul class="unstyled offset2">
					<?php foreach($this->product->replacements as $replacement): ?>
						<li<?= ( ($this->product->replacements[count($this->product->replacements)-1] == $replacement) ? " class=\"last\"" : "" ); ?>>
							<div class="span2 img-polaroid">
							<a class="img-link" href="/<?=$replacement->path.'/'.$replacement->invt_id.'-'.$replacement->alias?>.html">

								<? if(file_exists(getcwd().DS.'images'.DS.'products'.DS.'photos'.DS.'icons'.DS.$replacement->invt_id.'.gif')): ?>
								<? $icon = '/images/products/photos/icons/'.$replacement->invt_id.'.gif';?>
								<? else: ?>
								<? $icon = '/images/products/photos/icons/noimage.gif';?>
								<? endif; ?>

								<img src="<?=$icon?>" alt="<?=$replacement->title?>" />
							</a>
							</div>
							<div class="span8">
								<ul class="unstyled product-details">
									<li><a href="/<?=$replacement->path.'/'.$replacement->invt_id.'-'.$replacement->alias?>.html"><?=$replacement->title?></a></li>
									<li><strong><?=JTEXT::_('COM_BK_MODEL').' '.$replacement->invt_id?></strong></li>
									<li><p><?=((!empty($replacement->short_desc)) ? $replacement->short_desc : '&nbsp;');?></p></li>
								</ul>
							</div>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
			<?php endif; ?>

			<div id="overview" class="tab-pane overview<?=( !empty($this->product->replacements) ? "" : " active" )?>">
				<?php echo $this->product->long_desc; ?>
			</div>

			<?php
				if ($this->product->series > 0):
						$attributes = (array)$this->product->seriesInfo->params->get("attribs");
			?>
			<div id="models" class="tab-pane models">
				<table class="table">
					<thead>
						<tr>
							<th>Model</th>
							<?php
								if (is_array($attributes)):
									foreach ($attributes as $title => $models):
							?>

							<th><?php echo $title; ?></th>
							<?php
									endforeach;
								endif;
							?>

							<?php if ($this->params->get("show_msrp") && isset($model->list_price) && $model->list_price > 0): ?>
							<th>MSRP</th>
							<?php endif; ?>
						</tr>
					</thead>
					<tbody>
					<?php foreach($this->product->seriesInfo->products as $model): ?>
						<?php if ($model->invt_id == $this->product->invt_id): ?>
						<tr class="success">
						<?php else: ?>
						<tr>
						<?php endif; ?>
							<td><a href="<?php echo $model->path . "/" . $model->invt_id . "-" . $model->alias . ".html"; ?>"><?= $model->invt_id; ?></a></td>
							<?php
								if (is_array($attributes)):
									foreach ($attributes as $models):
										$models = (array)$models;
										$attribute = $this->getAttribute($models, $model->invt_id);
							?>

							<td><?php echo $attribute; ?></td>
							<?php
									endforeach;
								endif;
							?>

							<?php if ($this->params->get("show_msrp") && isset($model->list_price) && $model->list_price > 0): ?>

							<td>
								<?php echo "$" . number_format($model->list_price, 0); ?>
									<a class="btn btn-success btn-mini btn-wtb-results hidden-desktop" href="/where-to-buy.html?invt_id=<?php echo $model->invt_id; ?>">Where to Buy</a>
									<button
										type="button"
										class="btn btn-success btn-mini btn-wtb-results visible-desktop pull-right"
										data-html="true"
										data-placement="left"
										data-toggle="popover"
								<?php if (!empty($model->stock->stock)): ?>

										data-content='<ul class="unstyled wtb-results">
										<?php foreach ($model->stock->distributors as $distributor): ?>
											<li>
												<h6><?php echo $distributor['name']; ?></h6>
												<table class="table">
													<thead>
														<tr>
															<th>Model</th>
															<th>Qty</th>
														</tr>
													</thead>
													<tbody>
														<?php
															foreach ($model->stock->stock as $item):
																if ($item['dist_id'] == $distributor['dist_id']):
																	$lastUpdate = $item['last_update'];
														?>
															<tr><td><a href="<?php echo $distributor['url']; ?>" target="_blank"><?php echo $item['invt_id']; ?></a></td><td><?php echo $item['qty_onhand']?></td></tr>
														<?php
																endif;
															endforeach;
														?>
													</tbody>
												</table>
												<p><small>As of: <?php echo $lastUpdate; ?></small></p>
											</li>
										<?php endforeach; ?>
										<?php if ($model->stock->total > 3): ?>
										<li><p class="more-results"><a href="/where-to-buy.html?invt_id=<?php echo $model->invt_id; ?>">More results</a></p></li>
										<?php endif; ?>
										</ul>'

								<?php else: ?>
									data-content="<p>No distributors seem to have what you're looking for in stock. You can try searching again with a different part number or call us direct for a quote. (714) 921-9095</p>"
								<?php endif; ?>
									>Where to Buy</button>
							</td>
							<?php endif; ?>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
			</div>
			<?php endif; ?>

			<?php if( !empty($this->product->documentation) || !empty($this->product->software) ): ?>
			<div id="docsoft" class="tab-pane">

				<?php if( !empty($this->product->documentation) ): ?>

				<section class="documentation row-fluid">
					<div class="pull-left">
						<div class="ion-document-text"></div>
						<p class="documentation-title"><?= JText::_("COM_BKCONTENT_DOCUMENTATION"); ?></p>
					</div>
					<ul class="unstyled offset2">
						<?php foreach($this->product->documentation as $doc): ?>

						<li><span class="ion-document">&nbsp;</span><a href="<?= $doc->src; ?>" target="_blank"><?= JTEXT::_($doc->title); ?></a></li>

						<?php endforeach; ?>
					</ul>
				</section>
				<?php endif; ?>

				<?php if( !empty($this->product->software) ): ?>

				<section class="software row-fluid">
					<div class="pull-left">
						<div class="ion-gear-b"></div>
						<p class="software-title"><?= JText::_("COM_BKCONTENT_SOFTWARE"); ?></p>
					</div>
					<div class="offset2">
						<table class="table table-bordered">
							<tr>
								<th><?=JTEXT::_('COM_BKPRODUCT_SOFTWARE_DESC')?></th>
								<th><?=JTEXT::_('COM_BKPRODUCT_SOFTWARE_VERSION')?></th>
								<th><?=JTEXT::_('COM_BKPRODUCT_SOFTWARE_LINK')?></th>
							</tr>
							<?php foreach($this->product->software as $software): ?>

									<tr>
										<td><?=JTEXT::_($software->description)?></td>
										<td><?=$software->version?></td>
										<td><a class="btn btn-link" href="<?=$software->src?>"><?=$software->file_name?></a></td>
									</tr>
							<?php endforeach; ?>

						</table>
					</div>

				</section>
				<?php endif; ?>

			</div>
			<?php endif; ?>

			<?php if ( !empty($this->product->accessories) ): ?>
			<div id="product-accessories" class="tab-pane">
				<ul class="accessories unstyled">
					<?php foreach($this->product->accessories as $accessory): ?>
						<li class="row-fluid<?= ( ($this->product->accessories[ count($this->product->accessories)-1 ] == $accessory) ? " last" : ""); ?>">

							<div class="accessory-info span12">
								<div class="row-fluid">
									<div class="span3 accessory-img-container">
										<?php if ( ! empty($accessory->photo)): ?>
										<a
											href="#accessoryLightbox<?php echo str_replace(" ", "-", $accessory->invt_id); ?>"
											data-toggle="lightbox"
											id="<?php echo $accessory->invt_id; ?>">
										<?php endif; ?>
										<?php if ( ! empty($accessory->icon)): ?>
											<img
												class="img-polaroid accessory-img"
												src="<?php echo $accessory->icon; ?>"
												alt="<?php echo $accessory->title; ?>"
											/>
										<?php endif; ?>
										<?php if ( ! empty($accessory->photo)): ?>
										</a>
										<!-- Modal -->
										<div id="accessoryLightbox<?php echo str_replace(" ", "-", $accessory->invt_id); ?>" class="lightbox fade hide hidden-phone" tabindex="-1" role="dialog" aria-hidden="true">
											<div class='lightbox-dialog'>
										        <div class='lightbox-content'>
										        	<img src="<?php echo $accessory->photo; ?>">
										        </div>
										    </div>
										</div>
										<?php endif; ?>
									</div>
									<div class="span9">
										<p class="accessory-title"><?= $accessory->title; ?></p>
										<h5 class="accessory-model"><?= JTEXT::_('COM_BK_MODEL').' '.$accessory->invt_id; ?></h5>
										<?php if( !empty($accessory->description) ): ?>

										<div class="accessory-desc">
											<?= $accessory->description; ?>

										</div>
										<?php endif; ?>

										<?php if ($this->params->get("show_msrp") && isset($accessory->list_price)): ?>
										<p class="accessory-list-price">MSRP $<?= number_format($accessory->list_price, 2); ?></p>
										<?php endif; ?>
									</div>
								</div>
							</div>



						</li>
					<?php endforeach; ?>

				</ul>
			</div>
			<?php endif; ?>

		</div>

	</section>


	<div class="clearfix"></div>
</div>

<?php if( isset($promoLink) AND !empty($promoLink) ): ?>
<div class="promoDiv">
	<a href="<?=$promoLink?>">
		<img src="/images/promo_badge_45.png" />
	</a>
</div>
<?php endif; ?>
</div>