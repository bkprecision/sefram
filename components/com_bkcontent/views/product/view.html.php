<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for Categories in the BKContent Component
 */
class BkcontentViewProduct extends JViewLegacy {
	// Overwriting JView display method
	function display($tpl = null) {

		$doc 	= JFactory::getDocument(); //get the overall document
		$app 	= JFactory::getApplication(); //get the app
		// Get the component params
		$this->params = JComponentHelper::getParams('com_bkcontent');

		$doc->setMetaData("data-main", "/media/com_bkcontent/js/pages/product/main.js");
		$doc->addStyleSheet("/media/com_bkcontent/css/bootstrap-lightbox.min.css");
		$doc->addStyleSheet("/media/com_bkcontent/css/pages/product/styles.css");

		// Assign data to the view
		$this->product	= $this->get('Product');
		if (empty($this->product->invt_id) && empty($this->product->seriesInfo)) {
			$this->setError("Model does not exist.");
		}
		// Check for errors.
		if ( count($errors = $this->getErrors()) ) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// check for product path
		// ensure that the current path matches the product's path according to the db
		$uri = JURI::getInstance();
		$currentPath = substr(preg_replace('/^(.*)\/.+\.html$/i', '$1', $uri->getPath()), 1);
		// check to see if the category path is correct
		if ( $this->product->path != $currentPath ) {
			$app->redirect('/'.$this->product->path.'/'.$this->product->invt_id.'-'.$this->product->alias.'.html', TRUE);
		}
		// check if the model alias is correct
		$modelPath		= str_replace(" ", "%20", $this->product->invt_id.'-'.$this->product->alias);
		$currentPath 	= preg_replace('/^.*\/(.+)\.html$/i', '$1', $uri->getPath());
		if ( $modelPath != $currentPath ) {
			$app->redirect('/'.$this->product->path.'/'.$this->product->invt_id.'-'.$this->product->alias.'.html', TRUE);
		}

		$this->state 	= $this->get('state');

		// apply any menu supplied pageclass
		$pageclass = $this->state->params->get( 'pageclass_sfx' );

		if ( !empty($pageclass) ) {
			$pageclass .= ' ';
		}
		$pageclass .= 'ipop';

		//check to see if this is an individual product or series and set page class accordingly
		if ( isset($this->product->seriesInfo) ) {
			$pageclass 	.= ' '.$this->product->seriesInfo->alias;
		}
		else {
			$pageclass .= ' '.$this->product->invt_id;
		}

		// If the product is part of a series, we need to display it appropriately
		if( !empty($this->product->series) ) {
			//$this->setLayout('series');
			$pageclass .= ' '.$this->product->seriesInfo->alias;
		}

		if( $this->product->state == 2 ) {
			//$this->setLayout('discontinued');
			$pageclass .= ' discontinued';
		}


		// set the new pageclass
		$this->state->{'parameters.menu'}->set( 'pageclass_sfx',  $pageclass);
		$this->set('state', $this->state);

		// Setup the proper page titles, images and meta-data
		if( isset($this->product->photos[0]) ) {
			$doc->setMetaData('product-image', $this->product->photos[0]->src);
			$doc->setMetaData('thumbnail', $this->product->photos[0]->src);
		}

		// check for series and discontinued in the title
		if( !empty($this->product->series) AND !$this->product->seriesInfo->individualProduct ) {
			$doc->setTitle($this->product->seriesInfo->name.', '.$this->product->title);
		}
		elseif( $this->product->state == 2 ) {
			$doc->setTitle("Discontinued Model ".$this->product->invt_id.', '.$this->product->title);
		}
		else {
			$doc->setMetaData('invt_id', $this->product->invt_id);
			$doc->setTitle(JTEXT::_('COM_BK_MODEL')." ".$this->product->invt_id.', '.$this->product->title);
		}

		// Display the view
		parent::display($tpl);
	}

	protected function getAttribute($models, $model) {
		foreach ($models as $invt_id => $attribute) {
			if ($invt_id == $model) {
				$return = $attribute;
				break;
			}
		}
		return $return;
	}
}