<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>
<h1><?php echo $this->parent->title; ?></h1>

<div class="products">
	<ul class="unstyled">
		<?php foreach($this->products as $product): ?>
	<? if(file_exists(getcwd().DS.'images'.DS.'products'.DS.'photos'.DS.'icons'.DS.$product->invt_id.'.gif')): ?>
	<? $icon = '/images/products/photos/icons/'.$product->invt_id.'.gif';?>
	<? else: ?>
	<? $icon = '/images/products/photos/icons/noimage.gif';?>
	<? endif; ?>
			<li<?= ( ($this->products[count($this->products)-1] == $product) ? " class=\"last\"" : "" ); ?>>
				<div class="span2 img-polaroid">
				<a class="img-link" href="/<?=$product->path.'/'.$product->invt_id.'-'.$product->alias?>.html">
					<? if(file_exists(getcwd().DS.'images'.DS.'products'.DS.'photos'.DS.'icons'.DS.$product->file_name.'.gif')): ?>
					<? $icon = '/images/products/photos/icons/'.$product->file_name.'.gif';?>
					<? elseif(file_exists(getcwd().DS.'images'.DS.'products'.DS.'photos'.DS.'icons'.DS.$product->invt_id.'.gif')): ?>
					<? $icon = '/images/products/photos/icons/'.$product->invt_id.'.gif';?>
					<? elseif(file_exists(getcwd().DS.'images'.DS.'products'.DS.'photos'.DS.'icons'.DS.substr($product->invt_id, 2).'.gif')): ?>
					<? $icon = '/images/products/photos/icons/'.substr($product->invt_id, 2).'.gif';?>
					<? else: ?>
					<? $icon = '/images/products/photos/icons/noimage.gif';?>
					<? endif; ?>

					<img src="<?=$icon?>" alt="<?=$product->title?>" />
				</a>
				</div>
				<div class="span6">
					<ul class="unstyled product-details">
						<li><a href="/<?=$product->path.'/'.$product->invt_id.'-'.$product->alias?>.html"><?=$product->title?></a></li>
						<li><strong><?=JTEXT::_('COM_BK_MODEL').' '.$product->invt_id?></strong></li>
						<li><p><?=((!empty($product->short_desc)) ? $product->short_desc : '&nbsp;');?></p></li>
					</ul>
				</div>
			</li>
		<?php endforeach; ?>
	</ul>
	<? if($this->pagination->total > $this->pagination->limit || JRequest::getVar('limit') == '*'): ?>

		<div class="pull-right pagination-limit">
			<label for="pgLimit"><?=JTEXT::_('COM_BK_PAGINATIONSHOW')?>:
				<select name="limit" id="pgLimit" class="span1">
					<option value="10"<?=((JRequest::getVar('limit') == '10') ? ' selected="selected"' : '' )?>>10</option>
					<option value="*"<?=((JRequest::getVar('limit') == '*') ? ' selected="selected"' : '' )?>><?=JTEXT::_('COM_BK_PAGINATIONSHOWALL')?></option>
				</select>
			</label>
		</div>
	<? endif; ?>

	<?=$this->pagination->getPagesLinks()?>
</div>
<?php if(count($this->filters)): ?>
<div class="filters-list">
<?php foreach($this->filters as $filter): ?>
	<div class="accordion-group">
		<div class="accordion-heading">
			<h5>
				<button class="btn btn-link" data-toggle="collapse" data-target="#<?=$filter->name?>"><?=JTEXT::_($filter->title)?></button>
			</h5>
		</div>
		<div class="collapse in accordion-body" id="<?=$filter->name?>">
			<ul class="unstyled">
				<? $_odd = '';?>
				<?php foreach($filter->values as $value): ?>
					<? $_odd = ('odd' != $_odd) ? 'odd' : 'even'; ?>
					<li class="<?=$_odd?>">
						<label class="checkbox inline category-filter" for="<?=$filter->name.$value?>">
							<input type="checkbox" id="<?=$filter->name.$value?>" name="<?=$filter->name?>" value="<?=$value?>" />
							<?=$value?>
						</label>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
<?php endforeach; ?>
</div>
<?php endif; ?>