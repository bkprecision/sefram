<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for Categories in the BKContent Component
 */
class BkcontentViewProducts extends JViewLegacy {
	// Overwriting JView display method
	function display($tpl = null) {
		$profiler = new JProfiler();

		// Assign data to the view
		$this->json 			= new stdClass;
		$this->json->parent 	= $this->get('Parent');
		$this->json->products	= $this->get('Products');
		$this->json->filters	= $this->get('Filters');
		$this->json->total		= $this->get('Total');
		$this->json->pagination	= $this->get('Pagination')->getPagesLinks();
		$this->json->profiler   = $profiler->mark(" profile info.");

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, json_encode($errors));
			return false;
		}

		echo json_encode($this->json);
	}
}