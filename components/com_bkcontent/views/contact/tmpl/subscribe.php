<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$doc = JFactory::getDocument(); //get the overall document 
$uri = JFactory::getURI(); // get the URI
$doc->addScriptDeclaration('bkp.contact = {url:"'.JRoute::_('index.php?option=com_bkcontent&view=contact&Itemid=157').'"};'."\r\n");
$doc->addScript("/templates/bkpre2011/js/contact-subscribe.js", "text/javascript", true, true);
?>

<h1>Subscribe to Our Newsletter</h1>
<p style="padding-left: 1px;">Please enter ALL of the requested information. <br />
You will receive a email requesting you reconfirm your request to Subscribe. Please click on the link in that email to complete your request.</p>
<p style="padding-left: 1px;">The intent of this newsletter is to keep our customers/end users apprized of new products and product related technical information.<br />
Our e-newsletter generally will be distributed two to four times per year. (<label><em>*</label></em> = required field)</p>
<div class="form">
	<form>
		<fieldset>
			<legend>Subscribe to our Periodic Newsletter</legend>
			<ul>
				<li>
					<label for="Email">Email Address:<em>*</em></label>
					<input type="text" name="Email" class="required" />
				</li>
				<li>
					<label for="Email2">Email Address Again:<em>*</em></label>
					<input type="text" name="Email2" class="required" />
				</li>
				<li>
					<label for="FirstName">First Name:<em>*</em></label>
					<input type="text" name="FirstName" class="required" />
				</li>
				<li>
					<label for="LastName">Last Name:<em>*</em></label>
					<input type="text" name="LastName" class="required" />
				</li>
				<li>
					<label for="Company_Name">Company Name:</label>
					<input type="text" name="Company_Name">
				</li>
		    </ul>
		   <button name="btnSubscribe" type="submit" id="btnSubscribe">Subscribe</button>
		</fieldset>
		<input type="hidden" name="format" value="json" />
		<input type="hidden" name="layout" value="subscribe" />
		<input type="hidden" name="communicationType" value="subscribe" />
    </form>
</div>
<div class="clr">&nbsp;</div>
<p style="padding-left: 1px;">If you would like to UNSUBSCRIBE to our newsletter, please <a href="<?=JRoute::_('index.php?option=com_bkcontent&view=contact&Itemid=158')?>">click here</a>.</p>