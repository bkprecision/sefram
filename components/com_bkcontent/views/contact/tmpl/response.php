<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$doc 	= JFactory::getDocument(); //get the overall document
$uri 	= JFactory::getURI(); // get the URI
$input  = JFactory::getApplication()->input;

$doc->setMetaData("data-main", "/media/com_bkcontent/js/pages/support/contact-survey.js");
$doc->addStyleSheet("/media/com_bkcontent/css/pages/support/contact-survey.css");
date_default_timezone_set('America/Los_Angeles');
?>

<h1>Contact Us — Customer Response Form</h1>
<h2 class="hide">B&amp;K Precision Corp.</h2>
<hr>
<?php if ( ! empty($this->messages)): ?>

<div class="row-fluid">
	<div class="span6">
		<p><strong>Contact Us Ratings Survey</strong></p>
		<p>How did we do? Please rate our response(s) to this particular case:</p>
	</div>
	<div class="span6">
		<div class="row-fluid">
			<p class="span6"><strong><span>Customer Messages</span> | <span>B&amp;K Messages</strong></p>
			<p class="span6"><strong>Case #<?php echo $input->get("caseid", "", "string"); ?></strong></p>
		</div>
	</div>
</div>

<div class="row-fluid">
	<div class="span6 rating">
		<form class="form-horizontal" id="survey-form">
			<fieldset>
				<div class="control-group">
					<label for="survey_rating1" class="radio inline">
						<input type="radio" name="survey_rating" id="survey_rating1" value="1" required>
						Not helpful
					</label>
					<label for="survey_rating2" class="radio inline">
						<input type="radio" name="survey_rating" id="survey_rating2" value="2" required>
						Somewhat helpful
					</label>
					<label for="survey_rating3" class="radio inline">
						<input type="radio" name="survey_rating" id="survey_rating3" value="3" required>
						Helpful
					</label>
					<label for="survey_rating4" class="radio inline">
						<input type="radio" name="survey_rating" id="survey_rating4" value="4" required>
						Very helpful
					</label>
					<label for="survey_rating5" class="radio inline">
						<input type="radio" name="survey_rating" id="survey_rating5" value="5" required>
						Completely satisfied
					</label>
				</div>

				<div class="control-group">
					<label for="survey_comment">Explanation or comments on your rating:</label>
					<textarea name="survey_comment" class="span12" id="survey_comment" cols="75" rows="8"></textarea>
				</div>

				<div class="control-group">
					<button type="submit" class="btn btn-primary">Submit Survey</button>
				</div>

			</fieldset>
			<input type="hidden" name="format" value="json">
			<input type="hidden" name="email" value="<?php echo $input->get("email", "", "string"); ?>">
			<input type="hidden" name="caseid" value="<?php echo $input->get("caseid", "", "string"); ?>">
		</form>
	</div>

	<div id="correspondence" class="span6">
		<table id="tblCorrespondence" class="table table-striped">
			<thead>
				<tr>
					<th colspan="2">Full Message History for This Case</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><?php echo $this->messages->original->body; ?></td>
					<td><?php echo date("j M y", strtotime(date("Y-m-d H:i:s", strtotime($this->messages->original->created_at). " GMT-9" ))); ?></td>
				</tr>
				<?php
					foreach ($this->messages->correspondence->_embedded->entries as $message):

						if ($message->status === "sent") :

							if ($message->from !== $this->messages->original->from):
				?>
				<tr class="info">
				<?php else: ?>
				<tr>
				<?php endif; ?>
					<td>
						<?php if ($message->from !== $this->messages->original->from): ?>
						B&amp;K:
						<?php endif; ?>
						<?php echo $message->body; ?>
					</td>
					<td><?php echo date("j M y", strtotime(date("Y-m-d H:i:s", strtotime($message->created_at) . " GMT-9"))); ?></td>
				</tr>
				<?php
						endif;
					endforeach;
				?>
			</tbody>
		</table>
	</div>
</div><!-- end row-fluid -->

<div class="modal hide fade" id="thank-you">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Thank you for your feedback</h3>
	</div>
	<div class="modal-body">
		<p>Thank you for taking the time to provide feedback. Here at B&amp;K Precision, we understand that your time is valuable. That is why we take feedback very seriously.</p>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-primary" data-dismiss="modal">Close</a>
	</div>
</div><!-- end modal -->

<?php else: ?>
<h3>We're sorry, but it seems your request did not include required user/case validation.
<br>Please use one of the links in the email you received from us to respond.</h3>
<p>If you feel you've received this message in error, please submit a message via <a href="/contact-us.html">this form</a>.</p>
<?php endif; ?>