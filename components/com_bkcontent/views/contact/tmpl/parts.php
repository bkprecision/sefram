<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$doc = JFactory::getDocument(); //get the overall document
$uri = JFactory::getURI(); // get the URI

$doc->setMetaData("data-main", "/media/com_bkcontent/js/pages/support/parts.js");
$doc->addStyleSheet("/media/com_bkcontent/css/pages/support/contact-us.css");
?>
<div class="row-fluid">
	<h1>Request Parts or Manuals</h1>
	<p>Please use this form ONLY for Parts or Manuals requests for B&amp;K Precision instruments/accessories.<br>
	For general requests, <a href="/contact-us.html">click here</a>. For Technical Support requests, <a href="/support/contact-technician.html"> click here</a>.</p>
	<hr>
	<p>To ensure we can contact you, please be sure your information is correct before submitting.</p>
</div>
<div class="row-fluid">

		<div class="alert alert-success submit-success hide">
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
		  <strong>Message sent!</strong> Your message has been sent. Your case ID is: <strong><span class="case-id"></span></strong>.
		</div>
		<div class="alert alert-error submit-error hide">
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
		  <strong>Message <em>NOT</em> sent!</strong> Your message has not been sent. There was an error submitting your message. Please, try again in a few moments.
		</div>
		<form class="form-horizontal contact-us-form">

			<div class="span3">
				<fieldset>
				<div class="control-group">
					<label class="control-label" for="Model_Number">
						Model Number:<br>
						<small>Please enter exact <strong>model number</strong> only</small>
					</label>
					<div class="controls">
						<input
							type="text"
							name="Model_Number"
							id="Model_Number"
							placeholder="2516"
							required
							data-provide="typeahead"
							data-items="4"
							data-source="[<?= $this->products; ?>]">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Document Request:</label>
					<div class="controls">
						<label class="control-label checkbox" for="Instructional_Manual">
							Instructional Manual
							<input type="checkbox" name="manual_requests[]" value="Instructional Manual" id="Instructional_Manual" required>
						</label>
						<label class="control-label checkbox" for="Schematic">
							Schematic
							<input type="checkbox" name="manual_requests[]" value="Schematic" id="Schematic" required>
						</label>
						<label class="control-label checkbox" for="Calibration_Procedure">
							Calibration Procedure
							<input type="checkbox" name="manual_requests[]" value="Calibration Procedure" id="Calibration_Procedure" required>
						</label>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="Message_Detail">Parts Request/Comment:</label>
					<div class="controls">
						<textarea name="Message_Detail" id="Message_Detail" cols="34" rows="10" required></textarea>
					</div>
				</div>
				</fieldset>
			</div>
			<div class="span7 offset2">
				<fieldset>
				<div class="control-group">
					<label class="control-label" for="FirstName">First Name:</label>
					<div class="controls">
						<input type="text" name="FirstName" id="FirstName" placeholder="Wile E." required class="span9">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="LastName">Last Name:</label>
					<div class="controls">
						<input type="text" name="LastName" id="LastName" placeholder="Coyote" required class="span9">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="Email">Your Email:</label>
					<div class="controls">
						<input type="email" name="Email" id="Email" placeholder="wilecoyote@acme.com" required class="span9">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="Phone_Number">Phone:</label>
					<div class="controls">
						<input type="text" name="Phone_Number" id="Phone_Number" placeholder="123-456-7890" class="span9">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="Company_Name">Your Company:</label>
					<div class="controls">
						<input type="text" name="Company_Name" id="Company_Name" placeholder="ACME Corp." class="span9">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="Job_Title">Job Title:</label>
					<div class="controls">
						<input type="text" name="Job_Title" id="Job_Title" placeholder="Road Runner Hunter" class="span9">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="Country">Your Country:</label>
					<div class="controls">
						<select name="Country" id="Country" required class="span9">
							<option value="">Make a selection</option>
							<?php foreach($this->countries as $country): ?>

							<option value="<?=$country->CountryName?>"><?=$country->CountryName?></option>
							<?php endforeach; ?>

						</select>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<div id="dynamic_recaptcha_1" class="span9"></div>
					</div>
				</div>

				<div class="control-group">
					<div class="controls">
						<button name="btnContactUs" type="submit" id="btnSubmitMsg" class="btn btn-primary">
							<span class="btn-txt">Submit Message</span>
							<span class="ion-loading-d hide" data-animation="true"></span>
						</button>
					</div>
				</div>
				</fieldset>
			</div>

			<input type="hidden" name="communicationType" value="Parts_Manual_Requests">
			<input type="hidden" name="status" value="O">
			<input type="hidden" name="format" value="json">
	</form>

</div><!-- /form row -->
