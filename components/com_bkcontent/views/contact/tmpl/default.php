<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$doc 	= JFactory::getDocument(); //get the overall document
$uri 	= JFactory::getURI(); // get the URI
$input  = JFactory::getApplication()->input;

$doc->setMetaData("data-main", "/media/com_bkcontent/js/pages/support/contact-us.js");
$doc->addStyleSheet("/media/com_bkcontent/css/pages/support/contact-us.css");

$requestedComType = $input->get("communicationType", "", "string");
?>
<div class="row-fluid">
	<h1>Contact Us: General Requests</h1>
	<p>If you are seeking technical support on any level, or parts/manuals, please <a href="/support.html">click over to our Support page</a>.</p>
	<hr>
</div>
<div class="row-fluid">
	<div class="span3 offset2 bk-address">
		<address>
			<strong>B&amp;K Precision Corporation</strong> (<a href="http://maps.google.com/maps?f=q&amp;hl=en&amp;geocode=&amp;q=22820+Savi+Ranch+Parkway,+Yorba+Linda,+CA+92887&amp;sll=33.875671,-117.737968&amp;sspn=0.011687,0.015986&amp;ie=UTF8&amp;ll=33.875671,-117.737968&amp;spn=0.011687,0.015986&amp;z=16&amp;iwloc=addr">MAP US</a>)<br>
			22820 Savi Ranch Parkway<br>
			Yorba Linda, CA 92887-4610, USA<br><br>
			714-921-9095<br>
			714-921-6422 (fax)<br>
			800-462-9832 (US &amp; CAN toll free)
		</address>
	</div>
	<div class="span7">
		<div class="alert alert-success submit-success hide">
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
		  <strong>Message sent!</strong> Your message has been sent. Your case ID is: <strong><span class="case-id"></span></strong>.
		</div>
		<div class="alert alert-error submit-error hide">
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
		  <strong>Message <em>NOT</em> sent!</strong> Your message has not been sent. There was an error submitting your message. Please, try again in a few moments.
		</div>
		<form class="form-horizontal contact-us-form">
			<fieldset>
				<div class="control-group">
					<label class="control-label" for="communicationType">Communication Type:</label>
					<div class="controls">
						<select name="communicationType" id="communicationType" required class="span9">
						<?php if(isset($requestedComType) AND !empty($requestedComType)): ?>

							<option value="<?=$requestedComType?>"><?=$requestedComType?></option>
						<?php else: ?>

							<option value="">Please Select One</option>
							<option value="General_Unspecified">General/Unspecified</option>
							<option value="Education">Education</option>
							<option value="Media_Contact">Media Contact</option>
							<option value="Sales_Distribution">Sales/Distribution</option>
							<option value="Suggestion">Suggestion</option>
							<option value="Other">Other</option>
						<?php endif; ?>

						</select>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="FirstName">First Name:</label>
					<div class="controls">
						<input type="text" name="FirstName" id="FirstName" placeholder="Wile E." required class="span9">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="LastName">Last Name:</label>
					<div class="controls">
						<input type="text" name="LastName" id="LastName" placeholder="Coyote" required class="span9">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="Email">Your Email:</label>
					<div class="controls">
						<input type="email" name="Email" id="Email" placeholder="wilecoyote@acme.com" required class="span9">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="Phone_Number">Phone:</label>
					<div class="controls">
						<input type="text" name="Phone_Number" id="Phone_Number" placeholder="123-456-7890" class="span9">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="Company_Name">Your Company:</label>
					<div class="controls">
						<input type="text" name="Company_Name" id="Company_Name" placeholder="ACME Corp." class="span9">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="Job_Title">Job Title:</label>
					<div class="controls">
						<input type="text" name="Job_Title" id="Job_Title" placeholder="Road Runner Hunter" class="span9">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="Country">Your Country:</label>
					<div class="controls">
						<select name="Country" id="Country" required class="span9">
							<option value="">Make a selection</option>
							<?php foreach($this->countries as $country): ?>

							<option value="<?=$country->CountryName?>"><?=$country->CountryName?></option>
							<?php endforeach; ?>

						</select>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="Message_Detail">Message:</label>
					<div class="controls">
						<textarea name="Message_Detail" rows="4" required class="span9"></textarea>
					</div>
				</div>

				<div class="control-group">
					<div class="controls">
						<div id="dynamic_recaptcha_1" class="span9"></div>
					</div>
				</div>

				<div class="control-group">
					<div class="controls">
						<button name="btnContactUs" type="submit" id="btnSubmitMsg" class="btn btn-primary">
							<span class="btn-txt">Submit Message</span>
							<span class="ion-loading-d hide" data-animation="true"></span>
						</button>
					</div>
				</div>

			</fieldset>
			<input type="hidden" name="status" value="O" />
			<input type="hidden" name="format" value="json" />
	    </form>
	</div>

</div>
