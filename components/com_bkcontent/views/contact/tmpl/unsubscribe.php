<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$doc = JFactory::getDocument(); //get the overall document 
$uri = JFactory::getURI(); // get the URI
$doc->addScriptDeclaration('bkp.contact = {url:"'.JRoute::_('index.php?option=com_bkcontent&view=contact&Itemid=158').'"};'."\r\n");
$doc->addScript("/templates/bkpre2011/js/contact-unsubscribe.js", "text/javascript", true, true);
?>

<h1>Unsubscribe to Our Newsletter</h1>
<p style="padding-left: 1px;">Please correctly enter the email address with which you are subscribed so we may properly process your request to UNSUBSCRIBE from our Newsletter.</p>
<p style="padding-left: 1px;">(<label><em>*</label></em> = required field)</p>
<div class="form">
	<form>
		<fieldset>
			<legend>Unsubscribe from Our Periodic Newsletter</legend>
			<ul>
				<li>
					<label for="Email">Email Address:<em>*</em></label>
					<input type="text" name="Email" class="required" />
				</li>
				<li>
					<label for="Email2">Email Address Again:<em>*</em></label>
					<input type="text" name="Email2" class="required" />
				</li>
		    </ul>
		   <button name="btnUnsubscribe" type="submit" id="btnUnsubscribe">Unsubscribe</button>
		</fieldset>
		<input type="hidden" name="format" value="json" />
		<input type="hidden" name="layout" value="unsubscribe" />
		<input type="hidden" name="communicationType" value="unsubscribe" />
    </form>
</div>
<div class="clr">&nbsp;</div>