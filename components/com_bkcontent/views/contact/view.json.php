<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for Contact
 */
class BkcontentViewContact extends JViewLegacy {
	// Overwriting JView display method
	function display($tpl = null) {

		$currentLayout 	= $this->getLayout();
		if ($currentLayout != 'response') {
			if ($currentLayout != 'subscribe' AND $currentLayout != 'unsubscribe') {
				$this->form 	= $this->get('FormSubmit');
			}
			else if ($currentLayout == 'subscribe') {
				$this->form 	= $this->get('NewSubscription');
			}
			else if ($currentLayout == 'unsubscribe') {
				$this->form 	= $this->get('Unsubscribe');
			}
		}
		else {
			$this->form = $this->get("Survey");
		}
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// if we have errors set the status code to 401
		if ($this->form->success === false) {
			$web = JApplicationWeb::getInstance();
			header('HTTP/1.1 401 Unauthorized', true, 401);
			$web->sendHeaders();
		}

		// Change the layout from default to json
		$this->setLayout('json');

		// Display the view
		parent::display($tpl);
	}

	function getUserRating($rating = null) {

		if (isset($rating)) {
			switch ($rating) {
				case '1':
					$readableRating = "Not Helpful";
					break;
				case '2':
					$readableRating = "Somewhat Helpful";
					break;
				case '3':
					$readableRating = "Helpful";
					break;
				case '4':
					$readableRating = "Very Helpful";
					break;
				case '5':
					$readableRating = "Completely satisfied with the response";
					break;
				default:
					$readableRating = "Error - Not Given";
			}
		}

		return $readableRating;
	}

}