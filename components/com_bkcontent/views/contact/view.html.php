<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for Where to Buy
 */
class BkcontentViewContact extends JViewLegacy {
	// Overwriting JView display method
	function display($tpl = null) {

		JPluginHelper::importPlugin('captcha');
		$dispatcher = JDispatcher::getInstance();
		$dispatcher->trigger('onInit');

		$this->countries 	= $this->get('Countries');
		$this->products 	= "&quot;".implode("&quot;,&quot;", $this->get('Products'))."&quot;";
		$this->form			= $this->get('Form');
		$layout 			= $this->getLayout();

		if ($layout == 'response') {
			$this->messages = $this->get('MessageDetail');
		}
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Display the view
		parent::display($tpl);
	}

	function getUserRating($rating = null) {

		if ( isset($rating)) {
			switch($rating) {
				case '1':
					$readableRating = "Not Helpful";
					break;
				case '2':
					$readableRating = "Somewhat Helpful";
					break;
				case '3':
					$readableRating = "Helpful";
					break;
				case '4':
					$readableRating = "Very Helpful";
					break;
				case '5':
					$readableRating = "Completely satisfied with the response";
					break;
				default:
					$readableRating = "Error - Not Given";
			}
		}

		return $readableRating;
	}

}