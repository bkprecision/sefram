<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for Customers
 */
class BkcontentViewCustomers extends JViewLegacy {
	// Overwriting JView display method
	function display($tpl = null) {
		$layout = JFactory::getApplication()->input->get('layout');
		$this->layout = $this->get($layout);
		if ($this->layout == 'products' OR $this->layout == 'csaproducts') {
			$this->json			= $this->get('Products');
			$layout				= 'json';
		}
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Change the layout from default to what is provided
		$this->setLayout($layout);

		// Display the view
		parent::display($tpl);
	}

}