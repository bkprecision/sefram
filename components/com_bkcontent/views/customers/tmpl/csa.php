<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$doc = JFactory::getDocument(); //get the overall document 
$uri = JFactory::getURI(); // get the URI
$doc->addStyleSheet("/templates/bkpre2011/css/ui.jqgrid.css");
$doc->addScriptDeclaration('bkp.customer = {url:"'.JRoute::_('index.php?option=com_bkcontent&view=customers&Itemid=152').'"};'."\r\n");
$doc->addScript("/templates/bkpre2011/js/customers-csa.js", "text/javascript", true, true);
?>

<h1>CSA Approved Inventory</h1>
<div>
	<table id="stockGrid"></table>
	<div id="stockPgr"></div>
</div>