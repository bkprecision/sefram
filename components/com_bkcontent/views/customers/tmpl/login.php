<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$doc = JFactory::getDocument(); //get the overall document

$doc->addStyleSheet("/media/com_bkcontent/css/pages/customers/login.css");
?>

<div class="row-fluid">
	<h1>Existing Distributor Login</h1>
</div>
<div class="row-fluid">
	<div class="span6 offset3">
		<form class="form-horizontal" method="post" action="">
			<fieldset>
				<div class="control-group">
					<label class="control-label" for="customer_id">Account:</label>
					<div class="controls">
						<input type="text" name="customer_id" id="customer_id">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="passwd">Password:</label>
					<div class="controls">
						<input type="password" name="passwd" id="passwd">
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button class="btn btn-primary" type="submit">Login</button>
					</div>
				</div>
		    </fieldset>
		</form>
		<div class="additional-info">
		    <p><a href="/logo-usage.html">B&amp;K Precision Logo Download/Usage Guidelines</a></p>
		    <p><a href="/distributor-access/guest.html">Guest Access</a> | Call (714) 921-9095 to request an account.</p>
	    	<p><a href="/where-to-buy/csa-inventory.html"><img src="images/csa_bk_inventory.png" border="0" alt="Click here to view CSA approved B&amp;K Precision inventory in Canada." /></a></p>
		</div>
	</div>
</div>