<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$doc = JFactory::getDocument(); //get the overall document

$doc->setMetaData("data-main", "/media/com_bkcontent/js/pages/customers/guest.js");
$doc->addStyleSheet("/media/com_bkcontent/css/pages/customers/guest.css");
$doc->addStyleSheet("/media/com_bkcontent/css/dataTables-bootstrap.css");
?>

<?php if ($this->layout == "guest"): ?>
<h1>Inventory Access for Guests</h1>
<?php elseif ($this->layout == "csa"): ?>
<h1>CSA Approved Inventory</h1>
<?php endif; ?>
<div>
	<table id="stockGrid" class="table table-striped">
		<thead>
			<tr>
				<th>Model #</th>
				<th>Description</th>
				<th>Availability</th>
				<th>List Price</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($this->stock as $stock): ?>

			<tr>
				<td><?php echo $stock->invt_id; ?></td>
				<td><?php echo $stock->title; ?></td>
				<td><?php echo ( ( $stock->qty_avail > 0 ) ? "In " : "Out of "); ?>Stock</td>
				<td>
					<?php if ($stock->list_price > 0): ?>
					$<?php echo number_format($stock->list_price, 2); ?>
					<?php else: ?>
					N/A
					<?php endif; ?>
				</td>
			</tr>

			<?php endforeach; ?>

		</tbody>
	</table>
</div>