<?php if($this->layout->success): ?>
<table>
	<thead>
		<tr>
			<th colspan="2">Contact Information</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width: 25%;"><strong>Name:</strong></td>
			<td style="width: 75%;"><?=$this->layout->detail->Name?></td>
		</tr>
		<tr>
			<td style="width: 25%;"><strong>Email:</strong></td>
			<td style="width: 75%;"><?=$this->layout->detail->EMailAddr?></td>
		</tr>
		<tr>
			<td style="width: 25%;"><strong>Address:</strong></td>
			<td style="width: 75%;"><?=$this->layout->detail->Addr?></td>
		</tr>
		<tr>
			<td style="width: 25%;"><strong>Address2:</strong></td>
			<td style="width: 75%;"><?=$this->layout->detail->Addr2?></td>
		</tr>
		<tr>
			<td style="width: 25%;"><strong>Attention:</strong></td>
			<td style="width: 75%;"><?=$this->layout->detail->Attn?></td>
		</tr>
		<tr>
			<td style="width: 25%;"><strong>Phone:</strong></td>
			<td style="width: 75%;"><?=$this->layout->detail->Phone?></td>
		</tr>
		<tr>
			<td style="width: 25%;"><strong>Fax:</strong></td>
			<td style="width: 75%;"><?=$this->layout->detail->Fax?></td>
		</tr>
	</tbody>
</table>
<table>
	<thead>
		<tr>
			<th colspan="2">Billing Information</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="width: 25%;"><strong>Name:</strong></td>
			<td style="width: 75%;"><?=$this->layout->detail->BillName?></td>
		</tr>
		<tr>
			<td style="width: 25%;"><strong>Attention:</strong></td>
			<td style="width: 75%;"><?=$this->layout->detail->BillAttn?></td>
		</tr>
		<tr>
			<td style="width: 25%;"><strong>Address:</strong></td>
			<td style="width: 75%;"><?=$this->layout->detail->BillAddr?></td>
		</tr>
		<tr>
			<td style="width: 25%;"><strong>Address2:</strong></td>
			<td style="width: 75%;"><?=$this->layout->detail->BillAddr2?></td>
		</tr>
		<tr>
			<td style="width: 25%;"><strong>Phone:</strong></td>
			<td style="width: 75%;"><?=$this->layout->detail->BillPhone?></td>
		</tr>
		<tr>
			<td style="width: 25%;"><strong>Fax:</strong></td>
			<td style="width: 75%;"><?=$this->layout->detail->BillFax?></td>
		</tr>
		<tr>
			<td style="width: 25%;"><strong>City:</strong></td>
			<td style="width: 75%;"><?=$this->layout->detail->BillCity?></td>
		</tr>
		<tr>
			<td style="width: 25%;"><strong>State:</strong></td>
			<td style="width: 75%;"><?=$this->layout->detail->BillState?></td>
		</tr>
		<tr>
			<td style="width: 25%;"><strong>Zip/Postal Code:</strong></td>
			<td style="width: 75%;"><?=$this->layout->detail->BillZip?></td>
		</tr>
		<tr>
			<td style="width: 25%;"><strong>Country:</strong></td>
			<td style="width: 75%;"><?=$this->layout->detail->BillCountry?></td>
		</tr>
		<tr>
			<td colspan="2" class="alignCenter">
				Please review your contact information for accuracy. <br />
				Contact us toll-free at (800) 462-9832 to update your profile.
			</td>
		</tr>
	</tbody>
</table>
<?php else: ?>
<p><?=$this->layout->errorMsg?></p>
<?php endif; ?>