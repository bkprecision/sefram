<h2>Back-to-School Promotion</h2>
<ul>
	<li><a href="/downloads/pdf/BK-Precision-Ed-Promo-Eligibility-Form.pdf" target="_blank">Eligibility Document</a> (PDF file)</li>
	<li><a href="/downloads/pdf/BK-Precision-Ed-Promo-Dist-Terms-Conditions.pdf" target="_blank">Terms &amp; Conditions</a> (PDF file)</li>
</ul>