<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');


$doc = JFactory::getDocument(); //get the overall document

$doc->setMetaData("data-main", "/media/com_bkcontent/js/pages/customers/customers.js");
$doc->addStyleSheet("/media/com_bkcontent/css/pages/customers/customers.css");
$doc->addStyleSheet("/media/com_bkcontent/css/dataTables-bootstrap.css");
?>

<div class="row-fluid">
	<h1>Welcome <?php echo $this->user->name; ?></h1>
</div>
<div class="row-fluid">
	<button id="logout" class="btn btn-primary pull-right">Logout</button>
	<ul class="nav nav-tabs">
		<li class="active" ><a href="#overview" data-toggle="tab">Overview</a></li>
		<li><a href="#tools" data-toggle="tab">Your Downloads</a></li>
		<li><a href="#orders" data-toggle="tab">Your Orders</a></li>
		<li><a href="#stock" data-toggle="tab">Stock Check</a></li>
	</ul>

	<div class="tab-content">

		<div id="overview" class="tab-pane overview active">
				<?php echo $this->params->get("overview"); ?>
		</div><!-- end overview tab -->

		<div id="tools" class="tab-pane tools">
			<?php
				if ($this->user->domestic) {
					echo $this->params->get("domestic");
				}
				else {
					echo $this->params->get("international");
				}
			?>
		</div><!-- end tools tab -->

		<div id="orders" class="tab-pane orders">

			<div class="span4">
				<h3>Sales Order List</h3>
				<table id="salesOrderList" class="table table-striped">
					<thead>
						<tr>
							<th>Purchase Order</th>
							<th>Sales Order</th>
						</tr>
					</thead>
					<tbody>
						<?php if (!empty($this->orders)): ?>
							<?php foreach ($this->orderNumbers as $customerNumber => $orderNumber): ?>

							<tr id="<?php echo $orderNumber; ?>">
								<td><?php echo $customerNumber; ?></td>
								<td><?php echo $orderNumber; ?></td>
							</tr>
							<?php endforeach; ?>
						<?php else: ?>

						<tr><td colspan="2">No order history to display.</td></tr>
						<?php endif; ?>

					</tbody>
				</table>
			</div>

			<div class="span8">
				<div class="row-fluid">
					<h3>Order Summary</h3>
					<div id="orderSummary">
						<?php
							if (!empty($this->orders)):
								$order = $this->orders[0];
						?>

							<table class="table table-bordered">
								<tbody>
									<tr>
										<th>Customer ID</th>
										<td><?php echo $order->CustId; ?></td>
									</tr>
									<tr>
										<th>Sales Order</th>
										<td><?php echo $order->OrdNbr; ?></td>
									</tr>
									<tr>
										<th>Purchase Order</th>
										<td><?php echo $order->CustOrdNbr; ?></td>
									</tr>
									<tr>
										<th>Order Date</th>
										<td><?php echo $order->OrdDate; ?></td>
									</tr>
									<tr>
										<th>Shipping Tracking Number(s)</th>
										<td></td>
									</tr>
									<tr>
										<th>Shipping Method</th>
										<td><?php echo $order->ShipVia; ?></td>
									</tr>
									<tr>
										<th>Order Total</th>
										<td>$<?php echo $order->CuryTotOrd; ?></td>
									</tr>
								</tbody>
							</table>
						<?php endif; ?>
					</div>
				</div>
				<div class="row-fluid">
					<h3>Sales Order Line Items</h3>
					<div id="lineItems">
						<?php if (!empty($this->orders)): ?>
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>Invetory ID</th>
										<th>Ordered</th>
										<th>Qty Shipped</th>
										<th>MSRP</th>
										<th>Line Item Total</th>
									</tr>
								</thead>
								<tbody>
						<?php
								$orderNbr  = $this->orders[0];
								$orderNbr  = $orderNbr->OrdNbr;
								$lineItems = array();

								foreach ($this->orders as $order):
									if ($orderNbr == $order->OrdNbr):
						?>

									<tr>
										<td><?php echo $order->InvtId; ?></td>
										<td><?php echo $order->QtyOrd; ?></td>
										<td><?php echo $order->QtyShip; ?></td>
										<td><?php echo $order->CurySlsPrice; ?></td>
										<td><?php echo $order->ExtPrice; ?></td>
									</tr>
						<?php
									endif;
								endforeach;
								?>
								</tbody>
							</table>
						<?php endif; ?>
					</div>
				</div>
			</div>

		</div><!-- end orders tab -->

		<div id="stock" class="tab-pane stock">
			<table id="stockGrid" class="table table-striped">
				<thead>
					<tr>
						<th>Model #</th>
						<th>Description</th>
						<th>Availability</th>
						<th>List Price</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($this->stock as $stock): ?>

					<tr>
						<td><?php echo $stock->invt_id; ?></td>
						<td><?php echo $stock->title; ?></td>
						<td><?php echo ( ( $stock->qty_avail > 0 ) ? "In " : "Out of "); ?>Stock</td>
						<td>
							<?php if ($stock->list_price > 0): ?>
							$<?php echo number_format($stock->list_price, 2); ?>
							<?php else: ?>
							N/A
							<?php endif; ?>
						</td>
					</tr>

					<?php endforeach; ?>

				</tbody>
			</table>
		</div><!-- end stock tab -->

	</div><!-- end tab-content -->


</div>
<script type="text/javascript">
var BKP = BKP || {};

BKP.orders = <?php echo json_encode($this->orders, JSON_NUMERIC_CHECK); ?>;
BKP.tracking = <?php echo json_encode($this->trackingNumbers); ?>;
</script>
<script id="order-summary-tmpl" type="text/x-handlebars-template">
	<table class="table table-bordered">
		<tbody>
			<tr>
				<th>Customer ID</th>
				<td>{{CustId}}</td>
			</tr>
			<tr>
				<th>Sales Order</th>
				<td>{{OrdNbr}}</td>
			</tr>
			<tr>
				<th>Purchase Order</th>
				<td>{{CustOrdNbr}}</td>
			</tr>
			<tr>
				<th>Order Date</th>
				<td>{{OrdDate}}</td>
			</tr>
			<tr>
				<th>Shipping Tracking Number(s)</th>
				<td>{{tracking OrdNbr}}</td>
			</tr>
			<tr>
				<th>Shipping Method</th>
				<td>{{ShipVia}}</td>
			</tr>
			<tr>
				<th>Order Total</th>
				<td>${{msrp CuryTotOrd}}</td>
			</tr>
		</tbody>
	</table>
</script>
<script id="line-items-tmpl" type="text/x-handlebars-template">
<tbody>
{{#each orders}}
	<tr>
		<td>{{InvtId}}</td>
		<td>{{QtyOrd}}</td>
		{{#if QtyShip}}
			<td>{{QtyShip}}</td>
		{{else}}
			<td>Est. Ship Date: {{EtaDate}}</td>
		{{/if}}
		<td>{{msrp CurySlsPrice}}</td>
		<td>{{msrp ExtPrice}}</td>
	</tr>
{{/each}}
</tbody>
</script>