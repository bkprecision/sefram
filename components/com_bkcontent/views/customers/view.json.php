<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for Customers
 */
class BkcontentViewCustomers extends JViewLegacy {
	// Overwriting JView display method
	function display($tpl = null) {

		$this->json 			= new stdClass();
		$input					= JFactory::getApplication()->input;
		$reqType				= $input->get('type');
		$this->json->layout 	= $reqType;
		$currentLayout			= $this->json->layout;
		if ($currentLayout == 'orders') {
			$this->json			= $this->get('Orders');
		}
		else if ($currentLayout == 'order_detail') {
			$this->json			= $this->get('OrderDetail');
		}
		else if ($currentLayout == 'products' OR $currentLayout == 'csaproducts') {
			$this->json			= $this->get('Products');
		}
		else {
			$this->json 			= $this->get('Customer');
		}

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Change the layout from default to json
		$this->setLayout('json');

		// Display the view
		parent::display($tpl);
	}

}