<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for Customers
 */
class BkcontentViewCustomers extends JViewLegacy {

	// Overwriting JView display method
	function display($tpl = null) {

		$layout 	  = $this->getLayout();
		$this->layout = $layout;
		$model 		  = $this->getModel();
		$this->stock  = $this->get("GuestStock");
		$input		  = JFactory::getApplication()->input;
		$logout		  = $input->get("command");

		if (!empty($logout)) {
			$model->logout();
		}

		$session	  = JFactory::getSession();

		$this->user   = $session->get("customer.user");

		if (empty($this->user->email)) {
			$this->user   = $this->get("User");
		}

		$isGuest	  = $this->user->guest ? TRUE : FALSE;

		if ($layout == "guest" OR $layout == "csa") {
			$this->setLayout("guest");
		}
		else if ($isGuest) {
			$this->setLayout("login");
		}
		else {
			$this->orders 			= $this->get("Orders");
			$this->orderNumbers 	= $this->get("OrderNumbers");
			$this->trackingNumbers 	= $this->get("TrackingNumbers");
			$this->params			= JFactory::getApplication()->getParams();
		}

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Display the view
		parent::display($tpl);
	}

}