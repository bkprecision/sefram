<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>
<h1><?php echo $this->parent->title; ?></h1>
<h2 class="hidden">B&amp;K Precision</h2>
<ul class="accessories">
	<?php foreach($this->accessories as $accessory): ?>
<? if(file_exists(getcwd().DS.'images'.DS.'products'.DS.'photos'.DS.'icons'.DS.$accessory->invt_id.'.gif')): ?>
<? $icon = '/images/products/photos/icons/'.$accessory->invt_id.'.gif';?>
<? else: ?>
<? $icon = '/images/products/photos/icons/noimage.gif';?>
<? endif; ?>
		<li>
			<div class="alignCenter preview">
				<? if(file_exists(getcwd().DS.'images'.DS.'products'.DS.'photos'.DS.'accs'.DS.$accessory->invt_id.'.gif')): ?>
				<a href="/images/products/photos/accs/<?=$accessory->invt_id?>.gif" class="productThumb alignCenter fancybox" id="<?=$accessory->invt_id?>"><img src="<?=$icon?>" alt="<?php echo $accessory->title; ?>" /></a>
				<? else: ?>
				<a href="#" class="productThumb alignCenter" id="<?=$accessory->invt_id?>"><img src="<?=$icon?>" alt="<?php echo $accessory->title; ?>" /></a>
				<? endif; ?>
				<a href="<?=JRoute::_('index.php?option=com_bkcontent&view=wtb&Itemid=120&invt_id='.$accessory->invt_id)?>" title="Where to Buy">MSRP $<?php echo number_format($accessory->list_price, 2); ?></a>		
			</div>
			<ul>
				<li><?php echo $accessory->title; ?></li>
				<li class="modelNum">Model# <?php echo $accessory->invt_id; ?></li>
				<? if(file_exists(getcwd().DS.'downloads'.DS.'manuals'.DS.'en'.DS.$accessory->invt_id.'_manual.pdf')): ?>
				<? $accmanual = '/downloads/manuals/en/'.$accessory->invt_id.'_manual.pdf';?>
				<li><a href="<?php echo $accmanual; ?>">Download <?php echo "$accessory->invt_id"; ?> Manual</a></li>
				<? endif; ?>

				<li class="shortDesc"><p><?=((!empty($accessory->description)) ? $accessory->description : '&nbsp;');?></p></li>
			</ul>
			<div class="clr"></div>
		</li>
	<?php endforeach; ?>
	<div class="clr"></div>
</ul>
<div class="pagination">
<?php  echo $this->pagination->getPagesLinks(); ?>
</div>