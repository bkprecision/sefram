<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for Accessories in the BKContent Component
 */
class BkcontentViewAccessories extends JViewLegacy {

	// Overwriting JView display method
	function display($tpl = null) {
		// Assign data to the view
		$this->parent 		= $this->get('Parent');
		$this->accessories	= $this->get('Accessories');
		$this->total		= $this->get('Total');
		$this->pagination	= $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Display the view
		parent::display($tpl);
	}
}