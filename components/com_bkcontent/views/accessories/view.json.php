<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for Categories in the BKContent Component
 */
class BkcontentViewAccessories extends JViewLegacy {

	// Overwriting JView display method
	function display($tpl = null) {

		// Assign data to the view
		$this->json 			 = new stdClass;
		$this->json->parent 	 = $this->get('Parent');
		$this->json->accessories = $this->get('Accessories');
		$this->json->total		 = $this->get('Total');
		$this->json->pagination	 = $this->get('Pagination')->getPagesLinks();

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, json_encode($errors));
			return false;
		}

		echo json_encode($this->json);
	}
}

