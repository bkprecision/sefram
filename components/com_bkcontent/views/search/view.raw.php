<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

class BkcontentViewSearch extends JViewLegacy {

	function display($tpl = null) {

		$input 	  = JFactory::getApplication()->input;

		$kbSearch = $input->get('kb');
		if ($kbSearch == 'true') {
			$kb_search_url 	= 'http://kb.bkprecision.com/htmlsearch.php';
			$_qry 			= $input->get('q');
			$search_query 	= 'q=';
			$true_bit 		= false;
			if (is_array($_qry)) {
				foreach ($_qry as $row) {
					if($true_bit) $search_query .= '&q=';
					$search_query .= $row;
					$true_bit = true;
				}
			}
			else {
				$search_query .= $_qry;
			}
			$search_type 		= 'type=';
			$_type 				= $input->get('type', 'ars');
			$search_category 	= 'c=';
			$_c 				= $input->get('c', '0');
			$kb_search_url 		= $kb_search_url."?type=ars&c=0&".$search_query;
			$proxy_handler 		= fopen($kb_search_url, "r");
			if ($proxy_handler) {
				while( ! feof($proxy_handler)) {
					$buffer 	= fgets($proxy_handler, 4096);
					echo $buffer;
				}
				fclose($proxy_handler);
			}
		}
		else {
			parent::display($tpl);
		}
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}



	}

}