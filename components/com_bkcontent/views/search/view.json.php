<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

class BkcontentViewSearch extends JViewLegacy {

	function display($tpl = null) {

		// Assign data to the view
		$this->json = new stdClass();

		$this->json = $this->get('GSAResults');

		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		$this->setLayout('json');

		parent::display($tpl);

	}

}