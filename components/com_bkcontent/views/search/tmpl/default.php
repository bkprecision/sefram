<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$doc = JFactory::getDocument();
$app = JFactory::getApplication();
$doc->setMetaData("data-main", "/media/com_bkcontent/js/pages/search/main.js");
$doc->addStyleSheet("/media/com_bkcontent/css/pages/search/styles.css");

$query = $app->input->get("q", "", "string");
$start = $app->input->get("start", 1, "int");
$menu  = $app->getMenu()->getActive();
?>
<div class="row-fluid">
	<h1><?php echo $menu->title; ?></h1>
</div>
<div class="row-fluid">
	<form autocomplete="off" id="extSearchForm">
		<fieldset>
			<div class="controls controls-row">
				<input type="text" id="input-box" name="q" class="span11" value="<?php echo $query; ?>" placeholder="Search www.sefram.com" required>
				<button id="btnQuery" type="submit" class="btn btn-primary span1">Search</button>
			</div>
		</fieldset>
		<input type="hidden" value="<?php echo $start; ?>" name="start" id="startIndex">
	</form>
</div>
<div class="row-fluid">
	<div id="results"></div>
	<div id="enterSearchTerm" class="<?php echo (empty($query)) ? "" : "hide"; ?>">Enter a search term in the field above</div>
	<div id="xhrLoading" class="<?php echo ( ! empty($query)) ? "" : "hide"; ?>">
		<div class="ajax-loader ion-loading-d"></div>
		<div class="loading-text">Loading Results</div>
	</div>
</div>
<script id="ext-results-tmpl" type="text/x-handlebars-template">
<div id="resultsCount">About {{searchInformation.totalResults}} results found for '<?php echo $query; ?>'.</div>
<ul class="unstyled">
{{#each items}}
	<li
	{{#if @first}}
	 class="first"
	{{/if}}
	{{#if @last}}
	 class="last"
	{{/if}}
	>
		{{link this}}
		<div class="desc">{{cleanString snippet}}</div>
		<div class="resource">{{link link=this.link title=formattedUrl}}</div>
	</li>
{{/each}}
</ul>
<div class="pagination pagination-centered pagination-large">{{pagination}}</div>
</script>
