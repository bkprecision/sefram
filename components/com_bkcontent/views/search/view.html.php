<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

class BkcontentViewSearch extends JViewLegacy {

	// Overwriting JView display method
	function display($tpl = null) {

		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Display the view
		parent::display($tpl);
	}

}