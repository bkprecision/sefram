<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for Support
 */
class BkcontentViewSupport extends JViewLegacy {

	// Overwriting JView display method
	function display($tpl = null) {

		$input 		 	= JFactory::getApplication()->input;
		$this->state 	= $this->get("State");
		$type			= "get".ucwords($input->getString("supportType"));
		$invt_id 		= $input->getString("invt_id");
		$model 			= $this->getModel();

		$this->json 	= $model->$type($invt_id);

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {

			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Change the layout from default to json
		$this->setLayout("json");

		// Display the view
		parent::display($tpl);
	}
}