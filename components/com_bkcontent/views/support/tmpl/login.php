<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$doc = JFactory::getDocument(); //get the overall document

$doc->addStyleSheet("/media/com_bkcontent/css/pages/support/login.css");
?>

<div class="row-fluid">
	<h1><?php echo JText::_("COM_BKCONTENT_SUPPORT_LOGIN_TITLE"); ?></h1>
	<hr>
</div>
<div class="row-fluid">
	<div class="span6">
		<form method="post" action="">
			<h3>Returning Customers</h3>
			<fieldset>
				<div class="control-group">
					<div class="controls">
						<input class="input-xlarge" type="text" name="customer_id" id="customer_id" placeholder="Account" required>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input class="input-xlarge" type="password" name="passwd" id="passwd" placeholder="Password" required>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<p class="span6"><small>Forgot password? Click here to recover.</small></p>
						<button class="btn btn-primary pull-right" type="submit">Sign In</button>
					</div>
				</div>
		    </fieldset>
		    <input type="hidden" name="task" value="login">
		</form>
	</div>
	<div class="span6 guest-login">
		<h3>Guest Checkout</h3>
		<h4 class="muted">Proceed to checkout, and you can create an account at the end.</h4>
		<div class="control-group">
			<div class="controls">
				<h4>&nbsp;</h4>
			</div>
		</div>
		<div class="control-group">
			<div class="controls">
				<button class="btn btn-primary guest-checkout pull-right" type="submit">Continue as Guest</button>
			</div>
		</div>
	</div>
</div>