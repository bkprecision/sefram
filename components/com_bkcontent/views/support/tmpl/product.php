<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$doc  		= JFactory::getDocument(); //get the overall document
$menu 		= JFactory::getApplication()->getMenu()->getActive();
$params 	= $menu->params;
$leftPane 	= $params->get("leftPane");
$services 	= $params->get("services");

$doc->setMetaData("data-main", "/media/com_bkcontent/js/pages/support/product.js");
$doc->addStyleSheet("/media/com_bkcontent/css/pages/support/product.css");
$doc->addStyleSheet("/media/com_bkcontent/css/bootstrap-select.min.css");
?>

<div class="row-fluid">
	<h1><?php echo $menu->title; ?></h1>
	<h2 class="hide">B&amp;K Precision</h2>
	<hr>
</div>
<div class="row-fluid support-container">
	<section class="left-pane support span5 offset1 well">
		<?php echo $leftPane; ?>
	</section>
	<section class="right-pane support span5 well">
		<div class="row-fluid">
			<div class="pull-left">
				<div class="ion-help-buoy"></div>
			</div>
			<div class="support-title">
				<h3>Product Support</h3>
				<div>
					<label class="control-label" for="supportModels">Start by selecting your model number</label>
					<div class="controls controls-row">
						<select id="supportModels" data-live-search="true">
							<option value>Select a model</option>
							<?php foreach ($this->itemList as $model): ?>

							<option value="<?php echo $model; ?>"><?php echo $model; ?></option>
							<?php endforeach; ?>

						</select>
						<div class="hide ion-loading-c ajax-loader"></div>
					</div>
				</div>
				<p><small>Models not listed above have lapsed beyond our support window.</small></p>
			</div>
		</div>

	</section>
	<div class="clearfix"></div>
</div>



<script id="product-support-tmpl" type="text/x-handlebars-template">
	<div id="productSupportModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="supportModalLabel" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="supportModalLabel" class="title">Product Support: Model {{invt_id}}</h3>
			<h5 class="warranty-note">This product is covered by a <em>{{warranty}} year warranty</em> from the date of purchase.</h5>
		</div>
		<div class="modal-body">
			<div class="row-fluid">
				<section class="span8 support-docs" id="docsoft">

					{{#if documentation}}
						<section class="documentation row-fluid">
							<div class="pull-left">
								<div class="ion-document-text"></div>
								<p class="documentation-title"><?= JText::_("COM_BKCONTENT_DOCUMENTATION"); ?></p>
							</div>
							<ul class="unstyled offset2">
							{{#each documentation}}
								<li><span class="ion-document">&nbsp;</span>{{docLink this}}</a></li>
							{{/each}}
							</ul>
						</section>
					{{/if}}

					{{#if software}}
					<section class="software row-fluid">
						<div class="pull-left">
							<div class="ion-gear-b"></div>
							<p class="software-title"><?= JText::_("COM_BKCONTENT_SOFTWARE"); ?></p>
						</div>
						<div class="offset2">
							<table class="table">
								<thead>
									<tr>
										<th><?=JTEXT::_('COM_BKPRODUCT_SOFTWARE_DESC')?></th>
										<th><?=JTEXT::_('COM_BKPRODUCT_SOFTWARE_VERSION')?></th>
										<th><?=JTEXT::_('COM_BKPRODUCT_SOFTWARE_LINK')?></th>
									</tr>
								</thead>
								<tbody>
						{{#each software}}
								<tr>
									<td>{{this.description}}</td>
									<td>{{this.version}}</td>
									<td>{{softLink this}}</td>
								</tr>
						{{/each}}
								</tbody>
							</table>
						</div>
					</section>
					{{/if}}

					<section class="services row-fluid">
						<div class="pull-left">
							<div class="ion-help-buoy"></div>
							<p class="services-title">Services</p>
						</div>
						<div class="offset2"><?php echo $services; ?></div>
					</section>

				</section>
				<section class="span4 support-photos">
					{{#if photos}}
						{{introPhoto}}
					{{/if}}
				</section>
			</div>
		</div>
		<div class="modal-footer">
			{{productLink}}
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		</div>
	</div>
</script>