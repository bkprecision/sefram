<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$doc = JFactory::getDocument(); //get the overall document
$uri = JFactory::getURI(); // get the URI

$doc->setMetaData("data-main", "/media/com_bkcontent/js/pages/support/estimate.js");
$doc->addStyleSheet("/media/com_bkcontent/css/pages/support/estimate.css");
?>
<div class="row-fluid">
	<h1>Calibration &amp; Repair Cost Estimation</h1>
	<h2 class="hide">B&amp;K Precision</h2>
	<p class="support promise">
		B&amp;K Precision is dedicated to the goal of providing the best possible service to our customers.
		Our Service Department is equipped with the required instruments, procedures and personnel to service
		or calibrate your B&amp;K Precision instruments to their optimum performance specifications.
	</p>
	<hr>
</div>

<div class="row-fluid">

	<section class="span3">

		<p>
			<strong>How to Obtain Service or Calibration: </strong><br>
			An RMA (Return Material Authorization) number is required to accompany
			all instruments sent in to the service department. The RMA number allows
			you to obtain and track the progress of your request.
		</p>
		<p><img src="/images/rma-policy-button.png" alt="Units sent to B&K Precision without a valid RMA number will be returned unserviced."></p>
		<p><img src="/images/start_here_sign.png" alt="Use the form below to start RMA request process." /></p>
		<form id="support-estimate" action="" method="post">
			<fieldset>
				<div class="control-group">
					<div class="control-label">
						<label for="searchModel">Enter B&amp;K Model:</label>
					</div>
					<div class="controls">
						<input
							id="searchModel"
							class="input-medium"
							type="text"
							name="invt_id"
							autocomplete="off"
							data-provide="typeahead"
							data-items="4"
							data-source="[<?= "&quot;".implode("&quot;,&quot;", $this->itemList)."&quot;" ?>]">
						<button type="submit" id="btnSearchModel" class="btn btn-primary pull-right">Get Estimate</button>
					</div>
				</div>
			</fieldset>
			<input type="hidden" name="format" value="json">
			<input type="hidden" name="supportType" value="estimate">
		</form>

	</section>

	<section class="span9">

		<ul class="nav nav-tabs">
			<li class="hide estimate-title"><a href="#estimate" data-toggle="tab">Estimate</a></li>
			<li class="active"><a href="#options" data-toggle="tab">Options</a></li>
			<li><a href="#charges" data-toggle="tab">Charges</a></li>
			<li><a href="#tat" data-toggle="tab">Turn-around Time</a></li>
			<li><a href="#shipping" data-toggle="tab">Shipping</a></li>
			<li><a href="#discontinued" data-toggle="tab">Discontinued Products</a></li>
		</ul>

		<div class="tab-content">
			<div id="estimate" class="tab-pane hide">
				<table class="table estimate-table"></table>
			</div>

			<div id="options" class="tab-pane active">
				<p>The Service Department offers the following service options:</p>
				<ol>
					<li><strong>NIST Traceable Calibration (NTC)</strong> - The instrument is calibrated to
						the latest manufacturer's specifications using equipment
						traceable to NIST (National Institute of Standards and
						Technology). The instrument is returned with a Certificate of
						Calibration.</li>
					<li><strong>NIST Traceable Calibration with Data (NTCD)</strong> - The instrument is calibrated to
						the latest manufacturer's specifications using equipment
						traceable to NIST (National Institute of Standards and
						Technology). The instrument is returned with a Certificate of
						Calibration along with a Calibration Data Report (&quot;out
						readings&quot; only).</li>
					<li><strong>NIST Traceable Calibration with Before and After Data (NTCBAD)</strong> - The instrument is calibrated to
						the latest manufacturer's specifications using equipment
						traceable to NIST (National Institute of Standards and
						Technology). The instrument is returned with a Certificate of
						Calibration along with a Calibration Before &amp; After Data Report (with &quot;in &amp; out
						readings&quot;)</li>
					<li><strong>Repair, Warranty</strong> - An instrument purchased from an authorized B&amp;K Precision
						distributor is repaired and performance tested to the latest
						manufacturer's specifications at no charge. Proof of purchase
						is required. (No calibration certificate or test report is
						provided.) </li>
					<li><strong>Repair, Warranty with NTC</strong> - An instrument purchased from an
						authorized B&amp;K Precision distributor is repaired and
						performance tested to the latest manufacturer's specifications
						at no charge. Proof of purchase is required. The instrument is
						calibrated to the latest manufacturer's specifications using
						equipment traceable to NIST (National Institute of Standards
						and Technology). The instrument is returned with a Certificate
						of Calibration.</li>
					<li><strong>Repair, Warranty  with NTCD</strong> - An instrument
						purchased from an authorized B&amp;K Precision distributor is
						repaired and performance tested to the latest manufacturer's
						specifications at no charge. Proof of purchase is required.
						The instrument is calibrated to the latest manufacturer's
						specifications using equipment traceable to NIST (National
						Institute of Standards and Technology). The instrument is
						returned with a Certificate of Calibration along with a
						Calibration Data Report (&quot;out readings&quot; only).</li>
					<li><strong>Repair, Warranty with NTCBAD</strong> - An instrument
						purchased from an authorized B&amp;K Precision distributor is
						repaired and performance tested to the latest manufacturer's
						specifications at no charge. Proof of purchase is required.
						The instrument is calibrated to the latest manufacturer's
						specifications using equipment traceable to NIST (National
						Institute of Standards and Technology). The instrument is
						returned with a Certificate of Calibration along with a
						Calibration Before &amp; After Data Report (with &quot;in &amp; out readings&quot;).</li>
					<li><strong>Repair, Non-Warranty</strong> - An instrument that is no
						longer under warranty but in need of service is repaired and
						calibrated to the latest manufacturer's specifications. (No
						calibration certificate or test report is provided.)</li>
					<li><strong>Repair, Non-Warranty with NTC</strong> - An instrument that is no longer
						under warranty but in need of service is repaired and
						calibrated to the latest manufacturer's specifications. The
						instrument is calibrated to the latest manufacturer's
						specifications using equipment traceable to NIST (National
						Institute of Standards and Technology). The instrument is
						returned with a Certificate of Calibration.</li>
					<li><strong>Repair, Non-Warranty with NTCD</strong> - An instrument that is
						no longer under warranty but in need of service is repaired
						and calibrated to the latest manufacturer's specifications.
						The instrument is calibrated to the latest manufacturer's
						specifications using equipment traceable to NIST (National
						Institute of Standards and Technology). The instrument is
						returned with a Certificate of Calibration along with a
						Calibration Data Report (&quot;out readings&quot; only).</li>
					<li><strong>Repair, Non-Warranty with NTCBAD</strong> - An instrument that is
						no longer under warranty but in need of service is repaired
						and calibrated to the latest manufacturer's specifications.
						The instrument is calibrated to the latest manufacturer's
						specifications using equipment traceable to NIST (National
						Institute of Standards and Technology). The instrument is
						returned with a Certificate of Calibration along with a
						Calibration Before &amp; After Data Report (with &quot;in &amp; out readings&quot;).</li>
					<li><strong>Service Warranty</strong> - An
						instrument repaired by B&amp;K Precision carries a 90 day
						warranty. If for some reason you are not satisfied with the
						service, return the unit for re-evaluation with this option.
						Proof of prior service is required. (No calibration
						certificate or test report is provided.)</li>
				</ol>
			</div>

			<div id="charges" class="tab-pane">
				<p><em><strong>Payment Terms: </strong></em></p>
				<p>B&amp;K Precision requires prepayment before any service or calibration work is started.
					We accept Checks, Money Orders, Visa, Discover, Master Card or American Express credit cards.
					Company purchase orders accepted on pre-authorized accounts only, otherwise above
					prepayment methods required.</p>
				<p><em><strong>Service Charges: </strong></em></p>
				<p>Upon entering your instrument model number in the RMA process, you will be taken to a table of
					available service options and charges. The charges cover:</p>
				<ul>
					<li>Repairs: all labor and performance testing necessary to bring the
						instrument to the latest manufacturer's specifications plus
						return shipping. (*domestic US shipments only.)</li>
					<li>Calibrations: all labor and equipment necessary to meet the published factory
						specifications plus return shipping. (*domestic US shipment
						only.)Calibration is conducted by B&amp;K Precision, an
						ISO9001:2000 company, using test equipment traceable to
						national standards and includes either a certificate of
						calibration or certificate of calibration with data, as requested.</li>
				</ul>
				<p><strong>*All international shipping charges to be determined.</strong><br />
					Please contact the Service Department
					for an estimate, or to make collect/3rd party arrangements. B&amp;K Precision does not pay
					shipping charges for international customers.
					</p>
				<p>For all no-charge warranty repair requests, proof of purchase must be supplied with the instrument.
					Proof can be in the form of a copy of the instrument's dated invoice, receipt or packing slip from
					an authorized B&amp;K Precision distributor. </p>
				<p>A non-refundable evaluation charge applies to all repair requests. </p>
				<p>All charges are subject to change without notice. <br />
					The RMA request will be cancelled 90 days from entry if instrument is not received.</p>
			</div>

			<div id="tat" class="tab-pane">
				<p><em><strong>Turn-around Time:</strong></em></p>
				<p>Typical service or calibration Turn-around Time (TAT) is approximately 10 working days. Expedited service is available for an additional charge. For expedited charges and availability please contact the service department.</p>
			</div>

			<div id="shipping" class="tab-pane">
				<p><em><strong>Shipping:</strong></em></p>
				<p>Please follow these guidelines in packing and shipping your instrument to B&amp;K Precision.</p>
				<ul>
					<li>Use the original shipping container or a sturdy cardboard shipping carton, large enough for at least 3 inch foam padding around the instrument.</li>
					<li>Do not use loose plastic foam pieces, which can shift during transport.</li>
					<li>Secure the box flaps with packaging tape or box staples.</li>
					<li>Affix at copy of the RMA shipping label available at the end of the process or mark the carton with the RMA number received while setting up this order.</li>
				</ul>
				<p>B&amp;K Precision returns all instruments using either UPS ground or FedEx ground, unless otherwise requested. For international shipments see notes in <a href="#charges">Service Charges</a> section.</p>
			</div>

			<div id="discontinued" class="tab-pane">
				<p><em><strong>Discontinued Products: </strong></em></p>
				<p>B&amp;K Precision&rsquo;s policy is to continue servicing
					discontinued models for up to 7 years--parts permitting--after they are withdrawn from
					active sales. If your unit is one that is no longer serviced, B&amp;K offers a <a href="https://www.bkprecision.com/promotions/exchange-program.html" target="_blank">Discontinued
					Instrument Upgrade Program</a> that provides a significant discount towards the purchase of a new
					current model.</p>
			</div>
		</div>

	</section>
</div>

<script id="estimate-table-tmpl" type="text/x-handlebars-template">
<table class="table estimate-table">
	<thead><tr><th>Available Service Options</th><th>Service Cost</th><th>Shipping Cost</th><th>Total</th></tr></thead>
	<tbody>
	{{#if calibration_cost}}
		<tr class="cal option-1">
			<td>1. NIST Traceable Calibration</td>
			<td>${{calibration_cost}}</td>
			<td>${{ship_cost}}</td>
			<td>${{add calibration_cost ship_cost}}</td>
		</tr>
	{{/if}}
	{{#if calib_data_cost}}
		<tr class="cal_wdata option-2">
			<td>2. NIST Traceable Calibration with Data ("out data only")</td>
			<td>${{calib_data_cost}}</td>
			<td>${{ship_cost}}</td>
			<td>${{add calib_data_cost ship_cost}}</td>
		</tr>
	{{/if}}
	{{#if calib_badata_cost}}
		<tr class="cal_wbadata option-3">
			<td>3. NIST Traceable Calibration with Before &amp; After Data ("in &amp; out data")</td>
			<td>${{calib_badata_cost}}</td>
			<td>${{ship_cost}}</td>
			<td>${{add calib_badata_cost ship_cost}}</td>
		</tr>
	{{/if}}
	{{#if state}}
		<tr class="warranty option-4">
			<td>4. Warranty Repair</td>
			<td>No Charge</td>
			<td>No Charge</td>
			<td>No Charge</td>
		</tr>
		{{#if calibration_cost}}
		<tr class="warranty cal option-5">
			<td>5. Warranty Repair and NIST Traceable Calibration</td>
			<td>${{calibration_cost}}</td>
			<td>No Charge</td>
			<td>${{calibration_cost}}</td>
		</tr>
		{{/if}}
		{{#if calib_data_cost}}
		<tr class="warranty cal_wdata option-6">
			<td>6. Warranty Repair and NIST Traceable Calibration with Data ("out data only")</td>
			<td>${{calib_data_cost}}</td>
			<td>No Charge</td>
			<td>${{calib_data_cost}}</td>
		</tr>
		{{/if}}
		{{#if calib_badata_cost}}
		<tr class="warranty cal_wbadata option-7">
			<td>7. Warranty Repair and NIST Traceable Calibration with Before &amp; After Data ("in &amp; out data")</td>
			<td>${{calib_badata_cost}}</td>
			<td>No Charge</td>
			<td>${{calib_badata_cost}}</td>
		</tr>
		{{/if}}
	{{/if}}
	{{#if repair_cost}}
		<tr class="repair option-8">
			<td>8. Non-Warranty Repair</td>
			<td>${{repair_cost}}</td>
			<td>${{ship_cost}}</td>
			<td>${{add repair_cost ship_cost}}</td>
		</tr>
		{{#if calibration_cost}}
		<tr class="warranty option-9">
			<td>9. Non-Warranty Repair and NIST Traceable Calibration</td>
			<td>${{add repair_cost calibration_cost}}</td>
			<td>${{ship_cost}}</td>
			<td>${{add repair_cost calibration_cost ship_cost}}</td>
		</tr>
		{{/if}}
		{{#if calib_data_cost}}
		<tr class="warranty option-10">
			<td>10. Non-Warranty Repair and NIST Traceable Calibration with Data ("out data only")</td>
			<td>${{add repair_cost calib_data_cost}}</td>
			<td>${{ship_cost}}</td>
			<td>${{add repair_cost calib_data_cost ship_cost}}</td>
		</tr>
		{{/if}}
		{{#if calib_badata_cost}}
		<tr class="warranty option-11">
			<td>11. Non-Warranty Repair and NIST Traceable Calibration with Before &amp; After Data ("in &amp; out data")</td>
			<td>${{add repair_cost calib_badata_cost}}</td>
			<td>${{ship_cost}}</td>
			<td>${{add repair_cost calib_badata_cost ship_cost}}</td>
		</tr>
		{{/if}}
	{{/if}}
		<tr class="warranty option-12">
			<td>12. Service Warranty (prior service repair)</td>
			<td>No Charge</td>
			<td>No Charge</td>
			<td>No Charge</td>
		</tr>
	</tbody>
</table>
</script>