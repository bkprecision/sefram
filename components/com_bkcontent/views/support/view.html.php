<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for Where to Buy
 */
class BkcontentViewSupport extends JViewLegacy {

	// Overwriting JView display method
	function display($tpl = null) {

		// get the application state and variables from the model
		$this->state 	= $this->get('state');
		$this->itemList	= $this->get('ItemList');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {

			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Display the view
		parent::display($tpl);
	}
}