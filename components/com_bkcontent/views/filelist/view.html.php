<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HTML View class for a List of Images from a particular directory */
class BkcontentViewFilelist extends JViewLegacy {
	// Overwriting JView display method
	function display($tpl = null) {

		$input 			= JFactory::getApplication()->input;
		// Assign data to the view
		$folder 		= $input->get('folder', '', 'raw');
		$this->filetype	= $input->get('filetype', '', 'raw');
		$this->folder 	= preg_replace("/(\/)/i", DIRECTORY_SEPARATOR, $folder);

		// change the working directory to the image folder for file size and modified time
		chdir($this->folder);
		// grab the images
		$files			= array_filter( scandir( getcwd(), 0 ), array($this, 'isFile') );
		$model 			= $this->getModel();
		$this->files	= $model->getDescriptions($files);

		// Check for errors.
		if (count($errors = $this->get('Errors')) OR empty($this->folder)) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Display the view
		parent::display($tpl);
	}

	// checks for acceptable filetype
	function isFile($filename = null) {
		return (boolean)preg_match("/\.(?:".$this->filetype.")$/i", $filename);
	}

	// transform bytes to human readable format
	// this is used in the template to convert software file sizes to human readable format
	function byteConvert($bytes) {
		$s = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
		$e = floor(log($bytes)/log(1024));

		return sprintf('%.2f '.$s[$e], ($bytes/pow(1024, floor($e))));
	}
}