<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access')

?>
	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<th><?=JTEXT::_('COM_BKCONTENT_FILELIST_DESCRIPTION')?></th>
				<th><?=JTEXT::_('COM_BKCONTENT_FILELIST_SIZE')?></th>
				<th><?=JTEXT::_('COM_BKCONTENT_FILELIST_DATE')?></th>
				<th><?=JTEXT::_('COM_BKCONTENT_FILELIST_NAME')?></th>
			</tr>
		</thead>
		<tbody>
<?php
foreach($this->files as $file):
?>
		<tr>
			<td><?=(!empty($file->description) ? $file->description : 'N/A')?></td>
			<td><?=$this->byteConvert(filesize($file->filename))?></td>
			<td><?=date("n/j/Y", filemtime($file->filename))?></td>
			<td><a href="<?=$this->folder.DS.$file->filename?>"><?=$file->filename?></a></td>
		</tr>
<?php
endforeach;
?>
		</tbody>
	</table>