<?php
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.categories');

/**
 * BK Content Component Category Tree
 *
 * @static
 * @package		Joomla.Site
 * @subpackage	com_content
 * @since 1.6
 */
class BkcontentCategories extends JCategories
{
	public function __construct($options = array())
	{
		$options['table'] = '#__content';
		$options['extension'] = 'com_bkcontent';
		parent::__construct($options);
	}
}
