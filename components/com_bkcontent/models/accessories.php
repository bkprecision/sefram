<?php
// No direct access
defined('_JEXEC') or die;

use Aws\S3\S3Client;

class BkcontentModelAccessories extends JModelLegacy {
	/**
	* Products data
	*/
	protected $_parent 			= null;

	protected $_accessories		= array();

	protected $_unlimitedAccs	= null;

	protected $bucket;

	protected $client;

	protected $cachedIcons		= array();

	/**
	* Products total
	* @var integer
	*/
	var $_total					= null;

	/**
	* Pagination object
	* @var object
	*/
	var $_pagination			= null;

	public function __construct() {
		$params 			= JComponentHelper::getParams("com_bkcontent");

		$this->client 		= S3Client::factory(array(
				"key"		=> $params->get("accessKey"),
				"secret"	=> $params->get("secretKey")
		));
		$this->bucket 		= $params->get("s3bucket");
		$this->iconType 	= $params->get("iconType");
		$this->iconDir    	= $params->get("iconDir");
		$this->_photoTypes 	= explode(" ", $params->get("photoTypes"));
		$this->_photoDir    = $params->get("photoDir");

		parent::__construct(array());
	}

	/**
	* Method to auto-populate the model state.
	*
	* This method should only be called once per instantiation and is designed
	* to be called on the first call to the getState() method unless the model
	* configuration flag to ignore the request is set.
	*
	* Note: Calling getState in this method will result in recursion.
	*
	* @return 	void
	* @since	J1.6
	*/
	protected function populateState() {

		$app 		= JFactory::getApplication();
		$input		= $app->input;
		// Get the parent category id
		$parentId 	= $input->getInt('cat_id');
		$this->setState('category.parent.id', $parentId);

		// Get the pagination request variables
		$limit 		= $input->getInt("limit", "12");
		$limitStart = $input->get('start', 0, '', 'int');

		// Get the escaped fragment for Google Bot to follow ajax urls
		$frag 		= $input->getString("_escaped_fragment_");
		$this->setState("google._escaped_fragment_", $frag);

		if (isset($frag) && strpos($frag, "page") !== false) {
			$page = explode(":", $frag);
			$page = $page[1];
			$this->setState('app.page', $page);
		}

		if (isset($page) AND !empty($page))	{
			$limitStart = ($page-1)*$limit;
		}

		// In case limit has been changed, adjust it
		$limitStart = ($limit != 0 ? (floor($limitStart/$limit) * $limit) : 0);
		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitStart);
	}
	/**
	* Get the parent JCategoryNode object
	*
	* @return 	mixed	a JCategoryNode object or false on error
	* @see JCategories::get
	*/
	public function getParent() {
		jimport('joomla.application.categories');
		if (!is_object($this->_parent) OR !$this->_parent instanceof JCategoryNode) {
			$categories 			= JCategories::getInstance('Bkcontent');
			$this->_parent 			= $categories->get($this->getState('category.parent.id'));
			$params = new JRegistry;
			$params->loadString($this->_parent->params);
			$this->_parent->params = $params;
		}

		return $this->_parent;
	}
	/**
	* Get the accessories of a particular parent category and any descendent categories
	*
	* @return 	mixed 	an array of IDs or false on error
	*/
	public function getAccessories() {

		$db 	= JFactory::getDbo();
		$input  = JFactory::getApplication()->input;
		$catId  = $input->getInt("cat_id");
		$parent = $this->getState("category.parent.id");

		if (!empty($parent)) {

			// setup the query
			$query = $db->getQuery(true)
					->select("a.alias, a.cat_id, c.title, a.description, a.invt_id, a.title, x.list_price")
					->from("#__bkaccessories as a")
					->join("INNER", "#__xref_products_price as x USING(invt_id)")
					->join("INNER", "#__categories as c ON c.id=a.cat_id")
					->where("state = 1")
					->order("invt_id");

			// grab all subcats
			$subcats = $this->getSubCats();
			$subcats = implode("','", $subcats);
			if (!empty($subcats)) {
				$query->where("cat_id IN ('{$subcats}')");
			}
			else {
				$query->where("cat_id = {$catId}");
			}

			// Setup for pagination support
			$this->_unlimitedAccs 	= $this->_getList($query);
			$this->_accessories		= $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
			foreach ($this->_accessories as $accessory) {
				$this->getIcon($accessory);
				$this->getPhoto($accessory);
			}
			$this->_total 			= $this->_getListCount($query);
		}

		return $this->_accessories;
	}

	protected function getIcon(&$accessory) {

		$accessory->file_name = str_replace(" ", "-", $accessory->invt_id);

		$image = "/{$this->iconDir}/{$accessory->file_name}{$this->iconType}";

		if ( !isset($this->cachedPhotos[$accessory->file_name]) ) {
			if ($this->client->doesObjectExist($this->bucket, $image)) {
				$icon = $this->client->getObjectUrl($this->bucket, $image);
			}
			else {
				$icon = $this->client->getObjectUrl($this->bucket, "/{$this->iconDir}/no-image.jpg");
			}

			$this->cachedIcons[$accessory->file_name] = $icon;
		}

		$accessory->icon = $this->cachedIcons[$accessory->file_name];

	}

	/**
	 * Get photos for a product to setup product photo slide show images
	 */
	public function getPhoto(&$accessory) {

		$suffix 			  = $this->_photoTypes[0];
		$accessory->file_name = str_replace(" ", "-", $accessory->invt_id);
		$fileNameSrc 		  = "/{$this->_photoDir}/{$accessory->file_name}{$suffix}";

		if ($this->client->doesObjectExist($this->bucket, $fileNameSrc)) {
			$accessory->photo = $this->client->getObjectUrl($this->bucket, $fileNameSrc);
		}
		else {
			$accessory->photo = null;
		}
	}

	private function getSubCats() {
		$db 	= JFactory::getDbo();
		$parent = $this->getState("category.parent.id");

		$db->setQuery($db->getQuery(true)
			->select("id")
			->from("#__categories")
			->where("parent_id = {$parent}"));

		return $db->loadColumn(0);
	}

	public function getTotal() {

		if(empty($this->_total)) {
			$this->getAccessories();
		}

	 	return $this->_total;
	}

	public function getPagination() {

		$this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
		$this->_pagination->setAdditionalUrlParam('view', 'accessories');

		return $this->_pagination;
	}
}