<?php
// No direct access
defined('_JEXEC') or die;

class BkcontentModelWtb extends JModelLegacy {
	/**
	* Where to buy data objects
	*/

	protected $_results			= null;

	protected $_products 		= null;

	protected $_distributors 	= null;

	protected $_country			= null;

	/**
	* Method to auto-populate the model state.
	*
	* This method should only be called once per instantiation and is designed
	* to be called on the first call to the getState() method unless the model
	* configuration flag to ignore the request is set.
	*
	* Note: Calling getState in this method will result in recursion.
	*
	* @return 	void
	* @since	J1.6
	*/
	protected function populateState() {

		$input      = JFactory::getApplication()->input;

		// Get the product invt_id
		$invt_id 	= $input->get("invt_id", "", "string");
		$this->setState("wtb.invt_id", $invt_id);
	}

	public function getResults() {

		if (!isset($this->_results)) {

			$invtId = $this->getState("wtb.invt_id");
			if (!empty($invtId)) {

				$connOpts 	= array(
					"driver"	=> "mysql",
					"host"		=> "mysql.bkprecision.com",
					"user"		=> "webaccess",
					"password" 	=> "cust0m98",
					"database" 	=> "bk_wtbdb",
					"prefix"	=> ""
					);
				$db 		= JDatabase::getInstance($connOpts);
				$db->setQuery($db->getQuery(true)
								->select("name, dist_id, invt_id, dist_partno,
										 qty_onhand, last_update")
								->from("#__dist_inventory")
								->join("INNER", "#__distributors USING(dist_id)")
								->where("invt_id LIKE '{$invtId}%'")
								->where("qty_onhand > 0")
								->order("last_update DESC, qty_onhand DESC"));

				$this->_results = new stdClass();
				$stock			= $db->loadObjectList();
				$distIdList 	= array();

				foreach ($stock as $result) {
					$distIdList[] = $result->dist_id;
				}

				// check for list of ids
				if (!empty($distIdList)) {

					$distIdList = implode(",", array_unique($distIdList));

					$db->setQuery($db->getQuery(true)
									->select("*")
									->from("#__distributors as d")
									->leftJoin("#__dist_inventory as i USING(dist_id)")
									->where("d.dist_id IN ({$distIdList})")
									->where("i.qty_onhand > 0")
									->group("i.dist_id")
									->order("i.last_update DESC, i.qty_onhand DESC"));

					$this->_results->distributors 	= $db->loadObjectList();

					foreach ($this->_results->distributors as $distributor) {

						$distributor->last_update = date("M-j", strtotime($distributor->last_update));
						$distributor->stock = array();
						foreach ($stock as $item) {
							if ($item->dist_id == $distributor->dist_id) {
								$item->url = $distributor->url;
								$distributor->stock[] = $item;
							}
						}
					}
				}
			}

		}

		return $this->_results;
	}

	public function getProducts() {

		if (!isset($this->_products)) {

			$db = JFactory::getDbo();
			$db->setQuery($db->getQuery(true)
							->select("invt_id")
							->from("#__bkproducts")
							->where("state = 1 OR state = 2"));

			$this->_products = $db->loadColumn(0);

			$db->setQuery($db->getQuery(true)
					->select("invt_id")
					->from("#__bkaccessories")
					->where("state = 1 OR state = 2"));

			$this->_products = array_merge((array)$this->_products, (array)$db->loadColumn(0));
		}

		return $this->_products;
	}

	public function getIntDistributors() {

		if (!isset($this->_distributors)) {

			$db = JFactory::getDbo();
			$db->setQuery($db->getQuery(true)
							->select("*")
							->from("#__bkdistribution_intl")
							->order("country"));
			$this->_distributors = $db->loadObjectList();
		}

		return $this->_distributors;
	}

	public function getCountries() {

		if (!isset($this->_country)) {

			$db = JFactory::getDbo();
			$db->setQuery($db->getQuery(true)
							->select("country")
							->from("#__bkdistribution_intl")
							->group("country")
							->order("country"));
			$this->_country = $db->loadColumn(0); // return the first column as an array
		}

		return $this->_country;
	}

}