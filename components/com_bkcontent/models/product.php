<?php
// No direct access
defined('_JEXEC') or die;

use Aws\S3\S3Client;

class BkcontentModelProduct extends JModelItem {
	/**
	* Product data
	*
	*/

	protected $_context 		= 'com_bkcontent.product';

	protected $_product			= null;

	protected $_replacements 	= null;

	protected $_accessories 	= null;

	protected $_series			= null;

	protected $invtIdList		= array();

	protected $_software		= null;

	protected $_video			= null;

	protected $_photos			= null;

	protected $_photoTypes;

	protected $_photoDir;

	protected $cachedPhotos		= array();

	protected $_docs			= null;

	protected $_docTypes;

	protected $_results			= null;

	/*
	 * AWS settings
	 */
	private $bucket;

	private $client;

	public function __construct() {
		$params 			= JComponentHelper::getParams('com_bkcontent');

		$this->client 		= S3Client::factory(array(
				"key"		=> $params->get("accessKey"),
				"secret"	=> $params->get("secretKey")
		));
		$this->bucket 		= $params->get("s3bucket");
		$this->_docTypes   	= explode(" ", $params->get("docTypes"));
		$this->_photoTypes 	= explode(" ", $params->get("photoTypes"));
		$this->_photoDir    = $params->get("photoDir");

		parent::__construct(array());
	}

	/**
	* Method to auto-populate the model state.
	*
	* This method should only be called once per instantiation and is designed
	* to be called on the first call to the getState() method unless the model
	* configuration flag to ignore the request is set.
	*
	* Note: Calling getState in this method will result in recursion.
	*
	* @return 	void
	* @since	J1.6
	*/
	protected function populateState() {
		$app 		= JFactory::getApplication();
		$input 		= $app->input;
		$menu		= $app->getMenu()->getActive();

		// Load the application parameters
		$params 	= $app->getParams();
		$this->setState("params", $params);

		// Get the product language
		$this->setState("product.lang", $app->getLanguage()->getTag());

		// Load the current menu
		$this->setState("menu", $menu);

		// Get the product invt_id
		$invt_id 	= $input->get("invt_id", "", "raw");
		$this->setState("product.invt_id", $invt_id);


		// Get the product series
		$series 	= $input->getInt("series");
		$this->setState("product.series", $series);

		// get invt_id info for cat_id and parent cat_id
		$cat_id		= $input->getString("cat_id");
		if (empty($cat_id) && !empty($series)) {
			$db 	= JFactory::getDbo();
			$db->setQuery($db->getQuery(true)
							->select("cat_id")
							->from("#__bkproducts")
							->where("series='{$series}'"));
			$cat_id = $db->loadResult();
		}
		elseif (empty($cat_id) && !empty($invt_id)) {
			$lang   = $input->getString("language");
			$db 	= JFactory::getDbo();
			$db->setQuery($db->getQuery(true)
					->select("cat_id")
					->from("#__bkproducts")
					->join("INNER", "#__categories ON #__categories.id=#__bkproducts.cat_id")
					->where("invt_id='{$invt_id}'")
					->where("language='{$lang}'"));
			$cat_id = $db->loadResult();
		}
		$this->setState("product.cat_id", $cat_id);


	}
	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'Bkcontent', $prefix = 'BkcontentTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}
	/**
	* Get the products of a particular parent category and any descendent categories
	*
	* @return 	mixed 	an array of IDs or false on error
	*/
	public function getProduct() {

		if ( !isset($this->_product) ) {
			$db 		 = JFactory::getDbo();
			$lang  		 = JFactory::getLanguage();
			$defaultLang = JComponentHelper::getParams("com_languages")->get("site");
			// Get the component params
			$compParams  = JComponentHelper::getParams('com_bkcontent');

			$query  = $db->getQuery(true);
			$query->from('#__bkproducts')
					->join('LEFT OUTER', '#__xref_products_price USING(invt_id)');
			$select = "#__bkproducts.*,
						#__xref_products_price.calibration_cost,
						#__xref_products_price.calib_data_cost,
						#__xref_products_price.list_price,
						#__xref_products_price.repair_cost,
						#__xref_products_price.ship_cost,
						#__xref_products_price.upgrade_price";

			if( $defaultLang === $lang->getTag() ) {
				$query->join("INNER", "#__categories ON #__categories.id=#__bkproducts.cat_id");
				$select .= ",#__categories.path as path";
			}
			// the user is not currently on the default site language
			// we have to append the language tag to the path
			else {

				$query->leftJoin("#__menu ON #__menu.link LIKE concat('%cat_id=', #__bkproducts.cat_id,'%')")
					->leftJoin('#__languages ON #__languages.lang_code=#__menu.language');

				$select .= ",CONCAT_WS(\"/\", \"" . preg_replace("/(.[^-]).*/", "$1",$lang->getTag()) . "\" , #__menu.path) as path";
			}

			$query->select($select);

			$query->where("invt_id = '{$this->getState('product.invt_id')}'");
			$query->where("cat_id = {$this->getState("product.cat_id")}");

			$db->setQuery($query);

			$this->_product = $db->loadObject();
			if (!isset($this->_product->file_name) && !empty($this->_product->invt_id)) {
				$this->_product->file_name = $this->_product->invt_id;
			}

			if ( !empty($this->_product) ) {

				$this->_product->software = $this->getSoftware();

				// only get stock for BK US
				$conf = JFactory::getConfig();
				if( preg_replace("/^[^@]*@(.+)$/", "$1", $conf->get("mailfrom")) == "bkprecision.com" ) {
					$this->_product->results  = $this->getStock(); // get WTB results
				}

				// transform the json parameters string to a JRegistry object
				if( isset($this->_product->params) ) {
					$params = new JRegistry;
					$params->loadString($this->_product->params);
					$this->_product->params = $params;
				}

				// ensure we grab series info if the product is part of a series
				if( !empty($this->_product->series) ) {
					$this->_product->seriesInfo 				   = $this->getSeries($this->_product->series);
					$this->_product->title						   = $this->_product->seriesInfo->title;
					$this->_product->long_desc					   = $this->_product->seriesInfo->description;
					$this->_product->file_name 					   = $this->_product->seriesInfo->file_name;
					$this->_product->seriesInfo->individualProduct = true;
				}
			}
			else { // the product query returned nothing. this might be a series
				$menu = $this->getState('menu');
				if ( isset($menu->query["layout"]) AND $menu->query["layout"] == "series" ) { // we have a series request
					$this->_product->seriesInfo = $this->getSeries($menu->query["series"]);
					$this->_product->state		= $this->product->seriesInfo->products[0]->state;
					$this->_product->software	= $this->getSoftware($menu->query["series"]);
					$this->_product->file_name 	= $this->product->seriesInfo->file_name;
					$this->_product->warranty	= $this->product->seriesInfo->warranty;
				}
			}

			$this->_product->lang		   = strtolower($lang->getTag());
			$this->_product->documentation = $this->getDocumentation();
			$this->_product->accessories   = $this->getAccessories();
			$this->_product->photos		   = $this->getPhotos();
			if ($compParams->get("show_wtb_btn"))
			{
				$this->_product->stock = $this->getStock();
			}
			$this->_product->video		   = $this->getVideo();

			// this product has been discontinued
			if ( isset($this->_product->state) AND $this->_product->state == 2 ) {
				$this->_product->replacements = $this->getReplacements();
			}

		}

		return $this->_product;
	}

	/**
	 * Grab a list of all invtIds for products in a series.
	 */
	private function getSeriesInvtIdList() {

		if (!empty($this->_series->products) && $this->_product->series > 0) {
			foreach($this->_series->products as $product) {
				$this->invtIdList[] = $product->invt_id;
			}
		}

	}
	/**
	* Get the replacements of a particular product
	*
	* @return 	mixed 	an array of IDs or false on error
	*/
	public function getReplacements() {
		if( !isset($this->_replacements) ) {

			$this->_db->setQuery($this->_db->getQuery(true)
										->from('#__xref_prods_replacements AS x')
										->leftJoin('#__bkproducts AS p ON p.invt_id=x.replacement')
										->leftJoin("#__menu AS m ON m.link LIKE concat('%cat_id=', p.cat_id,'%')")
										->leftJoin('#__languages AS l ON l.lang_code=m.language')
										->select('x.replacement as invt_id, p.alias, concat_ws("/", l.sef, m.path) as path, p.short_desc, p.title')
										->where('x.invt_id = "'.$this->getState('product.invt_id').'"')
										->where('p.state = 1')
										->where('m.published=1')
										->group('x.replacement'));
			$this->_replacements = $this->_db->loadObjectList();
		}

		return $this->_replacements;
	}
	/**
	* Get the software for a given invt_id
	*
	* @return 	mixed			software object array on success, false on error
	*/
	public function getAccessories() {

		if ( ! isset($_accessories)) {
			$accArray 	= array();
			$invt_id 	= $this->getState('product.invt_id');
			$cat_id 	= $this->getState('product.cat_id');
			$db 		= JFactory::getDbo();
			$qry 		= $db->getQuery(true)
							->select("#__bkaccessories.*")
							->from("#__bkaccessories")
							->join("INNER", "#__xref_accessories ON #__xref_accessories.accessory_id=#__bkaccessories.id")
							->group("#__bkaccessories.invt_id")
							->order("#__bkaccessories.invt_id");

			if ( ! empty($invt_id)) {

				// get accessories for invt_id
				// first we must get the product row id from invt_id
				if ( ! isset($this->_product)) {
					$this->getProduct();
				}

				$qry->where("(type='invt_id' AND xref_id='{$this->_product->id}')", "OR");
				if (isset($this->_product->series) && $this->_product->series > 0) {
					$invtIds = implode("','", $this->invtIdList);
					$qry->where("( type='invt_id' AND xref_id IN ('{$invtIds}') )", "OR");
					$qry->where("(type='series' AND xref_id='{$this->_product->series}')", "OR");
				}

			}
			else {
				// get accessories for series
				$qry->where("(type='series' AND xref_id='{$this->getState('product.series')}')", "OR");
			}

			// get accessories for invt_id's cat_id
			$qry->where("(type='cat_id' AND xref_id='{$cat_id}')");

			$this->_accessories = $db->setQuery($qry)->loadObjectList();

			foreach ($this->_accessories as &$accessory) {
				$fileNameSrc = $accessory->invt_id . ".gif";
				if ($this->client->doesObjectExist($this->bucket, "icons/" . $fileNameSrc)) {
					$accessory->icon = $this->client->getObjectUrl($this->bucket, "icons/" . $fileNameSrc);
				}
				if ($this->client->doesObjectExist($this->bucket, "accessories/" . $fileNameSrc)) {
					$accessory->photo = $this->client->getObjectUrl($this->bucket, "accessories/" . $fileNameSrc);
				}
			}
		}

		return $this->_accessories;
	}
	/**
	* Get the software for a given invt_id
	*
	* @return 	mixed			software object array on success, false on error
	*/
	public function getSoftware($seriesId = null) {

		if( !isset($this->_software) ) {

			$invtId = $this->getState('product.invt_id');
			$db 	= JFactory::getDbo();
			$qry	= $db->getQuery(true)
						->select("description, file_name, version")
						->from("#__bksoftware");

			if( !empty($invtId) ) {

				$qry->where("invt_id = '{$invtId}'");

			}
			else {

				// get all the software applicable to the series
				$qry->where('invt_id IN ("'.implode('","', $this->invtIdList).'")')
					->group("file_name");
			}

			$db->setQuery($qry);

			$this->_software = $db->loadObjectList();

			// go through the results and make sure they exist on the file system
			foreach($this->_software as $key => $software) {
				$fileNameSrc = "downloads/software/{$software->file_name}";
				$bkusSoftPath = "downloads/bkus_software/{$software->file_name}";
				if ($this->client->doesObjectExist($this->bucket, $fileNameSrc)) {

					$this->_software[$key]->src = "https://{$this->bucket}.s3.amazonaws.com/{$fileNameSrc}";

				}
				else if ($this->client->doesObjectExist($this->bucket, $bkusSoftPath)) {

					$this->_software[$key]->src = "https://{$this->bucket}.s3.amazonaws.com/{$fileNameSrc}";

				}
				else {
					// file doesn't exist, unset the key
					unset($this->_software[$key]);
				}
			}
		}

		return $this->_software;
	}

	/**
	* Get the series information based on a particular product series id
	*
	* @param	int		seriesid in the db
	* @return 	obj		seriesInfo object
	*/
	public function getSeries($seriesId = null) {

		if( !isset($this->_series) ) {

			$db 		 = JFactory::getDbo();
			$lang  		 = JFactory::getLanguage();
			$defaultLang = JComponentHelper::getParams("com_languages")->get("site");
			// Get the component params
			$compParams  = JComponentHelper::getParams('com_bkcontent');

			$db->setQuery($db->getQuery(true)
							->select("*")
							->from("#__bkseries")
							->where("id = {$seriesId}"));

			$this->_series = $db->loadObject();
			$params = new JRegistry();
			$params->loadString($this->_series->params);
			$this->_series->params = $params;

			// get the price information for the series
			$query = $db->getQuery(true)
							->select("#__bkproducts.*")
							->from("#__bkproducts")
							->where("series = {$seriesId}")
							->where("state = 1")
							->order("LENGTH(#__bkproducts.invt_id), #__bkproducts.invt_id");

			if( $defaultLang === $lang->getTag() ) {
				$query
					->select("#__categories.path as path")
					->join("INNER", "#__categories ON #__categories.id=#__bkproducts.cat_id");
			}
			// the user is not currently on the default site language
			// we have to append the language tag to the path
			else {

				$query
					->select("CONCAT_WS(\"/\", \"" . preg_replace("/(.[^-]).*/", "$1",$lang->getTag()) . "\" , #__menu.path) as path")
					->leftJoin("#__menu ON #__menu.link LIKE concat('%cat_id=', #__bkproducts.cat_id,'%')")
					->leftJoin('#__languages ON #__languages.lang_code=#__menu.language');

			}

			$db->setQuery($query);

			$this->_series->products = $db->loadObjectList();
			$this->getSeriesInvtIdList();

			// get the stock information for each product in the series
			if ($compParams->get("show_wtb_btn"))
			{
				foreach ($this->_series->products as $product) {
					$this->setState("product.invt_id", $product->invt_id);
					$product->stock = $this->getStock();
				}
			}
			// set back to original invt_id
			$this->setState("product.invt_id", $this->_product->invt_id);

			if (empty($this->_product)) {
				$db->setQuery($db->getQuery(true)
						->select("DISTINCT file_name, warranty")
						->from("#__bkproducts")
						->where("series = {$seriesId}"));

				$seriesInfo = $db->loadObject();

			}
			else {
				$seriesInfo = $this->_product;
			}

			$this->_series->file_name = $seriesInfo->file_name;
			$this->_series->warranty  = $seriesInfo->warranty;

		}

		return $this->_series;
	}
	/**
	 * Grab the price list from the table to link to youtube
	 * @return mixed playlist on success, null on failure
	 */
	public function getVideo() {

		if ( ! isset($this->_video)) {
			$invtId 	= $this->getState("product.invt_id");
			$fileName 	= isset($this->_product->file_name) ? $this->_product->file_name : $invtId;
			$db 		= JFactory::getDbo();

			if ( ! empty($fileName) ) {
				$db->setQuery($db->getQuery(true)
							->select("playlist_id")
							->from("#__xref_prods_videos")
							->where("invt_ids LIKE '%{$fileName}%'")
							->group("playlist_id"));
				$this->_video = $db->loadResult();
			}
		}

		return $this->_video;
	}
	/**
	 * Get photos for a product to setup product photo slide show images
	 *
	 * @return array
	 */
	public function getPhotos() {

		if (isset($this->_product) && !isset($this->_photos) && isset($this->_product->file_name)) {

			if ( !isset($this->cachedPhotos[$this->_product->file_name]) ) {

				foreach($this->_photoTypes as $suffix) {

					$photo 			= new stdClass;
					$fileNameSrc 	= "/{$this->_photoDir}/{$this->_product->file_name}{$suffix}";
					$otherIdSrc 	= "/{$this->_photoDir}/" . substr($this->_product->invt_id, 2) . $suffix;
					$invtIdSrc 		= "/{$this->_photoDir}/{$this->_product->invt_id}{$suffix}";

					if ($this->client->doesObjectExist($this->bucket, $fileNameSrc)) {
						$photo->src = $this->client->getObjectUrl($this->bucket, $fileNameSrc);
					}
					else if ($this->client->doesObjectExist($this->bucket, $invtIdSrc)) {
						$photo->src = $this->client->getObjectUrl($this->bucket, $invtIdSrc);
					}
					else if ($this->client->doesObjectExist($this->bucket, $otherIdSrc)) {
						$photo->src = $this->client->getObjectUrl($this->bucket, $otherIdSrc);
					}

					if( !empty($photo->src) ) {
						$photo->slideTitle	  = JTEXT::_('COM_BK_MODEL') . " {$this->_product->invt_id}, {$this->_product->short_desc}";
						$photo->slideAltTag	  = JTEXT::_('COM_BK_MODEL') . " {$this->_product->invt_id} " . ucwords(preg_replace("/^.([^_]+)_.+$/i", "$1", $suffix));

						$this->cachedPhotos[$this->_product->file_name][] = $photo;
					}
				}

				if (isset($this->cachedPhotos[$this->_product->file_name])) {
					$this->_photos = $this->cachedPhotos[$this->_product->file_name];
				}
			}

		}

		return $this->_photos;
	}

	/**
	 * Get documentation for a product
	 * search for the manual
	 * first, by filename
	 * then by the invt_id
	 *
	 * @return array
	 */
	public function getDocumentation() {

		if( isset($this->_product) AND !isset($this->_docs) ) {

			$this->_docs = array();
			$dasModel = array("DAS30", "DAS50", "DAS60");
			$productId = $this->_product->invt_id;
			
			foreach($this->_docTypes as $docType) {

				$tmp  		= new stdClass();
				$tmp->title = "COM_BKPRODUCT_" . strtoupper($docType);
				$this->_product->lang = str_replace("en-gb", "en-us", $this->_product->lang);

				// Establishing a valid file_name	
				if (isset($this->_product->file_name) && !in_array($productId, $dasModel)) 
				{
					$src  		= "/downloads/{$docType}s/{$this->_product->lang}/{$this->_product->file_name}_{$docType}.pdf";
				}

				// Establishing alternate variables that does not using file_name
				if (isset($this->_product->invt_id) && !in_array($productId, $dasModel)) 
				{
					$altSrc		= "/downloads/{$docType}s/{$this->_product->lang}/{$this->_product->invt_id}_{$docType}.pdf";
					$bkProduct 	= substr($this->_product->invt_id, 2); // Parsing out BK from the invt_id to concat into $altSrc2.
					$altSrc2	= "/downloads/{$docType}s/{$this->_product->lang}/{$bkProduct}_{$docType}.pdf";
				}

				// Work around for DAS Products redundancy with BK.
				if (isset($this->_product->invt_id) && in_array($productId, $dasModel)) 
				{
					$altSrc3 = "/downloads/{$docType}s/{$this->_product->lang}/{$this->_product->invt_id}_{$docType}.pdf";
				}

				// If the file_name exists on S3, then give me a src value within $tmp that has the bucket and append $src.
				if (!empty($src) && $this->client->doesObjectExist($this->bucket, $src)) {
					$tmp->src = "https://{$this->bucket}.s3.amazonaws.com{$src}";
				}

				// If the above doesnt work then use invt_id instead.
				else if (!empty($altSrc) && $this->client->doesObjectExist($this->bucket, $altSrc)) {
					$tmp->src = "https://{$this->bucket}.s3.amazonaws.com{$altSrc}";
				}

				// try the luck at something that has BK in front of the model number.
				else if ( !isset($tmp->src)  // If the last two methods didnt work.
						&& !empty($altSrc2) // And $altSrc2 is not null.
						&& $this->client->doesObjectExist($this->bucket, $altSrc2)) // Exists on AWS
				{
					$tmp->src = "https://{$this->bucket}.s3.amazonaws.com{$altSrc2}";
				}

				// Work around for DAS Products redundancy with BK.
				else if (!isset($tmp->src) 
					&& !empty($altSrc3) 
					&& $this->client->doesObjectExist($this->bucket, $altSrc3))
				{
					$tmp->src = "https://sefram.s3-eu-west-1.amazonaws.com{$altSrc3}";
				}			

				if( isset($tmp->src) ) {
					$this->_docs[] = $tmp;
				}

			}

		}

		return $this->_docs;
	}

	/**
	 * Get stock information for Where to Buy
	 * Available only in BKUS
	 *
	 * @return stdClass
	 */
	public function getStock() {

		// set the proper connection options for the wtb db
		$connOpts 	= array(
			'driver'	=> 'mysql',
			'host'		=> 'mysql.bkprecision.com',
			'user'		=> 'webaccess',
			'password' 	=> 'cust0m98',
			'database' 	=> 'bk_wtbdb',
			'prefix'	=> ''
			);
		$db 	= JDatabase::getInstance($connOpts);
		$invtId = $this->getState('product.invt_id');

		if ( !empty($invtId) ) {
			$db->setQuery($db->getQuery(true)
									->select("name, dist_id, invt_id, dist_partno, qty_onhand, last_update")
									->from("#__dist_inventory")
									->join("INNER", "#__distributors USING(dist_id)")
									->where("invt_id  LIKE '{$invtId}%'")
									->where("qty_onhand > 0")
									->order("last_update DESC, qty_onhand DESC"));

			$this->_results 				= new stdClass();
			$this->_results->stock 			= $db->loadAssocList();
			$this->_results->distributors	= array();
		}

		$distIdList = array();

		if ( is_array($this->_results->stock) && !empty($this->_results->stock) ) {
			foreach($this->_results->stock as &$result) {
				$result['last_update'] = date('M-j', strtotime($result['last_update']));
				$distIdList[] = $result['dist_id'];
			}

			$this->_results->total = count($distIdList);

			$distIdList = array_slice(array_unique($distIdList), 0, 3);

			$db->setQuery($db->getQuery(true)
					->select('*')
					->from('#__distributors as d')
					->leftJoin('#__dist_inventory as i USING(dist_id)')
					->where('d.dist_id IN ('.implode(",", $distIdList).')')
					->where("i.qty_onhand > 0")
					->group('i.dist_id')
					->order('i.last_update DESC, i.qty_onhand DESC'));

			$this->_results->distributors 	= $db->loadAssocList();
		}

		return $this->_results;
	}

	// transform bytes to human readable format
	// this is used in the template to convert software file sizes to human readable format
	public function byteConvert($bytes)
	{
		$s = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
		$e = floor(log($bytes)/log(1024));

		return sprintf('%.2f '.$s[$e], ($bytes/pow(1024, floor($e))));
	}
}
