<?php
// No direct access
defined('_JEXEC') or die;

use Aws\S3\S3Client;

class BkcontentModelProducts extends JModelList {
	/**
	* Products data
	*/
	protected $_parent 			= null;

	protected $_products		= null;

	protected $_invtIdList;

	protected $_unlimitedProds	= null;

	protected $_filters			= null;

	/**
	* Products total
	* @var integer
	*/
	var $_total					= null;

	/**
	* Pagination object
	* @var object
	*/
	var $_pagination			= null;

	protected $bucket;

	protected $client;

	protected $iconType;

	protected $iconDir;

	protected $cachedIcons		= array();


	public function __construct() {
		$params 			= JComponentHelper::getParams('com_bkcontent');

		$this->client 		= S3Client::factory(array(
				"key"		=> $params->get("accessKey"),
				"secret"	=> $params->get("secretKey")
		));
		$this->bucket 		= $params->get("s3bucket");
		$this->iconType 	= $params->get("iconType");
		$this->iconDir    	= $params->get("iconDir");

		parent::__construct(array());
	}

	/**
	* Method to auto-populate the model state.
	*
	* This method should only be called once per instantiation and is designed
	* to be called on the first call to the getState() method unless the model
	* configuration flag to ignore the request is set.
	*
	* Note: Calling getState in this method will result in recursion.
	*
	* @return 	void
	* @since	J1.6
	*/
	protected function populateState($ordering = NULL, $direction = NULL) {
		$app 		= JFactory::getApplication();
		$input 		= $app->input;

		// Get the parent category id
		$parentId 	= $input->getInt("cat_id");
		$this->setState("category.parent.id", $parentId);

		// Load the parameters
		$params 	= $app->getParams();
		$this->setState("params", $params);

		// Get the state to include
		$state 		= $input->getInt("state", "1");
		$this->setState("state", $state);

		// Get the filters
		$filters 	= $input->get("filters", "", "array");
		$this->setState("filters", $filters);

		// Get the pagination request variables
		$limit 		= $input->getInt("limit", "12");
		// check for google-mini crawler
		$agent 		= $_SERVER['HTTP_USER_AGENT'];
		if($limit == '*' OR preg_match('/^gsa-crawler.*$/', $agent)) {
			$limit = 1000;
		}
		$limit 		= (int)$limit;
		$limitStart = $input->getInt("start", 0);
		// Get the escaped fragment for Google Bot to follow ajax urls
		$frag 		= $input->getString("_escaped_fragment_");
		$this->setState("google._escaped_fragment_", $frag);

		if (isset($frag) && strpos($frag, "page") !== false) {
			$page = explode(":", $frag);
			$page = $page[1];
			$this->setState('app.page', $page);
		}

		if (isset($page) AND !empty($page))	{
			$limitStart = ($page-1)*$limit;
		}

		// In case limit has been changed, adjust it
		$limitStart = ($limit != 0 ? (floor($limitStart/$limit) * $limit) : 0);
		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitStart);

		$sortOptions = $input->getString("sortOptions", "invt_id.desc");
		$this->setState("sortOptions", $sortOptions);
		$sortOptions = explode(".", $sortOptions);
		$this->setState("sortCol", $sortOptions[0]);
		$this->setState("sortDir", $sortOptions[1]);
	}
	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'Bkcontent', $prefix = 'BkcontentTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}
	/**
	* Get the parent JCategoryNode object
	*
	* @return 	mixed	a JCategoryNode object or false on error
	* @see JCategories::get
	*/
	public function getParent() {
		jimport('joomla.application.categories');
		if (!is_object($this->_parent) OR !$this->_parent instanceof JCategoryNode) {
			$categories 			= JCategories::getInstance('Bkcontent');
			$this->_parent 			= $categories->get($this->getState('category.parent.id'));
			$params 				= new JRegistry;
			$params->loadString($this->_parent->params);
			$this->_parent->params = $params;
		}

		return $this->_parent;
	}
	/**
	* Get the products of a particular parent category and any descendent categories
	*
	* @return 	mixed 	an array of IDs or false on error
	*/
	public function getProducts() {

		if(empty($this->_products)) {

			if (!is_object($this->_parent) OR !$this->_parent instanceof JCategoryNode) {
				$this->getParent();
			}

			$filters = $this->getState('filters');

			if(!empty($filters)) {
				$invtIdList = $this->getFilteredProducts($filters);
				$query = $this->_db->getQuery(true)
											->from('#__bkproducts')
											->join('INNER','#__categories ON #__categories.id=#__bkproducts.cat_id')
											->join('LEFT', '#__xref_products_price ON #__xref_products_price.invt_id=#__bkproducts.invt_id')
											->select('#__bkproducts.*, #__categories.path as path, jos_xref_products_price.list_price as list_price')
											->where('cat_id = "'.$this->_parent->id.'"')
											->where('#__bkproducts.invt_id IN (\''.implode("','", $invtIdList).'\')')
											->where('state ='.(int)$this->getState('state'))
											->order("list_price DESC, #__bkproducts.invt_id ASC");
				$this->_db->setQuery($query);
			}
			else {

				$catChildren = $this->_parent->getChildren();
				$childrenIds = array();
				foreach($catChildren as $child) {
					$childrenIds[] = $child->id;
				}
				// we are dealing with a filter view, meaning we have to take into consideration pagination
				if(!empty($childrenIds)) {
					//$this->setState('limit', 1000);
					$this->_db->setQuery($this->_db->getQuery(true)
												->from('#__bkproducts')
												->join('INNER','#__categories ON #__categories.id=#__bkproducts.cat_id')
												->select('#__bkproducts.*, #__categories.path as path')
												->where('cat_id IN ("'.$this->_parent->id.'", "'.implode('","', $childrenIds).'")')
												->where('state ='.(int)$this->getState('state')));
				}
				// This is a subcatgory view and will not include pagination, so set the limit to something enormous
				else {

					$query = $this->_db->getQuery(true)
						->from('#__bkproducts')
						->join('INNER','#__categories ON #__categories.id=#__bkproducts.cat_id')
						->join('LEFT', '#__xref_products_price ON #__xref_products_price.invt_id=#__bkproducts.invt_id')
						->select('#__bkproducts.*, #__categories.path as path, jos_xref_products_price.list_price as list_price')
						->where('cat_id = "'.$this->_parent->id.'"')
						->where('state ='.(int)$this->getState('state'))
						->order("list_price DESC, #__bkproducts.invt_id ASC");
					$this->_db->setQuery($query);
				}

			}

			// Setup for pagination support
			$qry 					= $this->_db->getQuery();
			$productsList 			= $this->_getList($qry);
			// column 6 is the zero-indexed column of invt_id
			$this->_invtIdList		= array();
			$this->_invtIdList 		= $this->_db->loadColumn(6);
			$this->_unlimitedProds 	= $productsList;
			$productModel 			= JModelLegacy::getInstance("Product","BkcontentModel");
			$this->_products 		= array_slice($productsList, $this->getState('limitstart'), $this->getState('limit'));

			foreach($this->_products as $product) {
				$this->getProductHref($product);
				$this->getProductIcon($product);
			}
			$this->_total 			= (int) count($productsList);

		}

		return $this->_products;
	}

	protected function sortProductsList($a, $b) {
		$sortCol 				= $this->getState("sortCol");
		$sortDir 				= $this->getState("sortDir");

		if ($a->$sortCol == $b->$sortCol) {
			return 0;
		}
		// $a->list_price < $b->list_price === SORT ORDER ASC
		// $a->list_price > $b->list_price === SORT ORDER DESC
		if ($sortDir == "desc") {
			return ($a->$sortCol > $b->$sortCol) ? -1 : 1;
		}
		else {
			return ($a->$sortCol < $b->$sortCol) ? -1 : 1;
		}
	}

	protected function getProductHref(&$product) {
		$product->href 	= "{$product->path}/{$product->invt_id}-{$product->alias}";
		$app 			= JFactory::getApplication();
		if ($app->getCfg("sef_suffix")) {
			$product->href 	.= ".html";
		}
	}

	protected function getProductIcon(&$product) {

		if (empty($product->file_name)) {
			$product->file_name = $product->invt_id;
		}

		$image 	= "/{$this->iconDir}/{$product->file_name}{$this->iconType}";
		$altImg = "/{$this->iconDir}/" . substr($product->invt_id, 2) . $this->iconType;

		if ( !isset($this->cachedPhotos[$product->file_name]) ) {
			if ($this->client->doesObjectExist($this->bucket, $image)) {
				$icon = $this->client->getObjectUrl($this->bucket, $image);
			}
			elseif ($this->client->doesObjectExist($this->bucket, $altImg)) {
				$icon = $this->client->getObjectUrl($this->bucket, $altImg);
			}
			else {
				$icon = $this->client->getObjectUrl($this->bucket, "/{$this->iconDir}/no-image.jpg");
			}

			$this->cachedIcons[$product->file_name] = $icon;
		}

		$product->icon = $this->cachedIcons[$product->file_name];

	}

	protected function getFilteredProducts($filters = array()) {
		$prodList = array();

		$masterQry = $this->_db->getQuery(true);
		$masterQry->select('invt_id')
				->from("#__filters")
				->leftJoin("#__xref_prods_filters on #__xref_prods_filters.filter_id=#__filters.id");

		if (!empty($filters)) {
			foreach($filters as $filter) {
				$masterQry->where('invt_id IN (select distinct invt_id from #__filters left join #__xref_prods_filters on #__xref_prods_filters.filter_id=#__filters.id where name = "'.$filter["name"].'" AND value = "'.htmlentities($filter["value"], ENT_QUOTES | ENT_IGNORE, "UTF-8").'")');
			}
		}

		$this->_db->setQuery($masterQry);
		$prodList = $this->_db->loadColumn();
		sort($prodList);
		$prodList = array_unique($prodList);

		return $prodList;
	}
	/**
	* Get the filters for an array of products
	*
	* @return 	mixed 	an array of Filter IDs or false on error
	*/
	public function getFilters() {

		if (empty($this->_filters)) {

			if (empty($this->_invtIdList)) {
				$this->getProducts();
			}

			$prods = $this->_invtIdList;

			if ( ! empty($prods)) {
				$this->_db->setQuery($this->_db->getQuery(true)
								->select('DISTINCT #__filters.id as id, #__filters.name as name, #__filters.title as title')
								->from('#__filters')
								->join('INNER','#__xref_prods_filters on #__xref_prods_filters.filter_id=#__filters.id')
								->where('invt_id IN (\''.implode("','", $prods).'\')'));
				$this->_filters = $this->_db->loadObjectList();
				sort($this->_filters);
			}

			if (is_array($this->_filters)) {
				foreach ($this->_filters as $filter) {
					$this->_db->setQuery($this->_db->getQuery(true)
									->select('DISTINCT value')
									->from('#__xref_prods_filters')
									->where('invt_id in (\''.implode("','", $prods).'\')')
									->where('filter_id = '.$filter->id));
					$filter->values = $this->_db->loadColumn();
					sort($filter->values);
				}
			}

		}

		return $this->_filters;
	}

	public function getTotal() {
		if(empty($this->_total)) {
			$this->getProducts();
		}

	 	return $this->_total;
	}

	public function getPagination() {
		if (empty($this->_pagination)) {
			jimport('joomla.html.pagination');
			$layout = JFactory::getApplication()->input->getString("layout");
			$this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
			if ($layout !== 'discontinued') {
				$this->_pagination->setAdditionalUrlParam('cat_id', $this->_parent->id);
			}
			$this->_pagination->setAdditionalUrlParam('view', 'products');

			if ( ! empty($layout)) {
				$this->_pagination->setAdditionalUrlParam("layout", $layout);
			}
		}

		return $this->_pagination;
	}
}