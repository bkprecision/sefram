<?php
// No direct access
defined('_JEXEC') or die;

class BkcontentModelContact extends JModelForm {

	protected $_results 	= null;

	protected $_products 	= null;

	protected $_country		= null;

	protected $_messages	= null;

	protected $_message		= null;

	protected $deskAuth 	= "jeremy@bkprecision.com:Bkplacentia\\";

	/**
	* Method to auto-populate the model state.
	*
	* This method should only be called once per instantiation and is designed
	* to be called on the first call to the getState() method unless the model
	* configuration flag to ignore the request is set.
	*
	* Note: Calling getState in this method will result in recursion.
	*
	* @return 	void
	* @since	J1.6
	*/
	protected function populateState() {
		$input  							= JFactory::getApplication()->input;
		$insertData							= array();
		$insertData["Communication_Type"] 	= $input->get("communicationType", "", "PATH");
		$insertData["Model_Number"] 		= $input->get("Model_Number", "", "string");
		$insertData['Name'] 				= $input->get('FirstName', "", "string")." ".$input->get('LastName', "", "string");
		$insertData['Company_Name'] 		= $input->get('Company_Name', "", "string");
		$insertData['Job_Title'] 			= $input->get('Job_Title', "", "string");
		$insertData['Address'] 				= $input->get('Address', "", "string");
		$insertData['Address2'] 			= $input->get('Address2', "", "string");
		$insertData['City'] 				= $input->get('City', "", "string");
		$insertData['State_Province'] 		= $input->get('State_Province', "", "string");
		$insertData['Country'] 				= $input->get('Country', "", "string");
		$insertData['Zip_Postal'] 			= $input->get('Zip_Postal', "", "string");
		$insertData['Email'] 				= $input->get('Email', '', 'String');
		$insertData['Phone_Number'] 		= $input->get('Phone_Number', "", "string");
		$insertData['Fax_Number'] 			= $input->get('Fax_Number', "", "string");
		$insertData['Message_Detail'] 		= $input->get('Message_Detail', "", "string");
		$insertData['Manual_Type'] 			= $input->get('manual_requests', "", "string");
		if (is_array($insertData['Manual_Type'])) {
			$insertData['Manual_Type'] 		= implode(", ", $insertData['Manual_Type']);
		}
		$insertData['system_date'] 			= date('Y-m-d h:i:s');
		$insertData['php_sys_date'] 		= date('Y-m-d h:i:s');
		$insertData['user_ip_address'] 		= $_SERVER['REMOTE_ADDR'];
		$insertData['user_browser'] 		= $_SERVER['HTTP_USER_AGENT'];
		$insertData['status'] 				= $input->get('status');

		$this->setState('form.insertData', $insertData);

		$contactIdMd5 	= $input->get('contact_id');
		$this->setState('response.contact_id', $contactIdMd5);
		$emailMd5		= $input->get('es');
		$this->setState('response.email', $emailMd5);
		$takeServey		= $input->get('extra', "", "string");
		$takeServey		= $takeServey == 'survey' ? true : false;
		$this->setState('response.survey', $takeServey);
		$msgType 		= $input->get('msg_type', "", "string");
		$this->setState('response.msg_type', $msgType);
		$theMsg			= $input->get('the_message', "", "string");
		$this->setState('response.the_message', $theMsg);
		$surveyComment 	= $input->get('survey_comment', "", "string");
		$this->setState('response.survey_comment', $surveyComment);
		$surveyRating	= $input->get('survey_rating', "", "string");
		$this->setState('response.survey_rating', $surveyRating);
		$surveyDate		= mktime(date('G'), date('i'), date('s'), date('n'), date('j'), date('Y'));
		$this->setState('response.survey_date', $surveyDate);
		$addedViaIp		= $input->get('added_via_ip');
		$this->setState('response.added_via_ip', $addedViaIp);
	}

	/**
	 * Method to get the contact form.
	 *
	 * The base form is loaded from XML and then an event is fired
	 *
	 *
	 * @param   array  $data		An optional array of data for the form to interrogate.
	 * @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return  JForm	A JForm object on success, false on failure
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true) {

		// Get the form.
		$form = $this->loadForm('com_bkcontent.contact', 'contact', array('control' => 'jform', 'load_data' => true));
		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	public function getFormSubmit() {

		if (!isset($this->_results)) {

			$input = JFactory::getApplication()->input;
			// Validate the posted data.
			$form = $this->getForm();
			// results object
			$this->_results = new stdClass();

			// load the captcha plugin
			// verify response
			JPluginHelper::importPlugin('captcha');
			$dispatcher = JDispatcher::getInstance();
			$this->_results->success = $dispatcher->trigger('onCheckAnswer');
			$this->_results->success = $this->_results->success[0];

			if ($this->_results->success) {
				$connOpts 	= array(
						'driver'	=> 'mysql',
						'host'		=> 'mysql.bkprecision.com',
						'user'		=> 'webaccess',
						'password' 	=> 'cust0m98',
						'database' 	=> 'webdb',
						'prefix'	=> ''
				);
				$webdb 		= JDatabase::getInstance($connOpts);
				$insertData = $this->getState('form.insertData');
				$columns 	= array();
				$values 	= array();

				foreach ($insertData as $columnName => $value) {
					$columns[] 	= $columnName;
					$values[] 	= $value;
				}
				$insertQry = "INSERT INTO #__contact_us (".implode(",", $columns).") VALUES (".implode(",", $webdb->quote($values)).")";
				$webdb->setQuery($insertQry);

				$this->_results->success = $webdb->query();
				if (!$this->_results->success) {
					$this->_results->errorMsg = $webdb->getErrorMsg();
				}
				$this->_results->caseId = $this->saveDeskCase();
			}
		}

		return $this->_results;
	}

	private function saveDeskCase() {

		// check if the user exists in desk.com
		$customerId = $this->getDeskCustomerId();

		// create a case with the user id from desk.com
		$case = new stdClass();
		$input = JFactory::getApplication()->input;
		$email = $input->get("Email", "", "String");

		$case->type = "email";
		$commType = str_replace("_", " ", $input->get("communicationType"));
		$case->subject = "New {$commType} message.";
		$case->status = "New";
		$case->_links = new stdClass();
		$case->_links->customer = new stdClass();
		$case->_links->customer->href = "/api/v2/customers/{$customerId}";
		$case->_links->customer->class = "customer";
		$case->custom_fields = new stdClass();
		$case->custom_fields->comm_type = $input->get("communicationType");
		$model = $input->get("Model_Number");
		if ( ! empty($model)) {
			$case->custom_fields->model = $model;
		}
		$case->message = new stdClass();
		$case->message->direction = "in";
		$case->message->status = "received";
		$case->message->to = "jeremy@bkprecision.com";
		$case->message->from = $email;
		$case->message->subject = "New {$commType} message.";
		$case->message->body = $input->getString("Message_Detail");

		$options = array(
				CURLOPT_URL 		   	=> "https://bkprecision.desk.com/api/v2/cases",
				CURLOPT_RETURNTRANSFER	=> true,
				CURLOPT_HTTPAUTH		=> CURLAUTH_BASIC,
				CURLOPT_USERPWD			=> $this->deskAuth,
				CURLOPT_POST			=> true,
				CURLOPT_POSTFIELDS		=> json_encode($case, JSON_NUMERIC_CHECK),
				CURLOPT_HTTPHEADER		=> array('Accept: application/json', 'Content-Type: application/json')
		);

		$deskCase = $this->getDeskResponse($options);

		return $deskCase->id;
	}

	private function getDeskCustomerId() {

		$input 	 = JFactory::getApplication()->input;
		$email 	 = $input->get("Email", "", "string");
		$options = array(
				CURLOPT_URL 		   	=> "https://bkprecision.desk.com/api/v2/customers/search?email={$email}",
				CURLOPT_RETURNTRANSFER 	=> true,
				CURLOPT_HTTPAUTH		=> CURLAUTH_BASIC,
				CURLOPT_USERPWD 		=> $this->deskAuth,
				CURLOPT_HTTPHEADER 		=> array('Accept: application/json')
		);
		$user 	 = $this->getDeskResponse($options);

		// if user exists update with currently supplied data
		if ($user->total_entries > 0) {
			return $user->_embedded->entries[0]->id;
		}

		// else create the user in desk.com
		else {

			$emailObj 		 = new stdClass();
			$emailObj->type  = "home";
			$emailObj->value = $email;
			$postData = array(
					"first_name" 	=> $input->get('FirstName', "", "string"),
					"last_name"  	=> $input->get('LastName', "", "string"),
					"company"	 	=> $input->get('Company_Name', "", "string"),
					"title"		 	=> $input->get('Job_Title', "", "string"),
					"emails"	 	=> array($emailObj)
			);
			$phone = $input->get("Phone_Number");
			if ( ! empty($phone)) {
				$phoneObj 				   	= new stdClass();
				$phoneObj->type 		   	= "home";
				$phoneObj->value 			= $phone;
				$postData["phone_numbers"] 	= array($phoneObj);
			}

			$options[CURLOPT_URL]  		 = "https://bkprecision.desk.com/api/v2/customers";
			$options[CURLOPT_POST] 		 = true;
			$options[CURLOPT_POSTFIELDS] = json_encode($postData, JSON_NUMERIC_CHECK);
			$options[CURLOPT_HTTPHEADER] = array('Accept: application/json', 'Content-Type: application/json');
			$user 						 = $this->getDeskResponse($options);

			return $user->id;
		}

		return false;
	}

	public function getNewSubscription() {
		return $this->saveToSalesForce();
	}

	public function getUnsubscribe()
	{
		jimport('salesforce.soapclient');
		$mySforceConnection = new SforcePartnerClient();
		$mySoapClient 		= $mySforceConnection->createConnection(SF_SOAPCLIENT_ROOT."partner.wsdl.xml");
		$mylogin 			= $mySforceConnection->login("gmello@bkprecision.com", "brazilrules01xllPKTLYMpWkSISH6a9ie8Xf");
		$timestamp 			= $mySforceConnection->getServerTimestamp()->timestamp;

		$formData = $this->getState('form.insertData');
		$Email = $formData['Email'];
		$query = "SELECT Id, Email, Name from Contact WHERE Email = '".$Email."'";
		$queryResult = $mySforceConnection->query($query);
		$records = $queryResult->records;
		$num_recs = count($records);
		if($num_recs == 0)
		{
			$response = new stdClass();
			$response->success = false;
			$response->errorMsg = 'We did not find any record of your information in our Newsletter database.';
		}
		else
		{
			$contactId = $records[0]->Id;
			$response = $mySforceConnection->delete(array($contactId));
		}

		return $response;
	}

	public function getMessageDetail() {

		$input 	 = JFactory::getApplication()->input;
		$caseId  = $input->get("caseid", "", "string");
		$email   = $input->get("email", "", "string");
		$options = array(
				CURLOPT_URL 		   	=> "https://bkprecision.desk.com/api/v2/cases/{$caseId}/message",
				CURLOPT_RETURNTRANSFER 	=> true,
				CURLOPT_HTTPAUTH		=> CURLAUTH_BASIC,
				CURLOPT_USERPWD 		=> $this->deskAuth,
				CURLOPT_HTTPHEADER 		=> array('Accept: application/json')
		);
		$message = $this->getDeskResponse($options);
		// check if the supplied email is the same as the email assigned in the caseid
		if (($message->from !== $email)) {
			$validEmail = false;
			$options[CURLOPT_URL] = "https://bkprecision.desk.com{$message->_links->customer->href}";
			$customer = $this->getDeskResponse($options);
			foreach($customer->emails as $custEmail) {
				if ($custEmail->value === $email) {
					$validEmail = true;
				}
			}
			if ( ! $validEmail) {
				return array();
			}
		}
		// get the rest of the correspondence
		$options[CURLOPT_URL] = "https://bkprecision.desk.com/api/v2/cases/{$caseId}/replies";
		$replies = $this->getDeskResponse($options);

		$this->_messages 				 = new stdClass();
		$this->_messages->original 		 = $message;
		$this->_messages->correspondence = $replies;

		return $this->_messages;
	}

	public function getSurvey() {

		$input 	 = JFactory::getApplication()->input;
		$caseId  = $input->get("caseid", "", "string");
		$email   = $input->get("email", "", "string");
		$rating  = $input->get("survey_rating", 0, "int");
		$comment = $input->get("survey_comment", "", "string");
		$survey  = new stdClass();
		$note 	 = new stdClass();

		$survey->custom_fields = new stdClass();
		$survey->custom_fields->survey_rating = $rating;
		$survey->labels = array("Survey");
		if ($rating === 1) {
			$survey->labels[] = "Negative Rating";
		}

		$options = array(
			CURLOPT_URL 		   	=> "https://bkprecision.desk.com/api/v2/cases/{$caseId}",
			CURLOPT_CUSTOMREQUEST  	=> "PATCH",
			CURLOPT_RETURNTRANSFER 	=> true,
			CURLOPT_HTTPAUTH		=> CURLAUTH_BASIC,
			CURLOPT_USERPWD 		=> $this->deskAuth,
			CURLOPT_HTTPHEADER 		=> array('Accept: application/json'),
			CURLOPT_POSTFIELDS 		=> json_encode($survey, JSON_NUMERIC_CHECK)
		);
		$res 		 = new stdClass();
		// first update the case with the rating
		$res->rating = $this->getDeskResponse($options);

		if ( ! empty($comment)) {

			$note->body = "Customer Survey Comment:\n\r\n\r{$comment}\n\r\n\r\n\rScore: {$rating}";
			unset($options[CURLOPT_CUSTOMREQUEST]);
			$options[CURLOPT_URL]  		 = "https://bkprecision.desk.com/api/v2/cases/{$caseId}/notes";
			$options[CURLOPT_POST] 		 = true;
			$options[CURLOPT_POSTFIELDS] = json_encode($note, JSON_NUMERIC_CHECK);
			// then add the note if a comment exists

			$res->note  = $this->getDeskResponse($options);
		}

		return $res;
	}

	private function getDeskResponse($options = array()) {

		if (empty($options)) {
			return false;
		}

		$ch  = curl_init();
		curl_setopt_array($ch, $options);
		$res = json_decode(curl_exec($ch));
		curl_close($ch);

		return $res;
	}

	public function getNewMsgDetail()
	{
		if(! isset($this->_message))
		{
			$connOpts 	= array(
				'driver'	=> 'mysql',
				'host'		=> 'mysql.bkprecision.com',
				'user'		=> 'webaccess',
				'password' 	=> 'cust0m98',
				'database' 	=> 'webdb',
				'prefix'	=> ''
				);
			$webdb 				= JDatabase::getInstance($connOpts);
			$this->_message 	= new stdClass();
			$msgType			= $this->getState('response.msg_type');
			$webdb->setQuery($webdb->getQuery(true)
									->select('*')
									->from('#__contact_us')
									->where('md5(contact_id) = "'.$this->getState('response.contact_id').'"')
									->where('md5(Email) = "'.$this->getState('response.email').'"'));
			$row 				= $webdb->loadObject();
			$contactRecordId 	= $row->contact_id;
			$contactEmail		= $row->Email;
			if(empty($contactRecordId))
			{
				$this->_message->error = true;
			}
			else
			{
				if($msgType == 'user_srvy')
				{
					$this->_message->detail 				= $this->newSurvey($contactRecordId, $contactEmail);
					$this->_message->detail->survey_comment = nl2br($this->_message->detail->survey_comment);
					$this->_message->detail->date_added 	= date("F jS, Y, g:i a", strtotime($this->_message->detail->date_added));
					$webdb->setQuery($webdb->getQuery(true)
											->update('#__contact_us')
											->set('status="C"')
											->where('md5(contact_id) = "'.$this->getState('response.contact_id').'"')
											->where('md5(Email) = "'.$this->getState('response.email').'"'));

				}
				else
				{
					$this->_message->detail 				= $this->newCorrespondence($contactRecordId, $contactEmail);
					$this->_message->detail->the_message 	= nl2br($this->_message->detail->the_message);
					$this->_message->detail->date_added 	= date("F jS, Y, g:i a", strtotime($this->_message->detail->date_added));
					$webdb->setQuery($webdb->getQuery(true)
											->update('#__contact_us')
											->set('status="P"')
											->where('md5(contact_id) = "'.$this->getState('response.contact_id').'"')
											->where('md5(Email) = "'.$this->getState('response.email').'"'));
				}

				$webdb->query();
				$this->emailNotification($contactRecordId, $contactEmail, $this->_message->detail->the_message);
			}
		}

		return $this->_message;
	}

	public function getProducts()
	{
		if(!isset($this->_products))
		{
			$this->_db->setQuery($this->_db->getQuery(true)
											->select('invt_id')
											->from('#__bkproducts'));

			$this->_products = $this->_db->loadColumn(0);
		}

		return $this->_products;
	}

	public function getCountries()
	{
		if(!isset($this->_country))
		{
			$this->_db->setQuery($this->_db->getQuery(true)
											->select('*')
											->from('#__countrycode')
											->order(' if (CountryID="US", -1, CountryName)'));
			$this->_country = $this->_db->loadObjectList();
		}

		return $this->_country;
	}

	private function newSurvey($contact_record_id = null, $mesg_from = null)
	{
		$connOpts 	= array(
			'driver'	=> 'mysql',
			'host'		=> 'mysql.bkprecision.com',
			'user'		=> 'webaccess',
			'password' 	=> 'cust0m98',
			'database' 	=> 'webdb',
			'prefix'	=> ''
			);
		$webdb 				= JDatabase::getInstance($connOpts);

		$insertQry = "INSERT INTO #__contact_us_messages (contact_record_id, msg_type, mesg_from, survey_rating, survey_comment, survey_date, date_added, added_via_ip) VALUES ('".$contact_record_id."', '".$this->getState('response.msg_type')."', '".$mesg_from."', '".$this->getState('response.survey_rating')."', '".$this->getState('response.survey_comment')."', '".$this->getState('response.survey_date')."', '".date('Y-m-d h:i:s')."', '".$this->getState('response.added_via_ip')."')";
		$webdb->setQuery($insertQry);
		$details = new stdClass();
		$details->success = $webdb->query();
		if( ! $details->success)
		{
			$details->errorMsg = $webdb->getErrorMsg();
		}
		$mesgId = $webdb->insertid();
		$webdb->setQuery($webdb->getQuery(true)
								->select('*')
								->from('#__contact_us_messages')
								->where('mesg_id = '.$mesgId));
		$details = $webdb->loadObject();

		return $details;
	}

	private function newCorrespondence($contact_record_id = null, $mesg_from = null)
	{
		$connOpts 	= array(
			'driver'	=> 'mysql',
			'host'		=> 'mysql.bkprecision.com',
			'user'		=> 'webaccess',
			'password' 	=> 'cust0m98',
			'database' 	=> 'webdb',
			'prefix'	=> ''
			);
		$webdb 				= JDatabase::getInstance($connOpts);

		$insertQry = "INSERT INTO #__contact_us_messages (contact_record_id, the_message, msg_type, mesg_from, date_added, added_via_ip) VALUES ('".$contact_record_id."', '".$this->getState('response.the_message')."', '".$this->getState('response.msg_type')."', '".$mesg_from."', '".date('Y-m-d h:i:s')."', '".$this->getState('response.added_via_ip')."')";
		$webdb->setQuery($insertQry);
		$details = new stdClass();
		$details->success = $webdb->query();
		if( ! $details->success)
		{
			$details->errorMsg = $webdb->getErrorMsg();
		}
		$mesgId = $webdb->insertid();
		$webdb->setQuery($webdb->getQuery(true)
								->select('*')
								->from('#__contact_us_messages')
								->where('mesg_id = '.$mesgId));
		$details = $webdb->loadObject();

		return $details;
	}

	private function emailNotification($contact_id = null, $email = null, $msg = null)
	{
		jimport('joomla.mail.mail');

		$connOpts 	= array(
			'driver'	=> 'mysql',
			'host'		=> 'mysql.bkprecision.com',
			'user'		=> 'webaccess',
			'password' 	=> 'cust0m98',
			'database' 	=> 'webdb',
			'prefix'	=> ''
			);
		$webdb 			= & JDatabase::getInstance($connOpts);
		$webdb->setQuery($webdb->getQuery(true)
								->select('*')
								->from('#__contact_us')
								->where('contact_id = "'.$contact_id.'"')
								->where('Email = "'.$email.'"'));
		$row 			= $webdb->loadObject();
		$endUserName	= $row->Name;
		$assignedMgrId	= $row->assigned_to_user_id;
		$msgType		= $this->getState('response.msg_type');

		$webdb->setQuery($webdb->getQuery(true)
								->select('*')
								->from('#__contact_us_mgrs')
								->where('user_id = '.$assignedMgrId));
		$row 			= $webdb->loadObject();
		$assignedMgrEmail = $row->bk_uname."@bkprecision.com";

		$jmail = JMail::getInstance();
		$jmail->addRecipient(array($assignedMgrEmail, 'rforbes@bkprecision.com', 'lmcculley@bkprecision.com'));
		$jmail->setSender(array('do-not-reply@bkprecision.com', 'B&K Precision'));

		if($msgType == 'user_srvy')
		{
			$_message_body = '
			<html>
			<head>
			<title>B&K Precision: Customer Rating Survey Response</title>
			</head>
			<body>


			<table width="472" border="0">
				<tr>
					<td colspan="2"><p>'.$endUserName.' ('.$email.') has submitted a rating on your responses</strong>, thus closing the case. If a low rating was given, you may need to take action.</p><br /></th>
				</tr>
				<tr>
					<td valign="top"><strong>Message Body:</strong></td><td valign="top"><p>'.$this->getState('response.survey_comment').'</p></td>
				</tr>
				<tr>
					<td valign="top"><strong>Rating:</strong></td><td valign="top">'.$this->getState('response.survey_rating').'</td>
				</tr>
				<tr>
					<td colspan="2"><a href="http://apps.bkprecision.com/manage08/contact/record_manager.php?action=manage_record&irecord='.$contact_id.'">Click here</a> review and/or manage the case.</td>
				</tr>
			</table>

			</body>
			</html>';
			$jmail->setSubject('B&K Precision: Customer Rating Survey Response');
		}
		else
		{
			$_message_body = '
			<html>
			<head>
			<title>B&K Precision: Customer Response via Contact Us Response Form</title>
			</head>
			<body>


			<table width="472" border="0">
				<tr>
					<td colspan="2"><p>'.$endUserName.' ('.$email.') has replied to your last response</strong>, thus reopening the case with a Pending status.</p><br /></th>
				</tr>
				<tr>
					<td colspan="2" valign="top"><strong>Case ID: '.$contact_id.'</strong></td>
				</tr>
				<tr>
					<td colspan="2" valign="top"><strong>Message Body:</strong></td>
				</tr>
				<tr>
					<td colspan="2"><p>'.$this->_message->detail->the_message.'</p><br /></td>
				</tr>
				<tr>
					<td colspan="2"><a href="http://apps.bkprecision.com/manage08/contact/record_manager.php?action=manage_record&irecord='.$contact_id.'">Click here</a> review and/or manage the case.</td>
				</tr>
			</table>

			</body>
			</html>';
			$jmail->setSubject('B&K Precision: Customer Response via Contact Us Response Form');
		}

		$jmail->setBody($_message_body);
		$jmail->IsHTML(true);

		$jmail->Send();

		if($msgType == 'user_srvy')
		{
			$rating = (int)$this->getState('response.survey_rating');
			if($rating < 3)
			{
				//send email to jorg
				$jmail2 = JMail::getInstance();
				$jmail2->addRecipient(array('jhesser@bkprecision.com', 'rforbes@bkprecision.com', 'lmcculley@bkprecision.com'));
				$jmail2->setSender(array('do-not-reply@bkprecision.com', 'B&K Precision'));
				$jmail2->setSubject('B&K Precision: Low-Scored Customer Rating Survey Alert');
				$_message_body = '
				<html>
				<head>
				<title>B&K Precision: Low-Scored Customer Rating Survey Alert</title>
				</head>
				<body>


				<table width="472" border="0">
					<tr>
						<td colspan="2"><p>'.$endUserName.' ('.$email.') has submitted a low-scored survey rating</strong>.</p><br /></th>
					</tr>
					<tr>
						<td valign="top"><strong>Message Body:</strong></td><td valign="top"><p>'.$this->getState('response.survey_comment').'</p></td>
					</tr>
					<tr>
						<td valign="top"><strong>Rating:</strong></td><td valign="top">'.$this->getState('response.survey_rating').'</td>
					</tr>
					<tr>
						<td colspan="2"><a href="http://apps.bkprecision.com/manage08/contact/record_manager.php?action=manage_record&irecord='.$contact_id.'">Click here</a> review and/or manage the case.</td>
					</tr>
				</table>

				</body>
				</html>';
				$jmail2->setBody($_message_body);
				$jmail2->IsHTML(true);

				$jmail2->Send();
			}
		}

	}

	private function saveToSalesForce()
	{
		jimport('salesforce.soapclient');
		$input 				= JFactory::getApplication()->input;
		$insertData 		= $this->getState('form.insertData');
		$Company_Name 		= htmlentities($insertData['Company_Name'], ENT_QUOTES | ENT_IGNORE, "UTF-8");
		$FirstName 			= htmlentities($input->get('FirstName'), ENT_QUOTES | ENT_IGNORE, "UTF-8");
		$LastName 			= htmlentities($input->get('LastName'), ENT_QUOTES | ENT_IGNORE, "UTF-8");
		$Email 				= htmlentities($insertData['Email'], ENT_QUOTES | ENT_IGNORE, "UTF-8");
		$Job_Title 			= htmlentities($insertData['Job_Title'], ENT_QUOTES | ENT_IGNORE, "UTF-8");
		$LeadSource 		= htmlentities('Contact us-'.$insertData['Communication_Type'], ENT_QUOTES | ENT_IGNORE, "UTF-8");
		if($insertData['Communication_Type'] == 'subscribe')
		{
			$LeadSource = 'Newsletter';
		}
		$Phone_Number		= htmlentities($insertData['Phone_Number'], ENT_QUOTES | ENT_IGNORE, "UTF-8");
		$Fax_Number			= htmlentities($insertData['Fax_Number'], ENT_QUOTES | ENT_IGNORE, "UTF-8");
		$RecordTypeId 		= '012A00000003rrmIAA'; //RecordTypeId = 'End Users' in salesforce
		$mySforceConnection = new SforcePartnerClient();
		$mySoapClient 		= $mySforceConnection->createConnection(SF_SOAPCLIENT_ROOT."partner.wsdl.xml");
		$mylogin 			= $mySforceConnection->login("gmello@bkprecision.com", "brazilrules01xllPKTLYMpWkSISH6a9ie8Xf");
		$timestamp 			= $mySforceConnection->getServerTimestamp()->timestamp;

		//first query the accounts (company name) for the account ID
		$query 				= "SELECT Id, Name from Account WHERE Name = '".$Company_Name."'";
		$queryResult 		= $mySforceConnection->query($query);
		$records 			= $queryResult->records;

		if(count($records) == 1)
		{
			//if the company exists just grab the id
			$accountId 		= $record[0]->Id;
		}
		else //if not, create it and then grab the created account id
		{
			if( ! empty($Company_Name))
			{
				$sObj 			= new SObject();
				$sObj->type 	= 'Account';
				$sObj->fields 	= array('Name' => $Company_Name);
				$response 		= $mySforceConnection->create($sObj);
				$accountId 		= $response->id;
			}
			else
			{
				$accountId 		= null;
			}
		}


		//second query the contacts db to see if the user exists already
		$query = "SELECT Id, Email, FirstName, LastName from Contact WHERE Email = '".$Email."'";
		$queryResult = $mySforceConnection->query($query);
		$records = $queryResult->records;
		$num_recs = count($records);
		if($num_recs > 0) //user already exists, update any info
		{
			$ContactId = $records[0]->Id;

			$sObj = new SObject();
			$sObj->type = 'Contact';
			$sObj->fields = array(
				'ID'=>$ContactId,
				'RecordTypeId'=>$RecordTypeId, //RecordTypeId = 'End Users' in salesforce
				'LeadSource'=>$LeadSource,
				'Email'=>$Email,
				'FirstName'=>$FirstName,
				'LastName'=>$LastName,
				'Title'=>$Job_Title,
				'Phone'=>$Phone_Number,
				'Fax'=>$Fax_Number,
				'AccountId'=>$accountId);
			$response = $mySforceConnection->update(array($sObj));
		}
		else
		{
			$sObj = new SObject();
			$sObj->type = 'Contact';
			$sObj->fields = array(
				'RecordTypeId'=>$RecordTypeId,
				'LeadSource'=>$LeadSource,
				'Email'=>$Email,
				'FirstName'=>$FirstName,
				'LastName'=>$LastName,
				'Title'=>$Job_Title,
				'Phone'=>$Phone_Number,
				'Fax'=>$Fax_Number,
				'Time_Stamp__c'=>$timestamp,
				'AccountId'=>$accountId);
			$response = $mySforceConnection->create(array($sObj));
		}

		return $response;
	}
}
