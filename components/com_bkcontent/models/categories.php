<?php
// No direct access
defined('_JEXEC') or die;

class BkcontentModelCategories extends JModelLegacy {
	/**
	* Items data
	*/
	protected $_items 			= null;

	protected $_parent 			= null;

	protected $_children 		= array();

	protected $_products		= null;

	protected $_productCats		= null;

	protected $_accessories		= null;

	/**
	* Method to auto-populate the model state.
	*
	* This method should only be called once per instantiation and is designed
	* to be called on the first call to the getState() method unless the model
	* configuration flag to ignore the request is set.
	*
	* Note: Calling getState in this method will result in recursion.
	*
	* @return 	void
	* @since	J1.6
	*/
	protected function populateState() {
		$app		= JFactory::getApplication();
		$input 		= $app->input;

		// Get the parent category id
		$parentId 	= $input->getInt("cat_id");
		$this->setState("category.parent.id", $parentId);

		// Get the escaped fragment for Google Bot to follow ajax urls
		$frag 		= $input->getString("_escaped_fragment_");
		$this->setState("google._escaped_fragment_", $frag);

		// Get the state to include
		$state 		= $input->getInt("state", "1");
		$this->setState("state", $state);

		// Load the parameters
		$params 	= $app->getParams();
		$this->setState("params", $params);
	}
	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'Bkcontent', $prefix = 'BkcontentTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}
	/**
	* Get the child categories of a parent cat id
	*
	* @return mixed 	an array of JCategoryNode objects or false if an error occurs.
	* @see JCategories::get
	*/
	public function getSubcats($recursive = false) {
		jimport('joomla.application.categories');
		if (!count($this->_items)) {
			$options 				= array();
			$categories 			= JCategories::getInstance('Bkcontent');
			$this->_parent 			= $categories->get($this->getState('category.parent.id'));
			if (is_object($this->_parent)) {
				$this->_items 		= $this->_parent->getChildren($recursive);
			}
			else {
				$this->_items 		= false;
			}
		}
		return $this->_items;
	}
	/**
	* Get the parent JCategoryNode object
	*
	* @return 	mixed	a JCategoryNode object or false on error
	* @see JCategories::get
	*/
	public function getParent() {
		if(!is_object($this->_parent)) {
			$this->getSubcats();
		}
		$params = new JRegistry;
		$params->loadString($this->_parent->params);
		$this->_parent->params = $params;

		return $this->_parent;
	}

	/**
	* Get the accessories of a particular parent category and any descendent categories
	*
	* @return 	array
	*/
	public function getAccessories() {
		$model = $this->getInstance("Accessories", "BkcontentModel");
		return $model->getAccessories();
	}
	/**
	* Get the products of a particular parent category and any descendent categories
	*
	* @param	JCategoryNode
	* @return 	mixed 	an array of IDs or false on error
	*/
	public function getProducts() {
		if (!count($this->_products)) {
			if (!is_object($this->_parent) OR !count($this->_items)) {
				$this->getSubcats();
			}

			// Clean out the array
			$this->_children = array();
			foreach ($this->_items as $node) {
				$this->getChildIdList($node);
			}
			$catName = str_replace('/', '', $this->getState('category.name'));
			if (isset($catName) AND !empty($catName)) {
				$this->_db->setQuery($this->_db->getQuery(true)
												->select('id')
												->from('#__categories')
												->where('alias="'.$catName.'"'));
				$catId = $this->_db->loadResult();
				$this->_db->setQuery($this->_db->getQuery(true)
											->from('#__bkproducts')
											->leftJoin('#__xref_products_price USING(invt_id)')
											->leftJoin('#__categories ON #__categories.id=#__bkproducts.cat_id')
											->select('#__bkproducts.*, #__categories.path as path, #__xref_products_price.calibration_cost, #__xref_products_price.calib_data_cost, #__xref_products_price.list_price, #__xref_products_price.repair_cost, #__xref_products_price.ship_cost, #__xref_products_price.upgrade_price')
											->where('cat_id IN (\''.$catId.'\')'));
			}
			else {
				$this->_db->setQuery($this->_db->getQuery(true)
											->from('#__bkproducts')
											->leftJoin('#__xref_products_price USING(invt_id)')
											->leftJoin('#__categories ON #__categories.id=#__bkproducts.cat_id')
											->select('#__bkproducts.*, #__categories.path as path, #__xref_products_price.calibration_cost, #__xref_products_price.calib_data_cost, #__xref_products_price.list_price, #__xref_products_price.repair_cost, #__xref_products_price.ship_cost, #__xref_products_price.upgrade_price')
											->where('cat_id IN (\''.$this->_parent->id.'\',\''.implode("','", $this->_children).'\')'));
			}

			$this->_products = $this->_db->loadObjectList();
		}

		return $this->_products;
	}

	public function getProductCategories() {

		$app 				= JFactory::getApplication();
		$menuItems			= $app->getMenu()->getItems("component", "com_bkcontent");
		$base 				= JURI::getInstance();
		$path 				= preg_replace('/^(.*\/).+\.html$/i', '$1', $base->getPath());
		$categories 		= $this->getSubcats();
		$this->_productCats = array();

		foreach($categories as $category) {
			// exclude all categories without
			if($category->note !== "hide") {
				$tmpObj 			= new stdClass();
				$tmpObj->title 		= $category->title;
				// grab each category parameters and load them so we can get values
				$params		 		= new JRegistry();
				$params->loadString($category->params);
				$category->params 	= $params;
				$tmpObj->image 		= $category->params->get('image');

				// Is this seriously the best way???
				// for now it is...
				foreach($menuItems as $item) {
					if(isset($item->query['cat_id']) AND $item->query['cat_id'] == $category->id) {
						$tmpObj->link 	= $path . $item->route . ".html";
						$tmpObj->class	= $item->alias . " product-category";
					}
				}

				if (isset($tmpObj->link)) {
					$this->_productCats[] = $tmpObj;
				}
			}
		}

		return array_chunk($this->_productCats, 4);

	}

	public function getNewProducts() {
		$db = JFactory::getDbo();
		$lang = JFactory::getLanguage()->getTag();
		$db->setQuery($db->getQuery(TRUE)
							->select("p.alias, p.file_name, p.invt_id, p.series, p.title as productTitle, s.name, s.title as seriesTitle, c.path, CONCAT_WS('/', c.path,CONCAT_WS('-', p.invt_id, p.alias)) as link")
							->from("#__bkproducts as p")
							->leftJoin("#__bkseries as s on s.id=p.series")
							->leftJoin("#__categories as c on c.id=p.cat_id")
							->where("p.state=1")
							->where("c.language = '{$lang}'")
							->order("p.modified DESC"), 0, 5);

		return $db->loadObjectList();
	}
	/**
	* Get a list of category IDs of a particular JCategoryNode and it's descendants
	*
	* @param 	JCategoryNode
	* @return 	void
	* @see 		JCategories::Get
	*/
	protected function getChildIdList($node = null) {
		if ($node instanceof JCategoryNode) {
			$this->_children[] = $node->id;
			if ($node->hasChildren()) {
				$children = $node->getChildren();
				foreach ($children as $cNode) {
					$this->getChildIdList($cNode);
				}
			}
		}
	}
}