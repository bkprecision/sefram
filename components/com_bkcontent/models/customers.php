<?php
// No direct access
defined('_JEXEC') or die;

class Ciphers {

	private $securekey, $iv;

	function __construct($textkey) {
		$this->securekey 	= hash('sha256', $textkey, TRUE);
		$this->iv			= mcrypt_create_iv(32, MCRYPT_DEV_URANDOM);
	}

	function encrypt($input) {
		return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->securekey, $input, MCRYPT_MODE_ECB, $this->iv));
	}

	function decrypt($input) {
		return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->securekey, base64_decode($input), MCRYPT_MODE_ECB, $this->iv));
	}
}

class BkcontentModelCustomers extends JModelLegacy {

	protected $customer 		= null;

	protected $_products 		= null;

	protected $_country			= null;

	protected $_profile			= null;

	protected $orders			= array();

	protected $orderNumbers		= array();

	protected $trackingNumbers 	= array();

	/**
	* Method to auto-populate the model state.
	*
	* This method should only be called once per instantiation and is designed
	* to be called on the first call to the getState() method unless the model
	* configuration flag to ignore the request is set.
	*
	* Note: Calling getState in this method will result in recursion.
	*
	* @return 	void
	* @since	J1.6
	*/
	protected function populateState() {

		// make sure to grab all needed variables
		$input			= JFactory::getApplication()->input;
		$customerId 	= $input->get('customer_id');
		$this->setState('customer.customer_id', $customerId);
		$passwd			= $input->get('passwd');
		$this->setState('customer.passwd', $passwd);

		$CustOrdNbr		= $input->get('CustOrdNbr');
		$this->setState('orders.CustOrdNbr', $CustOrdNbr);

		// json view type
		$type 			= $input->get('type');
		$this->setState('request.type', $type);

		parent::populateState();
	}

	/**
	 * login the user or return a default guest user
	 *
	 * @return JUser
	 */
	public function getUser() {

		$customerId = $this->getState("customer.customer_id");
		$password	= $this->getState("customer.passwd");
		$user 		= JUser::getInstance();
		$session	= JFactory::getSession();

		if (!empty($customerId) AND !empty($password)) {

			$db			= JFactory::getDbo();
			$db->setQuery($db->getQuery(true)
					->select("c.*")
					->from("#__xref_customer_active AS x")
					->join("INNER", "#__bkcustomer AS c USING(CustId)")
					->where("x.CustId='{$customerId}'")
					->where("x.MD5Password=md5('{$password}')"));

			$customer 	 	= $db->loadObject();

			if (!empty($customer->CustId)) {
				$user->id 	 	= $customerId;
				$user->username = $customerId;
				$user->email 	= $customer->EMailAddr;
				$user->name  	= $customer->BillName;
				$user->guest 	= 0;
				// determine if the customer is domestic or international
				$user->domestic = ($customer->Country == 'US' || $customer->Country == 'CA') ? true : false;
			}

			$session->set("customer.user", $user);
		}

		return $user;
	}

	/**
	 * Logout the user
	 * make sure to clear the state
	 */
	public function logout() {

		$user 	 	= JUser::getInstance();
		$session 	= JFactory::getSession();
		$customerId = $this->setState("customer.customer_id", "");
		$password	= $this->setState("customer.passwd", "");
		$session->set("customer.user", $user);
	}

	public function getGuestStock() {

		$db 	  = JFactory::getDbo();
		$prodsQry = $db->getQuery(true);
		$accsQry  = $db->getQuery(true);
		$layout   = JFactory::getApplication()->input->getCmd("layout", "default");

		// setup the products query
		$prodsQry->select("x.invt_id, p.title, x.qty_avail, pr.list_price")
			->join("INNER", "#__bkproducts as p on p.invt_id=x.invt_id")
			->join("LEFT OUTER", "#__xref_products_price as pr on pr.invt_id = p.invt_id")
			->where("x.invt_id IN (select invt_id from #__bkproducts where state = 1)");

		// setup the accessories query
		$accsQry->select("x.invt_id, a.title, x.qty_avail, pr.list_price")
			->join("INNER", "#__bkaccessories as a on a.invt_id=x.invt_id")
			->join("LEFT OUTER", "#__xref_products_price as pr on pr.invt_id = a.invt_id")
			->where("x.invt_id IN (select invt_id from #__bkaccessories where state = 1)");

		// make sure to get data from correct source
		if ($layout == "csa") {
			$prodsQry->from("#__xref_stock_csa as x")->where("x.qty_avail > 0");
			$accsQry->from("#__xref_stock_csa as x")->where("x.qty_avail > 0");
		}
		else {
			$prodsQry->from("#__xref_stock as x");
			$accsQry->from("#__xref_stock as x");
		}

		// union distinct products with accessories
		$prodsQry->unionDistinct($accsQry);

		$db->setQuery($prodsQry);
		$stock = $db->loadObjectList();
		usort($stock, array($this, "compareInvtId"));

		return $stock;
	}

	private function compareInvtId($a, $b) {
		return strcmp($a->invt_id, $b->invt_id);
	}

	/**
	 * Return full order information for customer
	 *
	 *
	 * @return array
	 */
	public function getOrders() {

		$session = JFactory::getSession();
		$user 	 = $session->get("customer.user");
		$custid	 = $user->id;
		$db		 = JFactory::getDbo();
		$query   = $db->getQuery(true);

		$query->select("CustId, BillName, OrdNbr, CustOrdNbr, OrdDate,
				 CuryTotMerch, CuryTotOrd, InvtId, AlternateId, QtyOrd,
				 QtyShip, CurySlsPrice, ExtPrice, EtaDate, ShipVia")
				 ->from("#__customer_orders")
				 ->where("CustId = '{$custid}'")
				 ->order("OrdDate DESC");
		$db->setQuery($query);

		$this->orders = $db->loadObjectList();

		// get a unique list of order numbers
		foreach ($this->orders as $order) {
			if (!in_array($order->OrdNbr, $this->orderNumbers)) {
				$this->orderNumbers[$order->CustOrdNbr] = $order->OrdNbr;
				$trackingQry = $db->getQuery(true);

				$trackingQry->select("TrackingNbr")
					->from("#__customer_orders_tracking")
					->where("OrdNbr = '{$order->OrdNbr}'");

				$db->setQuery($trackingQry);

				$this->trackingNumbers[$order->OrdNbr] = $db->loadColumn();

			}
		}

		return $this->orders;
	}

	public function getOrderNumbers() {
		return $this->orderNumbers;
	}

	public function getTrackingNumbers() {
		return $this->trackingNumbers;
	}

	public function getCountries()
	{
		if(!isset($this->_country))
		{
			$this->_db->setQuery($this->_db->getQuery(true)
											->select('*')
											->from('#__countrycode')
											->order(' if (CountryID="US", -1, CountryName)'));
			$this->_country = $this->_db->loadObjectList();
		}

		return $this->_country;
	}

}