<?php
// No direct access
defined('_JEXEC') or die;

class BkcontentModelFilelist extends JModelLegacy {

	function getDescriptions($files) {
		$results = array();
		foreach($files as $file) {
			$tmp 				= new stdClass();
			$tmp->filename 		= $file;

			$this->_db->setQuery($this->_db->getQuery(true)
											->select('description')
											->from('#__bkdownloads')
											->where('filename="'.$file.'"'));
			$tmp->description 	= $this->_db->loadResult();
			$results[] 			= $tmp;
		}

		return $results;
	}
}