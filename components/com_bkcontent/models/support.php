<?php
// No direct access
defined('_JEXEC') or die;

class BkcontentModelSupport extends JModelList {
	/**
	* Method to auto-populate the model state.
	*
	* This method should only be called once per instantiation and is designed
	* to be called on the first call to the getState() method unless the model
	* configuration flag to ignore the request is set.
	*
	* Note: Calling getState in this method will result in recursion.
	*
	* @return 	void
	* @since	J1.6
	*/
	protected function populateState($ordering = NULL, $direction = NULL) {

		$input  = JFactory::getApplication()->input;
		// Get the product layout type
		$layout = $input->get("layout", "default", "string");
		$this->setState("support.layout", $layout);

		parent::populateState($ordering, $direction);
	}

	public function getItemList() {

		$db 	= JFactory::getDbo();
		$query  = $db->getQuery(true);
		$query->select("invt_id")
			->from("#__bkproducts")
			->where("state = 1 OR state = 2")
			->order("invt_id");

		$db->setQuery($query);

		return $db->loadColumn();
	}

	public function getEstimate($invt_id = null) {

		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select("p.alias, p.invt_id, p.title, p.state,
				p.warranty, c.title AS catTitle, c.path, x.calibration_cost,
				x.calib_data_cost, ROUND(x.calib_data_cost*1.2, 2) AS calib_badata_cost,
				x.repair_cost, x.ship_cost")
			->from("#__xref_products_price AS x")
			->join("INNER", "#__bkproducts AS p ON p.invt_id=x.invt_id")
			->join("INNER", "#__categories AS c ON c.id=p.cat_id")
			->where("x.invt_id='{$invt_id}'");

		$db->setQuery($query);

		return $db->loadObject();
	}

	public function getProductSupport($invt_id = null) {
		$prodModel = JModelLegacy::getInstance("Product","BkcontentModel");
		$prodModel->getProduct();
		$product = $prodModel->getProduct();

		foreach($product->documentation AS &$doc) {
			$doc->title = JText::_($doc->title);
		}

		return $product;
	}
}