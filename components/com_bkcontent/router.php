<?php
defined('_JEXEC') or die;

function BkcontentBuildRoute(&$query)
{
	$segments = array();
	$app = JFactory::getApplication();
	$menu = $app->getMenu();
	$aMenu = $menu->getActive();
	$menuItems = $menu->getItems('component', 'com_bkcontent');
	$route = explode("/", $aMenu->route);

	if(isset($query['cat_id']))
	{
		$catId 	= explode(':', $query['cat_id']);
		foreach($menuItems as $item)
		{
			if(isset($item->query['cat_id']))
			{
				if($catId[0] == $item->query['cat_id'] AND $aMenu->component == 'com_bkcontent')
				{
					if(isset($aMenu->query["layout"]) AND $aMenu->query["layout"] != "filter")
					{
						$segments[] = $item->alias;
					}
				}
			}
		}

		unset($query['cat_id']);
	}
	if (isset($query['invt_id']) AND $query['view'] != "wtb") {

		if ($query["view"] == "products") {
			$segments[] = $query['invt_id'];
		}
		else {
			$invtId 	= explode(':', $query['invt_id']);
			$db 		= JFactory::getDBO();
			$qry 		= 'SELECT cat_id FROM #__bkproducts WHERE invt_id = "'.$invtId[0].'"';
			$db->setQuery($qry);

			$catId 		= $db->loadResult();

			foreach ($menuItems as $item) {
				if (isset($item->query['cat_id']) && $catId == $item->query['cat_id']) {
					if ($aMenu->query["view"] == "categories" AND isset($aMenu->query["layout"]) AND $aMenu->query["layout"] != "filter") {
						$segments[] = $item->alias;
					}
				}
			}
			$segments[] = $query['invt_id'];
		}
		unset($query['invt_id']);
	}
	unset($query['series']);
	unset($query['view']);
	unset($query['limitstart']);
	unset($query['layout']);
	unset($query['format']);


	return $segments;
}

function BkcontentParseRoute($segments) {
	$vars  = array();
	$app   = JFactory::getApplication();
	$cache = JFactory::getCache();
	$menu  = $app->getMenu();
	$item  = $menu->getActive();
	// Count segments
	$count = count($segments);
	// id for cache
	$id    = md5(json_encode($segments));

	//!apc_fetch($id)
	if ($count) {
		$seg = str_replace(":", "-", $segments[0]);
		if ($seg === $item->alias) {
			$app->redirect("/{$item->alias}.html", TRUE);
		}
	}
	// check for more than one segment
	// if we have more, it is most likely a product
	if ($count > 1) {

		if ($segments[0] === $item->alias) {
			$app->redirect("/{$item->alias}.html", TRUE);
		}

		$vars['view']	 = 'product';
		$invtId			 = explode(":", $segments[1]);
		if ( ! isset($invtId[1])) {
			$invtId[1] = null;
		}

		// Check to see if we have a product with a dash in the name (e.g. XLN8018-GL)
		$invtIdSuffix		= explode("-", $invtId[1]); // preg_replace('/^(\w{1,})-.*$/', '$1', $invtId[1]);
		$invtIdSuffix		= $invtIdSuffix[0];

		$db = JFactory::getDBO();
		$qry = $db->getQuery(true)
			->select("cat_id")
			->from("#__bkproducts")
			->where("invt_id = '{$invtId[0]}-{$invtIdSuffix}'");

		$db->setQuery($qry)->execute();

		if ($db->getNumRows() > 0) {
			$vars['invt_id'] 	= $invtId[0].'-'.$invtIdSuffix;
		}
		else {
			$vars['invt_id'] 	= $invtId[0];
		}

		$lang   = $app->input->getString("language");
		$db->setQuery($db->getQuery(true)
				->select("cat_id")
				->from("#__bkproducts")
				->join("INNER", "#__categories ON #__categories.id=#__bkproducts.cat_id")
				->where("invt_id='{$vars['invt_id']}'")
				->where("language='{$lang}'"));

		$cat_id = $db->loadResult();

		$db->setQuery($db->getQuery(true)
				->select("id, title, link")
				->from("#__menu")
				->where("link LIKE '%cat_id={$cat_id}'"));
		$catInfo = $db->loadObject();

		$pathway			= $app->getPathway();
		$pathway->addItem($catInfo->title, $catInfo->link . "&Itemid=" . $catInfo->id);
		$pathway->addItem(JText::_('COM_BK_MODEL').' '.$vars['invt_id']);
	}
	elseif ($count) {

		// Handle the View and Identifier
		switch($item->query['view']) {
			case 'categories':
				// this is actually a product
				if (isset($item->query['layout']) AND ($item->query['layout'] == 'filter' OR $item->query['layout'] == 'discontinued' OR $item->query['layout'] == 'subcats')) {
					$pathway	= $app->getPathway();

					$vars['view']		= 'product';
					$vars['cat_id']		= $item->query['cat_id'];
					$invtId				= explode(":", $segments[0]);
					if ( ! isset($invtId[1])) {
						$invtId[1] = null;
					}
					// Check to see if we have a product with a dash in the name (e.g. XLN8018-GL)
					$invtIdSuffix		= explode("-", $invtId[1]); // preg_replace('/^(\w{1,})-.*$/', '$1', $invtId[1]);
					$invtIdSuffix		= $invtIdSuffix[0];

					$db 		= JFactory::getDBO();
					$qry 		= $db->getQuery(true)
									->select("cat_id")
									->from("#__bkproducts")
									->where("invt_id = '{$invtId[0]}-{$invtIdSuffix}'");


					$db->setQuery($qry)->execute();

					if($db->getNumRows() > 0) {
						$vars['invt_id'] 	= $invtId[0].'-'.$invtIdSuffix;
					}
					else {
						$vars['invt_id'] 	= $invtId[0];
					}

					$seriesQry 	= $db->getQuery(true)
									->select('s.name')
									->from('#__bkproducts AS p')
									->leftJoin('#__bkseries AS s ON s.id=p.series')
									->where('p.invt_id = "'.$vars['invt_id'].'"')
									->group('s.id');

					$db->setQuery($seriesQry);
					$seriesName = $db->loadResult();

					if ( !empty($seriesName) ) {
						$pathway->addItem($seriesName.': '.JText::_('COM_BK_MODEL').' '.$vars['invt_id']);
					}
					else {
						$pathway->addItem(JText::_('COM_BK_MODEL').' '.$vars['invt_id']);
					}

				}
				else {
					$vars['view'] 		= 'categories';
					$vars['layout']		= isset($item->query['layout']) ? $item->query['layout'] : "default";
					$cat_id				= explode(":", $item->query['cat_id']);
					$vars['cat_id']		= $cat_id[0];

					if ($vars['layout'] === "default") {
						$app->redirect("/{$item->alias}.html", TRUE);
					}
				}
				break;
			case 'products':
				if ($count) {

					$vars['view']		= 'product'; // we are likely working with a product with a subcategory parent
					$vars['cat_id']		= $item->query['cat_id'];
					$invtId				= explode(":", $segments[0]);
					if ( ! isset($invtId[1])) {
						$invtId[1] = null;
					}

					// Check to see if we have a product with a dash in the name (e.g. XLN8018-GL)
					$invtIdSuffix 		= preg_replace('/^([^-]*).*$/i', '$1', $invtId[1]);
					$db 				= JFactory::getDBO();
					$qry 				= 'SELECT cat_id FROM #__bkproducts WHERE invt_id = "'.$invtId[0].'-'.$invtIdSuffix.'"';

					$db->setQuery($qry)->query();

					if ($db->getNumRows() > 0) {
						$vars['invt_id'] = $invtId[0].'-'.$invtIdSuffix;
					}
					else {
						$vars['invt_id'] = $invtId[0];
					}

					$pathway			= $app->getPathway();
					$pathwayItems		= $pathway->getPathway();
					$pathwayItems[2]->link = preg_replace('/^(.+)(\/.+)\.html$/i', '$1.html#!$2',JRoute::_($pathwayItems[2]->link));
					$pathway->setPathway($pathwayItems);
					$pathway->addItem(JText::_('COM_BK_MODEL').' '.$vars['invt_id']);
				}
				else {
					$vars['view']		= 'products';
					$vars['format']		= $item->query['format'];
					$cat_id				= explode(":", $item->query['cat_id']);
					$vars['cat_id']		= $cat_id[0];
				}
				break;
			case 'product':
				$vars['view']		= 'product';
				$vars['layout']		= $item->query['layout'];
				$vars['invt_id'] 	= $segments[0];

				$pathway			= $app->getPathway();
				$pathway->addItem(JText::_('COM_BK_MODEL').' '.$vars['invt_id']);
				break;
			case 'search':
				$vars['view']		= 'product';
				$vars['invt_id'] 	= str_replace(":", "-", $segments[0]);

				break;
		}
	}

	return $vars;
}
