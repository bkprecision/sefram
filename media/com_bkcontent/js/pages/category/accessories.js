// Require.js allows us to configure shortcut alias
require.config({
	baseUrl: '/media/com_bkcontent/js',
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		'bootstrap': {
			deps: [
			    'jquery',
			    'handlebars'
			],
			exports: 'Bootstrap'
		},
		'global': {
			deps: [
			   'bootstrap'
			]
		}
	},
	paths: {
		jquery: 'lib/jquery/jquery.min',
		bootstrap: 'lib/bootstrap/bootstrap.min',
		handlebars: 'lib/handlebars/handlebars-v1.3.0',
		global: '/templates/sefram2015/js/global',
		text: 'lib/require/text'
	}
});

require(['jquery', 'handlebars', 'global'], function( $ ) {

	$(document).ready(function () {

		var postData	  = {
				"view": 		 "accessories",
				"format": 		 "json",
				"cat_id": 		 parseInt($('#parentId').val(), 10),
				"limit": 		 isNaN(parseInt($("#pgLimit").val(), 10)) ? "1000" : $("#pgLimit").val(),
				"start": 		 parseInt($('#limitStart').val(), 10),
				"filterOptions": $("#sort-options option:selected").val()
			},
			updateContent  = function () {

				// hide the main content and show the loading image
				$(".products-container").children().not(".loading, input[type='hidden']").addClass("hide");
				$(".loading").show();
				// return the use to the top of the page
				$('html, body').animate({scrollTop: 0}, 400);

				$.post(window.location.pathname, $params, function (products) {

					if (history.pushState) {
						// make sure to keep a history
						// the second argument is for IE11
						history.pushState($params, document.title);
					}

					$(".products-list").replaceWith( productsTmpl(products) );

					if (products.pagination) {
						$(".pagination")
							.replaceWith(products.pagination);
						$(".pagination a")
							.on("click", changePage);
					}
					else {
						$(".pagination").empty();
					}

					if (products.total < parseInt($('#pgLimit').val(), 10)) {
						$('.pagination-limit').addClass("hide");
					}
					else {
						$('.pagination-limit').removeClass("hide");
					}

					$(".loading").hide();
					$(".products-container").children().not(".loading, input[type='hidden'], .pagination-limit").removeClass("hide");
				});
			},
			changePage	 = function (e) {

				var page,
					title = document.title.split("-");

				if (e && e.currentTarget.href) {
					page = this.href.match(/#!page:(\d+)/i) !== null ? this.href.match(/#!page:(\d+)/i)[1] : 0;
				}
				else if (window.location.hash.match(/#!page:(\d+)/i)) {
					page = window.location.hash.match(/#!page:(\d+)/i)[1];
				}

				window.location.hash = page ? "!page:" + page : "";

				// remove any previous pages from the title
				$(title).each(function (i, elem) {
					if ( /(\sPage\s\d{1,}\s)/.test(elem) ) {
						title.splice(i, 1);
					}
				});
				// if we have a new page, add it back into the page title
				if (page) {
					title.splice(title.length-1, 0, " Page " + page + " ");
				}
				document.title = title.join("-");

				$params 			  = $.extend({}, postData);
				if (page) {
					$params.start 		= (parseInt(page)-1) * parseInt($('#pgLimit').val());
				}
				else {
					$params.start 		= 0;
				}
				$params.limit 		  = isNaN(parseInt($("#pgLimit").val(), 10)) ? "1000" : $("#pgLimit").val();
				$params.cat_id		  = $("#sort-options option:selected").val();

				updateContent();

				return false;
			},
			productsTmpl = Handlebars.compile($("#accessory-list-template").html()),
			$params;

		Handlebars.registerHelper("msrp", function() {
		  return parseFloat(this.list_price).formatMoney(0);
		});

		Handlebars.registerHelper('imgTile', function() {
			var result = '<img class="intro-img" src="' + this.icon + '" alt="' + this.title +  '">';
			return new Handlebars.SafeString(result);
		});

		Handlebars.registerHelper('photo', function() {
			var result = '<img src="' + this.photo + '" alt="' + this.title +  '">';
			return new Handlebars.SafeString(result);
		});

		Handlebars.registerHelper('description', function() {
			return new Handlebars.SafeString(this.description);
		});

		Handlebars.registerHelper("each", function (context, options) {

			var products = '<div class="products-list">';

			for (var i = 0, j = 1; i < context.length; i += 1) {
				if (j == 1) {
					products = products + '<div class="row-fluid"><ul class="inline">';
				}

				products = products + options.fn(context[i]);

				if (j == 4 || i == context.length-1) {
					products = products + '</ul></div>';
					j = 1;
				}
				else {
					j += 1;
				}
			}

			products = products + '</div>';
			return products;
		});

		$('.pagination a').on('click', changePage);

		$("#sort-options, #pgLimit").on("change", function () {

			window.location.hash  = '';
			$params 			  = $.extend({}, postData);
			$params.start 		  = parseInt(this.name, 10);
			$params.limit 		  = isNaN(parseInt($("#pgLimit").val(), 10)) ? "1000" : $("#pgLimit").val();
			$params.cat_id		  = $("#sort-options option:selected").val();

			updateContent();
		});

		// move the large intro image to the header element
		$('.category-header-img').appendTo("header.main-nav-wrapper").slideDown();
		window.postData = postData;

		if (window.location.hash.replace(/^.+page:(\d+).*$/, "$1").length) {
			changePage();
		}

	});

});