define(['jquery', 'global'], function( $ ) {

	$(document).ready(function () {

		var postData		= {
				"view": 		"products",
				"format": 		"json",
				"filters":		$(".filters-list input:checkbox:checked").serializeArray(),
				"cat_id": 		parseInt($('#parentId').val(), 10),
				"limit": 		isNaN(parseInt($("#pgLimit").val(), 10)) ? "1000" : $("#pgLimit").val(),
				"start": 		parseInt($('#limitStart').val(), 10),
				"sort-options": $("#sort-options option:selected").val(),
				"state": 		parseInt($('#state').val(), 10)
			},
			productsTmpl	= Handlebars.compile($("#products-list-template").html()),
			updateContent	= function () {

				// hide the main content and show the loading image
				$(".products-container").children().not(".loading, input[type='hidden']").addClass("hide");
				$(".loading").show();
				// return the use to the top of the page
				$('html, body').animate({scrollTop: 0}, 400);

				$.post(window.location.pathname, $params, function (products) {

					if (history.pushState) {
						// make sure to keep a history
						// the second argument is for IE11
						history.pushState($params, document.title);
					}

					$(".products-list").replaceWith( productsTmpl(products) );

					// update the filters, re-enabling the ones that apply
					$(products.filters).each(function (idx, filter) {

						$(filter.values).each(function (idx, value) {
							var $filter = $("input[name='" + filter.name + "'][value='" + $("<div>"+value+"</div>").text() + "']");

							if ($filter) {
								$filter.attr('disabled', false).parent().removeClass("disabled");
							}
						});

					});

					// add disabled class to disabled inputs
					$(".filters-list input[type='checkbox']:disabled").parent().addClass("disabled");

					if (products.pagination) {
						$(".pagination")
							.replaceWith(products.pagination);
						$(".pagination a")
							.on("click", changePage);
					}
					else {
						$(".pagination").empty();
					}

					if (products.total < parseInt($('#pgLimit').val(), 10)) {
						$('.pagination-limit').addClass("hide");
					}
					else {
						$('.pagination-limit').removeClass("hide");
					}

					$(".loading").hide();
					$(".products-container").children().not(".loading, input[type='hidden'], .pagination-limit").removeClass("hide");
				});
			},
			changePage		= function (e) {
				var $inputs 	= $(".filters-list input:checkbox:checked"),
				 	title 		= document.title.split("-"),
					page;

				if (e && e.currentTarget.href) {
					page = this.href.match(/#!page:(\d+)/i) !== null ? this.href.match(/#!page:(\d+)/i)[1] : 0;
				}
				else if (window.location.hash.match(/#!page:(\d+)/i)) {
					page = window.location.hash.match(/#!page:(\d+)/i)[1];
				}

				window.location.hash = page ? "!page:" + page : "";

				// remove any previous pages from the title
				$(title).each(function (i, elem) {
					if ( /(\sPage\s\d{1,}\s)/.test(elem) ) {
						title.splice(i, 1);
					}
				});
				// if we have a new page, add it back into the page title
				if (page) {
					title.splice(title.length-1, 0, " Page " + page + " ");
				}
				document.title = title.join("-");

				$params 			= $.extend({}, postData);
				$params.filters 	= $inputs.serializeArray();
				if (page) {
					$params.start 		= (parseInt(page)-1) * parseInt($('#pgLimit').val());
				}
				else {
					$params.start 		= 0;
				}
				$params.limit 		= $('#pgLimit').val();
				$params.sortOptions = $("#sort-options option:selected").val();

				updateContent();

				return false;
			},
			updateFilters	= function () {

				$params					= $.extend({}, postData);
				$params.filters 		= $(".filters-list input:checkbox:checked").serializeArray();
				$params.start 			= 0;
				window.location.hash 	= '';
				$params.limit 			= $('#pgLimit').val();
				$params.sortOptions		= $("#sort-options option:selected").val();
				// disable all the filters
				$(".filters-list input").attr('disabled', true);
				updateContent();
			}, $params, $history;

			Handlebars.registerHelper("msrp", function() {
			  return parseFloat(this.list_price).formatMoney(0);
			});

			Handlebars.registerHelper('imgTile', function() {
				var result = '<img class="intro-img" src="' + this.icon + '" alt="' + this.title +  '" itemprop="image">';
				return new Handlebars.SafeString(result);
			});

			Handlebars.registerHelper("productEach", function (context, options) {

				var products = '<div class="products-list">';

				for (var i = 0, j = 1; i < context.length; i += 1) {
					if (j == 1) {
						products = products + '<div class="row-fluid"><ul class="inline">';
					}

					products = products + options.fn(context[i]);

					if (j == 4 || i == context.length-1) {
						products = products + '</ul></div>';
						j = 1;
					}
					else {
						j += 1;
					}
				}

				products = products + '</div>';
				return products;
			});

			$('#search-wait')
				.modal({
					backdrop: 	'static',
					keyboard: 	false,
					show: 		false
				});

			$('.category-filter, #pgLimit, #sort-options')
				.on('change', updateFilters);

			$('.pagination a').on('click', changePage);

			if ($(".filters-list input:checkbox:checked").length) {
				updateFilters();
			}

			if (window.location.hash.replace(/^.+page:(\d+).*$/, "$1").length) {
				changePage();
			}

			// move the large intro image to the header element
			$('.category-header-img').appendTo("header.main-nav-wrapper").slideDown();
	});
	var getUrlParam = function(e) {
		var t = new RegExp("[?&]" + e.replace(/[\[\]]/g, "\\$&") + "(=([^&#]*)|&|#|$)"),
			a = t.exec(window.location.href);
		return a && a[2] ? decodeURIComponent(a[2].replace(/\+/g, " ")) : ""
	};

	var sjUI = (function(w, d, x, a, e, s, c, r) {
		s = [];
		var b = function() {
				s.push(arguments);
			},
			q = "ui";
		b.arr = s;
		w[a] = w[a] || [];
		w[a][q] = w[a][q] || [];
		w[a][q].push(b);
		c = d.createElement(x);
		c.async = 1;
		c.src = e;
		r = d.getElementsByTagName(x)[0];
		r.parentNode.insertBefore(c, r);
		return b;
	})(window, document, "script", "sajari", "//cdn.sajari.net/js/integrations/website-search-1.2.0.js");

	sjUI("config", {
		project: "1511813858357986041", // Set this to your project.
		collection: "www-sefram-com", // Set this to your collection.
		pipeline: "website", // Run the website pipeline.
		attachSearchBox: document.getElementById("search-box"), // DOM element to render search box.
		attachSearchResponse: document.getElementById("search-response"), // DOM element to render search results.
		searchBoxPlaceHolder: "Type to search", // Placeholder text for the search box.
		results: {
			"showImages": true
		}, // Configure the results
		values: {
			"resultsPerPage": "5",
			"q": getUrlParam("q"),
			"q.override": true
		}, // Set default values
		tabFilters: null, // Make user selectable filters
		overlay: true // Whether to render an overlay or in-page
	});

});
