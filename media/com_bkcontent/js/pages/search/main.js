// Require.js allows us to configure shortcut alias
require.config({
	baseUrl: '/media/com_bkcontent/js',
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		'bootstrap': {
			deps: [
			    'jquery',
			    'handlebars'
			],
			exports: 'Bootstrap'
		},
		'global': {
			deps: [
			   'jquery',
			   'bootstrap',
			   'handlebars'
			]
		}
	},
	paths: {
		jquery: 	'lib/jquery/jquery.min',
		bootstrap: 	'lib/bootstrap/bootstrap.min',
		handlebars: 'lib/handlebars/handlebars-v1.3.0',
		global: 	'/templates/sefram2015/js/global',
		text: 		'lib/require/text'
	}
});

require(['jquery', 'global'], function( $ ) {

	$(document).ready(function () {

		var $query = $("#query").val(),
			$start = $("#startIndex").val(),
			data   = {
				key: 	"AIzaSyCMGfdDaSfjqv5zYoS0mTJnOT3e9MURWkU",
				cx:  	"008331306769126933763:2u8ivn40jtk",
				num: 	10,
				lr: "lang_"+$("html").prop("lang"),
				start: 	$start,
				q: 	 	$query
			},
			tmpl   = Handlebars.compile( $("#ext-results-tmpl").html() ),

		Handlebars.registerHelper("link", function(result) {
			if (result.hasOwnProperty("hash")) {
				return new Handlebars.SafeString("<a href='" + result.hash.link +"'>" + result.hash.title.replace(" - Sefram", "") + "</a>");
			}
			return new Handlebars.SafeString("<a href='" + result.link +"' class='title'>" + result.title.replace(" - Sefram", "") + "</a>");
		});

		Handlebars.registerHelper("cleanString", function (string) {
			return new Handlebars.SafeString(string);
		});

		Handlebars.registerHelper("pagination", function () {

			var totalResults = parseInt(this.searchInformation.totalResults, 10),
				count		 = parseInt(data.num, 10),
				start		 = parseInt(this.queries.request[0].startIndex, 10),
				pages		 = Math.ceil(totalResults/count),
				ret			 = "<ul>",
				currentPage  = ((start === 1) ? 1 : (start-1)/count+1 ),
				maxPages	 = 10,
				pageNums	 = [];

			if (totalResults <= count) {
				return;
			}
			if (currentPage !== 1) {
				ret += "<li><a href='?q="+$query+"&start="+ this.queries.previousPage[0].startIndex +"'>Prev</a></li>";
			}
			else {
				ret += "<li class='disabled'><a href='#'>Prev</a></li>";
			}

			// setup to grab previous pages
			var j = currentPage-1;
			while (j > 0 && pageNums.length < Math.ceil(maxPages * 0.5) ) {
				pageNums.unshift(j);
				j -= 1;
			}
			// setup for all current and next pages
			for (var i = currentPage; i <= pages && pageNums.length <= maxPages-1; i += 1) {
				pageNums.push(i);
			}

			$(pageNums).each(function () {

				var page 		= parseInt(this, 10),
					startIndex 	= page === 1 ? 1 : (page-1)*count+1;

				ret += "<li";
				if (currentPage === page) {
					ret += " class='active'";
				}
				ret += "><a href='?q="+$query+"&start="+startIndex+"'>" + page + "</a></li>";
			});

			if (currentPage === pages) {
				ret += "<li class='disabled'><a href='#'>Next</a></li>";
			}
			else {
				ret += "<li><a href='?q="+$query+"&start="+ this.queries.nextPage[0].startIndex +"'>Next</a></li>";
			}

			return new Handlebars.SafeString( ret + "</ul>");
		});

	});

});
