define(['jquery', 'global'], function( $ ) {

	$(document).ready(function () {
		$("#carouselContainer").carousel();

		$(".home .marketing div").hover(function () {
			$(this).find(".opacity").toggleClass("active");
		});

	});
		
});