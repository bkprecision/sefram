// Require.js allows us to configure shortcut alias
require.config({
	baseUrl: '/media/com_bkcontent/js',
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		'bootstrap': {
			deps: [
			    'jquery',
			    'handlebars'
			],
			exports: 'Bootstrap'
		},
		'global': {
			deps: [
			   'jquery',
			   'bootstrap'
			]
		}
	},
	paths: {
		jquery: 'lib/jquery/jquery.min',
		handlebars: 'lib/handlebars/handlebars-v1.3.0',
		bootstrap: 'lib/bootstrap/bootstrap.min',
		global: '/templates/sefram2015/js/global',
		text: 'lib/require/text'
	}
});

require(['pages/home/home']);