// Require.js allows us to configure shortcut alias
require.config({
	baseUrl: '/media/com_bkcontent/js',
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		'bootstrap': {
			deps: [
			    'jquery',
			    'handlebars'
			],
			exports: 'Bootstrap'
		},
		'lightbox': {
			deps: ['bootstrap']
		},
		'global': {
			deps: [
			    'jquery',
				'bootstrap',
				'handlebars'
			]
		}
	},
	paths: {
		jquery: 'lib/jquery/jquery.min',
		bootstrap: 'lib/bootstrap/bootstrap.min',
		lightbox: 'lib/bootstrap/bootstrap-lightbox.min',
		handlebars: 'lib/handlebars/handlebars-v1.3.0',
		global: '/templates/sefram2015/js/global',
		text: 'lib/require/text'
	}
});

define(['jquery', 'global', 'lightbox'], function( $ ) {

	$(document).ready(function () {
		// make all slides the same height
		var slideHeight = 0,
			$photos 	= $("#photoGallery .slide-show .item");

		$photos.each(function () {
			slideHeight = Math.max(slideHeight, $(this).height());
		})
		.height(slideHeight);

		$("[data-toggle='popover']").popover();

		$("body").on("click", function (e) {
		    //only buttons
			$("[data-toggle='popover']").each(function () {
		        //the 'is' for buttons that trigger popups
		        //the 'has' for icons within a button that triggers a popup
		        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $(".popover").has(e.target).length === 0) {
		            $(this).popover("hide");
		        }
		    });
		});
	});

});