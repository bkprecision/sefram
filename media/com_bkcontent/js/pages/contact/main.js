// Require.js allows us to configure shortcut alias
require.config({
	baseUrl: '/media/com_bkcontent/js',
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		'bootstrap': {
			deps: [
			    'jquery',
			    'handlebars'
			],
			exports: 'Bootstrap'
		},
		'global': {
			deps: ['bootstrap']
		}
	},
	paths: {
		jquery: 'lib/jquery/jquery.min',
		bootstrap: 'lib/bootstrap/bootstrap.min',
		handlebars: 'lib/handlebars/handlebars-v1.3.0',
		global: '/templates/sefram2015/js/global',
		text: 'lib/require/text'
	}
});

require(['jquery', 'global'], function( $ ) {

	$(document).ready(function () {

		var options = {
				"sitekey": "6LdpqEkUAAAAAL7MwJKs511-Ovn5BYqorZg5hWTR",
				"theme": "light"
		};

		grecaptcha.render("grecaptcha", options);

		$(".contact-us-form").submit(function () {

			// if you don't create new reference, it won't get the recaptcha data
			var $formData = $(".contact-us-form").serializeArray();

			if (!$("#g-recaptcha-response").val()) {
				return false;
			}

			$("#btnSubmitMsg").attr("disabled", true).find(".btn-txt").text( $("#sending-button-text").text() );
			$("#btnSubmitMsg>.ion-loading-d").removeClass("hide");

			$.post("/", $formData, function (caseInfo) {
				$("html, body").animate({scrollTop: 0}, 400);
				$("#btnSubmitMsg").find(".btn-txt").text( $("#success-button-text").text() );
				$("#btnSubmitMsg>.ion-loading-d").addClass("hide");
				$(".submit-success").removeClass("hide");
			})
			.error(function() {
				$("html, body").animate({scrollTop: 0}, 400);
				$("#btnSubmitMsg").attr("disabled", false).find(".btn-txt").text( $("#submit-button-text").text() );
				$("#btnSubmitMsg>.ion-loading-d").addClass("hide");
				$(".submit-error").removeClass("hide");
				grecaptcha.reset();
			});

			return false;
		});
	});

});
