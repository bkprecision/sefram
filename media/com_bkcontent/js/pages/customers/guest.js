// Require.js allows us to configure shortcut alias
require.config({
	baseUrl: '/media/com_bkcontent/js',
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		'bootstrap': {
			deps: [
			    'jquery'
			],
			exports: 'Bootstrap'
		},
		'datatables': {
			deps: [
			     'bootstrap',
			     'jqdatatables'
			]
		},
		'jqdatatables': {
			deps: ['jquery']
		},
		'global': {
			deps: [
			    'jquery',
			    'bootstrap',
			    'handlebars'
			]
		}
	},
	paths: {
		jqdatatables: 'lib/datatables/jquery.dataTables',
		datatables: 'lib/datatables/dataTables-bootstrap',
		jquery: 'lib/jquery/jquery.min',
		handlebars: 'lib/handlebars/handlebars-v1.3.0',
		bootstrap: 'lib/bootstrap/bootstrap.min',
		global: '/templates/sefram2015/js/global',
		text: 'lib/require/text'
	}
});

require(['jquery', 'global', 'datatables'],	function( $ ) {

		$(document).ready(function () {

			$('#stockGrid').dataTable({
				"sDom": "<'row-fluid'<'span6'f>r>t<'row-fluid'<'span4'i><'span8'p>>",
				"sPaginationType": "bootstrap",
				"oLanguage": {
					"sLengthMenu": "_MENU_ files per page"
				}
			});

		});

});