// Require.js allows us to configure shortcut alias
require.config({
	baseUrl: '/media/com_bkcontent/js',
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		'bootstrap': {
			deps: [
			    'jquery',
			    'handlebars'
			],
			exports: 'Bootstrap'
		},
		'datatables': {
			deps: [
			     'bootstrap',
			     'jqdatatables'
			]
		},
		'jqdatatables': {
			deps: ['jquery']
		},
		'global': {
			deps: [
			    'bootstrap',
			    'jquery',
			    'handlebars'
			]
		}
	},
	paths: {
		jqdatatables: 'lib/datatables/jquery.dataTables',
		datatables: 'lib/datatables/dataTables-bootstrap',
		jquery: 'lib/jquery/jquery.min',
		bootstrap: 'lib/bootstrap/bootstrap.min',
		handlebars: 'lib/handlebars/handlebars-v1.3.0',
		global: '/templates/sefram2015/js/global',
		text: 'lib/require/text'
	}
});

require(['jquery', 'global', 'datatables'], function( $ ) {

		$(document).ready(function () {

			var orderSummaryTmpl 	 = Handlebars.compile($("#order-summary-tmpl").html()),
				lineItemsTmpl	 	 = Handlebars.compile($("#line-items-tmpl").html()),
				userLogout			 = function () {
					var form  = document.createElement("form"),
						field = document.createElement("input");

					form.setAttribute("method", "post");
					form.setAttribute("action", window.location.href);
					field.setAttribute("type", "hidden");
					field.setAttribute("name", "command");
					field.setAttribute("value", "logout");
					form.appendChild(field);

					document.body.appendChild(form);
					form.submit();
				},
				salesOrdersListClick = function () {
					var OrdNbr = $(this).parent().attr("id"),
						orders = {"orders":[]};

					if (OrdNbr === "salesOrderList_wrapper") {
						OrdNbr = $("#salesOrderList tbody tr").eq(0).attr("id");
					}

					$(BKP.orders).each(function () {
						if (this.OrdNbr === OrdNbr) {
							orders.orders.push(this);
						}
					});

					$("#orderSummary table").replaceWith( orderSummaryTmpl(orders.orders[0]) );
					$("#lineItems table tbody").replaceWith( lineItemsTmpl(orders) );

				};

			Handlebars.registerHelper("msrp", function(price) {
				return parseFloat(price).formatMoney(2);
			});

			Handlebars.registerHelper("tracking", function(ordNbr) {
				var trackingNbr = BKP.tracking[ordNbr];

				if (trackingNbr.length) {
					return new Handlebars.SafeString(
							trackingNbr.join("<br>")
					);
				}
				else {
					return "No tracking information available.";
				}
			});

			$("#salesOrderList td").on("click", salesOrdersListClick);

			$('#salesOrderList').dataTable({
				"sDom": "<'row-fluid'<'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
				"sPaginationType": "bootstrap",
				"oLanguage": {
					"sLengthMenu": "_MENU_ files per page",
					"sSearch": "Search by Order Number"
				},
				"fnDrawCallback": salesOrdersListClick,
				"aaSorting": [[1,'desc']]
			});

			if ($("#stockGrid").length) {

				$('#stockGrid').dataTable({
					"sDom": "<'row-fluid'<'span6'f>r>t<'row-fluid'<'span4'i><'span8'p>>",
					"sPaginationType": "bootstrap",
					"oLanguage": {
						"sLengthMenu": "_MENU_ files per page"
					}
				});
			}

			$("#logout").on("click", userLogout);

		});

});