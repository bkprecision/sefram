// Require.js allows us to configure shortcut alias
require.config({
	baseUrl: '/media/com_bkcontent/js',
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		'bootstrap': {
			deps: [
			    'jquery',
			    'handlebars'
			],
			exports: 'Bootstrap'
		},
		'lightbox': {
			deps: ['bootstrap']
		},
		'global': {
			deps: [
			    'jquery',
			    'bootstrap',
			    'handlebars'
			]
		}
	},
	paths: {
		jquery: 'lib/jquery/jquery.min',
		bootstrap: 'lib/bootstrap/bootstrap.min',
		handlebars: 'lib/handlebars/handlebars-v1.3.0',
		global: '/templates/sefram2015/js/global',
		text: 'lib/require/text'
	}
});

require(['jquery', 'global'], function( $ ) {

	$(document).ready(function () {
		if ($(".bk-docs-sidebar")) {
			$("body").scrollspy({"target":".bk-docs-sidebar", "offset":50});
		}

		// stop youtube player from playing when hiding a modal
		$(".modal").on("hide", function () {
			var iframe = $(this).children(".modal-body").find("iframe"),
				src = iframe.attr("src");

			iframe.attr("src", "");
			iframe.attr("src", src);
		});
	});
	var getUrlParam = function(e) {
		var t = new RegExp("[?&]" + e.replace(/[\[\]]/g, "\\$&") + "(=([^&#]*)|&|#|$)"),
			a = t.exec(window.location.href);
		return a && a[2] ? decodeURIComponent(a[2].replace(/\+/g, " ")) : ""
	};

	var sjUI = (function(w, d, x, a, e, s, c, r) {
		s = [];
		var b = function() {
				s.push(arguments);
			},
			q = "ui";
		b.arr = s;
		w[a] = w[a] || [];
		w[a][q] = w[a][q] || [];
		w[a][q].push(b);
		c = d.createElement(x);
		c.async = 1;
		c.src = e;
		r = d.getElementsByTagName(x)[0];
		r.parentNode.insertBefore(c, r);
		return b;
	})(window, document, "script", "sajari", "//cdn.sajari.net/js/integrations/website-search-1.2.0.js");

	sjUI("config", {
		project: "1511813858357986041", // Set this to your project.
		collection: "www-sefram-com", // Set this to your collection.
		pipeline: "website", // Run the website pipeline.
		attachSearchBox: document.getElementById("search-box"), // DOM element to render search box.
		attachSearchResponse: document.getElementById("search-response"), // DOM element to render search results.
		searchBoxPlaceHolder: "Type to search", // Placeholder text for the search box.
		results: {
			"showImages": true
		}, // Configure the results
		values: {
			"resultsPerPage": "5",
			"q": getUrlParam("q"),
			"q.override": true
		}, // Set default values
		tabFilters: null, // Make user selectable filters
		overlay: true // Whether to render an overlay or in-page
	});
});
