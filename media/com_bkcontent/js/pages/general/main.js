// Require.js allows us to configure shortcut alias
require.config({
	baseUrl: '/media/com_bkcontent/js',
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		'bootstrap': {
			deps: [
			    'jquery',
			    'handlebars'
			],
			exports: 'Bootstrap'
		},
		'lightbox': {
			deps: ['bootstrap']
		},
		'global': {
			deps: [
			    'jquery',
			    'bootstrap',
			    'handlebars'
			]
		}
	},
	paths: {
		jquery: 'lib/jquery/jquery.min',
		bootstrap: 'lib/bootstrap/bootstrap.min',
		handlebars: 'lib/handlebars/handlebars-v1.3.0',
		global: '/templates/sefram2015/js/global',
		text: 'lib/require/text'
	}
});

require(['jquery', 'global'], function( $ ) {

	$(document).ready(function () {
		if ($(".bk-docs-sidebar")) {
			$("body").scrollspy({"target":".bk-docs-sidebar", "offset":50});
		}

		// stop youtube player from playing when hiding a modal
		$(".modal").on("hide", function () {
			var iframe = $(this).children(".modal-body").find("iframe"),
				src = iframe.attr("src");

			iframe.attr("src", "");
			iframe.attr("src", src);
		});
	});
	var getUrlParam = function(e) {
		var t = new RegExp("[?&]" + e.replace(/[\[\]]/g, "\\$&") + "(=([^&#]*)|&|#|$)"),
			a = t.exec(window.location.href);
		return a && a[2] ? decodeURIComponent(a[2].replace(/\+/g, " ")) : ""
	};

});
