// Require.js allows us to configure shortcut alias
require.config({
	baseUrl: '/media/com_bkcontent/js',
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		'bootstrap': {
			deps: [
			    'jquery',
			    'handlebars'
			],
			exports: 'Bootstrap'
		},
		'global': {
			deps: ['bootstrap']
		}
	},
	paths: {
		jquery: 'lib/jquery/jquery.min',
		bootstrap: 'lib/bootstrap/bootstrap.min',
		handlebars: 'lib/handlebars/handlebars-v1.3.0',
		global: '/templates/sefram2015/js/global',
		text: 'lib/require/text'
	}
});

define(['jquery', 'global', 'handlebars'], function ( $ ) {

	$(document).ready(function () {

		var estimateTmpl = Handlebars.compile($("#estimate-table-tmpl").html());

		Handlebars.registerHelper("add", function() {
			var total = 0;

			for (var i = 0; i < arguments.length-1; i += 1) {
				total = total+parseInt(arguments[i], 10);
			}

			return total.toFixed(2);
		});

		Handlebars.registerHelper("if", function(conditional, options) {
			if (parseFloat(conditional) === 1 || parseFloat(conditional) > 2) {
				return options.fn(this);
			}
			else {
				return options.inverse(this);
			}
		});

		$("#support-estimate").on("submit", function (e) {

			var $data = $(this).serialize();

			e.preventDefault();

			$.getJSON(window.location.href, $data, function (estimate) {
				$(".estimate-table").replaceWith( estimateTmpl(estimate) );
				$(".estimate-title > a").tab("show");
				if ($(".estimate-title").hasClass("hide")) {
					$(".estimate-title, #estimate").removeClass("hide");
				}
			});
		});

	});

});