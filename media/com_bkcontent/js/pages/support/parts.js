// Require.js allows us to configure shortcut alias
require.config({
	baseUrl: '/media/com_bkcontent/js',
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		'bootstrap': {
			deps: [
			    'jquery',
			    'handlebars'
			],
			exports: 'Bootstrap'
		},
		'global': {
			deps: ['bootstrap']
		}
	},
	paths: {
		jquery: 'lib/jquery/jquery.min',
		bootstrap: 'lib/bootstrap/bootstrap.min',
		handlebars: 'lib/handlebars/handlebars-v1.3.0',
		global: '/templates/sefram2015/js/global',
		text: 'lib/require/text'
	}
});

require(['jquery', 'global'], function( $ ) {

	$(document).ready(function () {

		Recaptcha.create("6LeK-e8SAAAAAJRJ-YeWN6jbWTSFhhlQAA0EkFe-", "dynamic_recaptcha_1", {theme: "clean", lang: 'en', tabindex: 0});

		$(".contact-us-form input[type='checkbox']").on("click", function () {

			if ($(this).prop("checked")) {
				$(".contact-us-form input[type='checkbox']").prop("required", false);
			}
			else {
				$(".contact-us-form input[type='checkbox']").prop("required", true);
			}

		});

		$(".contact-us-form").submit(function () {
			// if you don't create new reference, it won't get the recaptcha data
			var $formData = $(this).serializeArray();

			Recaptcha.destroy();
			$("#btnSubmitMsg").attr("disabled", true).find(".btn-txt").text("Submitting...");
			$("#btnSubmitMsg>.ion-loading-d").removeClass("hide");

			$.post(window.location.href, $formData, function (caseInfo) {
				$("html, body").animate({scrollTop: 0}, 400);
				$("#btnSubmitMsg").find(".btn-txt").text("Message Sent!");
				$("#btnSubmitMsg>.ion-loading-d").addClass("hide");
				$(".submit-success").removeClass("hide").find(".case-id").text(caseInfo.caseId);
			})
			.error(function() {
				$("html, body").animate({scrollTop: 0}, 400);
				$("#btnSubmitMsg").attr("disabled", false).find(".btn-txt").text("Submit Message");
				$("#btnSubmitMsg>.ion-loading-d").addClass("hide");
				$(".submit-error").removeClass("hide");
				Recaptcha.create("6LeK-e8SAAAAAJRJ-YeWN6jbWTSFhhlQAA0EkFe-", "dynamic_recaptcha_1", {theme: "clean", lang: 'en', tabindex: 0});
			});

			return false;
		});
	});

});