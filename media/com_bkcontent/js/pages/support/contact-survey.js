// Require.js allows us to configure shortcut alias
require.config({
	baseUrl: '/media/com_bkcontent/js',
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		'bootstrap': {
			deps: [
			    'jquery',
			    'handlebars'
			],
			exports: 'Bootstrap'
		},
		'global': {
			deps: ['bootstrap']
		}
	},
	paths: {
		jquery: 'lib/jquery/jquery.min',
		bootstrap: 'lib/bootstrap/bootstrap.min',
		handlebars: 'lib/handlebars/handlebars-v1.3.0',
		global: '/templates/sefram2015/js/global',
		text: 'lib/require/text'
	}
});

require(['jquery', 'global'], function( $ ) {

	$(document).ready(function () {

		$("#thank-you").modal({ show: false });
		$("#thank-you").on("hide", function () {
			window.location.replace("/");
		});

		$("#survey-form").on("submit", function (e) {

			e.preventDefault();

			$("#thank-you").modal('show');

			$.post(window.location.href, $(this).serializeArray(), function (e) { });
		});

	});

});
