// Require.js allows us to configure shortcut alias
require.config({
	baseUrl: '/media/com_bkcontent/js',
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		'bootstrap': {
			deps: [
			    'jquery',
			    'handlebars'
			],
			exports: 'Bootstrap'
		},
		'bootstrap-select': { deps: ['jquery'] },
		'global': { deps: ['bootstrap'] }
	},
	paths: {
		jquery: 'lib/jquery/jquery.min',
		bootstrap: 'lib/bootstrap/bootstrap.min',
		'bootstrap-select': 'lib/bootstrap/bootstrap-select.min',
		handlebars: 'lib/handlebars/handlebars-v1.3.0',
		global: '/templates/sefram2015/js/global',
		text: 'lib/require/text'
	}
});

require(['jquery', 'global', 'bootstrap-select', 'handlebars'], function( $ ) {

	$(document).ready(function () {
		var wellHeights = 0,
			supportTmpl = Handlebars.compile( $("#product-support-tmpl").html() ),
			showSupport = function () {

				var invtId = $(this).val(),
					data   = {invt_id: invtId, format: "json", supportType: "ProductSupport"};

				if (invtId.length) {
					_gaq.push(["_trackEvent", "SelectProductSupport", "ViewInvtID", "Model " + invtId]);
					$(".ajax-loader").removeClass("hide");
					$.getJSON(window.location.href, data, function (product) {
						$("article").append( supportTmpl(product) );
						$("#productSupportModal")
							.modal()
							.on("hidden", function () { $(this).remove(); })
							.find(".modal-body")
								.css("max-height", Math.ceil($(window).height()*0.56));
						$(".ajax-loader").addClass("hide");
					});
				}
		};

		Handlebars.registerHelper("introPhoto", function() {
			var src 	= Handlebars.Utils.escapeExpression(this.photos[0].src),
				result  = '<img src="' + src + '">';

			return new Handlebars.SafeString( result );
		});

		Handlebars.registerHelper("productLink", function() {
			var src 	= this.path + "/" + this.invt_id + "-" + this.alias + ".html",
				result  = '<a href="/' + src + '" class="btn btn-primary" target="_blank">Visit Product Page</a>';

			return new Handlebars.SafeString( result );
		});

		Handlebars.registerHelper("softLink", function(software) {
			var src 	= Handlebars.Utils.escapeExpression(software.src),
				result  = '<a href="' + src + '">' + software.file_name + '</a>';

			return new Handlebars.SafeString( result );
		});

		Handlebars.registerHelper("docLink", function(doc) {
			var src 	= Handlebars.Utils.escapeExpression(doc.src),
				result  = '<a href="' + src + '" target="_blank">' + doc.title + '</a>';

			return new Handlebars.SafeString( result );
		});

		$(".support-container .well").each(function () {
			var padding = parseInt($(this).css("padding-top"))+parseInt($(this).css("padding-bottom"));
			wellHeights = Math.max($(this).height()+padding, wellHeights);
		}).css({"min-height": wellHeights});

		$("#supportModels")
			.selectpicker({size:4})
			.on("change", showSupport);
	});

});