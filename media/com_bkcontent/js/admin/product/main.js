// Require.js allows us to configure shortcut alias
require.config({
	baseUrl: '/media/com_bkcontent/js',
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		'handlebars': {
			exports: 'Handlebars'
		}
	},
	paths: {
		handlebars: 'lib/handlebars/handlebars-v1.3.0',
		text: 'lib/require/text'
	}
});

define(['handlebars'], function( Handlebars ) {

		var $ 				= jQuery.noConflict(),
			newFilterTmpl 	= Handlebars.compile($("#new-filter-tmpl").html());

		$(document).ready(function () {

			var saveProduct = function () {
					setTimeout(function () {
						Joomla.submitbutton("bkproduct.apply", document.getElementById("product-form"));
					}, 50);
				};

			$(".remove-filter, .remove-replacement").on("click", function () {
				$(this).parent().parent().remove();
				saveProduct();
			});

			$(".add-replacement").on("click", saveProduct);

			$("#jform_newFilter").on("change", function () {
				var	newFilterContent = {
						"label": $("option:selected", this).text(),
						"type":	 $("option:selected", this).val()
					},
					newFilterHtml 	 = newFilterTmpl(newFilterContent),
					$parentDiv 	  	 = $(this).parent().parent();

				if(newFilterContent.type) {
					$(newFilterHtml).insertBefore($parentDiv).find("input").focus();
				}
			});

			$(".typeahead").typeahead({source: BKP.models});

		});


});