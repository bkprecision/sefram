// Require.js allows us to configure shortcut alias
require.config({
	baseUrl: '/media/com_bkcontent/js',
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		'handlebars': {
			exports: 'Handlebars'
		}
	},
	paths: {
		handlebars: 'lib/handlebars/handlebars-v1.3.0',
		text: 'lib/require/text'
	}
});

define(['handlebars'], function( Handlebars ) {

		var $ 		 = jQuery.noConflict(),
			iterator = 0;

		$(document).ready(function () {

			$(".btn-remove-item-cross").on("click", function () {
				$(this).parentsUntil(".control-group").parent().remove();
				setTimeout(function () {
					Joomla.submitbutton("accessorieslist.apply", document.getElementById("accessory-form"));
				}, 50);
			});

			$("#crossType").on("change", function () {
				var $crossType = $(this).val(),
					$fieldset  = $(this).parentsUntil("fieldset").parent().eq(0),
					$newCross  = $("#" + $crossType + "_tmpl").parentsUntil(".control-group").parent().clone(),
					$elemId	   = $newCross.find(".control-label label").attr("for");

				if ($crossType !== "0") {
					$newCross.find(".control-label label").attr("for", $elemId + "_" + iterator);
					$newCross.find(".controls > div.chzn-container").remove();
					$newCross.find(".controls > select").attr("id", $elemId + "_" + iterator);
					$newCross.removeClass("hide").appendTo($fieldset);
					$newCross.find(".controls > select").chosen();
					iterator += 1;
				}
			});

		});

});