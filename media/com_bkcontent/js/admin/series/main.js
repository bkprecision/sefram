// Require.js allows us to configure shortcut alias
require.config({
	baseUrl: '/media/com_bkcontent/js',
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		'handlebars': {
			exports: 'Handlebars'
		}
	},
	paths: {
		handlebars: 'lib/handlebars/handlebars-v1.3.0',
		text: 'lib/require/text'
	}
});

define(['handlebars'], function( Handlebars ) {

		var $ 		 	= jQuery.noConflict(),
			timer;

		$(document).ready(function () {

			$("input.attrib-title").on("keyup", function () {
				var $input 	 = $(this),
					$attribs = $input.parentsUntil("fieldset").parent().children("div.control-group").not(":first");

				if (timer) {
					clearTimeout(timer);
				}

				if ($input.val().length > 2) {
					timer = setTimeout(function () {
						$input.attr("name", "jform[attribs][" + $input.val().trim() + "]");
						$attribs.each(function () {
							$("input", this).attr("name", "jform[attribs][" + $input.val().trim() + "][" + $(this).children(".control-label").text().trim() + "]");
						});
					}, 150);
				}
			});

			$(".attrib-remove").on("click", function () {
				$(this).parentsUntil(".attribute-container").parent().remove();
				setTimeout(function () {
					Joomla.submitbutton("serieslist.apply", document.getElementById("series-form"));
				}, 50);
			});

		});

});