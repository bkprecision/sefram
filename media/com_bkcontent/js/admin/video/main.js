// Require.js allows us to configure shortcut alias
require.config({
	baseUrl: '/media/com_bkcontent/js',
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		'handlebars': {
			exports: 'Handlebars'
		}
	},
	paths: {
		handlebars: 'lib/handlebars/handlebars-v1.3.0',
		text: 'lib/require/text'
	}
});

define(['handlebars'], function( Handlebars ) {

		var $ = jQuery.noConflict();

		$(document).ready(function () {

			var saveProduct = function () {
					setTimeout(function () {
						Joomla.submitbutton("video.apply", document.getElementById("video-form"));
					}, 50);
				};

			$(".remove-invt-id").on("click", function () {
				$(this).parent().parent().remove();
				saveProduct();
			});

			$(".typeahead").typeahead({source: BKP.models});

		});


});