// Require.js allows us to configure shortcut alias
require.config({
	baseUrl: '/media/com_bkcontent/js',
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		'handlebars': {
			exports: 'Handlebars'
		}
	},
	paths: {
		handlebars: 'lib/handlebars/handlebars-v1.3.0',
		text: 'lib/require/text'
	}
});

define(['handlebars'], function( Handlebars ) {

		var $ 			= jQuery.noConflict(),
			saveProduct = function () {
				setTimeout(function () {
					Joomla.submitbutton("software.apply", document.getElementById("software-form"));
				}, 50);
			},
			newModel    = document.createElement("input");

		newModel.setAttribute("type", "hidden");
		newModel.setAttribute("name", "jform[modelList][]");

		$(document).ready(function () {

			$(".remove-software-model").on("click", function () {
				$(this).parent().parent().remove();
				saveProduct();
			});

			$("#jform_models_list").on("change", function () {

				var $newModel = $(this).val();

				if ($newModel) {
					newModel.value = $newModel;
				}

				document.adminForm.appendChild(newModel);
				saveProduct();
			});

		});


});