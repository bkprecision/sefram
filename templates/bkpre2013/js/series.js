jQuery(document).ready(function ($) {
	
	var modelIdx = $('#' + $('.selectedModel a').text()).index();
	
	$('#' + $('.selectedModel a').text())
		.parent()
		.siblings()
		.andSelf()
		.each(function () {
			$(this).children().eq(modelIdx).addClass('selectedModel');
		});
	
});