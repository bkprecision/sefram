jQuery(document).ready(function($) { 
	
	var $tabs 	= $(".tabs").tabs(),
		$pPane	= $("div#accessories"),
		$anchor = [],
		p 		= document.createElement("p"),
		txt 	= document.createTextNode("No products to display"),
		noProds = document.createDocumentFragment(),
		clkSC	= function () {
			
			var newHash = this.href.replace(/^\/|\.html$/, "").split("/");
			
			loadingStart();
			
			newHash = "!/" + newHash[newHash.length-1];
			document.location.hash = newHash;
			if($tabs.tabs( "option", "selected" )) {
				$tabs.tabs( "select", 0 );
			}
			$.post(this.href.replace(/^(.+)\.html#\!(\/.+)$/, "$1$2.html"), {'format':'raw'}, postCB);
			$(".categories li").removeClass("category-on").find('.selected').removeClass('selected');
			$(this).siblings('span').addClass("selected").parent().addClass("category-on");
			
			return false;
		},
		postCB	= function (r) {
			var $ul = $(r).filter(function () { return this.nodeType !== 3; }).filter("ul.accessories");
			
			loadingStop();
			
			if($ul.length) {
				$pPane.children("ul").remove();
				$pPane.append($ul);
			}
			else {
				$pPane.html(noProds);
			}
			
			$('.fancybox').fancybox({'titlePosition':'inside'});
		},
		loadingStart = function () {
			$(".loadWait").fadeIn("fast");
		},
		loadingStop	= function () {
			$(".loadWait").fadeOut("fast");
			$("html, body").animate({ scrollTop: 0 }, 400);
		};
	
	// Remove all products for headless browsers
	//$('div.hidden.no-js').remove();
	
	p.appendChild(txt);
	noProds.appendChild(p);
	
	//attach click event to all anchors
	$("ul.categories a").each(function () { 

		$(this).click(clkSC); 
	});
	
	//get the selected or first category anchor
	$anchor = $(".categories .selected").eq(0).length ? $(".categories .category-on a").eq(0) : $(".categories a").eq(0);
	$anchor.trigger("click").parent().addClass("category-on");
	
});