var SEFRAM = SEFRAM || {};

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-448292-1']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

// Setup the Google Custom Search (GCS)
// @docs https://developers.google.com/web-search/docs/reference
google.load('search', '1');

SEFRAM.search = {

		onLoad : function () {
			var search = SEFRAM.search,
				lang   = "lang_" + document.documentElement.lang;

			// create the searcher for our object
			search.webSearch 	 = new google.search.WebSearch();
			search.webSearch.setLinkTarget( google.search.Search.LINK_TARGET_SELF );

			search.searchInput 	 = document.getElementById("search-input");
			search.clearButton	 = document.getElementById("search-clear");
			// attach to our results element
			search.resultsDiv 	 = document.getElementById("search-results");

			// Restrict our search to pages from our CSE
			search.webSearch.setSiteRestriction('008331306769126933763:2u8ivn40jtk');
			search.webSearch.setRestriction(google.search.Search.RESTRICT_EXTENDED_ARGS, { "lr" : lang });
			search.webSearch.setSearchCompleteCallback(this, SEFRAM.search.searchComplete);
		},

		query : function ( str ) {
			var search = SEFRAM.search;

			search.webSearch.execute( str );
		},

		clearSearch : function () {
			var search 	   = SEFRAM.search,
				queryInput = search.searchInput,
				resultsDiv = search.resultsDiv,
				clearBtn   = search.clearButton;

			// clear any previous results
			resultsDiv.innerHTML = "";
			queryInput.value 	 = "";

			// hide the results div
			resultsDiv.className = resultsDiv.className + " hidden";
			resultsDiv.className = resultsDiv.className.replace(/\s{2,}/g, ' ');

			//hide the clear button
			clearBtn.className 	 = clearBtn.className + " hidden";
			clearBtn.className 	 = clearBtn.className.replace(/\s{2,}/g, ' ');

			// clear the results from GCS
			search.webSearch.clearResults();

			// set focus back on search input
			queryInput.focus();
		},

		searchComplete : function () {
			var search 	   = SEFRAM.search,
				input 	   = search.searchInput,
				results    = search.webSearch.results,
				resultsDiv = search.resultsDiv,
				clearBtn   = search.clearButton,
				list 	   = document.createElement("ul"),
				noResults  = document.createElement("p");

			noResults.innerHTML  = "Your query did not return results. Please, refine your search and try again.";
			list.className 		 = "unstyled";

			// clear any previous results
			resultsDiv.innerHTML = "";
			// make things visible
			resultsDiv.className = resultsDiv.className.replace(/\bhidden\b/g, "").replace(/\s+$/g, "");
			clearBtn.className 	 = clearBtn.className.replace(/\bhidden\b/g, "").replace(/\s+$/g, "");

			if( results && results.length > 0 ) {

				for( var i = 0; i < results.length; i += 1 ) {
					var result 	  = results[i],
						listItem  = document.createElement("li"),
						className = i % 2 == 0 ? "even" : "odd";
						node	  = result.html.cloneNode(true);

					if( i == 0 ) {
						className = className + " first";
					}

					if( i == results.length-1 ) {
						className = className + " last";
					}

					listItem.className = className;
					listItem.appendChild(node);
					list.appendChild(listItem);
				}

				resultsDiv.appendChild(list);
			}
			else if( input.value.length === 0 ) {
				search.clearSearch();
			}
			else {
				resultsDiv.appendChild(noResults);
			}
		}
};
google.setOnLoadCallback(SEFRAM.search.onLoad);

jQuery(document).ready(function($) {
	var timerId;
	$("#search-input").on("keyup", function (e) {
		var self = this;
		if( timerId !== undefined || timerId !== null ) {
			clearTimeout(timerId);
		}
		if( e.which !== 13 ) {
			timerId = setTimeout(function () {
				SEFRAM.search.query( $(self).val() );
			}, 250);
		}
		else if ( e.which === 13 && $(this).val().length > 1 ) {
			SEFRAM.search.query( $(this).val() );
		}
	});

	if( $("#search-input").val().length ) {
		$("#search-clear").removeClass("hidden");
	}
	// make sure we grab the submit request
	$("#search-form").on("submit", function (e) { e.preventDefault(); });

	// attach the clear results handler
	$("#search-clear").on("click", SEFRAM.search.clearSearch);
});