(function ($, undefined) {
	
	$.widget("ui.fancyForm", {
		
		options 	: {
			helperText : true
		},
		
		// Set up the form widget
		_create 	: function () {
			
			var self 		= this,
				options 	= self.options,
				$form 		= $(this.element).addClass('ui-widget'),
				$inputs		= $form.find('input, textarea'),
				$buttons 	= $form.find('button').button();
			
			options.$form 		= $form;
			options.$inputs 	= $inputs;
			options.$buttons 	= $buttons;
			
			// Add relavent jQuery UI classes
			$form.find('fieldset').addClass('ui-widget-content ui-corner-all');
			$form.find('legend').addClass('ui-widget-header ui-corner-all');
			$inputs
				.each(function (idx, input) {
					$(input).addClass('ui-state-default ui-corner-all');

					// Do we need to create a label for this input
					if( ! $(input).parent().is('label') && ! $(input).siblings().eq(0).is('label') ) {
						$(input).wrap("<label />");
					}

					// Ensure we are working with inputs of a specific type (i.e. no hidden elements need customization, so they are not included)
					if($(input).is("input[type='text']") || $(input).is("input[type='password']") || $(input).is("textarea")) {

						$(input)
							.addClass('text')
							.focus(function () {
								// Mouse enter event
								$(input).addClass('ui-state-focus');

								if( options.helperText && $(input).attr('title') !== undefined  && ! $(input).parent().find('label.helperText').length ) {
									// Setup Helper text if available
									$(input)
										.parent()
										.append( $(document.createElement("LABEL"))
													.addClass('helperText')
													.css('display', 'none')
													.append(document.createTextNode( $(input).attr('title')) ) );
								}

								if( $(input).parent().find('label.helperText').length ) {
									$(input)
										.parent()
										.find('label.helperText')
										.fadeIn();
								}
							})
							.blur(function () {
								// Mouse leave event
								$(this).removeClass('ui-state-focus');

								if( $(this).parent().find('.helperText').length ) { // Hide any helper text
									$(this)
										.parent()
										.find('.helperText')
										.fadeOut('fast');
								}
							});

					}
				})
				.eq(0)
				.focus();
			
		},
		
		_setOption 	: function (key, value) {
			
			$.Widget.prototype._setOption.apply(this, arguments);
			
		},
		
		destroy 	: function () {
			
			$.Widget.prototype.destroy.call(this);
			
		}
		
	});
	
}(jQuery));

jQuery(document).ready(function ($) {
	
	var $dialog 	= $(document.createElement("DIV"))
							.addClass('alignCenter')
							.dialog({'model':true, 'autoOpen':false})
							.append( $(document.createElement("P"))
										.addClass('alignCenter')
										.append( document.createTextNode("Sending Message...")) )
							.append( $(document.createElement("IMG"))
										.attr("src","/images/ajax-load-2.gif") ),
		$successMsg = $(document.createElement("P"))
							.css("display","none")
							.append(document.createTextNode("Thank you for submitting your subscription request."))
							.append(document.createElement("BR"))
							.append(document.createTextNode("Your information has been submitted and you will be included in future newsletters."));
	
	$('form')
		.submit(function (e) {
			
			var $form 	= $(this),
				$inputs = $form.find('input, textarea'),
				isValid = false;
			
			if($('input[name="Email"]').val() === $('input[name="Email2"]').val()) {
				isValid = true;
			}
				
			$('input.required').each(function () {
				if(! $(this).val().length > 0 ) {
					isValid = false;
				}
			});
			
			if(isValid)
			{
				$dialog.dialog('open');
				
				$.getJSON(bkp.contact.url, $form.serialize(), function (r) {

					if(r.success === true) {
						$dialog.dialog('close');
						$form
							.children("fieldset")
								.children()
								.not("legend")
									.hide()
									.remove();
						$form
							.children("fieldset")
								.append($successMsg)
								.children()
									.fadeIn();
					}

				});
			}
			
			e.preventDefault();
		})
		.fancyForm();
		
});