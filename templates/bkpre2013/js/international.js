jQuery(document).ready(function ($) {
	$("#filter-country").on("change", function () {

		var countrySelector = $("option:selected", this).val();
		if(!countrySelector) {
			$("table tbody tr").show();
		}
		else {
			$("table tbody tr").show().not("table tr." + countrySelector).hide();
		}

	});
});