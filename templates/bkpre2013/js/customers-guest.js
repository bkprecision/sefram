jQuery(document).ready(function ($) {
	
	$('#stockGrid')
		.jqGrid({
			autowidth:true,
			height:'auto',
			datatype:"json",
			colNames:['Model#', 'Description', 'Availability', 'Lead Time (Weeks)', 'List Price'],
			colModel: [
				{name:'invt_id', index:'invt_id', align:'center', width: 120},
				{name:'title', index:'title', align:'left', width: 325, sortable: false, search: false},
				{name:'qty_avail', index:'qty_avail', align:'center', width: 130},
				{name:'lead_time', index:'lead_time', align:'right', width: 180, sortable: false, search: false},
				{name:'list_price', index:'list_price', align:'right', width: 180, sortable: false, search: false}
			],
			postData:{
				'format':'json',
				'type':'products'
			},
			url:bkp.customer.url,
			rowNum:15,
			rowList:[15, 25, 50],
			pager:'#stockPgr',
			sortname:'invt_id',
			sortorder:'ASC'
		})
		.navGrid('#stockPgr', {search:true,edit:false,add:false,del:false},
		{}, //prmEdit
		{}, //prmAdd
		{}, //prmDel
		{ 
			sopt:['bw'], 
			closeAfterSearch: true, 
			closeOnEscape: true, 
			afterShowSearch: function (sObj) { 
				$('input.vdata', sObj).focus(); 
			} 
		}, //prmSearch
		{});
	
});