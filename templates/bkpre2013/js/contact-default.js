jQuery(document).ready(function ($) {

	$('.modal')
		.modal({
			backdrop: 	'static',
			keyboard: 	false,
			show: 		false
		});

	$('#modal-error, #modal-success').on('hidden', function () {
		window.location.href = SEFRAM.contact.url;
	});

	$('form').submit(function () {
		$('#modal-submit').modal('show');

		$.post(SEFRAM.contact.url, $(this).serialize(), function () {
			$('.modal').modal('hide');
			$('#modal-success').modal('show');
		})
		.error(function() {
			$('.modal').modal('hide');
			$('#modal-error').modal('show');
		});

		return false;
	});

});