<?php
// no direct access
defined('_JEXEC') or die;

?>
<!-- <form id="search-form" class="navbar-search pull-right">
	<input id="search-input" name="q" class="search-query" type="text" placeholder="Search" />
	<button id="search-clear" type="button" class="close hidden" aria-hidden="true">×</button>
</form>
<div id="search-results" class="span5 hidden"></div> -->

<a class="hidden-desktop mobile-search" href="/search.html">Search</a>
<form id="search-form" class="navbar-search pull-right visible-desktop" autocomplete="off">
	<input id="input-box" class="span2" name="q" type="text" /><i id="site-search-icon" class="icon-search"></i>
	<button id="search-clear" type="button" class="close hide" aria-hidden="true"><i class="icon-remove-circle"></i></button>
	<!--<i class="ion-loading-c search-ajax hide"></i>-->
</form>

