<?php

// no direct access
defined('_JEXEC') or die;

/*
 * Code for Discontinued products program
 * 
 */

$userAction = JRequest::getVar('user_action');
$oldModel 	= JRequest::getVar('old_model');
$newModel	= JRequest::getVar('new_model');
if(isset($userAction) AND ! empty($userAction) AND isset($oldModel) AND ! empty($oldModel))
{
	$this->wrapper->url = $this->wrapper->url.'?user_action='.$userAction.'&old_model='.$oldModel;
	if(isset($newModel) AND ! empty($newModel))
	{
		$this->wrapper->url .= '&new_model='.$newModel;
	}
}

?>
<script type="text/javascript">
function iFrameHeight() {
	var h = 0;
	if (!document.all) {
		h = document.getElementById('blockrandom').contentDocument.height;
		document.getElementById('blockrandom').style.height = h + 60 + 'px';
	} else if (document.all) {
		h = document.frames('blockrandom').document.body.scrollHeight;
		document.all.blockrandom.style.height = h + 20 + 'px';
	}
}
</script>
<div class="contentpane<?php echo $this->pageclass_sfx; ?>">
<?php if ($this->params->get('show_page_heading', 1)) : ?>
	<h1>
		<?php if ($this->escape($this->params->get('page_heading'))) :?>
			<?php echo $this->escape($this->params->get('page_heading')); ?>
		<?php else : ?>
			<?php echo $this->escape($this->params->get('page_title')); ?>
		<?php endif; ?>
	</h1>
<?php endif; ?>
<iframe <?php echo $this->wrapper->load; ?>
	id="blockrandom"
	name="iframe"
	src="<?php echo $this->escape($this->wrapper->url); ?>"
	width="<?php echo $this->escape($this->params->get('width')); ?>"
	height="<?php echo $this->escape($this->params->get('height')); ?>"
	scrolling="<?php echo $this->escape($this->params->get('scrolling')); ?>"
	frameborder=0
	class="wrapper<?php echo $this->pageclass_sfx; ?>">
	<?php echo JText::_('COM_WRAPPER_NO_IFRAMES'); ?>
</iframe>
</div>
