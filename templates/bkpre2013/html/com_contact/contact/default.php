<?php
 /**
 * @package		Joomla.Site
 * @subpackage	com_contact
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$uri		= JFactory::getURI();

$cparams 	= JComponentHelper::getParams('com_media');

// code to wipe out the lame javascript that gets inserted automagically to $this->item->email_to
$db 		= JFactory::getDBO();

$qry 		= $db->getQuery(true)
					->select('*')
					->from('#__contact_details')
					->where('id='.(int)$this->item->id);

$results 	= $db->setQuery($qry)->loadObjectList();

$this->item->email_to = $results[0]->email_to;

// get the menu page title or assign default heading
$app 		= JFactory::getApplication();
$menu		= $app->getMenu()->getActive();
$pageTitle 	= $menu->params->get('page_title');

$heading 	= (!empty($pageTitle)) 
				? $pageTitle 
				: JText::_('COM_CONTACT_FORM_LABEL');
?>

<h1><?=$heading?></h1>

<?php if($this->contact->misc && $this->params->get('show_misc')): ?>
	<?php echo $this->contact->misc; ?>
<?php endif; ?>

<section class="form rightPane width-70">
	<?php  echo $this->loadTemplate('form');  ?>
</section>

<script type="text/javascript">
bkp.contact 			= {url:"<?=$uri->getPath()?>"};
bkp.contact.success 	= "<?=$this->contact->params->getValue('redirect')?>";
bkp.contact.errorMsg 	= "<?=JText::_('COM_CONTACT_ERROR_FIELD')?>";
bkp.contact.sending 	= "<?=JText::_('COM_CONTACT_SEND_MSG')?>";
 // Add a script element as a child of the body
bkp.loadJsOnLoad = function () {
	var element = document.createElement("script");
	element.src = "/templates/bkpre2011/js/contact-default.js";
	document.body.appendChild(element);
}

// Check for browser support of event handling capability
if (window.addEventListener) {
	window.addEventListener("load", bkp.loadJsOnLoad, false);
}
else if (window.attachEvent) {
	window.attachEvent("onload", bkp.loadJsOnLoad);
}
else {
	window.onload = bkp.loadJsOnLoad;
}

</script>