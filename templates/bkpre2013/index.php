<?php

// No direct access.
defined('_JEXEC') or die( 'Restricted access' );

// we don't want the default generator
$this->setGenerator("SEFRAM");

$app     	= JFactory::getApplication();
$doc     	= JFactory::getDocument();
$langTag  	= preg_replace("/^([^-]*).*$/", "$1", JFactory::getLanguage()->getTag());
$menus     	= $app->getMenu();
$menu     	= $menus->getActive();
$url     	= JURI::getInstance();
$urlBase  	= $url->getScheme()."://".$url->getHost()."/";
$pageclass  = "";
$tagline   	= JText::_("COM_BK_TAGLINE");

if( is_object( $menu ) ) {
  $pageclass = $menu->params->get("pageclass_sfx");
}


$doc->addStyleSheet($urlBase."templates/".$this->template."/css/bootstrap.css");
$doc->addStyleSheet($urlBase."templates/".$this->template."/css/template.css");

//replace all mootools junk with jquery & bootstrap
unset($this->_script);
$this->_script = array();
unset($this->_scripts['/media/system/js/mootools-core.js']);
unset($this->_scripts['/media/system/js/core.js']);
unset($this->_scripts['/media/system/js/caption.js']);


$doc->addScript($urlBase.'templates/'.$this->template.'/js/modernizr.js');
$doc->addScript($urlBase.'templates/'.$this->template.'/js/lib/jquery/jquery-1.8.2.min.js');
$doc->addScript($urlBase.'templates/'.$this->template.'/js/bootstrap.min.js');

// add google custom search stuff
$doc->addScript("http://www.google.com/jsapi");

$title = $doc->getTitle();
if($title != 'B&K Precision Corporation') {
  $title = $title.' - SEFRAM';
  $doc->setTitle($title);
}

?>
<!DOCTYPE html>
<html lang="<?= $langTag; ?>">
   <head>
    <jdoc:include type="head" />
    <!--[if lt IE 8]>
    <link rel="stylesheet" href="<?=$urlBase.'templates/'.$this->template.'/css/ie7.css'?>" type="text/css">
    <![endif]-->
   </head>
  <body>
  <div class="container">

    <header class="row">
      <div class="span12">

        <div class="row">
          <div class="span3 logo-container">
            <a href="http://<?=$url->getHost()?>">
              <img border="0" src="templates/<?=$this->template?>/images/sefram_bk_logo.jpg" />
            </a>
          </div>

          <div class="span8">
            <div class="lang-nav span8">
              <nav class="top-navigation btn-group pull-right">
                <jdoc:include type="modules" name="topNav" />
              </nav>
            </div>
            <div class="main-nav span8">
              <nav id="navigation" class="main-navigation navbar">
                <div class="navbar-inner">
                  <jdoc:include type="modules" name="navigation" />
                </div>
              </nav>
            </div>
            <div class="span8">
              <div class="pull-right">
                <jdoc:include type="modules" name="search" />
              </div>
            </div>
          </div>
          <? if( !empty($tagline)): ?>
          <p class="span8 pull-left tag-line"><small><em><?= $tagline; ?></em></small></p>
          <? endif; ?>
        </div>

      </div>
    </header>

<? if(!empty($pageclass)): ?>
  <article class="row <?=$pageclass?>">
<? else: ?>
    <article class="row">
<? endif; ?>
    <hr />
    <jdoc:include type="modules" name="breadcrumbs" />
    <jdoc:include type="component" />
    </article>

    <footer class="row">
    <div class="span6 offset3 footer-navigation">
      <jdoc:include type="modules" name="footer-nav" />
    </div>
        <p class="span12"><small>32, rue Edouard Martel — BP55 F42009 — Saint-Etienne Cedex — &copy; 2008-<?=date('y')?> Sefram</small></p>
    </footer>
  </div>
  <script src="<?= "{$urlBase}templates/{$this->template}/js/global.js"; ?>"></script>
  </body>
</html>
