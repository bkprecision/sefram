<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$langTag = preg_replace("/^([^-]*).*$/", "$1", JFactory::getLanguage()->getTag());
?>
<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/WebPage" lang="<?= $langTag; ?>">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<meta name="robots" content="index, follow">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title><?php echo $this->error->getMessage(); ?> - B&amp;K Precision Corporation</title>
		<link href="/templates/bkpre2013/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">
      	<link rel="stylesheet" href="https://bkpmedia.s3.amazonaws.com/css/bootstrap.min.css" type="text/css">
      	<link rel="stylesheet" href="https://bkpmedia.s3.amazonaws.com/css/bootstrap-responsive.min.css" type="text/css">
      	<link rel="stylesheet" href="https://bkpmedia.s3.amazonaws.com/css/ionicons.css" type="text/css">
      	<link rel="stylesheet" href="https://bkpmedia.s3.amazonaws.com/css/template.css" type="text/css">
	</head>
	<body>
		<header class="main-nav-wrapper">
		  	<div class="container-fluid">
				<nav id="navigation" class="main-navigation navbar navbar-inverse">
					<div class="navbar-inner">
						<a class="brand" href="http://www.bkprecision.com/">
							<img border="0" src="http://bkpmedia.s3.amazonaws.com/external/images/bk-fff-logo.png" alt="Click to go to the B&amp;K Precision Home Page">
						</a>
					</div>
				</nav>
			</div>
		</header>
	    <article class="container-fluid">
			<div id="errorMsg" class="row-fluid">
				<h2><?php echo $this->error->getCode(); ?> - <?php echo $this->error->getMessage(); ?></h2>
				<p><strong><?php echo JText::_('You may not be able to visit this page because of:'); ?></strong></p>
				<ol>
					<li><?php echo JText::_('An out-of-date bookmark/favourite'); ?></li>
					<li><?php echo JText::_('A search engine that has an out-of-date listing for this site'); ?></li>
					<li><?php echo JText::_('A mis-typed address'); ?></li>
					<li><?php echo JText::_('You have no access to this page'); ?></li>
					<li><?php echo JText::_('The requested resource was not found'); ?></li>
					<li><?php echo JText::_('An error has occurred while processing your request.'); ?></li>
				</ol>
				<p><strong><?php echo JText::_('Please try one of the following pages:'); ?></strong></p>
				<p>
					<ul>
						<li><a href="<?php echo $this->baseurl; ?>/" title="<?php echo JText::_('Go to the home page'); ?>"><?php echo JText::_('Home Page'); ?></a></li>
					</ul>
				</p>
				<p><?php echo JText::_('If difficulties persist, please contact the system administrator of this site.'); ?></p>
			</div>
		</article>
	    <footer class="container-fluid">
	    	<hr>
    		<div class="row-fluid">
    			<p><small><?= JText::_('COM_BK_FOOTER_COMPANY'); ?> &copy; 20<?= date('y'); ?> (<a href="/sitemap.html">Site Map</a>)</small></p>
	      	</div>
	    </footer>
	</body>
</html>
