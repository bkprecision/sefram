// Credit to http://stackoverflow.com/a/149099/577264
// for supplying such an elegant answer
Number.prototype.formatMoney = function (c, d, t) {
	var n = this,
	    c = isNaN(c = Math.abs(c)) ? 2 : c,
	    d = d == undefined ? "." : d,
	    t = t == undefined ? "," : t,
	    s = n < 0 ? "-" : "",
	    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
	    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

// Only show the search modal if there is a query parameter present in the browser.
if (window.location.search !== "" ) {
	// Does not return a blank response, thus a query does exist.
	$('#esModal').modal('toggle')
} else {
	// Response is blank. No query.
	console.log('Elasticsearch is not working properly.');
};

define['jquery'], function ( $ ) {

	$(document).ready(function() {

		$("#navigation .menu i[class^='menu-']").on("click", function () {
			window.location.href = $(this).siblings("a").attr("href");
		});

		$("#navigation .popover-toggle")
			.each(function (i, elem) {
				$(elem)
					.popover({
						"html":true,
						"trigger":"manual",
						"content": $(elem).next().children()
					})
					.mouseover(function () {
						var self = this;

						clearTimeout($(this).data('timeoutId'));

						if (!$(this).siblings(".popover").length) {
							$(this)
							.popover("show")
							.siblings(".popover")
							.mouseover(function () {
								clearTimeout($(self).data('timeoutId'));
							})
							.mouseout(function () {
								$(self).trigger("mouseout");
							});
						}
					})
					.mouseout(function () {
						var $self = $(this),
						timeoutId = setTimeout(function () {
							if ( !$self.is(":hover") && !$self.siblings(".popover").is(":hover") ) {
								$self.popover("hide");
							}
						}, 10);
						$self.data("timeoutId", timeoutId);
					});
			});
			var getUrlParam = function(e) {
				var t = new RegExp("[?&]" + e.replace(/[\[\]]/g, "\\$&") + "(=([^&#]*)|&|#|$)"),
					a = t.exec(window.location.href);
				return a && a[2] ? decodeURIComponent(a[2].replace(/\+/g, " ")) : ""
			};

	});

};
