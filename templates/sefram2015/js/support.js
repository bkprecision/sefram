$(document).ready(function ($) {
	if($('#kbQueryBox').length) {
		
		var searchTimer;
		
		$('#kbQueryBox').keyup(function () {
			
			var self = this;
			
			clearTimeout(searchTimer);
			
			searchTimer = setTimeout(function () {
				
				var kbUrlBase 	= '/search.html',
					kbQuery 	= $(self).val().replace(/[\s\:\&\!\,\(\)\;]+/g, " "),
					arsWords 	= [];

				searchWords = kbQuery.split(" ");

				for(i=0,num=searchWords.length; i<num; i++)
				{
					if(searchWords[i].length>2) 
					{
						arsWords.push(searchWords[i]);
					}
				}
				
				if(kbQuery.length > 1) {
					
					$('.ajaxLoading').show();
					
					kbQuery = arsWords.join(" ");
					$("#kbResults").remove();
					$.ajax({
						type: "POST",
						url: kbUrlBase,
						traditional: true,
						data: { 'q': arsWords, "c" : "0", "type" : "ars", "format":"raw", "kb":"true" },
						success: function(html){
							var $div = $(document.createElement("DIV")).attr('id','kbResults').addClass("ui-widget-content ui-corner-all shadow").css({"position":"absolute", "padding":"3%"}),
								newRsts = $('span.Text, ol.InfoList', html);
								
							$('.ajaxLoading').hide();

							$div.append(newRsts).append( $(document.createElement("DIV")).addClass("clr") );

							if($(newRsts).length) {

								
								$('#kbSearch').append($div);
								$('.CloseIcon').removeAttr('onclick');
								$('.CloseIcon').live('click', function(){
									$(this).parent().remove();
								});
								$('#kbResults').fadeIn();
							}
						}
					});
				}				
				
			}, 1000);
			

		});
	}
});
