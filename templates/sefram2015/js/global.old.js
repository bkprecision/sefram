// Credit to http://stackoverflow.com/a/149099/577264
// for supplying such an elegant answer
Number.prototype.formatMoney = function (c, d, t) {
	var n = this,
	    c = isNaN(c = Math.abs(c)) ? 2 : c,
	    d = d == undefined ? "." : d,
	    t = t == undefined ? "," : t,
	    s = n < 0 ? "-" : "",
	    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
	    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

define(['jquery'], function ( $ ) {

	$(document).ready(function() {

		var timerId,
			resultsTmpl  = Handlebars.compile( $("#search-results-tmpl").html() ),
			clearResults = function () {
				$("#search-clear").hide();
				$("#search-results").hide().html("");
			},
			parseResults = function (reply) {

				var results = {
						items: [],
						totalResults: reply.searchInformation.totalResults,
						extSearch: reply.queries.hasOwnProperty("nextPage"),
						query: reply.queries.request[0].searchTerms
					};

				if (reply.hasOwnProperty("promotions")) {
					$(reply.promotions).each(function () {
						results.items.push({title: this.title, link: this.link, image: this.image.source});
					});
				}

				if (reply.hasOwnProperty("items")) {
					$(reply.items).each(function () {

						var image = "";

						// first try the thumbnail set on every product page
						if (this.hasOwnProperty("pagemap") && this.pagemap.hasOwnProperty("thumbnail")) {
							image = this.pagemap.thumbnail[0].src;
						}
						// then try every page that has a product-image
						else if (this.hasOwnProperty("pagemap") && this.pagemap.hasOwnProperty("metatags") && this.pagemap.metatags[0].hasOwnProperty("product-image")) {
							image = this.pagemap.metatags[0]["product-image"];
						}
						// if all else failes, try to get a CSE thumbnail provided by google search
						else if (this.hasOwnProperty("pagemap") && this.pagemap.hasOwnProperty("cse_thumbnail")) {
							image = this.pagemap.cse_thumbnail[0].src;
						}

						if (this.title) {
							results.items.push({title: this.title, link: this.link, image: image});
						}
					});
				}

				$("#search-form .search-ajax").hide();
				$("#search-clear").show();
				$("#search-results").show().html( resultsTmpl(results) );
			},
			search 		 = function () {

				var query = arguments[0],
					data  = {
						key: "AIzaSyCMGfdDaSfjqv5zYoS0mTJnOT3e9MURWkU",
						cx:  "015050291191371551375:cnehyusxixa",
						num: 5,
						lr: "lang_"+$("html").prop("lang"),
						q: 	 query
					};

				$("#search-clear").hide();
				$("#search-form .search-ajax").show();
				_gaq.push(["_trackEvent", "SiteSearch", "SearchBar", query]);
				$.getJSON("https://www.googleapis.com/customsearch/v1", data, parseResults);
			};

		Handlebars.registerHelper("item", function() {

			var query 		= "",
				itemClass 	= "",
				data  		= arguments[0].data;

			if (this.image) {
				query = '<div class="span4 preview"><img class="preview" src="' + this.image + '" alt="' + this.title +  '"></div><div class="span8"><h7 class="title">' + this.title + '</h7></div>';
			}
			else {
				query = '<h7 class="title">' + this.title + '</h7>';
			}
			// get the item attributes for classes
			if (data.first) {
				itemClass = "first";
			}
			if (data.last) {
				itemClass = "last";
			}
			var item = '<li class="'+ itemClass +'"><a href="' + this.link + '"><div class="row-fluid">' + query + '</div></a></li>';

			return new Handlebars.SafeString(item);
		});

		$("#search-input")
			.on("keyup", function (e) {

				var $self = $(this);
				// check for previous timers and clear them
				if ( timerId !== undefined && timerId !== null ) {
					clearTimeout(timerId);
				}
				// check for length of search query
				if ( $self.val().length <= 1) {
					clearResults();
					return;
				}
				// check for enter key
				if ( e.which !== 13 ) {
					timerId = setTimeout(function () {
						search($self.val());
					}, 375);
				}
				else  {
					search($self.val());
				}
			})
			.on("focus", function () {

				var $self = $(this);

				if ( $self.val().length > 1) {
					search($self.val());
				}

			})
			.on("blur", function () {

				var $results = $("#search-results");
				// blur from search and not hovering over results, or clear button
				if ( ! $results.is(":hover") && ! $("#search-clear").is(":hover")) {
					clearResults();
				}

			});

		$("#search-results").on("mouseleave", function () {

			var $search = $("#search-input");
			// if mouse leaves the search results and the search
			// is not in focus, clear results
			if ( ! $search.is(":focus")) {
				$search.val("");
				clearResults();
			}

		});

		//if( $("#search-input").val().length ) {
		//	$("#site-search-icon").addClass("hidden");
		//	$("#search-clear").removeClass("hidden");
		//}
		// make sure we grab the submit request
		//$("#search-form").on("submit", function (e) { e.preventDefault(); });

		// attach the clear results handler
		$("#search-clear").on("click", function () {
			$("#search-input").val("");
			clearResults();
		});

		// prevent clicks from disabled breadcrumbs
		$(".breadcrumb .disabled-link").on("click", function (e) {
			e.preventDefault();
		});

		$("#navigation .menu i[class^='menu-']").on("click", function () {
			window.location.href = $(this).siblings("a").attr("href");
		});

		$("#navigation .popover-toggle")
			.each(function (i, elem) {
				$(elem)
					.popover({
						"html":true,
						"trigger":"manual",
						"content": $(elem).next().children()
					})
					.mouseover(function () {
						var self = this;

						clearTimeout($(this).data('timeoutId'));

						if (!$(this).siblings(".popover").length) {
							$(this)
							.popover("show")
							.siblings(".popover")
							.mouseover(function () {
								clearTimeout($(self).data('timeoutId'));
							})
							.mouseout(function () {
								$(self).trigger("mouseout");
							});
						}
					})
					.mouseout(function () {
						var $self = $(this),
						timeoutId = setTimeout(function () {
							if ( !$self.is(":hover") && !$self.siblings(".popover").is(":hover") ) {
								$self.popover("hide");
							}
						}, 10);
						$self.data("timeoutId", timeoutId);
					});
			});
	});

});
