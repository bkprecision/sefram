// Credit to http://stackoverflow.com/a/149099/577264
// for supplying such an elegant answer
Number.prototype.formatMoney = function (c, d, t) {
	var n = this,
	    c = isNaN(c = Math.abs(c)) ? 2 : c,
	    d = d == undefined ? "." : d,
	    t = t == undefined ? "," : t,
	    s = n < 0 ? "-" : "",
	    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
	    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

define(['jquery'], function ( $ ) {

	$(document).ready(function() {

		$("#navigation .menu i[class^='menu-']").on("click", function () {
			window.location.href = $(this).siblings("a").attr("href");
		});

		$("#navigation .popover-toggle")
			.each(function (i, elem) {
				$(elem)
					.popover({
						"html":true,
						"trigger":"manual",
						"content": $(elem).next().children()
					})
					.mouseover(function () {
						var self = this;

						clearTimeout($(this).data('timeoutId'));

						if (!$(this).siblings(".popover").length) {
							$(this)
							.popover("show")
							.siblings(".popover")
							.mouseover(function () {
								clearTimeout($(self).data('timeoutId'));
							})
							.mouseout(function () {
								$(self).trigger("mouseout");
							});
						}
					})
					.mouseout(function () {
						var $self = $(this),
						timeoutId = setTimeout(function () {
							if ( !$self.is(":hover") && !$self.siblings(".popover").is(":hover") ) {
								$self.popover("hide");
							}
						}, 10);
						$self.data("timeoutId", timeoutId);
					});
			});

			var getUrlParam = function(e){var t = new RegExp("[?&]" + e.replace(/[\[\]]/g, "\\$&") + "(=([^&#]*)|&|#|$)"),a = t.exec(window.location.href);return a && a[2] ? decodeURIComponent(a[2].replace(/\+/g, " ")) : ""};

			var sjUI = (function(w,d,x,a,e,s,c,r){s = [];var b=function(){s.push(arguments);},q="ui";b.arr=s;w[a]=w[a]||[];w[a][q]=w[a][q]||[];w[a][q].push(b);c=d.createElement(x);c.async=1;c.src=e;r=d.getElementsByTagName(x)[0];r.parentNode.insertBefore(c,r);return b;})(window, document, "script", "sajari", "//cdn.sajari.net/js/integrations/website-search-1.2.0.js");

			sjUI("config", {
			project: "1511813858357986041",       // Set this to your project.
			collection: "www-sefram-com", // Set this to your collection.
			pipeline: "website",     // Run the website pipeline.
			attachSearchBox: document.getElementById("search-box"), // DOM element to render search box.
			attachSearchResponse: document.getElementById("search-response"), // DOM element to render search results.
			searchBoxPlaceHolder: "Type to search", // Placeholder text for the search box.
			results: {"showImages": false}, // Configure the results
			values: {"q.override": true,"resultsPerPage": "10","q": getUrlParam("q")}, // Set default values
			tabFilters: null, // Make user selectable filters
			overlay: true // Whether to render an overlay or in-page
		});
	});

});
