jQuery(document).ready(function ($) { 
	$("#searchWtb")
		.autocomplete({
			source:bkp.models,
			select:function (event, ui) {
				
				$(".loading").show();
				
				$("#results")
					.children(":not(.loading)")
					.remove();
					
				$.getJSON(bkp.wtb.url, {"format":"json","invt_id":ui.item.value}, function (obj) {
					
					var $rstList = $(document.createElement("UL"));
					
					$(".loading").hide();
					
					$("#searchWtb").autocomplete("close");
					
					$(obj.distributors)
						.each(function () {
							
							var self 		= this,
								$listItem 	= $(document.createElement("LI"))
												.append($(document.createElement("H3"))
																.append(document.createTextNode(self.name))),
								$rstTable	= $(document.createElement("TABLE"))
												.append($(document.createElement("THEAD"))
																.append($(document.createElement("TR"))
																				.append($(document.createElement("TH")).append(document.createTextNode("Model")))
																				.append($(document.createElement("TH")).append(document.createTextNode("Qty"))))),
								$rstTblBody = $(document.createElement("TBODY")),
								$footer		= $(document.createElement("P")).addClass("alignRight");
							
							$(obj.stock).each(function () {
								if(this.dist_id === self.dist_id) {
									var $tr = $(document.createElement("TR"))
													.append($(document.createElement("TD"))
																.append($(document.createElement("A"))
																					.attr({
																						"href":self.url,
																						"target":"_blank"
																					})
																					.append(document.createTextNode(this.invt_id))))
													.append($(document.createElement("TD"))
																.append(document.createTextNode(this.qty_onhand)));
									$tr.appendTo($rstTblBody);
									$footer.text("As of: " + this.last_update);
								}
							});
							
							$rstTable
								.append($rstTblBody)
								.appendTo($listItem);
								
							$listItem
								.append($footer)
								.appendTo($rstList);
							
						});
						
						
					if($rstList.children().length === 0) {
						$rstList.append($(document.createElement("P"))
											.append(document.createTextNode("No distributors seem to have what you're looking for in stock. You can try searching again with a different part number or call us direct for a quote. (714) 921-9095")));
					}
						
					$("#results")
						.append($rstList)
						.fadeIn();
						
					$("#searchWtb").autocomplete("close");
				});
			}
		})
		.keypress(function (e) {
			if(e.keyCode === 13) {
				
				$(".loading").show();
				
				$(this).autocomplete("close");
				
				$("#results")
					.children(":not(.loading)")
					.remove();
					
				$.getJSON(bkp.wtb.url, {"format":"json","invt_id":$(this).val()}, function (obj) {
					
					var $rstList = $(document.createElement("UL"));
					
					$(".loading").hide();
					
					$("#searchWtb").autocomplete("close");
					
					$(obj.distributors)
						.each(function () {
							
							var self 		= this,
								$listItem 	= $(document.createElement("LI"))
												.append($(document.createElement("H3"))
																.append(document.createTextNode(self.name))),
								$rstTable	= $(document.createElement("TABLE"))
												.append($(document.createElement("THEAD"))
																.append($(document.createElement("TR"))
																				.append($(document.createElement("TH")).append(document.createTextNode("Model")))
																				.append($(document.createElement("TH")).append(document.createTextNode("Qty"))))),
								$rstTblBody = $(document.createElement("TBODY")),
								$footer		= $(document.createElement("P")).addClass("alignRight");
							
							$(obj.stock).each(function () {
								if(this.dist_id === self.dist_id) {
									var $tr = $(document.createElement("TR"))
													.append($(document.createElement("TD"))
																.append($(document.createElement("A"))
																					.attr({
																						"href":self.url,
																						"target":"_blank"
																					})
																					.append(document.createTextNode(this.invt_id))))
													.append($(document.createElement("TD"))
																.append(document.createTextNode(this.qty_onhand)));
									$tr.appendTo($rstTblBody);
									$footer.text("As of: " + this.last_update);
								}
							});
							
							$rstTable
								.append($rstTblBody)
								.appendTo($listItem);
								
							$listItem
								.append($footer)
								.appendTo($rstList);
							
						});
						
						
					if($rstList.children().length === 0) {
						$rstList.append($(document.createElement("P"))
											.append(document.createTextNode("No distributors seem to have what you're looking for in stock. You can try searching again with a different part number or call us direct for a quote. (714) 921-9095")));
					}
						
					$("#results")
						.append($rstList)
						.fadeIn();
						
					$("#searchWtb").autocomplete("close");
				});
			}
		})
		.focus();
		 
	$(".tabs").tabs(); 

	$("#countryList").live("change", function () {
		var self = this,
			targ = '#intDistributors',
			$dest = $(document.createElement("UL"))
						.attr("id","intDistributorList"),
			prep = function () {
				if(self.value === this.country) {
					var distObj = this,
						$li		= $(document.createElement("LI"))
									.append( $(document.createElement("UL"))
												.append( $(document.createElement("LI"))
															.append(document.createTextNode("Distributor: "+distObj.name)) )
												.append( $(document.createElement("LI"))
												 			.append(document.createTextNode("City: "+distObj.city)) )
												.append( $(document.createElement("LI"))
												 			.append(document.createTextNode("Country: "+distObj.country)) ) 
												.append( $(document.createElement("LI"))
												 			.append(document.createTextNode("Phone: "+distObj.phone)) ) 
												.append( $(document.createElement("LI"))
												 			.append(document.createTextNode("Email: "))
												 			.append( $(document.createElement("A"))
																		.attr({'href':'mailto:'+distObj.email})
																		.append(document.createTextNode(distObj.email)) ) ) 
											);
				
					$dest.append($li);
				}
			};
		if(self.value) {
			$(bkp.distibutors.international).each(prep);
			$(targ)
				.hide()
				.empty()
				.append($dest)
				.fadeIn();
		}
	});
	
});