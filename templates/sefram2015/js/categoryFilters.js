jQuery(document).ready(function($) {

	var catId			= parseInt($('#parentId').val(), 10),
		start			= parseInt($('#limitStart').val(), 10),
		state			= parseInt($('#state').val(), 10),
		appPath			= {},
		postData		= {
			"view"	: "products",
			"format": "raw",
			"cat_id": catId,
			"limit"	: 10,
			"start"	: start,
			"state" : state
		},
		$fitlersTarg 	= $(".filters-list"),
		$productsTarg 	= $("div#products"),
		updateContent	= function () {
			$.post(window.location.pathname, $params, function (resp) {
				var $resp 		= $(resp).filter(function () { return this.nodeType !== 3; }),
					$prods 		= $resp.filter(".products").children('ul'),
					$filters 	= $resp.filter("div.filters-list"),
					$pagination = $resp.filter(".products").children('div');

				$('html, body').animate({scrollTop: 0}, 400);

				$('.modal').modal('toggle');

				$filters.find("input").each(function () {
					var $filter = $(".filters-list input[name='" + this.name + "'][value='" + this.value + "']");

					if($filter) {
						$filter.attr('disabled', false);
					}
				});

				$pagination
					.find("a")
					.each(function () {
						$(this).on('click', changePage);
					});

				$productsTarg.children().remove();
				$productsTarg.append($prods).append($pagination);
				$('#pgLimit').change(updateFilters);
			});
		},
		changePage		= function (event) {
			var $inputs 	= $(".filters-list input:checkbox:checked"),
				filterData 	= {filters : $inputs.serializeArray()},
				newStart	= parseInt(this.name, 10),
				page 		= this.href.match(/#!page:(\d+)/i) !== null ? this.href.match(/#!page:(\d+)/i)[1] : 0;

			event.preventDefault();

			$('.modal').modal('toggle');

			window.location.hash = page ? "!page:" + page : "";

			$params 		= $.extend({}, filterData, postData);
			$params.start 	= newStart;
			$params.limit 	= $('#pgLimit').val();

			updateContent();
		},
		updateFilters	= function () {

			var $inputs 	= $(".filters-list input:checkbox:checked"),
				filterData 	= {filters : $inputs.serializeArray()};

			$('.modal').modal('toggle');

			$(".filters-list input")
				.attr('disabled', true);

			$params					= $.extend({}, filterData, postData);
			$params.start 			= 0;
			window.location.hash 	= '';
			$params.limit 			= $('#pgLimit').val();

			updateContent();
		}, $params;

		$('.modal')
			.modal({
				backdrop: 	'static',
				keyboard: 	false,
				show: 		false
			});

		$('.category-filter, #pgLimit')
			.on('change', updateFilters);

		$('.pagination')
			.find('a')
			.each(function () {
				$(this).on('click', changePage);
			});

		// is the user on the default page or on a specific page
		if( window.location.hash.indexOf("page") != -1 ) {
			$('.modal').modal('toggle');

			$params  = $.extend({}, postData),
			$params.limit = parseInt( $('#pgLimit').val(), 10 ),
			$params.start = ( parseInt( window.location.hash.replace(/#!page:(\d+)/i, "$1"), 10 ) - 1 )  * $params.limit;

			updateContent();
		}
});