<?php
// no direct access
defined('_JEXEC') or die;

require_once $_SERVER['DOCUMENT_ROOT'].'/app/init.php'; 

if(isset($_GET['q'])) {
	$q = $_GET['q'];
	
	$query = $es->search([
		'body' => [
			'query'=> [
				'bool'=> [
					'should'=> [
						'match' => ['product' => $q],
						'match' => ['description' => $q],
						'match' => ['keywords' => $q]
					]
				]
			]
		]
	]);
}
?>
<a class="hidden-desktop mobile-search" href="/search.html">Search</a>
<form id="search-form" class="navbar-search pull-right visible-desktop" autocomplete="off">
	<input id="input-box" class="span2" name="q" type="text" /><i id="site-search-icon" class="icon-search"></i>
	<button id="search-clear" type="button" class="close hide" aria-hidden="true"><i class="icon-remove-circle"></i></button>
	<i class="ion-loading-c search-ajax hide"></i>
</form>