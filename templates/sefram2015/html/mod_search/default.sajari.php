<?php
// no direct access
defined('_JEXEC') or die;

?>
<a class="hidden-desktop mobile-search" href="/search.html">Search</a>

<div id="nav-search-box" class="span2">
	<form id="search-form" class="navbar-search pull-right visible-desktop" autocomplete="off">
	<button id="search-clear" type="button" class="close hide" aria-hidden="true"><i class="icon-remove-circle"></i></button>
	<input id="input-box" name="q" type="text" /><i id="site-search-icon" class="icon-search"></i>
</div>
	<!--<i class="ion-loading-c search-ajax hide"></i>-->
</form>
<div id="search-results" class="span3 hide"></div>
<script id="search-results-tmpl" type="text/x-handlebars-template">
<div class="row-fluid">
	<h6 class="results-total">Showing {{items.length}} of {{totalResults}} results.</h6>
</div>
<div class="row-fluid">
	<ul class="results-list unstyled">
	{{#each items}}
		{{item}}
	{{/each}}
	</ul>
</div>
{{#if extSearch}}
<div class="row-fluid ext-results">
<a href="/search.html?q={{query}}">More Results</a>
</div>
{{/if}}
</script>
