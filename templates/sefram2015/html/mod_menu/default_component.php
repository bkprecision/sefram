<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_menu
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

// Note. It is important to remove spaces between elements.
$class = $item->anchor_css ? 'class="'.$item->anchor_css.'" ' : '';
if ($item->deeper) {
	$class = "class=\"popover-toggle desktop-menu-item\"";
}
$title = $item->anchor_title ? 'title="'.$item->anchor_title.'" ' : '';
if ($item->menu_image) {
	$item->params->get('menu_text', 1 ) ?
	$linktype = '<img src="'.$item->menu_image.'" alt="'.$item->title.'" /><span class="image-title">'.$item->title.'</span> ' :
	$linktype = '<img src="'.$item->menu_image.'" alt="'.$item->title.'" />';
}
else {
	$linktype = $item->title;
}
$menuId = strtolower($item->title);
$menuId = JFilterOutput::cleanText($menuId);
$menuId = JFilterOutput::stringURLUnicodeSlug($menuId);
$menuId = preg_replace("/\s+/", "-", $menuId);

switch ($item->browserNav) :
	default:
	case 0:
?>
<?php if (!$item->deeper): ?>
<i class="menu-<?php echo $menuId; ?>"></i>
<?php endif; ?>
	<?php if ($item->deeper): ?>
	<a class="mobile-menu-item" href="<?php echo $item->flink; ?>"><?php echo $linktype; ?></a>
	<?php endif; ?>
	<a
	<?php echo $class; ?>
	href="<?php echo $item->flink; ?>"
	<?php echo $title; ?>
	id="<?php echo $menuId; ?>"
	<?php if ($item->deeper): ?>
	data-toggle="popover"
	data-placement="bottom"
	data-trigger="hover"
	<?php endif; ?>
	>
	<?php echo $linktype; ?></a>

<?php
		break;
	case 1:
		// _blank
?><a <?php echo $class; ?>href="<?php echo $item->flink; ?>" target="_blank" <?php echo $title; ?>><?php echo $linktype; ?></a><?php
		break;
	case 2:
	// window.open
?><a <?php echo $class; ?>href="<?php echo $item->flink; ?>" onclick="window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes');return false;" <?php echo $title; ?>><?php echo $linktype; ?></a>
<?php
		break;
endswitch;
