<?php
// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * This is a file to add template specific chrome to pagination rendering.
 *
 * pagination_list_footer
 * 	Input variable $list is an array with offsets:
 * 		$list[limit]		: int
 * 		$list[limitstart]	: int
 * 		$list[total]		: int
 * 		$list[limitfield]	: string
 * 		$list[pagescounter]	: string
 * 		$list[pageslinks]	: string
 *
 * pagination_list_render
 * 	Input variable $list is an array with offsets:
 * 		$list[all]
 * 			[data]		: string
 * 			[active]	: boolean
 * 		$list[start]
 * 			[data]		: string
 * 			[active]	: boolean
 * 		$list[previous]
 * 			[data]		: string
 * 			[active]	: boolean
 * 		$list[next]
 * 			[data]		: string
 * 			[active]	: boolean
 * 		$list[end]
 * 			[data]		: string
 * 			[active]	: boolean
 * 		$list[pages]
 * 			[{PAGE}][data]		: string
 * 			[{PAGE}][active]	: boolean
 *
 * pagination_item_active
 * 	Input variable $item is an object with fields:
 * 		$item->base	: integer
 * 		$item->link	: string
 * 		$item->text	: string
 *
 * pagination_item_inactive
 * 	Input variable $item is an object with fields:
 * 		$item->base	: integer
 * 		$item->link	: string
 * 		$item->text	: string
 *
 * This gives template designers ultimate control over how pagination is rendered.
 *
 * NOTE: If you override pagination_item_active OR pagination_item_inactive you MUST override them both
 */

function pagination_list_footer($list)
{
	$html = "<div class=\"list-footer\">\n";

	$html .= "\n<div class=\"limit\">".JText::_('Display Num').$list['limitfield']."</div>";
	$html .= $list['pageslinks'];
	$html .= "\n<div class=\"counter\">".$list['pagescounter']."</div>";

	$html .= "\n<input type=\"hidden\" name=\"limitstart\" value=\"".$list['limitstart']."\" />";
	$html .= "\n</div>";

	return $html;
}

function pagination_list_render($list)
{
	// Initialize variables
	$html = "<div class=\"pagination pagination-left\">";
	$html .= "<ul>";
	$html .= "<li class=\"pagination-start\">".$list['start']['data']."</li>";
	$html .= "<li class=\"pagination-prev\">".$list['previous']['data']."</li>";

	foreach( $list['pages'] as $page )
	{
		$html .= "<li>".$page['data']."</li>";
	}

	$html .= "<li class=\"pagination-next\">".$list['next']['data']."</li>";
	$html .= "<li class=\"pagination-end\">".$list['end']['data']."</li>";
	$html .= "</ul>";

	$html .= "</div>";
	return $html;
}

function pagination_item_active(&$item) {
	$start = preg_replace('/^.+\?(.+)/', '$1', $item->link);

	$start = explode("=", $start);
	$start = (int)$start[count($start)-1];
	$href = preg_replace('/^(.*)\?.*$/', '$1', $item->link);

	// Get the pagination request variables
	$input = JFactory::getApplication()->input;
	$limit = $input->get("limit", "12");
	if($limit == '*') {
		$limit = 1000;
	}
	$limit = (int)$limit;
	// avoid divide by zero
	if ($limit < 1) {
		$page = 0;
	}
	else {
		$page  = ($start/$limit) >= 1 ? (($start/$limit)+1) : 0;
	}

	if(!preg_match('/^.*\?(q=.+)&.*$/', $item->link)) {
		$item->link = $page > 0 ? $href.'#!page:'.$page : $href;
	}

	return "<a href=\"".$item->link."\" title=\"".$item->text."\" name=\"".$start."\">".$item->text."</a>";
}

function pagination_item_inactive(&$item) {
	return "<span>".$item->text."</span>";
}
?>