<?php
// No direct access.
defined('_JEXEC') or die( 'Restricted access' );

require_once $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';

$app 		= JFactory::getApplication();
$doc 		= JFactory::getDocument();
$doc->setGenerator("");
$langTag	= preg_replace("/^([^-]*).*$/", "$1", JFactory::getLanguage()->getTag());
$menus 		= $app->getMenu();
$menu 		= $menus->getActive();
$url 		= JURI::getInstance();
$urlBase	= $url->getScheme().'://'.$url->getHost().'/';
$pageclass  = "";
$tagline 	= JText::_('COM_BK_TAGLINE');
$dataMain   = $this->getMetaData("data-main");
$es = new Elasticsearch\Client(['hosts'=> ['52.208.30.60:9200']]);

if( is_object( $menu ) ) {
    $pageclass = $menu->params->get( 'pageclass_sfx' );
}

// first, save all applied styles to a temp var
// then, set appropriate styles and add back the saved ones
$styles = $this->_styleSheets;
unset($this->_styleSheets);
$this->_styleSheets = array();

$this->setMetaData("viewport", "width=device-width, initial-scale=1.0");
$this->addStyleSheet("{$urlBase}media/com_bkcontent/css/bootstrap.min.css");
$this->addStyleSheet("{$urlBase}media/com_bkcontent/css/bootstrap-responsive.min.css");
$this->addStyleSheet("{$urlBase}media/com_bkcontent/css/ionicons.css");
$this->addStyleSheet("{$urlBase}templates/{$this->template}/css/template.css");
$this->addStyleSheet("{$urlBase}templates/{$this->template}/css/search.css");

$this->_styleSheets = array_merge($this->_styleSheets, $styles);

//replace all mootools junk with jquery & bootstrap
unset($this->_script);
$this->_script  = array();

// add google custom search stuff
//$this->addScript("//www.google.com/jsapi");

if(isset($_GET['q'])) {

    $q = $_GET['q'];

//	$query= $es->search([
//		'body' => [
//			'query'=> [
//				'bool'=> [
//					'should'=> [
//						'match' => ['product' => $q],
//						'match' => ['description' => $q],
//						'match' => ['keywords' => $q]
//					]
//				]
//			]
//		]
//	]);

//Searching Products but filtering out english and french products
//Added must_not 
if($langTag == "en"){
    $params = [
        'index' => 'products',
        'body' => [
            'query' => [
                'bool' => [
                    'should' => [
                    'multi_match' => [
                    'query' => $q,
                    'fields' => [
                        'product',
                        'description',
                        'keywords',
                    ],
                    ]
                    ],
                    'must_not' => [
                            'terms' => [
                                 'cat_id' => [224, 242, 243, 244, 245, 246, 253, 289, 290, 291, 292, 293, 294, 295, 296, 297]
                            ],
                    ], 
                    ]
                    ]
                ]
    ];
}else if ($langTag == "fr"){
    $params = [
        'index' => 'products',
        'body' => [
            'query' => [
                'bool' => [
                    'should' => [
                    'multi_match' => [
                    'query' => $q,
                    'fields' => [
                        'product',
                        'description',
                        'keywords',
                    ],
                    ]
                    ],
                    'must_not' => [
                            'terms' => [
                                 'cat_id' => [7, 9, 12, 14, 15, 16, 17, 18, 19, 20, 21, 204, 205, 298, 299, 300]
                            ],
                    ], 
                    ]
                    ]
                ]
    ];
}


    $query = $es->search($params);

    
    if($query['hits']['total'] >= 1) {
        $results = $query['hits']['hits'];
    }
}

$title = $this->getTitle();
if($title != 'Sefram') {
    $title = $title.' - Sefram';
    $this->setTitle($title);
}

// look for other components adding 'main.js'
//if ( empty($dataMain) ) {
//	$dataMain = "{$urlBase}media/com_bkcontent/js/pages/general/main.js";
//}

?>
<!DOCTYPE html>
<html itemscope itemtype="//schema.org/WebPage" lang="<?= $langTag; ?>">
<head>
    <!--[if lt IE 9]>
    <script src="<?= $urlBase; ?>media/com_bkcontent/js/assets/html5shiv.js"></script>
    <script src="<?= $urlBase; ?>media/com_bkcontent/js/assets/json2.js"></script>
    <![endif]-->
    <jdoc:include type="head" />
</head>
<? if(!empty($pageclass)): ?>

<body class="<?= $pageclass; ?>">

<? else: ?>

<body>
<? endif; ?>

<jdoc:include type="modules" name="position-1" />
<header class="main-nav-wrapper">
    <div class="container-fluid">
        <nav id="navigation" class="main-navigation navbar navbar-inverse">
            <div class="navbar-inner">
                <a class="brand" href="//<?= $url->getHost(); ?>" itemprop="brand" itemscope itemtype="//schema.org/Brand">
                    <img border="0" src="images/logo/sefram_logo_mobile.png" itemprop="logo" class="hidden-desktop mobile">
                    <img border="0" src="images/logo/sefram_logo.png" itemprop="logo"  class="visible-desktop desktop">
                    <meta itemprop="name" content="B&K Precision Corp.">
                </a>

                <ul class="phone-number">
                    <li>
                        <img src="templates/sefram2015/images/phone-icon.png" width="25px" height="25px" alt="">+33 (0).4.77.59.01.01
                    </li>
                </ul>

                <button type="button" class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="nav-collapse collapse pull-right">
                    <jdoc:include type="modules" name="navigation" />
                    <jdoc:include type="modules" name="search" />
                </div>
            </div>
        </nav>
    </div>
</header>

<jdoc:include type="modules" name="position-2" />
<article class="container-fluid">
    <jdoc:include type="component" />
    <jdoc:include type="modules" name="breadcrumbs" />

     <!-- Elasticsearch Modal -->
    <div id="esModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="esModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Search Results</h3>
    </div>
    <div class="modal-body">
        <!-- Our Elasticsearch results start -->
        <?php
        if(isset($results)) {
            // var_dump($results);
            foreach($results as $r) {
        ?>
                <div class="row-fluid">
                        <!-- Product Image -->
                        <!-- Our result URL concat -->
                        <a href="produits/<?=$r['_source']['cat_id'];?>/<?= $r['_source']['product']; ?> ">
                        <!-- Query Result -->
                        <?= $r['_source']['product']; ?></a>
                        <br>

                        <?=
                            $r['_source']['title'];
                        ?>

                    <hr />
                </div>
                <?php
            }
        }
        ?>
        <!-- Our Elasticsearch results end-->
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
    </div>
</article>

<footer class="container-fluid">
    <div class="row-fluid">
        <jdoc:include type="modules" name="position-3" />
    </div>
    <hr />
    </div>

    <div class="row-fluid">
        <p class="span5">
            <small><?= JText::_('COM_BK_FOOTER_ADDRESS'); ?> &copy; 20<?= date('y'); ?> <?= JText::_('COM_BK_FOOTER_COMPANY'); ?></small>
        </p>
        <div class="footer-navigation span7">
            <div class="container-fluid pull-right">
                <jdoc:include type="modules" name="footer-nav" />
            </div>
        </div>




</footer>
<script data-main="<?= $dataMain; ?>" src="<?= $urlBase; ?>media/com_bkcontent/js/lib/require/require.js"></script>
</body>
</html>