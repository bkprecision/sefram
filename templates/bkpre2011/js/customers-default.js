(function ($, undefined) {
	
	$.widget("ui.fancyForm", {
		
		options 	: {
			helperText : true
		},
		
		// Set up the form widget
		_create 	: function () {
			
			var self 		= this,
				options 	= self.options,
				$form 		= $(this.element).addClass('ui-widget'),
				$inputs		= $form.find('input, textarea'),
				$buttons 	= $form.find('button').button();
			
			options.$form 		= $form;
			options.$inputs 	= $inputs;
			options.$buttons 	= $buttons;
			
			// Add relavent jQuery UI classes
			$form.find('fieldset').addClass('ui-widget-content ui-corner-all');
			$form.find('legend').addClass('ui-widget-header ui-corner-all');
			$inputs
				.each(function (idx, input) {
					$(input).addClass('ui-state-default ui-corner-all');

					// Do we need to create a label for this input
					if( ! $(input).parent().is('label') && ! $(input).siblings().eq(0).is('label') ) {
						$(input).wrap("<label />");
					}

					// Ensure we are working with inputs of a specific type (i.e. no hidden elements need customization, so they are not included)
					if($(input).is("input[type='text']") || $(input).is("input[type='password']") || $(input).is("textarea")) {

						$(input)
							.addClass('text')
							.focus(function () {
								// Mouse enter event
								$(input).addClass('ui-state-focus');

								if( options.helperText && $(input).attr('title') !== undefined  && ! $(input).parent().find('label.helperText').length ) {
									// Setup Helper text if available
									$(input)
										.parent()
										.append( $(document.createElement("LABEL"))
													.addClass('helperText')
													.css('display', 'none')
													.append(document.createTextNode( $(input).attr('title')) ) );
								}

								if( $(input).parent().find('label.helperText').length ) {
									$(input)
										.parent()
										.find('label.helperText')
										.fadeIn();
								}
							})
							.blur(function () {
								// Mouse leave event
								$(this).removeClass('ui-state-focus');

								if( $(this).parent().find('.helperText').length ) { // Hide any helper text
									$(this)
										.parent()
										.find('.helperText')
										.fadeOut('fast');
								}
							});

					}
				})
				.eq(0)
				.focus();
			
		},
		
		_setOption 	: function (key, value) {
			
			$.Widget.prototype._setOption.apply(this, arguments);
			
		},
		
		destroy 	: function () {
			
			$.Widget.prototype.destroy.call(this);
			
		}
		
	});
	
}(jQuery));

jQuery(document).ready(function ($) {
	
	var $dialog 	= $(document.createElement("DIV"))
							.addClass('alignCenter')
							.dialog({
								'model'		: true, 
								'autoOpen'	: false
							})
							.append( $(document.createElement("P"))
										.addClass('alignCenter')
										.append( document.createTextNode("Sending Message...")) )
							.append( $(document.createElement("IMG"))
										.attr("src","/images/ajax-load-2.gif") ),
		$errorMsg 	= $(document.createElement("DIV"))
							.append( $(document.createElement("P"))
										.attr("id","errorMsgTxt")
										.addClass('alignCenter') )
							.dialog({
								'autoOpen'	: false,
								'modal'		: true,
								'buttons'	: {
									'OK' : function () {
										$errorMsg.dialog("close");
									}
								},
								'title'		: 'Login Error'
							}),
		$form = $('form');
	
	bkp.customer.initIface = function () {
		
		var $logoutBtn = $(document.createElement("DIV"))
							.addClass('alignRight')
							.css({
								'margin-top':'-60px',
								'margin-bottom':'35px'
							})
							.append( $(document.createElement("BUTTON"))
										.attr({
											'type':'button',
											'id':'logoutBtn'
										})
										.append(document.createTextNode("Logout"))
										.button() )
							.appendTo( $('article') ),
			$tabs = $(document.createElement("DIV"))
						.attr('id', 'customerTabs')
						.append( $(document.createElement("UL"))
									.append( $(document.createElement("LI"))
						 						.append( $(document.createElement("A"))
						 									.attr('href', '#orders')
						 									.append(document.createTextNode("Orders (Past 90 Days)")) ) )
									.append( $(document.createElement("LI"))
						 						.append( $(document.createElement("A"))
						 									.attr('href', '#stock')
						 									.append(document.createTextNode("Stock Report")) ) )
						 			.append( $(document.createElement("LI"))
						 						.append( $(document.createElement("A"))
															.attr('href', bkp.customer.url + '?layout=profile')
						 									.append(document.createTextNode("Profile")) ) )
 									.append( $(document.createElement("LI"))
						 						.append( $(document.createElement("A"))
															.attr('href', bkp.customer.url + '?layout=promodocs')
						 									.append(document.createTextNode("Promotional Support Documents")) ) ) )
						.append( $(document.createElement("DIV"))
						 			.attr('id', 'orders')
						 			.append( $(document.createElement("TABLE"))
						 						.attr('id', 'ordersGrid') ) )
						.append( $(document.createElement("DIV"))
						 			.attr('id', 'stock')
						 			.append( $(document.createElement("TABLE"))
						 						.attr('id', 'stockGrid') )
						 			.append( $(document.createElement("DIV"))
						 						.attr('id', 'stockPgr') ) )
						.appendTo( $('article') )
						.tabs({
							'ajaxOptions': { 
								'data': {
									'format':'raw', 
									'token':bkp.customer.token, 
									'key':bkp.customer.key
								} 
							} 
						});
		
		$('#ordersGrid')
			.jqGrid({
				autowidth:true,
				height:'auto',
				datatype:"json",
				colNames:['Order Number', 'Quantity Ordered', 'Order Date'],
				colModel: [
					{name:'CustOrdNbr', index:'CustOrdNbr', align:'center', width: 305},
					{name:'QtyOrd', index:'QtyOrd', align:'center', width: 305, sortable: false, search: false},
					{name:'OrdDate', index:'OrdDate', align:'center', width: 305}
				],
				postData:{
					'format':'json',
					'type':'orders',
					'token':bkp.customer.token,
					'key':bkp.customer.key
				},
				url:bkp.customer.url,
				rowNum:25,
				sortname:'OrdDate',
				sortorder:'DESC',
				subGrid:true,
				subGridOptions: { 
					plusicon : "ui-icon-plus",
					minusicon : "ui-icon-minus",
					openicon: "no-icon", 
					expandOnLoad: false, 
					selectOnExpand : false, 
					reloadOnExpand : true 
				},
				subGridRowExpanded:function (subgridId, rowId) {
					var subgridTableId,
						custOrdNbr = $(this).jqGrid('getCell', rowId, 'CustOrdNbr');
						
					subgridTableId = subgridId + "_t";
					$('#'+subgridId).html("<table id='"+subgridTableId+"' class='scroll'></table>");
					$('#'+subgridTableId)
						.jqGrid({
							url:bkp.customer.url,
							datatype:"json",
							colNames : ['Model#', 'Qty. to Ship', 'Qty. Ordered', 'Qty. Prev. Ship', 'Qty. Shipped', 'Ship Date', 'Tracking#'],
							colModel:[
								{name:'InvtId',index:'InvtId',align:'center', width:120, serach:false, sortable:false},
								{name:'QtyFuture',index:'QtyFuture',align:'center', width:125, serach:false, sortable:false},
								{name:'QtyOrd',index:'QtyOrd',align:'center', width:125, serach:false, sortable:false},
								{name:'QtyPrevShip',index:'QtyPrevShip',align:'center', width:115, serach:false, sortable:false},
								{name:'QtyShip',index:'QtyShip',align:'center', width:115, serach:false, sortable:false},
								{name:'ShipDate',index:'ShipDate',align:'center', width:160, serach:false, sortable:false},
								{name:'TrackingNbr',index:'TrackingNbr',align:'center', width:145, serach:false, sortable:false}
							],
							postData:{
								'format':'json',
								'type':'order_detail',
								'token':bkp.customer.token,
								'key':bkp.customer.key,
								'CustOrdNbr':custOrdNbr
							},
							height:'100%',
							rowNum:25,
							sortname:'OrdDate',
							sortorder:'DESC'
						});
				}
			});
		$('#stockGrid')
			.jqGrid({
				autowidth:true,
				height:'auto',
				datatype:"json",
				colNames:['Model#', 'Description', 'Quantity Available', 'Lead Time (Weeks)', 'List Price'],
				colModel: [
					{name:'invt_id', index:'invt_id', align:'center', width: 120},
					{name:'title', index:'title', align:'left', width: 325, sortable: false, search: false},
					{name:'qty_avail', index:'qty_avail', align:'center', width: 130},
					{name:'lead_time', index:'lead_time', align:'right', width: 180, sortable: false, search: false},
					{name:'list_price', index:'list_price', align:'right', width: 180, sortable: false, search: false}
				],
				postData:{
					'format':'json',
					'type':'products',
					'token':bkp.customer.token,
					'key':bkp.customer.key
				},
				url:bkp.customer.url,
				rowNum:15,
				rowList:[15, 25, 50],
				pager:'#stockPgr',
				sortname:'invt_id',
				sortorder:'ASC'
			})
			.navGrid('#stockPgr', {search:true,edit:false,add:false,del:false}, //navGrid Params
			{}, //prmEdit
			{}, //prmAdd
			{}, //prmDel
			{ 
				sopt:['bw'], 
				closeAfterSearch: true, 
				closeOnEscape: true, 
				afterShowSearch: function (sObj) { 
					$('input.vdata', sObj).focus(); 
				} 
			}, //prmSearch
			{});
			
		$('#logoutBtn').click(function () {
			bkp.customer.token = bkp.customer.key = '';

			window.location.href=bkp.customer.url;

		});
	};
	
	$('form')
		.submit(function (e) {
			$dialog.dialog("open");
			$.getJSON(bkp.customer.url, $form.serialize(), function (r) {
				$dialog.dialog("close");
				if(r.success === true) {
					bkp.customer.detail	= r.detail;
					bkp.customer.key 	= r.key;
					bkp.customer.token 	= r.token;
					$('article')
						.children(':not(h1)')
						.hide()
						.remove();
					bkp.customer.initIface();
				}
				else {
					//show login error message
					$errorMsg
						.find('#errorMsgTxt')
						.empty()
						.append(document.createTextNode(r.errorMsg))
						.append(document.createElement("BR"))
						.append(document.createTextNode('If you believe this is in error, please call our customer service at (714) 921-9095.'));
					$errorMsg.dialog("open");
				}
				
			});
			
			e.preventDefault();
		})
		.fancyForm();
		
});