/*** setup additional global helpers ***/
/*** for information regarding remedial functions see
**** http://javascript.crockford.com/remedial.html ***/
if ( ! String.prototype.trim ) {
    String.prototype.trim = function () {
		
		"use strict";
		
        return this.replace(/^\s*(\S*(?:\s+\S+)*)\s*$/, "$1");
    };
}

if( ! Array.prototype.forEach ) {

	Array.prototype.forEach = function(fun /*, thisp */) {
		
		"use strict";
		
		if (this === void 0 || this === null) {
			throw new TypeError();
		}
		
		var t = Object(this);
		var len = t.length >>> 0;
		if(typeof fun !== "function") {
			throw new TypeError();
		}
		
		var thisp = arguments[1];
		for(var i = 0; i < len; i += 1) {
			
			if(i in t) {
				fun.call(thisp, t[i], i, t);
			}
		
		}
	
	};

}
// Production steps of ECMA-262, Edition 5, 15.4.4.18
if (!Array.prototype.forEach) {

	Array.prototype.forEach = function (callbackfn, thisArg) {

		"use strict";

		var T,
			O = Object(this),
			len = O.length >>> 0,
			k = 0,
			Pk,
			kPresent,
			kValue;

		if (!callbackfn || !callbackfn.call) {
			throw new TypeError();
		}

		if (thisArg) {
			T = thisArg;
		}

		while (k < len) {

			Pk = String(k);
			kPresent = O.hasOwnProperty(Pk);

			if (kPresent) {
				kValue = O[Pk];

				callbackfn.call(T, kValue, k, O);
			}

			k += 1;
		
		}
	
	};

}

/*** global bkp ***/
var bkp = {};

bkp.menu = {
	
	'defaults' : {
		'selector' : '#navigation .menu'
	},
	
	'init' : function (settings) {
		
		"use strict";
		
		var config 	= $.extend({}, bkp.menu.defaults, settings),
			$menu 	= $(config.selector),
			$LIs 	= $menu.children();
		
		$LIs.each(function () {
			$(this).click(bkp.menu.click);
		});
	},
	
	'click' : function () {
		
		document.location = $('a', this).attr('href');
		
	}
	
};

bkp.prodTabs = {
	
	'config' : {
		'selector' : '.bkTabs',
		'options' : { 
			selected : 0
		}
	},
	
	'init' : function (config) {
		
		"use strict";
		
		var conf = bkp.prodTabs.config;
			
		if (config && typeof config === 'object') {
			$.extend(conf, config);
		}
		
		$(conf.selector).tabs(conf.options);
	}
};

bkp.aboutAnim = {
	
	'config' : {
		'selector' : '.about_bk',
		'options' : {
			'autoHeight' : false,
			'collapsible' : true
		}
	},
	
	'init' : function (config) {
		
		"use strict";
		
		var conf = bkp.aboutAnim.config;
			
		if (config && typeof config === 'object') {
			$.extend(conf, config);
		}
		
		$(conf.selector).accordion(conf.options);
	}
};

bkp.loadJs = function (path) {
	
		var oScript = document.createElement("script"),
			oHead = document.getElementsByTagName("head")[0],
			done = false;

		oScript.setAttribute("src", path);
		oScript.setAttribute("type", "text/javascript");

		oScript.onLoad = oScript.onreadystatechange = function () {

			if( !done && ( ! this.readyState || this.readyState === 'loaded' || this.readyState === 'complete' ) )
			{
				done = true;

				//code for memory leak in IE, script.onreadystatechange = null
				oScript.onLoad = null;
				oHead.removeChild(oScript);
			}

		};

		oHead.appendChild(oScript);
};

bkp.videos = {

    'config' : {

        'container' : '#mainContent',

        'plID' : '#playlist_id',

        'plBaseUrl' : 'http://gdata.youtube.com/feeds/api/playlists/',

        'playerBaseUrl' : 'http://www.youtube.com/embed/',

        'playerWidth' : 640,

        'playerHeight' : 390

    },

    

    'run' : function (config) {

        $.extend(bkp.videos.config, config);

        

        bkp.videos.$container = $(bkp.videos.config.container);

        

        bkp.videos.$plNav = $('<ul />').

            attr('id','playlist').

            appendTo(bkp.videos.$container);

        

        bkp.videos.$vidIframe = $('<iframe />').

            attr('id','player').

            attr('width',bkp.videos.config.playerWidth).

            attr('height',bkp.videos.config.playerHeight).

            attr('frameborder','0').

            attr('type','text/html').

            insertAfter(bkp.videos.$plNav);
			
		$(document.createElement("DIV")).
			addClass("clr").
			insertAfter(bkp.videos.$vidIframe);

            
		bkp.loadJs(bkp.videos.config.plBaseUrl + $(bkp.videos.config.plID).val() + '?v=2&alt=json-in-script&callback=bkp.videos.buildPlayList');

    },

    

    'buildPlayList' : function (jsonFeed) {

        var entryArray = jsonFeed.feed.entry;

        

        $(entryArray).each(function () {

            

            var vidObj = this, //grab the video obj

                $listObj = $('<li />').append('<a />'); //create an li element to append to playlist with an anchor



            $listObj.

                find('a').

                attr('href', 'javascript:void(0);'). //make sure we aren't linking to anything

                attr('id', vidObj.media$group.yt$videoid.$t). //setup an id so we can select the video later

                click(bkp.videos.thumbnailClk). //attach the click event

                append('<img />'). //create the thumbnail image element

                find('img').

                attr('src', vidObj.media$group.media$thumbnail[0].url). //set the source of the img to the feed image

                attr('border', '0').

                attr('height', vidObj.media$group.media$thumbnail[0].height).

                attr('width', vidObj.media$group.media$thumbnail[0].width);

            

            $listObj.append(document.createTextNode(vidObj.title.$t)); //remember to add the title to the end of the li 

            

            $listObj.appendTo(bkp.videos.$plNav); //finally, slap it on the playlist navigation

            

        });

        

        if(entryArray.length > 0) { bkp.videos.$vidIframe.attr('src',bkp.videos.config.playerBaseUrl + entryArray[0].media$group.yt$videoid.$t + '?enablejsapi=1'); }

    },

    'thumbnailClk' : function (e) {

        bkp.videos.$vidIframe.attr('src',bkp.videos.config.playerBaseUrl + $(this).attr('id') + '?enablejsapi=1'); //update the frame with the new source

        

        e.preventDefault();

    }

};

bkp.search = {
	
	'defaults' : {
		'selector' 		: '#mod-search-searchword',
		'urlBase' 		: '/search.html',
		'destination' 	: 'div.search'
	},
	
	'init' : function (settings) {
		
		var config =  $.extend(bkp.search.defaults, settings);
		
		bkp.search.config = config;
		
		$(config.selector)
			.val('')
			.autocomplete({
				source : function (query, add) {
					
					$.getJSON(config.urlBase, {gsa:true, q:query.term, format:'json'}, function (rst) {
						var suggestions = [];
						if(rst.GM) {
							$(rst.GM).each(function () {
								suggestions.push({
									label:this.GD,
									value:this.GD,
									href:this.GL.replace(/^.*\/{2,2}.+\.com(.+)$/i, '$1'),
									result:this
								});
							});
						}
						if(rst.RES) {
							$(rst.RES.R).each(function () {
								suggestions.push({
									label:this.T,
									value:this.S,
									href:this.U.replace(/^.*\/{2,2}.+\.com(.+)$/i, '$1'),
									result:this
								});
							});
						}
						
						add(suggestions);
					});
				},
				focus : function (e, ui) {
					
					return false;
				},
				minLength : 2,
				select : function (e, ui) {
					window.location.href = ui.item.href;
					
					return false;
				},
				position : {
					my:"right top",
					at:"right bottom",
					of:config.destination
				}
			})
			.keypress(function (e) {
				if(e.keyCode === 13) {
					window.location.href = config.urlBase + '?q=' + this.value;
				}
			})
			.data( "autocomplete" )._renderItem = function( ul, item ) {
				
				var $li = $( document.createElement("LI") )
								.data( "item.autocomplete", item )
								.append( $(document.createElement("A"))
								 			.append( $(document.createElement("DIV"))
														.addClass('title')
														.css({'font-size':'1.2em'})
														.append(item.label) )
											.append( $(document.createElement("DIV"))
														.addClass('description')
														.append(item.value)
														.css({'padding-top':'10px','font-size':'0.8em'}) )
											.append( $(document.createElement("DIV"))
														.addClass('source')
														.css({'font-size':'0.7em','color':'green'})
														.append('Source document: '+item.href.slice(1)) )
											.append( $(document.createElement("DIV")).addClass('clr').css({'padding-bottom':'10px'}) ) );
				
				$(item.result.MT).each(function () {
					if(this["@attributes"].N === 'product-image') {
						$li.find('div.title').before( $(document.createElement("IMG")).attr('src', '/photos/?src=' + this["@attributes"].V.slice(1) + '&maxWidth=75').css({'float':'left','width':'75px','padding-right':'8px'}) );
					}
				});
				
				return 	$li.appendTo( ul );
			};
			
		$(config.selector)
			.data("autocomplete")
			._renderMenu = function( ul, items ) {
				var self = this;
				$.each( items, function( index, item ) {
					self._renderItem( ul, item );
				});
				$(ul).addClass('shadow');
			};
		
	}
	
};

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-448292-1']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

jQuery(document).ready(function($) { 
	bkp.search.init(); 
	bkp.prodTabs.init(); 
	bkp.aboutAnim.init(); 
	if($("#playlist_id").length) { 
		bkp.videos.run({playerWidth:700, playerHeight:410, container:"article"}); 
	}
	if($(".innerfade").length) { 
		$(".innerfade").innerfade({speed:"slow",timeout:"7000"}).fadeIn();
	}
});