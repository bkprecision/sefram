jQuery(document).ready(function($) { 
	
	var $tabs 	= $(".tabs").tabs(),
		$pPane	= $("div#products"),
		$anchor = [],
		p 		= document.createElement("p"),
		txt 	= document.createTextNode("No products to display"),
		noProds = document.createDocumentFragment(),
		clkSC	= function () {
			
			var newHash = this.href.replace(/^\/|\.html$/, "").split("/");
			
			newHash = "!/" + newHash[newHash.length-1];
			document.location.hash = newHash;
			if($tabs.tabs( "option", "selected" )) {
				$tabs.tabs( "select", 0 );
			}
			
			$("ul.categories li").removeClass("category-on");
			$(this).addClass("selected").parent().addClass("category-on");
			
			return false;
		},
		postCB	= function (r) {
			var $ul = $(r).filter(function () { return this.nodeType !== 3; }).filter("ul.products");
			
			loadingStop();
			
			if($ul.length) {
				$pPane.children("ul").remove();
				$pPane.append($ul);
			}
			else {
				$pPane.html(noProds);
			}
		},
		loadingStart	= function () {
			$(".loadWait").fadeIn("fast");
		},
		loadingStop		= function () {
			$(".loadWait").fadeOut("fast");
			$("html, body").animate({ scrollTop: 0 }, 400);
		};
	
	// Remove all products for headless browsers
	$('div.hidden.no-js').remove();
	
	p.appendChild(txt);
	noProds.appendChild(p);
	
	//attach click event to all anchors
	$("ul.categories a").each(function () { 
		h = this.href.replace(/^\/|^\!\/|\.html$/, "").split("/"); 
		h = h[h.length-1];
		
		if(document.location.hash.replace(/\!\//, "").slice(1) === h) {
			$(this).addClass("selected");
		} 
		$(this).click(clkSC); 
	});
	
	$(window).hashchange(function () {
		
		var url = this.window.location.pathname+this.window.location.hash;
		
		loadingStart();
		
		$('ul.categories').children().removeClass('category-on').children('a').removeClass('selected');
		
		$.post(url.replace(/^(.+)\.html#\!(\/.+)$/, "$1$2.html"), {'format':'raw', 'limit':'*'}, postCB);
		
		$('ul.categories a[href="'+url+'"]').addClass('selected').parent().addClass('category-on');
		
	});
	
	//$('div.hidden.no-js').remove();
	
	
	//get the selected or first category anchor
	$anchor = $("ul.categories a.selected").eq(0).length ? $("ul.categories a.selected").eq(0) : $("ul.categories a").eq(0);
	$anchor.trigger("click").parent().addClass("category-on");
	$(window).hashchange();
});