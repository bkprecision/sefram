jQuery(document).ready(function($) { 
	
	var catId			= parseInt($('#parentId').val(), 10),
		start			= parseInt($('#limitStart').val(), 10),
		appPath			= {},
		postData		= {
			"view"	: "products", 
			"format": "raw", 
			"cat_id": catId, 
			"limit"	: 10, 
			"start"	: start
		},
		$fitlersTarg 	= $(".filters-list"),
		$productsTarg 	= $("div#products"),
		updateContent	= function () {
			$.post("index.php", $params, function (resp) {
				var $resp 		= $(resp).filter(function () { return this.nodeType !== 3; }),
					$prods 		= $resp.filter(".products").children('ul'),
					$filters 	= $resp.filter("div.filters-list"),
					$pagination = $resp.filter(".products").children('div');
				
				$('html, body').animate({scrollTop: 0}, 400);
				
				$('.modal').modal('toggle');
				
				$filters.find("input").each(function () {
					var $filter = $(".filters-list input[name='" + this.name + "'][value='" + this.value + "']");
					
					if($filter) {
						$filter.attr('disabled', false);
					}
				});
				
				$pagination
					.find("a")
					.each(function () {
						$(this).on('click', changePage);
					});
				
				$productsTarg.children().remove();
				$productsTarg.append($prods).append($pagination);
				$('#pgLimit').change(updateFilters);
			});
		},
		changePage		= function (event) {
			var $inputs 	= $(".filters-list input:checkbox:checked"),
				filterData 	= {filters : $inputs.serializeArray()},
				newStart	= parseInt(this.name, 10),
				page 		= parseInt(this.href.replace(/^.*#\!(.+)$/i, '$1').split(":")[1], 10);
			
			$('.modal').modal('toggle');
			
			window.location.hash = page ? "!page:" + page : "";
				
			$params 		= $.extend({}, filterData, postData);
			$params.start 	= newStart;
			$params.limit 	= $('#pgLimit').val();
			
			updateContent();
			
			event.preventDefault();
		},
		updateFilters	= function () {
			
			var $inputs 	= $(".filters-list input:checkbox:checked"),
				filterData 	= {filters : $inputs.serializeArray()};
				
			$('.modal').modal('toggle');	
			
			$(".filters-list input")
				.attr('disabled', true);
			
			$params					= $.extend({}, filterData, postData);
			$params.start 			= 0;
			window.location.hash 	= '';
			$params.limit 			= $('#pgLimit').val();
			
			updateContent();
		}, $params;
		
		$('.modal')
			.modal({
				backdrop: 	'static',
				keyboard: 	false,
				show: 		false
			});
		
		$('.category-filter, #pgLimit')
			.on('change', updateFilters);
		
		$('.pagination')
			.find('a')
			.each(function () { 
				$(this).on('click', changePage);
			});
	
});