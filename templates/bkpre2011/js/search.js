bkp.search = {
	
	'defaults' : {
		'selector' 		: '#mod-search-searchword',
		'urlBase' 		: '/search.html',
		'destination' 	: 'div.search'
	},
	
	'init' : function (settings) {
		
		var config =  $.extend(bkp.search.defaults, settings);
		
		bkp.search.config = config;
		
		$(config.selector)
			.val('')
			.autocomplete({
				source : function (query, add) {
					
					$.getJSON(config.urlBase, {gsa:true, q:query.term, format:'json'}, function (rst) {
						var suggestions = [];
						if(rst.GM) {
							$(rst.GM).each(function () {
								suggestions.push({
									label:this.GD,
									value:this.GD,
									href:this.GL.replace(/^.*\/{2,2}.+\.com(.+)$/i, '$1'),
									result:this
								});
							});
						}
						if(rst.RES) {
							$(rst.RES.R).each(function () {
								suggestions.push({
									label:this.T,
									value:this.S,
									href:this.U.replace(/^.*\/{2,2}.+\.com(.+)$/i, '$1'),
									result:this
								});
							});
						}
						
						add(suggestions);
					});
				},
				focus : function (e, ui) {
					
					return false;
				},
				minLength : 2,
				select : function (e, ui) {
					window.location.href = ui.item.href;
					
					return false;
				},
				position : {
					my:"right top",
					at:"right bottom",
					of:config.destination
				}
			})
			.keypress(function (e) {
				if(e.keyCode === 13) {
					window.location.href = config.urlBase + '?q=' + this.value;
				}
			})
			.data( "autocomplete" )._renderItem = function( ul, item ) {
				
				var $li = $( document.createElement("LI") )
								.data( "item.autocomplete", item )
								.append( $(document.createElement("A"))
								 			.append( $(document.createElement("DIV"))
														.addClass('title')
														.css({'font-size':'1.2em'})
														.append(item.label) )
											.append( $(document.createElement("DIV"))
														.addClass('description')
														.append(item.value)
														.css({'padding-top':'10px','font-size':'0.8em'}) )
											.append( $(document.createElement("DIV"))
														.addClass('source')
														.css({'font-size':'0.7em','color':'green'})
														.append('Source document: '+item.href.slice(1)) )
											.append( $(document.createElement("DIV")).addClass('clr').css({'padding-bottom':'10px'}) ) );
				
				$(item.result.MT).each(function () {
					if(this["@attributes"].N === 'product-image') {
						$li.find('div.title').before( $(document.createElement("IMG")).attr('src', '/photos/?src=' + this["@attributes"].V.slice(1) + '&maxWidth=75').css({'float':'left','width':'75px','padding-right':'8px'}) );
					}
				});
				
				return 	$li.appendTo( ul );
			};
			
		$(config.selector)
			.data("autocomplete")
			._renderMenu = function( ul, items ) {
				var self = this;
				$.each( items, function( index, item ) {
					self._renderItem( ul, item );
				});
				$(ul).addClass('shadow');
			};
		
	}
	
};