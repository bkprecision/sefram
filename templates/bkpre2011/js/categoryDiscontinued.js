jQuery(document).ready(function($) { 
	
	var $tabs 			= $(".tabs").tabs(),
		$pPane			= $("div#products"),
		start			= parseInt($('#limitStart').val(), 10),
		postData		= {
			'limit':10,
			'format':'raw',
			'layout':'subcats',
			'start':start
		},
		$anchor 		= [],
		p 				= document.createElement("p"),
		txt 			= document.createTextNode("No products to display"),
		noProds 		= document.createDocumentFragment(),
		setupPagination = function () {
			var start 	= parseInt(this.href.replace(/^(.*)\?(.+)$/, '$2').split("=")[1], 10),
				href 	= '/',
				path 	= this.href.replace(/http:\/\/(.+)\.com\/(.*)/, '$2').replace(/^(.+)(\/.+)\.html\?(.+)$/, '$1').split("/"),
				page 	= (start/parseInt(postData.limit)) ? (start/parseInt(postData.limit)+1) : 0,
				i;
				
			for(i=0;i<path.length-1;i+=1) {
				if(i>0) { href+='/'; }
				href += path[i];
			}
			href+='.html#!/'+path[path.length-1];
			
			this.href	= page ? href + ',page:' + page : href;
			this.name	= start;
			
			$(this).click(changePage);
		},
		changePage 		= function (evnt) {
			
			var newStart 	= parseInt(this.name, 10),
				page 		= parseInt(this.href.replace(/^.*#\!(.+)$/i, '$1').split(":")[1], 10),
				baseHash	= window.location.hash.replace(/^#\!\/(.*)$/, '$1').split(",")[0],
				path		= window.location.pathname.replace(/^\/(.+)\.html$/, '$1').split("/"),
				url			= '/',
				i;
			
			loadingStart();
			
			for(i=0;i<=path.length-1;i+=1) {
				if(i>0) { url+='/'; }
				url += path[i];
			}
			url += '/' + baseHash + '.html';
			
			postData.start = newStart;
			postData.limit = $('#pgLimit').val();
			
			hash ='!/' + baseHash;
			hash += page ? ",page:" + page : "";
			
			window.location.hash = hash;
			
			$.post(url, postData, postCB);
			
			evnt.preventDefault();
		},
		changeLimit		= function () {
			
			var baseHash	= window.location.hash.replace(/^#\!\/(.*)$/, '$1').split(",")[0],
				path		= window.location.pathname.replace(/^\/(.+)\.html$/, '$1').split("/"),
				url			= '/',
				i;
			
			loadingStart();
			
			for(i=0;i<=path.length-1;i+=1) {
				if(i>0) { url+='/'; }
				url += path[i];
			}
			url += '/' + baseHash + '.html';
			
			postData.start = 0;
			postData.limit = $('#pgLimit').val();
			
			window.location.hash = '!/' + baseHash;
			
			$.post(url, postData, postCB);
			
		},
		clkSC			= function () {
			
			var newHash = this.href.replace(/^\/|\.html$/, "").split("/");
			
			loadingStart();
			
			newHash = "!/" + newHash[newHash.length-1];
			document.location.hash = newHash;
			if($tabs.tabs( "option", "selected" )) {
				$tabs.tabs( "select", 0 );
			}
			
			$.post(this.href.replace(/^(.+)\.html#\!(\/.+)$/, "$1$2.html"), postData, postCB);
			$("ul.categories li").removeClass("category-on");
			$(this).addClass("selected").parent().addClass("category-on");
			
			return false;
		},
		postCB			= function (r) {
			var $resp 		= $(r).filter(function () { return this.nodeType !== 3; }),
				$ul 		= $resp.filter("ul.products"),
				$pagination = $resp.filter("div.pagination");
			
			$pagination.find("a").each(setupPagination);
			
			loadingStop();
			
			if($ul.length) {
				$pPane.children("ul").remove();
				$pPane.children("div.pagination").remove();
				$pPane.append($ul).append($pagination);
				$('#pgLimit').change(changeLimit);
			}
			else {
				$pPane.html(noProds);
			}
		},
		loadingStart	= function () {
			$(".loadWait").fadeIn("fast");
		},
		loadingStop		= function () {
			$(".loadWait").fadeOut("fast");
			$("html, body").animate({ scrollTop: 0 }, 400);
		};
	
	// Remove all products for headless browsers
	//$('div.hidden.no-js').remove();
	
	p.appendChild(txt);
	noProds.appendChild(p);
	
	//attach click event to all anchors
	$("ul.categories a").each(function () { 
		h = this.href.replace(/^\/|^\!\/|\.html$/, "").split("/"); 
		h = h[h.length-1];
		
		if(document.location.hash.replace(/\!\//, "").slice(1) === h) {
			$(this).addClass("selected");
		} 
		$(this).click(clkSC); 
	});
	
	$('div.hidden.no-js').remove();
	
	//get the selected or first category anchor
	$anchor = $("ul.categories a.selected").eq(0).length ? $("ul.categories a.selected").eq(0) : $("ul.categories a").eq(0);
	$anchor.trigger("click").parent().addClass("category-on");
	
});