(function ($, undefined) {
	
	$.widget("ui.fancyForm", {
		
		options 	: {
			helperText : true
		},
		
		// Set up the form widget
		_create 	: function () {
			
			var self 		= this,
				options 	= self.options,
				$form 		= $(this.element).addClass('ui-widget'),
				$inputs		= $form.find('input, textarea'),
				$buttons 	= $form.find('button').button();
			
			options.$form 		= $form;
			options.$inputs 	= $inputs;
			options.$buttons 	= $buttons;
			
			// Add relavent jQuery UI classes
			$form.find('fieldset').addClass('ui-widget-content ui-corner-all');
			$form.find('legend').addClass('ui-widget-header ui-corner-all');
			$inputs
				.each(function (idx, input) {
					$(input).addClass('ui-state-default ui-corner-all');

					// Do we need to create a label for this input
					if( ! $(input).parent().is('label') && ! $(input).siblings().eq(0).is('label') && $(input).attr('type') !== 'hidden' && $(input).attr('type') !== 'radio' ) {
						$(input).wrap("<label />");
					}

					// Ensure we are working with inputs of a specific type (i.e. no hidden elements need customization, so they are not included)
					if($(input).is("input[type='text']") || $(input).is("input[type='password']") || $(input).is("textarea")) {

						$(input)
							.addClass('text')
							.focus(function () {
								// Mouse enter event
								$(input).addClass('ui-state-focus');

								if( options.helperText && $(input).attr('title') !== undefined  && ! $(input).parent().find('label.helperText').length ) {
									// Setup Helper text if available
									$(input)
										.parent()
										.append( $(document.createElement("LABEL"))
													.addClass('helperText')
													.css('display', 'none')
													.append(document.createTextNode( $(input).attr('title')) ) );
								}

								if( $(input).parent().find('label.helperText').length ) {
									$(input)
										.parent()
										.find('label.helperText')
										.fadeIn();
								}
							})
							.blur(function () {
								// Mouse leave event
								$(this).removeClass('ui-state-focus');

								if( $(this).parent().find('.helperText').length ) { // Hide any helper text
									$(this)
										.parent()
										.find('.helperText')
										.fadeOut('fast');
								}
							});

					}
				})
				.eq(0)
				.focus();
			
		},
		
		_setOption 	: function (key, value) {
			
			$.Widget.prototype._setOption.apply(this, arguments);
			
		},
		
		destroy 	: function () {
			
			$.Widget.prototype.destroy.call(this);
			
		}
		
	});
	
}(jQuery));

jQuery(document).ready(function ($) {
	
	var $dialog 	= $(document.createElement("DIV"))
							.addClass('alignCenter')
							.dialog({'model':true, 'autoOpen':false})
							.append( $(document.createElement("P"))
										.addClass('alignCenter')
										.append( document.createTextNode("Sending Message...")) )
							.append( $(document.createElement("IMG"))
										.attr("src","/images/ajax-load-2.gif") ),
		$newMsg 	= $(document.createElement("TR"))
							.append( $(document.createElement("TD"))
							 			.css({"padding":"5px", "display":"none"}) )
							.append( $(document.createElement("TD"))
							 			.css({"padding":"5px", "display":"none"}) ),
		$strong 	= $(document.createElement("STRONG"));
	
	$('form')
		.submit(function (e) {
			
			var $form 	= $(this),
				$inputs = $form.find('input, textarea'),
				isValid = false;
			
			isValid = ($('textarea').val().length > 0);
			isValid = $('#ratings').length ? ($('#ratings input:radio:checked').length > 0) : isValid;
			
			if(isValid) {
				
				$dialog.dialog('open');

				$.getJSON(bkp.contact.url, $form.serialize(), function (r) {
					var $msgDetail 	= $newMsg.clone(true),
						$msgDate	= $msgDetail.children().eq(0),
						$msg		= $msgDetail.children().eq(1);

					// Close the wait message dialog
					$dialog.dialog('close');

					// Reset the form
					$('textarea', $form)
						.val("")
						.focus();
					$('#ratings label.ui-button.ui-state-active')
						.removeClass('ui-state-active')
						.attr('aria-pressed', false);
					$('#ratings input:radio:checked')
						.attr('checked', false);

					// Set up the html to be pasted to the doc
					$msgDate
						.append(document.createTextNode(r.detail.date_added));

					if(r.detail.msg_type === 'user_srvy') {
						$msgDate
							.css('background-color','#adc')

						$msg
							.css('background-color','#adc')
							.append( $strong.clone(true).append(document.createTextNode("User Rating: ")) )
							.append( document.createTextNode(r.detail.survey_readable_rating + " — ("+ r.detail.survey_rating +")") )
							.append( document.createElement("BR") )
							.append( $strong.clone(true).append(document.createTextNode("User Comment: ")) )
							.append( r.detail.survey_comment );

					}
					else {
						$msgDate
							.css('background-color','#ffc')
						
						$msg
							.css('background-color','#ffc')
							.append( r.detail.the_message );
					}

					// Append the message to the doc
					$msgDetail
						.prependTo($('#tblCorrespondence tbody'))
						.children()
						.fadeIn();

				});
				
			}
			else {
				// show some error codes
			}
			
			e.preventDefault();
		})
		.fancyForm();
		
	if($('#ratings').length) {
		$('#ratings').buttonset();
	}
});