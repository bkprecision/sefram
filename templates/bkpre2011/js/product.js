// Require.js allows us to configure shortcut alias
require.config({
	// The shim config allows us to configure dependencies for
	paths: {
		'jquery.fancybox': 'lib/jquery/jquery.fancybox-1.3.4.pack',
		'jquery.effects': 'lib/jquery/jquery-ui-1.9.1.effectsCore.min',
		text: 'lib/require/text'
	}
});

require([
	'jquery.fancybox',
	'jquery.effects'
], function () {

	$('.menu-item')
		.on('click', function () {
			var itemIndex 	= $(this).index(),
				slideWidth 	= parseInt($('#slides .slide').eq(0).width(), 10),
				clickMargin = '-'+itemIndex*slideWidth+'px';

			$('#slides')
				.stop()
				.animate({'margin-left':clickMargin}, 750, 'easeOutBack');
		});
		
	$('.slide')
		.hover(function () { 
			$(this).find('.magnifier').removeClass('hidden'); 
		}, 
		function () { 
			$(this).find('.magnifier').addClass('hidden'); 
		});

	$('.slide a').fancybox({'titleShow':false});

	$('a.fancybox').fancybox({'padding':'70','titleShow':false});

});