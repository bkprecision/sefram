<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<!DOCTYPE html>
<html>
	<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="robots" content="index, follow" />
	<meta name="generator" content="bkp" />
	<title><?php echo $this->error->getMessage(); ?> - B&amp;K Precision Corporation</title>
	<link href="/templates/bkpre2011/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
	<link rel="stylesheet" href="/templates/bkpre2011/css/template.css" type="text/css" />
	<script src="/templates/bkpre2011/js/aglobal-min.js" type="text/javascript"></script>
	<script src="/templates/bkpre2011/js/jquery.innerfade.js" type="text/javascript"></script>
	<script src="/templates/bkpre2011/js/support.js" type="text/javascript"></script>
	<script src="/templates/bkpre2011/js/search.js" type="text/javascript"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) { $(".innerfade").innerfade({speed:"slow",timeout:"7000"}).fadeIn(); });
		jQuery(document).ready(function($) { 
			bkp.search.init(); 
			bkp.prodTabs.init(); 
			bkp.aboutAnim.init(); 
			if($("#playlist_id").length) { 
				bkp.videos.run({playerWidth:700, playerHeight:410, container:"article"}); 
			}
		});
	</script>

	<!--[if gte IE 9]><link rel="stylesheet" href="/templates/bkpre2011/css/ie9.css" type="text/css" /><![endif]-->
	<!--[if IE 8]><link rel="stylesheet" href="/templates/bkpre2011/css/ie8.css" type="text/css" /><![endif]-->
	<!--[if lte IE 7]><link rel="stylesheet" href="/templates/bkpre2011/css/ie7.css" type="text/css" /><![endif]-->
    
</head>
<body> 
	<header>
		<div id="logobk">
			<a href="/"><img border="0" src="/templates/bkpre2011/images/bk_11logo.png" id="bklogo" alt="Click to go to our Home Page" /></a>
		</div>
		<nav id="topNav">
			<ul class="menu">
				<li class="item-103 parent"><a href="/about-us.html" >About Us</a></li>
				<li class="item-123 parent"><a href="/contact-us.html" >Contact Us</a></li>
				<li class="item-124 parent"><a href="/news.html" >News &amp; Media</a></li>
				<li class="item-125 parent"><a href="/education.html" >Education</a></li>
				<li class="item-126"><a href="/employment.html" >Employment</a></li>
			</ul>
			<p class="tagline">Specializing in the design and manufacture of high quality, cost-effective test and measurement products for 60 years</p>
		</nav>
		
		<nav id="navigation" class="alignCenter">
			<ul class="menu">
				<li class="item-101 current active"><a href="/" >Home</a></li>
				<li class="item-102 parent"><a href="/products.html" >Products</a></li>
				<li class="item-120"><a href="/where-to-buy.html" >Where to Buy</a></li>
				<li class="item-121 parent"><a href="/support.html" >Support &amp; Service</a></li>
				<li class="item-122 parent"><a href="/distributor-access.html" >Distributor Access</a></li>
			</ul>
			<div class="search" style="z-index: 999;">
				<input name="q" id="mod-search-searchword"  class="inputbox" type="text" value="Search..." />
			</div>
		</nav>
    </header>
    
    <article>
		<div id="errorMsg">
			<p><strong><?php echo $this->error->getCode(); ?> - <?php echo $this->error->getMessage(); ?></strong></p>

			<p><strong><?php echo JText::_('You may not be able to visit this page because of:'); ?></strong></p>
			<ol>
				<li><?php echo JText::_('An out-of-date bookmark/favourite'); ?></li>
				<li><?php echo JText::_('A search engine that has an out-of-date listing for this site'); ?></li>
				<li><?php echo JText::_('A mis-typed address'); ?></li>
				<li><?php echo JText::_('You have no access to this page'); ?></li>
				<li><?php echo JText::_('The requested resource was not found'); ?></li>
				<li><?php echo JText::_('An error has occurred while processing your request.'); ?></li>
			</ol>
			<p><strong><?php echo JText::_('Please try one of the following pages:'); ?></strong></p>
			<p>
				<ul>
					<li><a href="<?php echo $this->baseurl; ?>/" title="<?php echo JText::_('Go to the home page'); ?>"><?php echo JText::_('Home Page'); ?></a></li>
				</ul>
			</p>
			<p><?php echo JText::_('If difficulties persist, please contact the system administrator of this site.'); ?></p>
		</div>
	</article>

    <footer class="alignCenter">
      <p>22820 Savi Ranch Parkway — Yorba Linda, CA 92887 — &copy; 2008-12 B&amp;K Precision Corp. (<a href="/sitemap.html">Site Map</a>)</p>
    </footer>

  </body>
</html>
	