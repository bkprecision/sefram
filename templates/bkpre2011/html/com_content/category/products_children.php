<?php
/**
 * @version		$Id: blog_children.php 20196 2011-01-09 02:40:25Z ian $
 * @package		Joomla.Site
 * @subpackage	com_content
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$class = ' class="first"';
?>

<? if(count($this->children[$this->category->id]) > 0 && $this->maxLevel != 0): ?>
	<ul>
<? foreach($this->children[$this->category->id] as $id => $child): 
	
		if($this->params->get('show_empty_categories') || $child->numitems || count($child->getChildren())):
			if( ! isset($this->children[$this->category->id][$id + 1])):
				$class = ' class="last"';
			endif;
?>
		<li<?=$class?>>
			<?php $class = ''; ?>
			<span class="item-title">
				<a href="<?php echo JRoute::_(ContentHelperRoute::getCategoryRoute($child->id));?>"><?=$this->escape($child->title)?></a>
			</span>
		</li>
		<?php endif; ?>
	<?php endforeach; ?>
	</ul>
<?php endif;