<?php
// no direct access
defined('_JEXEC') or die;

for($i = 0; $i < $count; $i++)
{
	if(isset($list[$i-1])) 
	{
		if($list[$i-1]->link == $list[$i]->link)
		{
			unset($list[$i]);
		}
	}
	
}
$count = count($list);
if($count > 1):
?>
<ul class="breadcrumb">
<?php for($i = 0; $i < $count; $i++):

	// If not the last item in the breadcrumbs add the separator
	// <li><a href="#">Home</a> <span class="divider">/</span></li>
	if ($i < $count-1) 
	{
		echo '<li>';
		if (!empty($list[$i]->link)) 
		{
			echo '<a href="'.$list[$i]->link.'">'.$list[$i]->name.'</a>';
		} else {
			echo '<span class="no-link">'.$list[$i]->name.'</span>';
		}
		if($count-2 >= $i) 
		{
			echo '<span class="divider">/</span>';
		}
		echo '</li>';
	}  
	elseif ($params->get('showLast', 1)) 
	{ // when $i == $count -1 and 'showLast' is true
		echo '<li class="active">'.$list[$i]->name.'</li>';
	}
	
endfor; ?>
</ul>
<? endif; ?>