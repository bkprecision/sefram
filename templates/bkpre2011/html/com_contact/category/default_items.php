<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_contact
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

?>
<?php if (empty($this->items)) : ?>
	<p> <?php echo JText::_('COM_CONTACT_NO_ARTICLES'); ?>	 </p>
<?php else : ?>

<form class="form-horizontal" action="<?php echo htmlspecialchars(JFactory::getURI()->toString()); ?>" method="post" name="adminForm" id="adminForm">

	<div class="control-group well contact-type">
		<?php if( !empty($this->category->note) ): ?>
		<legend><?php echo $this->category->note; ?></legend>
		<?php endif; ?>
		<?php foreach( $this->items as $i => $item ): ?>
		<label class="radio inline span2">
			<input type="radio" name="id" value="<?php echo $item->slug; ?>"<?php echo ($i == 0) ? "checked=\"checked\"" : ""; ?>>
			<?php echo $item->name; ?>
		</label>
		<?php endforeach; ?>
	</div>

	<p><strong><?php echo JText::_('COM_CONTACT_CONTACT_ENTER_MESSAGE_LABEL').":"; ?></strong><span class="red-text">*</span></p>
	<div class="control-group">
	    <div class="controls">
	      <textarea name="jform[contact_message]" id="inputMessage" rows="3" required></textarea>
	    </div>
  	</div>

	<div class="control-group">
	    <label class="control-label" for="inputName"><?php echo JText::_('COM_CONTACT_CONTACT_EMAIL_NAME_LABEL').":"; ?><em>*</em></label>
	    <div class="controls">
	      <input name="jform[contact_name]" type="text" id="inputName" placeholder="<?php echo JText::_('COM_CONTACT_CONTACT_EMAIL_NAME_LABEL'); ?>" required>
	    </div>
  	</div>

  	<div class="control-group">
	    <label class="control-label" for="inputCompany"><?php echo JText::_('COM_CONTACT_CONTACT_EMAIL_COMPANY_LABEL').":"; ?></label>
	    <div class="controls">
	      <input name="jform[contact_company]" type="text" id="inputCompany" placeholder="<?php echo JText::_('COM_CONTACT_CONTACT_EMAIL_COMPANY_LABEL'); ?>">
	    </div>
  	</div>

  	<div class="control-group">
	    <label class="control-label" for="inputPostalCode"><?php echo JText::_('COM_CONTACT_CONTACT_EMAIL_POSTALCODE_LABEL').":"; ?><em>*</em></label>
	    <div class="controls">
	      <input name="jform[contact_postalcode]" type="text" id="inputPostalCode" placeholder="<?php echo JText::_('COM_CONTACT_CONTACT_EMAIL_POSTALCODE_LABEL'); ?>" required>
	    </div>
  	</div>

  	<div class="control-group">
	    <label class="control-label" for="inputPhone"><?php echo JText::_('COM_CONTACT_CONTACT_EMAIL_PHONE_LABEL').":"; ?><em>*</em></label>
	    <div class="controls">
	      <input name="jform[contact_phone]" type="text" id="inputPhone" placeholder="<?php echo JText::_('COM_CONTACT_CONTACT_EMAIL_PHONE_LABEL'); ?>" required>
	    </div>
  	</div>

  	<div class="control-group">
	    <label class="control-label" for="inputEmail"><?php echo JText::_('COM_CONTACT_EMAIL_LABEL').":"; ?><em>*</em></label>
	    <div class="controls">
	      <input name="jform[contact_email]" type="email" id="inputEmail" placeholder="<?php echo JText::_('COM_CONTACT_EMAIL_LABEL'); ?>" required>
	     </div>
  	</div>

  	<div class="control-group">
  		<div class="controls">
  			<p><strong>Note:</strong> <?php echo JText::_('COM_CONTACT_CONTACT_REQUIRED_NOTE'); ?><span class="red-text">*</span></p>
  			<button type="submit" class="btn"><?php echo JText::_('COM_CONTACT_CONTACT_SEND'); ?></button>
  		</div>
  	</div>

	<div>
		<input type="hidden" name="jform[contact_subject]" value="DEMANDE A PARTIR DU SITE WEB" />
		<input type="hidden" name="option" value="com_contact" />
		<input type="hidden" name="task" value="contact.submit" />
		<input type="hidden" name="return" value="<?php echo $this->custom_redirect;?>" />
		<?php echo JHtml::_( 'form.token' ); ?>
	</div>

</form>
<?php endif; ?>
