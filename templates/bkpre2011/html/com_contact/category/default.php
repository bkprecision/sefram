<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_contact
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

$doc = JFactory::getDocument();
$doc->addStyleSheet("/templates/bkpre2011/css/contact.css");

$this->custom_redirect = $this->state->{"parameters.menu"}->get("redirect") ? : htmlspecialchars(JFactory::getURI()->toString());

?>
<div class="contact-category<?php echo $this->pageclass_sfx;?> span12">
	<div class="row">


<?php if($this->params->get('show_category_title', 1)) : ?>

	<h2><?php echo JHtml::_('content.prepare', $this->category->title, '', 'com_contact.category'); ?></h2>

<?php endif; ?>

<?php if ($this->params->def('show_description', 1) || $this->params->def('show_description_image', 1)) : ?>
	<div class="span3">
		<div class="category-desc">
		<?php if ($this->params->get('show_description_image') && $this->category->getParams()->get('image')) : ?>
			<img src="<?php echo $this->category->getParams()->get('image'); ?>"/>
		<?php endif; ?>
		<?php if ($this->params->get('show_description') && $this->category->description) : ?>
			<?php echo JHtml::_('content.prepare', $this->category->description, '', 'com_contact.category'); ?>
		<?php endif; ?>
			<div class="clr"></div>
		</div>
	</div>
<?php endif; ?>

	<div class="span8 offset1">
<?php if ($this->params->get('show_page_heading')) : ?>

	<h5><?php echo $this->escape($this->params->get('page_heading')); ?></h5>

<?php endif; ?>
	<?php echo $this->loadTemplate('items'); ?>
	</div>

	</div>
</div><!-- end multi-contact form -->

<div id="modal-submit" class="modal hidden" tabindex="-1" role="dialog">
  <div class="modal-header">
    <h3><?php echo JText::_('COM_CONTACT_CONTACT_EMAIL_SUBMIT'); ?></h3>
  </div>
  <div class="modal-body">
	<div class="progress progress-info progress-striped active">
	  <div class="bar" style="width: 100%"></div>
	</div>
  </div>
</div><!-- end modal-submit template -->

<div id="modal-success" class="modal hidden" tabindex="-1" role="dialog">
  <div class="modal-body">
  	<div class="alert alert-success">
  		<p><?php echo JText::_('COM_CONTACT_CONTACT_EMAIL_SUCCESS'); ?></p>
  	</div>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Close</a>
  </div>
</div><!-- end modal-success template -->

<div id="modal-error" class="modal hidden" tabindex="-1" role="dialog">
  <div class="modal-body">
  	<div class="alert alert-error">
  		<p><?php echo JText::_('COM_CONTACT_CONTACT_EMAIL_ERROR'); ?></p>
  	</div>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Close</a>
  </div>
</div><!-- end modal-success template -->

<script>
var SEFRAM = SEFRAM || {};
SEFRAM.contact = {'url' : '<?php echo htmlspecialchars(JFactory::getURI()->toString()); ?>'};
</script>

<script src="<?php echo $this->baseurl; ?>/templates/bkpre2011/js/contact-default.js"></script>